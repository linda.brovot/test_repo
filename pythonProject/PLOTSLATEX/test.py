import matplotlib.pyplot as plt
import numpy as np
from matplotlib import font_manager
import locale
from matplotlib.ticker import FuncFormatter

locale.setlocale(locale.LC_NUMERIC, 'de_DE')
plt.rcParams['axes.formatter.use_locale'] = True

# Pfad zur Schriftart
font_path = font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=10)



categories = ['EZ', 'MFH', 'AB', 'SQ']
heat_prices = [0.328, 0.336, 0.315, 0.858]
electricity_prices = [2.51, 2.386, 2.33, 1.157]

bar_width = 0.3
index = np.arange(len(categories))

# Erstellen des Figure- und Axis-Objekts
fig, ax1 = plt.subplots(figsize=(4, 2.2))
ax1.set_axisbelow(True)
ax1.yaxis.grid(True, zorder=0)

# Strompreise plotten
ax1.bar(index - bar_width/2, electricity_prices, color='grey', width=bar_width,edgecolor='black', linewidth=0.5, label='Specific Electricity Price', zorder=3)

# Zweite Achse für die Wärmepreise erstellen
ax2 = ax1.twinx()
ax2.bar(index + bar_width/2, heat_prices, color='firebrick', width=bar_width,edgecolor='black', linewidth=0.5, label='Specific Heat Price', zorder=3)

# Achsenbeschriftungen und Titel setzen
ax1.set_xticks(index)
ax1.set_xticklabels(categories, fontproperties=prop)
ax1.set_ylabel('Elektrische Kosten in $\mathrm{€/kWh_{el}}$', fontproperties=prop)
ax2.set_ylabel('Thermische Kosten in $\mathrm{€/kWh_{th}}$', fontproperties=prop)

def custom_formatter(x, pos):
    if x == 0:
        return "0"
    else:
        return f"{x:.1f}".replace('.', ',')

ax1.yaxis.set_major_formatter(FuncFormatter(custom_formatter))
ax2.yaxis.set_major_formatter(FuncFormatter(custom_formatter))

# Schriftart auf alle Elemente anwenden
for label in (ax1.get_xticklabels() + ax1.get_yticklabels() + ax2.get_yticklabels()):
    label.set_fontproperties(prop)

# Speichern und Anzeigen des Diagramms
plt.tight_layout(rect=[-0.03, -0.05, 1.03, 1.05])
plt.savefig("Spec_TAC_Quartier.eps", dpi=150)
plt.show()