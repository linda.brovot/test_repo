import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Schriftart setzen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
matplotlib.font_manager.fontManager.addfont(font_path)
matplotlib.rcParams['font.family'] = 'Heuristica'

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')


matplotlib.rcParams['svg.fonttype'] = 'path'  # Schriftart in SVG-Dateien als Text erhalten


def read_csv_files(results_base_dir, mass_flow):
    folder_path = os.path.join(results_base_dir, f"results_{mass_flow}")
    csv_file = os.path.join(folder_path, f"system_results_{mass_flow}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für mass_flow {mass_flow} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für mass_flow {mass_flow} nicht gefunden: {csv_file}")
        return None


def format_func(x, pos):
    if x.is_integer():
        return locale.format_string("%.0f", x)
    else:
        return locale.format_string("%.2f", x).replace('.', ',')


def combined_plot(results_base_dir, output_dir, script_name):
    frequencies_combined = []
    eta_system_combined = {}
    hours_combined = {}

    for mass_flow in [0.05, 0.15, 0.25]:
        data_frame = read_csv_files(results_base_dir, mass_flow)
        if data_frame is not None:
            frequencies = []
            eta_system_list = []
            hours_list = []
            for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
                filtered_df = data_frame[data_frame['Name'].str.contains(f"_{mass_flow}_{value}.csv")]
                if not filtered_df.empty:
                    eta_system = filtered_df['Eta_WPSS'].values[0]
                    hours = filtered_df['Hours'].values[0]

                    if not pd.isna(eta_system) and not pd.isna(hours):
                        eta_system_list.append(eta_system)
                        hours_list.append(hours)
                        frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

            eta_system_combined[mass_flow] = eta_system_list
            hours_combined[mass_flow] = hours_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    if frequencies_combined:
        fig, ax1 = plt.subplots(figsize=(8, 5))  # Vergrößern der Breite und Höhe des Plots
       # fig.suptitle('Exergetischer Nutzungsgrad bei unterschiedlichen Massenströmen', fontsize=16, y=0.95)

        bar_width = 1
        offsets = [-bar_width, 0, bar_width]
        colors = ['darkred', 'red', 'grey']

        # Beladedauer-Balkenplot
        for mass_flow, offset, color in zip([0.05, 0.15, 0.25], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], hours_combined[mass_flow], width=bar_width, color=color,
                    label=f'{mass_flow} kg/s')

        # Nutzungsgrad-Linienplot
        ax2 = ax1.twinx()
        for mass_flow, color in zip([0.05, 0.15, 0.25], ['darkred', 'red', 'grey']):
            ax2.plot(frequencies_combined, eta_system_combined[mass_flow],
                     marker='o',  # 'o' für Punkte
                     linestyle='--',  # Gestrichelte Linie
                     linewidth=2.5,  # Dickere Linien
                     markersize=8,  # Größere Punkte
                     color=color, label=f'{mass_flow} kg/s')

        ax1.set_xlabel('Drehzahl in Hz', fontsize=13)
        ax1.set_ylabel('Beladedauer in h', fontsize=13)
        ax2.set_ylabel('Nutzungsgrad WPSS', fontsize=13)

        # Set y-limits for ax1 (Beladedauer)
        ax1.set_ylim(0, 8.5)

        # Automatically determine y-limits for ax2 (Nutzungsgrad) and set ticks
        ax2.set_ylim(0.15, 0.24)  # Set the desired range
        ax2.yaxis.set_major_locator(ticker.AutoLocator())  # Automatically determine tick locations
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        ax1.grid(False)

        # Legenden manuell erstellen
        beladedauer_handles, beladedauer_labels = ax1.get_legend_handles_labels()
        beladedauer_labels = [label.replace('.', ',') for label in beladedauer_labels]
        beladedauer_legend = ax1.legend(beladedauer_handles, beladedauer_labels, loc='best',
                                        bbox_to_anchor=(0.865, 1),
                                        title="Beladedauer", ncol=3, fontsize=13, title_fontsize=13)

        nutzungsgrad_handles, nutzungsgrad_labels = ax2.get_legend_handles_labels()
        nutzungsgrad_labels = [label.replace('.', ',') for label in nutzungsgrad_labels]
        nutzungsgrad_legend = ax2.legend(nutzungsgrad_handles, nutzungsgrad_labels, loc='best',
                                         bbox_to_anchor=(0.15, 1),
                                         title="Nutzungsgrad", ncol=3, fontsize=13, title_fontsize=13)

        ax1.add_artist(beladedauer_legend)
        ax2.add_artist(nutzungsgrad_legend)

        ax1.tick_params(axis='both', which='major', labelsize=13)
        ax2.tick_params(axis='both', which='major', labelsize=13)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        combined_plot_filename_eps = os.path.join(output_dir, f"{script_name}_combined.eps")

        plt.tight_layout(rect=[0, 0, 1, 0.85])

        plt.savefig(combined_plot_filename_png)
        plt.savefig(combined_plot_filename_svg)
        plt.savefig(combined_plot_filename_eps, format='eps')
        plt.show()
        plt.close(fig)
        print(
            f"Kombinierter Plot gespeichert: {combined_plot_filename_png}, {combined_plot_filename_svg} und {combined_plot_filename_eps}")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_7\Massenstrom\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Vergleich7und15\PLOTS"
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
