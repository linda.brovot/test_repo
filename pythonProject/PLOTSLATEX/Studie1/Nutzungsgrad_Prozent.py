import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager  # Für Schriftarten

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

matplotlib.rcParams['svg.fonttype'] = 'path' # Schriftart in SVG-Dateien als Text erhalten


def read_csv_files(results_base_dir, mass_flow):
    folder_path = os.path.join(results_base_dir, f"results_{mass_flow}")
    csv_file = os.path.join(folder_path, f"system_results_{mass_flow}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        return data_frame
    else:
        print(f"CSV-Datei für mass_flow {mass_flow} nicht gefunden: {csv_file}")
        return None


def calculate_percentage_difference(value_earlier, value_later):
    return ((value_later - value_earlier) / value_earlier) * 100


def save_percentage_differences_to_csv(frequencies, eta_system_combined, hours_combined, output_dir, script_name):
    percentage_differences = []
    for i, freq in enumerate(frequencies):
        diff_eta_05_to_15 = calculate_percentage_difference(eta_system_combined[0.05][i], eta_system_combined[0.15][i])
        diff_eta_15_to_25 = calculate_percentage_difference(eta_system_combined[0.15][i], eta_system_combined[0.25][i])
        diff_hours_05_to_25 = calculate_percentage_difference(hours_combined[0.05][i], hours_combined[0.25][i])
        percentage_differences.append({
            'Frequency (Hz)': freq,
            'Eta 0.05 to 0.15 (%)': diff_eta_05_to_15,
            'Eta 0.15 to 0.25 (%)': diff_eta_15_to_25,
            'Hours 0.05 to 0.25 (%)': diff_hours_05_to_25
        })
    percentage_df = pd.DataFrame(percentage_differences)
    csv_filename = os.path.join(output_dir, f"{script_name}_percentage_differences.csv")
    percentage_df.to_csv(csv_filename, sep=';', index=False)


def load_percentage_differences_from_csv(output_dir, script_name):
    csv_filename = os.path.join(output_dir, f"{script_name}_percentage_differences.csv")
    percentage_df = pd.read_csv(csv_filename, sep=';')
    return percentage_df


def combined_plot(results_base_dir, output_dir, script_name):
    frequencies_combined = []
    eta_system_combined = {}
    hours_combined = {}

    for mass_flow in [0.05, 0.15, 0.25]:
        data_frame = read_csv_files(results_base_dir, mass_flow)
        if data_frame is not None:
            frequencies = []
            eta_system_list = []
            hours_list = []
            for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
                filtered_df = data_frame[data_frame['Name'].str.contains(f"_{mass_flow}_{value}.csv")]
                if not filtered_df.empty:
                    eta_system = filtered_df['Eta_WPSS'].values[0]
                    hours = filtered_df['Hours'].values[0]
                    if not pd.isna(eta_system) and not pd.isna(hours):
                        eta_system_list.append(eta_system)
                        hours_list.append(hours)
                        frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern
            eta_system_combined[mass_flow] = eta_system_list
            hours_combined[mass_flow] = hours_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    save_percentage_differences_to_csv(frequencies_combined, eta_system_combined, hours_combined, output_dir,
                                       script_name)
    percentage_df = load_percentage_differences_from_csv(output_dir, script_name)

    if frequencies_combined:
        fig, ax1 = plt.subplots(figsize=(8, 5))
        bar_width = 1
        offsets = [-bar_width, 0, bar_width]
        colors = ['darkred', 'red', 'grey']



        # Beladedauer-Balkenplot
        for mass_flow, offset, color in zip([0.05, 0.15, 0.25], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], hours_combined[mass_flow], width=bar_width, color=color,
                    label=f'{mass_flow} kg/s')

        ax2 = ax1.twinx()
        for mass_flow, color in zip([0.05, 0.15, 0.25], ['darkred', 'red', 'grey']):
            ax2.plot(frequencies_combined, eta_system_combined[mass_flow],
                     marker='o', linestyle='--', linewidth=2.5, markersize=8, color=color, label=f'{mass_flow} kg/s')

        ax1.set_xlabel('Drehzahl in Hz', fontsize=12, fontproperties=prop)
        ax1.set_ylabel('Beladedauer in h', fontsize=12, fontproperties=prop)
        ax2.set_ylabel('Nutzungsgrad WPSS', fontsize=12, fontproperties=prop)

        ax1.set_ylim(0, 8.5)
        ax2.set_ylim(0.12, 0.22)
        ax2.yaxis.set_major_locator(ticker.AutoLocator())
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.2f', x)))

        ax1.grid(False)

        def add_percentage_annotation(ax, x, y_start, y_end, percentage, text_offset=2, text_color='black',
                                      arrow_color='black'):
            ax.annotate('', xy=(x, y_end), xytext=(x, y_start),
                        arrowprops=dict(arrowstyle='->', color=arrow_color, lw=2))

            # Text mit Berücksichtigung des Minuszeichens bei negativen Werten
            ax.text(x + text_offset, (y_start + y_end) / 2,
                    f'{percentage:+.1f}%'.replace('.', ','),  # + sorgt für Anzeige des Vorzeichens
                    color=text_color, fontsize=12,
                    verticalalignment='center', fontproperties=prop)

        # Füge die Annotation für die 5,4% Abweichung bei 90 Hz hinzu
        add_percentage_annotation(ax2, 90, eta_system_combined[0.05][6], eta_system_combined[0.25][6], 5.4,
                                  text_offset=2, text_color='black', arrow_color='black')

        for freq, idx in zip([30, 60, 90], [0, 3, 6]):
            percentage_data = percentage_df[percentage_df['Frequency (Hz)'] == freq]
            diff_eta_05_to_15 = percentage_data['Eta 0.05 to 0.15 (%)'].values[0]
            diff_eta_15_to_25 = percentage_data['Eta 0.15 to 0.25 (%)'].values[0]
            add_percentage_annotation(ax2, freq, eta_system_combined[0.05][idx], eta_system_combined[0.15][idx],
                                      diff_eta_05_to_15)
            add_percentage_annotation(ax2, freq, eta_system_combined[0.15][idx], eta_system_combined[0.25][idx],
                                      diff_eta_15_to_25)

            # Legenden manuell erstellen
            beladedauer_handles, beladedauer_labels = ax1.get_legend_handles_labels()
            beladedauer_labels = [label.replace('.', ',') for label in beladedauer_labels]
            beladedauer_legend = ax1.legend(beladedauer_handles, beladedauer_labels, loc='upper center',
                                            bbox_to_anchor=(0.5, 1.53),
                                            title="Beladedauer", ncol=3, fontsize=13,
                                            prop=prop, title_fontproperties=prop)


            nutzungsgrad_handles, nutzungsgrad_labels = ax2.get_legend_handles_labels()
            nutzungsgrad_labels = [label.replace('.', ',') for label in nutzungsgrad_labels]
            nutzungsgrad_legend = ax2.legend(nutzungsgrad_handles, nutzungsgrad_labels, loc='upper center',
                                             bbox_to_anchor=(0.5, 1.3),
                                             title="Nutzungsgrad", ncol=3, fontsize=13,
                                             prop=prop, title_fontproperties=prop)

        # Setze die Schriftart auf die Tick-Labels der x- und y-Achsen
        for label in (ax1.get_xticklabels() + ax1.get_yticklabels() + ax2.get_xticklabels() + ax2.get_yticklabels()):
            label.set_fontproperties(prop)

        ax1.add_artist(beladedauer_legend)
        ax2.add_artist(nutzungsgrad_legend)

        ax1.tick_params(axis='both', which='major', labelsize=13)
        ax2.tick_params(axis='both', which='major', labelsize=13)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        combined_plot_filename_eps = os.path.join(output_dir, f"{script_name}_combined.eps")

        plt.tight_layout(rect=[0, 0, 1, 0.75])
        plt.savefig(combined_plot_filename_png, format='png')
        plt.savefig(combined_plot_filename_svg, format='svg')
        plt.savefig(combined_plot_filename_eps, format='eps')  # Speichern als EPS
        plt.show()
        plt.close(fig)


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Studie1\PLOTS"
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
