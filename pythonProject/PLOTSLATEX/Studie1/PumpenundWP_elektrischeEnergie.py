import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import numpy as np
import matplotlib.font_manager as font_manager
import matplotlib.ticker as ticker  # Import für benutzerdefinierte Achsenformatierung

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def format_func(x, pos):
    return locale.format_string("%.1f", x).replace('.', ',')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None


def get_sums(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    pel_sums = []
    pel_Pumpe_sums = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]

        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
            pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600

            pel_sums.append(pel_sum)
            pel_Pumpe_sums.append(pel_Pumpe_sum)

    return drehzahlen, pel_sums, pel_Pumpe_sums


def calculate_pump_percentage(pel_sums, pel_Pumpe_sums):
    percentages = []
    for wp, pumpe in zip(pel_sums, pel_Pumpe_sums):
        total = wp + pumpe
        if total > 0:
            percentage = (pumpe / total) * 100
        else:
            percentage = 0
        percentages.append(percentage)
    return percentages


def calculate_wp_increase(pel_sums):
    increase_30_to_40 = []
    increase_40_to_50 = []
    if len(pel_sums) >= 3:
        if pel_sums[0] > 0:
            increase_30_to_40.append(((pel_sums[1] - pel_sums[0]) / pel_sums[0]) * 100)
        else:
            increase_30_to_40.append(0)
        if pel_sums[1] > 0:
            increase_40_to_50.append(((pel_sums[2] - pel_sums[1]) / pel_sums[1]) * 100)
        else:
            increase_40_to_50.append(0)
    return increase_30_to_40, increase_40_to_50


def create_broken_axis(ax, ylim, break_position=5.0, **kwargs):
    """
    Create a broken y-axis on the given axis object.
    """
    ax.set_ylim(ylim)

    # Add break lines
    d = .015  # how big to make the diagonal lines in axes coordinates
    kwargs.update(transform=ax.transAxes, color='k', clip_on=False)
    ax.plot((-d, +d), (-d, +d), **kwargs)  # bottom-left diagonal
   # ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # bottom-right diagonal


def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")

    # Speichern des Plots als EPS-Datei
    fig.savefig(f"{filename_base}.eps", format='eps')
    print(f"Plot gespeichert: {filename_base}.eps")


def save_percentages_to_csv(output_dir, filename, data):
    df = pd.DataFrame(data)
    output_file = os.path.join(output_dir, filename)
    df.to_csv(output_file, index=False, sep=';', decimal=',')
    print(f"Prozentuale Anteile in CSV gespeichert: {output_file}")


def plot_sum_comparison(results_base_dir, output_dir, script_name, identifiers, labels, colors, hatches):
    # Erstellen der Subplots mit unterschiedlichen Breiten
    fig, (ax_zoom, ax_total) = plt.subplots(1, 2, figsize=(8, 4), gridspec_kw={'width_ratios': [2, 1]})

    bar_width = 0.3
    index = np.arange(3)  # Positionen für 30, 40 und 50 Hz

    results_data = []

    for idx, identifier in enumerate(identifiers):
        system_results_df, layer_results_df = read_csv_files(results_base_dir, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums = get_sums(system_results_df, identifier)
            if drehzahlen:
                drehzahlen_zoom = drehzahlen[:3]
                pel_sums_zoom = pel_sums[:3]
                pel_Pumpe_sums_zoom = pel_Pumpe_sums[:3]
                combined_sums_zoom = [wp + pumpe for wp, pumpe in zip(pel_sums_zoom, pel_Pumpe_sums_zoom)]

                ax_zoom.bar(index + idx * bar_width, pel_sums_zoom, bar_width, color=colors[idx], edgecolor='black',
                            label=f'WP {labels["label"]} {locale.format_string("%.1f", identifier)}')
                ax_zoom.bar(index + idx * bar_width, pel_Pumpe_sums_zoom, bar_width, bottom=pel_sums_zoom,
                            hatch=hatches[idx], color=colors[idx], edgecolor='black')

                combined_sums_total = [wp + pumpe for wp, pumpe in zip(pel_sums, pel_Pumpe_sums)]

                if labels["label"] == "Massenstrom":
                    ax_total.plot(drehzahlen, combined_sums_total, marker='o', linestyle='-',
                                  label=f'{locale.format_string("%.1f", identifier)} {labels["unit"]}',
                                  color=colors[idx])
                else:
                    ax_total.plot(drehzahlen, combined_sums_total, marker='o', linestyle='-',
                                  label=f'{identifier} {labels["unit"]}', color=colors[idx])

                pump_percentages = calculate_pump_percentage(pel_sums_zoom, pel_Pumpe_sums_zoom)
                wp_increase_30_to_40, wp_increase_40_to_50 = calculate_wp_increase(pel_sums_zoom)

                results_data.append({
                    "Massenstrom": f"{identifier} {labels['unit']}",
                    "Drehzahl_30_Hz": f"{pump_percentages[0]:.2f}%",
                    "Drehzahl_40_Hz": f"{pump_percentages[1]:.2f}%",
                    "Drehzahl_50_Hz": f"{pump_percentages[2]:.2f}%",
                    "WP Anstieg 30->40 Hz": f"{wp_increase_30_to_40[0]:.2f}%" if wp_increase_30_to_40 else "n/a",
                    "WP Anstieg 40->50 Hz": f"{wp_increase_40_to_50[0]:.2f}%" if wp_increase_40_to_50 else "n/a"
                })

                # Prozentzahlen formatieren: 0,05 kg/s mit zwei Nachkommastellen, andere mit einer Nachkommastelle
                for i, percentage in enumerate(pump_percentages):
                    if identifier == 0.05:
                        formatted_percentage = locale.format_string("%.2f", percentage).replace('.', ',')
                    else:
                        formatted_percentage = locale.format_string("%.1f", percentage).replace('.', ',')
                    ax_zoom.text(index[i] + idx * bar_width, combined_sums_zoom[i] + 0.05,
                                 f"{formatted_percentage}%", fontsize=11, ha='center', fontproperties=prop)
    create_broken_axis(ax_zoom, ylim=[4.92, 5.6])  # Verwende die Funktion hier
    ax_zoom.set_xlabel('Drehzahl in Hz', fontproperties=prop, fontsize=13)
    ax_zoom.set_ylabel('el. Energie in kWh', fontproperties=prop, fontsize=13)
    ax_zoom.grid(False)
    ax_zoom.set_xticks(index + bar_width)
    ax_zoom.set_xticklabels([30, 40, 50], fontproperties=prop)
    ax_zoom.tick_params(axis='both', which='major', labelsize=12)
    ax_zoom.set_ylim([4.91, 5.6])

    # Anwenden des benutzerdefinierten Formatierers auf die Y-Achse und Setzen der Schriftart
    ax_zoom.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

    # Setzen der Schriftart für Y-Achsenticks
    for label in ax_zoom.get_yticklabels():
        label.set_fontproperties(prop)

    ax_total.set_xlabel('Drehzahl in Hz', fontproperties=prop, fontsize=13)
    ax_total.set_ylabel('el. Gesamtenergie in kWh', fontproperties=prop, fontsize=13)
    ax_total.grid(False)
    ax_total.set_xticks(drehzahlen)
    ax_total.set_xticklabels([str(hz).replace('.', ',') for hz in drehzahlen], fontproperties=prop)
    ax_total.set_ylim([4.65, None])
    # Anwenden des benutzerdefinierten Formatierers auf die Y-Achse im rechten Plot
    ax_total.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

    # Setzen der Schriftart für Y-Achsenticks im rechten Plot
    for label in ax_total.get_yticklabels():
        label.set_fontproperties(prop)

    # Achsenunterbrechung hinzufügen

    # Nach der Erstellung der Plots und Achsen


    plt.subplots_adjust(left=0.07, right=0.93, top=0.88, bottom=0.12, wspace=0.15)
    plt.tight_layout(rect=[0, 0, 1, 0.95])

    # Neue Legende für die Farben (Massenströme)
    color_legend = [
        Patch(facecolor='darkred', edgecolor='black', label='0,05 kg/s'),
        Patch(facecolor='red', edgecolor='black', label='0,15 kg/s'),
        Patch(facecolor='grey', edgecolor='black', label='0,25 kg/s')
    ]

    # Text-Hinweis im Plot für Farbcodierung
   # ax_zoom.text(0.5, 1.02, 'Volle Farbe: Wärmepumpe | Schraffiert: Umwälzpumpe', transform=ax_zoom.transAxes,
    #             fontsize=11, ha='center', fontproperties=prop)

    # Positionieren der Farblegende oberhalb des Plots
    ax_zoom.legend(handles=color_legend, loc='upper left', bbox_to_anchor=(0.01, 0.99),
                   fontsize=12, prop=prop, ncol=1, handletextpad=0.5, labelspacing=0.5)

    custom_legend_lines = [
        Line2D([0], [0], marker='o', linestyle='-', color=color, lw=2,
               label=f'{locale.format_string("%.2f", identifier).replace(".", ",")} {labels["unit"]}' if labels[
                                                                                                             "label"] == "Massenstrom" else f'$\\dot{{m}}_{{\\mathrm{{Sek}}}}${identifier} {labels["unit"]}')
        for color, identifier in zip(colors, identifiers)
    ]

    legend2 = ax_total.legend(handles=custom_legend_lines, loc='best', fontsize=12,  prop=prop)
  #  legend2.set_title('Gesamtleistung', prop=prop)

    plot_and_save(fig, os.path.join(output_dir, f"{script_name}_{labels['filename']}"))

    save_percentages_to_csv(output_dir, f"{script_name}_prozentuale_Anteile.csv", results_data)

    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Studie1\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    hatches = ['//', '//', '//']

    plot_sum_comparison(results_base_dir_1, output_dir, script_name,
                        identifiers=[5, 8, 10],
                        labels={"title": "Temperaturdifferenzen", "label": "Delta T", "filename": "delta_t",
                                "unit": "K"},
                        colors=['orange', 'blue', 'green'],
                        hatches=hatches)

    plot_sum_comparison(results_base_dir_2, output_dir, script_name,
                        identifiers=[0.05, 0.15, 0.25],
                        labels={"title": "Massenströme", "label": "Massenstrom", "filename": "mass_flow",
                                "unit": "kg/s"},
                        colors=['darkred', 'red', 'grey'],
                        hatches=hatches)
