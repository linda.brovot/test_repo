import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import matplotlib.font_manager as font_manager  # Import für Schriftarten
import matplotlib.ticker as ticker  # Import für benutzerdefinierte Achsenformatierung

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(system_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return system_results_df
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {system_csv_file}")
        return None
def format_func_mixed(x, pos):
    if x == 0:
        return '0'  # Keine Nachkommastellen für 0
    else:
        return locale.format_string("%.1f", x).replace('.', ',')  # Eine Nachkommastelle für alle anderen Werte


def format_func_2dec(x, pos):
    return locale.format_string("%.2f", x).replace('.', ',')

def format_func_1dec(x, pos):
    return locale.format_string("%.1f", x).replace('.', ',')

def plot_exergie_and_percentage_changes(results_base_dir, output_dir, script_name, identifiers, labels, colors, bar_colors):
    fig, axes = plt.subplots(1, 2, figsize=(8, 4), gridspec_kw={'width_ratios': [1.5, 1]})
  # Zwei Subplots nebeneinander

    drehzahlen = [30, 40, 50, 60, 70, 80, 90]  # Alle Drehzahlen
    index = np.arange(len(drehzahlen))  # Positionen für alle Drehzahlen

    all_exergies = {}

    # Linker Plot: Exergie im Speicher als Liniendiagramm
    for idx, identifier in enumerate(identifiers):
        system_results_df = read_csv_files(results_base_dir, identifier)
        if system_results_df is not None:
            # Berechne die Exergie für alle Drehzahlen
            sum_diff_exergies = [
                system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")][
                    'Sum_Differences_Exergies'].values[0] / 1000 / 3600
                for hz in drehzahlen
            ]

            # Plot als Liniendiagramm
            axes[0].plot(drehzahlen, sum_diff_exergies, marker='o', label=f'{locale.format_string("%.2f", identifier).replace(".", ",")} {labels["unit"]}',
                         color=colors[idx], linewidth=2)

            # Exergie für diesen Identifier speichern
            all_exergies[identifier] = sum_diff_exergies

    axes[0].set_xlabel('Drehzahl in Hz', fontproperties=prop, fontsize=13)
    axes[0].set_ylabel('Exergie im Speicher in kWh', fontproperties=prop, fontsize=13)
    axes[0].legend(loc='best', fontsize=12, prop=prop)
    axes[0].tick_params(axis='both', which='major', labelsize=12)

    # Setzen der Schriftart und des Formatierers für die Ticks im linken Plot
    axes[0].xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: f"{int(x):.0f}"))
    axes[0].yaxis.set_major_formatter(ticker.FuncFormatter(format_func_2dec))

    for label in axes[0].get_xticklabels() + axes[0].get_yticklabels():
        label.set_fontproperties(prop)

    # Rechter Plot: Prozentuale Änderung der Leistung und Exergie
    data = {
        40: {  # Frequenz 40 Hz
            0.15: {'power': 5.05, 'exergy': 1.05},
            0.25: {'power': 5.11, 'exergy': 1.06}
        },
        50: {  # Frequenz 50 Hz
            0.15: {'power': 5.28, 'exergy': 1.04},
            0.25: {'power': 5.32, 'exergy': 1.05}
        }
    }

    percentage_changes = []

    # Berechnung der prozentualen Änderungen
    for frequency in [40, 50]:
        power_15 = data[frequency][0.15]['power']
        power_25 = data[frequency][0.25]['power']
        exergy_15 = data[frequency][0.15]['exergy']
        exergy_25 = data[frequency][0.25]['exergy']

        power_change = ((power_25 - power_15) / power_15) * 100
        exergy_change = ((exergy_25 - exergy_15) / exergy_15) * 100

        percentage_changes.append({
            'frequency': frequency,
            'power_change (%)': power_change,
            'exergy_change (%)': exergy_change
        })

    # Erstellen des Balkendiagramms für die prozentualen Änderungen
    frequencies = [40, 50]
    power_changes = [item['power_change (%)'] for item in percentage_changes]
    exergy_changes = [item['exergy_change (%)'] for item in percentage_changes]

    bar_width = 0.4
    index = np.arange(len(frequencies))

    # Balken für die prozentualen Änderungen mit benutzerdefinierten Farben
    bars1 = axes[1].bar(index, power_changes, bar_width, edgecolor='black',label='El. Energie', color=bar_colors[0])
    bars2 = axes[1].bar(index + bar_width, exergy_changes, bar_width, edgecolor='black',label='Exergie',
                        color=bar_colors[1])

    # Werte über die Balken schreiben (mit vergrößerter Schriftgröße)
    for bars in [bars1, bars2]:
        for bar in bars:
            yval = bar.get_height()
            axes[1].text(bar.get_x() + bar.get_width() / 2, yval, f'{locale.format_string("%.2f", yval).replace(".", ",")}%', ha='center', va='bottom', fontsize=12, fontproperties=prop)

    axes[1].set_xlabel('Drehzahl in Hz', fontproperties=prop, fontsize= 13)
    axes[1].set_ylabel('Prozentuale Änderung in %', fontproperties=prop, fontsize =13)
    axes[1].set_ylim([0, 2.5])
    axes[1].set_xticks(index + bar_width / 2)
    axes[1].set_xticklabels(frequencies)
    axes[1].legend(loc='best', fontsize=12, prop=prop)
    axes[1].tick_params(axis='both', which='major', labelsize=12)

    # Setzen der Schriftart und des Formatierers für die Ticks im rechten Plot
    axes[1].xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: f"{int(x):.0f}"))
    axes[1].yaxis.set_major_formatter(ticker.FuncFormatter(format_func_mixed))
    axes[1].set_xticklabels([40, 50])


    for label in axes[1].get_xticklabels() + axes[1].get_yticklabels():
        label.set_fontproperties(prop)

    # Layout anpassen und Plot anzeigen
    plt.tight_layout()

    # Sicherstellen, dass der Plot im richtigen Verzeichnis gespeichert wird
    plot_file_path_png = os.path.join(output_dir, f"{script_name}_exergie_and_percentage_changes.png")
    plot_file_path_svg = os.path.join(output_dir, f"{script_name}_exergie_and_percentage_changes.svg")
    plot_file_path_eps = os.path.join(output_dir, f"{script_name}_exergie_and_percentage_changes.eps")

    plt.savefig(plot_file_path_png)
    plt.savefig(plot_file_path_svg)
    plt.savefig(plot_file_path_eps, format='eps')

    print(f"Plot gespeichert unter: {plot_file_path_png}")
    print(f"Plot gespeichert unter: {plot_file_path_svg}")
    print(f"Plot gespeichert unter: {plot_file_path_eps}")
    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Studie1\PLOTS"

    # Plot für Massenströme Exergie und prozentuale Änderungen
    plot_exergie_and_percentage_changes(results_base_dir, output_dir, script_name,
                                        identifiers=[0.05, 0.15, 0.25],
                                        labels={"unit": "kg/s"},
                                        colors=['darkred', 'red', 'grey'],
                                        bar_colors=['grey', 'steelblue'])
