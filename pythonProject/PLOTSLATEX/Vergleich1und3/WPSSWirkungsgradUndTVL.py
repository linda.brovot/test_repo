import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re
import matplotlib.font_manager as font_manager

# Setzen der Schriftart auf 'Palatino Linotype' vor dem Erstellen von Plots
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=13)
matplotlib.rcParams['svg.fonttype'] = 'none'

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()

def read_time_dependent_csv_files(base_dir, type):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren des Wertes (z.B. Temperatur oder Frequenz)

    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    value = float(match.group(1))
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')

                    if type == 'temperature':
                        temp_celsius = value - 273.15  # Umrechnung von Kelvin in Celsius
                        df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    elif type == 'frequency':
                        df['Frequency'] = value  # Hinzufügen der extrahierten Frequenz

                    data_frames.append(df)
    return data_frames

def read_exergy_results(base_dir):
    file_path = os.path.join(base_dir, "results", "time_dependent_results.csv")
    print(f"Versuche Datei zu lesen: {file_path}")

    if not os.path.isfile(file_path):
        print(f"Die Datei {file_path} wurde nicht gefunden.")
        return pd.DataFrame()

    df = pd.read_csv(file_path, sep=',', decimal='.')
    return df

def format_func(x, pos, decimal_places=2):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_time_dependent_results(temperature_data_frames, frequency_data_frames, exergy_data_frame, output_dir):
    # Define fixed colors for specific frequencies and temperatures
    frequency_colors = {0.3: 'tab:blue', 0.4: 'tab:orange', 0.5: 'tab:green',
                        0.6: 'tab:red', 0.7: 'tab:purple', 0.8: 'tab:brown', 0.9: 'tab:pink'}
    temperature_colors = {65.0: 'magenta', 70.0: 'gold'}

    columns_to_plot = [('Wirkungsgrad_gesamt', 'Wirkungsgrad vom Wärmepumpen-Speicher-System')]

    # Layout für zwei Subplots (zwei Spalten, zwei Zeilen, jede Spalte für einen Massenstrom)
    fig, axs = plt.subplots(2, 2, figsize=(8, 6), sharex='col', gridspec_kw={'height_ratios': [1, 1]})

    # Konfigurationen für die beiden Subplots
    mass_flow_config = [
        {'mass_flow': 0.15, 'frequencies_to_plot': [0.8, 0.9], 'ax_wirkungsgrad': axs[0, 0], 'ax_tvl': axs[1, 0],
         'title': 'Massenstrom 0,15 kg/s'},
        {'mass_flow': 0.25, 'frequencies_to_plot': [0.3, 0.4, 0.5, 0.6, 0.7], 'ax_wirkungsgrad': axs[0, 1],
         'ax_tvl': axs[1, 1], 'title': 'Massenstrom 0,25 kg/s'}
    ]

    temperatures_to_plot = [65.0, 70.0]  # Temperaturen, die ebenfalls geplottet werden sollen

    for column, title in columns_to_plot:
        for config in mass_flow_config:
            mass_flow = config['mass_flow']
            frequencies_to_plot = config['frequencies_to_plot']
            ax_wirkungsgrad = config['ax_wirkungsgrad']
            ax_tvl = config['ax_tvl']

           # ax_wirkungsgrad.set_title(f'{config["title"]}', fontsize=12, fontproperties=prop)

            # Plot für Frequenzen (Wirkungsgrad, oben)
            for df in frequency_data_frames:
                for frequency in frequencies_to_plot:
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
                    if not filtered_df.empty:
                        freq_label = locale.format_string('%.0f', frequency * 100)  # Frequenz in Hz ohne Dezimalstellen
                        smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                        ax_wirkungsgrad.plot(filtered_df['Sekunde'] / 3600, filtered_df[column], label=f'{freq_label} Hz',
                                             color=frequency_colors[frequency])

            # Plot für Temperaturen (Wirkungsgrad, oben)
            for df in temperature_data_frames:
                temp_celsius = df['Temperature'].iloc[0]
                if temp_celsius in temperatures_to_plot:
                    temp_label = locale.format_string('%.0f', temp_celsius)
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}.csv")]
                    if not filtered_df.empty:
                        smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                        ax_wirkungsgrad.plot(filtered_df['Sekunde'] / 3600, filtered_df[column],
                                             label=f'TVL = {temp_label} °C', color=temperature_colors[temp_celsius])

            # Plot für Temperaturen (Vorlauftemperatur, unten)
            for df in temperature_data_frames:
                temp_celsius = df['Temperature'].iloc[0]
                if temp_celsius in temperatures_to_plot:
                    temp_label = locale.format_string('%.0f', temp_celsius)
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}.csv")]
                    if not filtered_df.empty:
                        if 'Vorlauftemperatur' in filtered_df.columns:
                            filtered_df = filtered_df.copy()
                            filtered_df['Vorlauftemperatur_Celsius'] = filtered_df[
                                                                           'Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                            smoothed_data = moving_average(filtered_df['Vorlauftemperatur_Celsius'],
                                                           window_size=5)  # Glätte die Daten
                            ax_tvl.plot(filtered_df['Sekunde'] / 3600, smoothed_data,
                                        label=f'$T_{{VL}} = {temp_label} \, °C$', color=temperature_colors[temp_celsius])

            # Plot für Frequenzen (Vorlauftemperatur, unten)
            for df in frequency_data_frames:
                for frequency in frequencies_to_plot:
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
                    if not filtered_df.empty:
                        if 'Vorlauftemperatur' in filtered_df.columns:
                            filtered_df = filtered_df.copy()
                            filtered_df['Vorlauftemperatur_Celsius'] = filtered_df[
                                                                           'Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                            smoothed_data = moving_average(filtered_df['Vorlauftemperatur_Celsius'],
                                                           window_size=5)  # Glätte die Daten
                            ax_tvl.plot(filtered_df['Sekunde'] / 3600, smoothed_data, label=f'{frequency * 100} Hz',
                                        color=frequency_colors[frequency])

            # Exergie-Daten für 0.15 kg/s hinzufügen
            if mass_flow == 0.15 and not exergy_data_frame.empty:
                smoothed_data = moving_average(exergy_data_frame[column], window_size=5)  # Glätte die Daten
                ax_wirkungsgrad.plot(exergy_data_frame['Sekunde'] / 3600, smoothed_data, label=r'$COP_{\mathrm{max}}$',
                                     linestyle='--')

            # Setze Achsentitel und Formatierung
            ax_wirkungsgrad.set_ylabel('Wirkungsgrad', fontsize=13, fontproperties=prop)
            ax_wirkungsgrad.grid(False)
            ax_wirkungsgrad.tick_params(axis='both', which='major', labelsize=13)
            for label in ax_wirkungsgrad.get_yticklabels():
                label.set_fontproperties(prop)
            ax_wirkungsgrad.yaxis.set_major_formatter(ticker.FuncFormatter(
                lambda y, _: locale.format_string('%.0f' if y.is_integer() else '%.2f', y).replace('.', ',')))

            ax_tvl.set_xlabel('Beladedauer in h', fontsize=13, fontproperties=prop)
            ax_tvl.set_ylabel('Vorlauftemperatur in °C', fontsize=13, fontproperties=prop)
            ax_tvl.grid(False)
            ax_tvl.tick_params(axis='both', which='major', labelsize=12)
            for label in ax_tvl.get_xticklabels() + ax_tvl.get_yticklabels():
                label.set_fontproperties(prop)

    # Einheitliche Legende außerhalb des Plots
    handles_legend = []
    labels_legend = []

    # Frequenzen
    for frequency in sorted(frequency_colors.keys()):
        handles_legend.append(plt.Line2D([0], [0], color=frequency_colors[frequency], lw=2))
        labels_legend.append(f"{int(frequency * 100)} Hz")

    # Temperaturen
    for temp in sorted(temperature_colors.keys()):
        handles_legend.append(plt.Line2D([0], [0], color=temperature_colors[temp], lw=2))
        labels_legend.append(f"TVL = {int(temp)} °C")

    fig.legend(handles_legend, labels_legend, loc='upper center', fontsize=8, prop=prop,
               bbox_to_anchor=(0.5, 1), ncol=5, frameon=True, fancybox=True)

    plt.tight_layout(rect=[0, 0, 1, 0.9])

    # Speicher und Anzeige des Plots
    plot_filename = os.path.join(output_dir, "Vergleich_Massenströme_Wirkungsgrad_und_Vorlauftemperatur")
    fig.savefig(f"{plot_filename}.png", format='png')  # Speichern als PNG
    fig.savefig(f"{plot_filename}.svg", format='svg')  # Speichern als SVG
    fig.savefig(f"{plot_filename}.eps", format='eps')  # Speichern als EPS

    plt.show()


if __name__ == "__main__":
    temperature_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy_Exergie_pro_Zeitschritt"
    frequency_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    exergy_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Beladestrategie\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Vergleich1und3\PLOTS\WPSSWirkungsgradUndTVL"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    temperature_data_frames = read_time_dependent_csv_files(temperature_base_dir, 'temperature')
    frequency_data_frames = read_time_dependent_csv_files(frequency_base_dir, 'frequency')
    exergy_data_frame = read_exergy_results(exergy_base_dir)

    plot_time_dependent_results(temperature_data_frames, frequency_data_frames, exergy_data_frame, output_dir)
