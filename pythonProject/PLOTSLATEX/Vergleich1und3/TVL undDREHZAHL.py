import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re
import matplotlib.font_manager as font_manager
import matplotlib

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

matplotlib.rcParams['svg.fonttype'] = 'path'  # Schriftart in SVG-Dateien als Text erhalten


def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()


def read_time_dependent_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    if 'Vorlauftemperatur' in df.columns:
                        df['Vorlauftemperatur'] = df['Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames


def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True).replace('.', ',')
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True).replace('.', ',')


def plot_individual_temperature_results(data_frames, output_dir, temperature):
    identifiers = [0.05, 0.15, 0.25]
    colors = ['darkred', 'red', 'grey']  # Farben: Rot, Dunkelrot und Grau

    fig, (ax_temp, ax_rpm) = plt.subplots(2, 1, figsize=(8, 6))

    # Subplot für Vorlauftemperatur
    for i, df in enumerate(data_frames):
        if df['Temperature'].iloc[0] == temperature:
            for j, identifier in enumerate(identifiers):
                color = colors[j % len(colors)]  # Wähle die Farbe aus
                filtered_df = df[df['Dateiname'].str.contains(f"_{identifier}.csv")]
                if not filtered_df.empty:
                    print(f"Plotte Vorlauftemperatur für {temperature}°C, Massenstrom {identifier} kg/s")
                    smoothed_data = moving_average(filtered_df['Vorlauftemperatur'], window_size=5)
                    ax_temp.plot(filtered_df['Sekunde'] / 3600, smoothed_data, color=color, label=f'{str(identifier).replace(".", ",")} kg/s')

                    # Überprüfen, wann die Temperatur 65°C erreicht
                    temp_reached_65 = filtered_df[filtered_df['Vorlauftemperatur'] >= 65]
                    if not temp_reached_65.empty:
                        time_to_65 = temp_reached_65['Sekunde'].iloc[0] / 3600
                        ax_temp.axvline(x=time_to_65, color=color, linestyle=':', label=None)  # Keine Beschriftung in der Legende
                        ax_temp.text(time_to_65, 66, f'{time_to_65:.2f} h'.replace('.', ','), color=color, fontsize=10, fontproperties=prop)

    ax_temp.set_ylabel('Vorlauftemperatur in °C', fontsize=13, fontproperties=prop)
    ax_temp.grid(False)
    ax_temp.legend(fontsize=12, prop=prop)
    ax_temp.locator_params(axis='x', nbins=5)
    ax_temp.locator_params(axis='y', nbins=5)
    ax_temp.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax_temp.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 0)))
    ax_temp.tick_params(axis='both', which='major', labelsize=12)
    for label in ax_temp.get_xticklabels() + ax_temp.get_yticklabels():
        label.set_fontproperties(prop)

    # Horizontale Linie bei 65°C hinzufügen
    if temperature == 65:
        ax_temp.axhline(y=65, color='orange', linestyle='--', label='65°C Linie')

    # Subplot für Verdichterdrehzahl
    for i, df in enumerate(data_frames):
        if df['Temperature'].iloc[0] == temperature:
            for j, identifier in enumerate(identifiers):
                color = colors[j % len(colors)]  # Wähle die Farbe aus
                filtered_df = df[df['Dateiname'].str.contains(f"_{identifier}.csv")]
                if not filtered_df.empty:
                    print(f"Plotte Verdichterdrehzahl für {temperature}°C, Massenstrom {identifier} kg/s")
                    smoothed_data = moving_average(filtered_df['Verdichterdrehzahl'], window_size=5)
                    ax_rpm.plot(filtered_df['Sekunde'] / 3600, smoothed_data, color=color, label=f'{str(identifier).replace(".", ",")} kg/s')

    ax_rpm.set_xlabel('Beladedauer in h', fontsize=13, fontproperties=prop)
    ax_rpm.set_ylabel('Drehzahl in Hz', fontsize=13, fontproperties=prop)
    ax_rpm.grid(False)
    ax_rpm.legend(fontsize=12, prop=prop)
    ax_rpm.locator_params(axis='x', nbins=5)
    ax_rpm.locator_params(axis='y', nbins=5)
    ax_rpm.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax_rpm.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.0f', x * 100).replace('.', ',')))
    ax_rpm.tick_params(axis='both', which='major', labelsize=12)
    for label in ax_rpm.get_xticklabels() + ax_rpm.get_yticklabels():
        label.set_fontproperties(prop)

    # Speichern der Diagramme
    plot_filename_combined = os.path.join(output_dir,
                                          f"Verdichterdrehzahl_Vorlauftemperatur_{temperature}C".replace(' ',
                                                                                                         '_').replace(
                                              '.', ','))
    fig.tight_layout(rect=[0, 0, 1, 0.96])  # Platz für den Supertitel lassen
    fig.savefig(f"{plot_filename_combined}.png", format='png')
    fig.savefig(f"{plot_filename_combined}.svg", format='svg')
    fig.savefig(f"{plot_filename_combined}.eps", format='eps')
    print(f"Plot gespeichert: {plot_filename_combined}.png, {plot_filename_combined}.svg und {plot_filename_combined}.eps")

    plt.show()


if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Vergleich1und3\PLOTS\Drehzahl_TVL"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)

    # Erstelle separate Plots für 65°C und 70°C
    plot_individual_temperature_results(data_frames, output_dir, 65)
    plot_individual_temperature_results(data_frames, output_dir, 70)
