import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
import os
from matplotlib.lines import Line2D  # Für Legenden-Einträge
from matplotlib import gridspec


matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Ausgewählte Parameter
massenstrom_ziel = 0.15  # Nur 0,15 kg/s für den 3D-Plot
verdampfer_temperaturen = [7, 15]  # 7°C und 15°C Verdampfertemperatur für den 3D-Plot
ruecklauf_temp_2D = 30  # 30°C Rücklauftemperatur für den 2D-Plot
farben = ['red', 'gray']  # Farben für die Oberflächen und Linien

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\CombinedPlot'
os.makedirs(plot_base_dir, exist_ok=True)


# Erstellen des Figures
fig = plt.figure(figsize=(18, 8))

# Linker Subplot (größerer 3D-Plot) - Quadrat, aber größer
ax1 = fig.add_axes([0.05, 0.1, 0.55, 0.8], projection='3d')  # [left, bottom, width, height]

# Rechter Subplot (kleinerer 2D-Plot) - Quadrat, aber kleiner
ax2 = fig.add_axes([0.7, 0.25, 0.25, 0.5])  # [left, bottom, width, height]



surface_plots = []  # Liste für legendeneinträge
labels = []  # Liste für die labels

for i, verdampfer_temp in enumerate(verdampfer_temperaturen):
    # Filtere Daten für den gewünschten Massenstrom und die spezifische Verdampfereintrittstemperatur
    filtered_temp_data = data[
        (data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel) &
        (data['T_eva_in in C'] == verdampfer_temp)
    ]

    x = filtered_temp_data['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
    y = filtered_temp_data['T_con_in in C']  # Kondensatortemperatur
    cop = filtered_temp_data['COP in - (Coefficient of performance)']

    # Überprüfen, ob genügend Variabilität vorhanden ist
    if len(np.unique(x)) < 2 or len(np.unique(y)) < 2:
        print(
            f"Nicht genügend Variabilität in den Daten für Verdampfertemperatur {verdampfer_temp:.2f} °C und Massenstrom {massenstrom_ziel:.2f} kg/s. Interpolation übersprungen.")
        continue

    # NaN-Werte ignorieren
    valid_data_mask = np.isfinite(x) & np.isfinite(y) & np.isfinite(cop)
    x = x[valid_data_mask]
    y = y[valid_data_mask]
    cop = cop[valid_data_mask]

    # Gitter für die Interpolation erstellen
    xi = np.linspace(x.min(), x.max(), 100)
    yi = np.linspace(y.min(), y.max(), 100)
    xi, yi = np.meshgrid(xi, yi)

    # Interpolation für die Oberfläche
    try:
        cop_interpolated = griddata((x, y), cop, (xi, yi), method='linear')
    except Exception as e:
        print(
            f"Fehler bei der Interpolation für Verdampfertemperatur {verdampfer_temp:.2f} °C und Massenstrom {massenstrom_ziel:.2f} kg/s: {e}")
        continue

    # Oberfläche plotten mit den neuen Farben
    surf = ax1.plot_surface(xi, yi, cop_interpolated, color=farben[i], alpha=0.7)

    # Füge Oberflächen zur Legende hinzu
    surface_plots.append(Line2D([0], [0], color=farben[i], lw=4))
    labels.append(rf'${{T}}_{{\mathrm{{Quelle,ein}}}}$: {verdampfer_temp} °C')

# Achsen und Labels für den linken Subplot
ax1.set_xlabel('Drehzahl in Hz', fontsize=11, labelpad=12)
ax1.set_ylabel('Rücklauftemperatur in °C', fontsize=10, labelpad=12)
ax1.set_zlabel('COP', fontsize=11, labelpad=12)
ax1.view_init(elev=20, azim=60)
ax1.invert_xaxis()
ax1.legend(surface_plots, labels, loc='upper right', fontsize=10, bbox_to_anchor=(0.96, 0.65))

# Rechter Subplot (2D-Plot)
ax2 = fig.add_subplot(122)

filtered_data = data[(data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel) &
                     (data['T_con_in in C'] == ruecklauf_temp_2D)]

for i, quellentemp in enumerate(verdampfer_temperaturen):
    quellen_data = filtered_data[filtered_data['T_eva_in in C'] == quellentemp]
    if not quellen_data.empty:
        ax2.plot(quellen_data['n in - (Relative compressor speed)'] * 100,
                 quellen_data['COP in - (Coefficient of performance)'],
                 label=f'$T_{{Quelle, ein}}$ = {str(quellentemp).replace(".", ",")} °C', linestyle='--', marker='o',
                 color=farben[i])

ax2.set_xlabel('Drehzahl (Hz)', fontsize=14)
ax2.set_ylabel('COP', fontsize=14)
ax2.legend()
ax2.grid(False)

# Plot speichern
plot_file_svg = os.path.join(plot_base_dir, f'CombinedPlot.svg')
plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

print(f'Der Plot "{plot_file_svg}" wurde erfolgreich erstellt.')

# Plot anzeigen
plt.show()
