import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
import os

matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Rücklauftemperaturen (TRL) und Quellentemperaturen (T_eva_in) extrahieren
ruecklauf_temperaturen = data['T_con_in in C'].unique()
quellen_temperaturen = data['T_eva_in in C'].unique()

# Massenströme (ṁ_sek) extrahieren
massenstroeme = data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'].unique()

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\COPPlot'
os.makedirs(plot_base_dir, exist_ok=True)

# Plots erstellen
for massenstrom_ziel in massenstroeme:
    fig = plt.figure()  # Plot ohne feste Größe erstellen
    ax = fig.add_subplot(111, projection='3d')
    filtered_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

    for quellen_temp in quellen_temperaturen:
        filtered_temp_data = filtered_data[filtered_data['T_eva_in in C'] == quellen_temp]

        x = filtered_temp_data['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
        y = filtered_temp_data['T_con_in in C']  # Rücklauftemperatur auf der Y-Achse
        cop = filtered_temp_data['COP in - (Coefficient of performance)']

        # Überprüfen, ob genug Datenpunkte vorhanden sind
        if len(filtered_temp_data) < 3:
            print(f"Nicht genügend Daten für Quellentemperatur {quellen_temp:.2f} °C.")
            continue

        # NaN-Werte ignorieren
        valid_data_mask = np.isfinite(x) & np.isfinite(y) & np.isfinite(cop)
        x = x[valid_data_mask]
        y = y[valid_data_mask]
        cop = cop[valid_data_mask]

        # Gitter für die Interpolation erstellen
        xi = np.linspace(x.min(), x.max(), 100)
        yi = np.linspace(y.min(), y.max(), 100)
        xi, yi = np.meshgrid(xi, yi)

        # Interpolation für die Oberfläche
        cop_interpolated = griddata((x, y), cop, (xi, yi), method='linear')

        # Oberfläche plotten mit Transparenz und unterschiedlichen Farben
        surf = ax.plot_surface(xi, yi, cop_interpolated, label=f'Quelleintrittstemp {quellen_temp:.1f} °C', alpha=0.6)

    ax.set_xlabel('Drehzahl (Hz)', fontsize=11, labelpad=12)
    ax.set_ylabel('Rücklauftemperatur (°C)', fontsize=10, labelpad=12)
    ax.set_zlabel('COP', fontsize=11, labelpad=12)

    # Diagramm drehen, so dass die Rücklauftemperatur links ist
    ax.view_init(elev=40, azim=60)

    # Legende erstellen
    ax.legend(title='Quellentemperatur (°C)', fontsize=9)

    # Zahlenformatierung für die Achsen
    ax.xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f} Hz'))
    ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
    ax.zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))

    # Plot speichern
    plot_file_svg = os.path.join(plot_base_dir, f'COPPlot_massenstrom_{massenstrom_ziel:.2f}.svg')

    # Speichern als SVG
    plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

    print(f'Der Plot "{plot_file_svg}" wurde erfolgreich erstellt.')

    # Plot anzeigen
    plt.show()
