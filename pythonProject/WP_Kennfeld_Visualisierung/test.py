import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
#matplotlib.use('TkAgg')
from scipy.interpolate import griddata
import numpy as np
import os

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Rücklauftemperaturen (TRL) extrahieren
ruecklauf_temperaturen = data['T_con_in in C'].unique()

# Massenstrom (ṁ_sek) auf 0.15 kg/s beschränken
massenstrom_ziel = 0.15
filtered_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\COPPlot'
os.makedirs(plot_base_dir, exist_ok=True)

# Plots erstellen für alle Rücklauftemperaturen in einem Diagramm
fig = plt.figure(figsize=(12, 8))  # Größeren Plot erstellen
ax = fig.add_subplot(111, projection='3d')

# Farbcodierung für Rücklauftemperaturen
norm = plt.Normalize(vmin=ruecklauf_temperaturen.min(), vmax=ruecklauf_temperaturen.max())
colors = plt.cm.viridis(norm(ruecklauf_temperaturen))

for ruecklauf_temp, color in zip(ruecklauf_temperaturen, colors):
    filtered_temp_data = filtered_data[filtered_data['T_con_in in C'] == ruecklauf_temp]

    x = filtered_temp_data['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
    y = filtered_temp_data['T_eva_in in C']
    cop = filtered_temp_data['COP in - (Coefficient of performance)']

    # Überprüfen, ob genug Datenpunkte vorhanden sind
    if len(filtered_temp_data) < 3:
        print(f"Nicht genügend Daten für TRL {ruecklauf_temp:.2f} °C.")
        continue

    # NaN-Werte ignorieren
    valid_data_mask = np.isfinite(x) & np.isfinite(y) & np.isfinite(cop)
    x = x[valid_data_mask]
    y = y[valid_data_mask]
    cop = cop[valid_data_mask]

    # Gitter für die Interpolation erstellen
    xi = np.linspace(x.min(), x.max(), 100)
    yi = np.linspace(y.min(), y.max(), 100)
    xi, yi = np.meshgrid(xi, yi)

    # Interpolation für die Oberfläche
    cop_interpolated = griddata((x, y), cop, (xi, yi), method='linear')

    # Oberfläche plotten
    surf = ax.plot_surface(xi, yi, cop_interpolated, color=color, alpha=0.6, edgecolor='none')

ax.set_xlabel('Drehzahl (Hz)', fontsize=14, labelpad=15)
ax.set_ylabel('Quelleintrittstemperatur (°C)', fontsize=14, labelpad=15)
ax.set_zlabel('COP', fontsize=14, labelpad=15)
ax.set_title(rf'COP bei verschiedenen Rücklauftemperaturen, $\dot{{m}}_{{\mathrm{{sek}}}}$ = {str(massenstrom_ziel).replace(".", ",")} kg/s', fontsize=16, pad=30)

# Colorbar hinzufügen
mappable = plt.cm.ScalarMappable(cmap='viridis', norm=norm)
mappable.set_array(ruecklauf_temperaturen)
cbar = fig.colorbar(mappable, ax=ax, shrink=0.5, aspect=5, pad=0.15)
cbar.set_label('Rücklauftemperatur (°C)', fontsize=14)

# Zahlenformatierung für die Colorbar und Achsen
ax.xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f} Hz'))
ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
ax.zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))
cbar.ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.0f}'.replace('.', ',')))

# Plot speichern
plot_file = os.path.join(plot_base_dir, f'COPPlot_massenstrom_{massenstrom_ziel:.2f}_alle_TRLs.png')
plt.savefig(plot_file)
print(f'Der Plot "{plot_file}" wurde erfolgreich erstellt.')

# Plot anzeigen
plt.show()
