import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
import os

matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Rücklauftemperaturen (TRL) extrahieren
ruecklauf_temperaturen = data['T_con_in in C'].unique()

# Massenstrom (ṁ_sek) auf 0.15 kg/s beschränken
massenstrom_ziel = 0.15
filtered_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\COPPlot'
os.makedirs(plot_base_dir, exist_ok=True)

# Plots erstellen
for ruecklauf_temp in ruecklauf_temperaturen:
    fig = plt.figure()  # Plot ohne feste Größe erstellen
    ax = fig.add_subplot(111, projection='3d')
    filtered_temp_data = filtered_data[filtered_data['T_con_in in C'] == ruecklauf_temp]

    x = filtered_temp_data['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
    y = filtered_temp_data['T_eva_in in C']
    cop = filtered_temp_data['COP in - (Coefficient of performance)']

    # Überprüfen, ob genug Datenpunkte vorhanden sind
    if len(filtered_temp_data) < 3:
        print(f"Nicht genügend Daten für TRL {ruecklauf_temp:.2f} °C.")
        continue

    # NaN-Werte ignorieren
    valid_data_mask = np.isfinite(x) & np.isfinite(y) & np.isfinite(cop)
    x = x[valid_data_mask]
    y = y[valid_data_mask]
    cop = cop[valid_data_mask]

    # Gitter für die Interpolation erstellen
    xi = np.linspace(x.min(), x.max(), 100)
    yi = np.linspace(y.min(), y.max(), 100)
    xi, yi = np.meshgrid(xi, yi)

    # Interpolation für die Oberfläche
    cop_interpolated = griddata((x, y), cop, (xi, yi), method='linear')

    # Oberfläche plotten
    surf = ax.plot_surface(xi, yi, cop_interpolated, cmap='viridis')
    ax.set_xlabel('Drehzahl in Hz', fontsize=11, labelpad=12)
    ax.set_ylabel('Quelleintrittstemperatur in °C', fontsize=10, labelpad=12)
    ax.set_zlabel('COP', fontsize=11, labelpad=12)
    #ax.set_title(r'COP bei $\(T_{\mathrm{RL}}$ = {:.0f} °C, $\dot{m}_{\mathrm{sek}}$ = {:.2f} kg/s'.format(ruecklauf_temp, massenstrom_ziel).replace(".", ","), fontsize=12, pad=30)






    # Colorbar hinzufügen
    cbar = fig.colorbar(surf, ax=ax, shrink=0.5, aspect=5, pad=0.15)
    cbar.set_label('COP', fontsize=11)

    # Zahlenformatierung für die Colorbar und Achsen
    ax.xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f}'))
    ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
    ax.zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))
    cbar.ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))

    # Plot speichern
    plot_file_svg = os.path.join(plot_base_dir, f'COPPlot_massenstrom_{massenstrom_ziel:.2f}_TRL_{ruecklauf_temp:.1f}.svg')

    # Speichern als SVG
    plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

    print(f'Der Plot "{plot_file_svg}" wurde erfolgreich erstellt.')

    # Plot anzeigen
    plt.show()