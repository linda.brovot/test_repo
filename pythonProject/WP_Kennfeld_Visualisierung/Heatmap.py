import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in_C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in_C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Filterung des Massenstroms auf 0.15 kg/s
massenstrom_ziel = 0.15
filtered_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

# Überprüfung auf ausreichende Datenpunkte
if filtered_data.empty:
    print(f"Keine Daten für einen Massenstrom von {massenstrom_ziel} kg/s gefunden.")
else:
    # Erstellen der Heatmap
    pivot_table = filtered_data.pivot_table(values='COP in - (Coefficient of performance)',
                                            index='T_eva_in_C',
                                            columns='n in - (Relative compressor speed)')

    plt.figure(figsize=(10, 8))
    sns.heatmap(pivot_table, cmap='viridis', annot=True, fmt=".2f")

    # Achsenbeschriftungen und Titel
    plt.xlabel('Drehzahl (%)')
    plt.ylabel('Quelleintrittstemperatur (°C)')
    plt.title(f'COP-Heatmap (Massenstrom = {massenstrom_ziel} kg/s)')

    # Verzeichnis für die Speicherung des Plots
    plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\COPPlot'
    os.makedirs(plot_base_dir, exist_ok=True)
    plot_file_svg = os.path.join(plot_base_dir, f'COP_Heatmap_massenstrom_{massenstrom_ziel:.2f}.svg')

    # Speichern des Plots
    plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

    print(f'Der Plot wurde erfolgreich unter "{plot_file_svg}" gespeichert.')

    # Anzeigen des Plots
    plt.show()
