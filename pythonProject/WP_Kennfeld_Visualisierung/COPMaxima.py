import os
import pandas as pd
import numpy as np

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Einzigartige Massenströme, Rücklauftemperaturen (TRL) und Quelleintrittstemperaturen (T_eva_in) extrahieren
massenstrom_values = data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'].unique()
ruecklauf_temperaturen = data['T_con_in in C'].unique()
quelleintrittstemperaturen = data['T_eva_in in C'].unique()

# Basisverzeichnis festlegen
base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\csvResults'
os.makedirs(base_dir, exist_ok=True)

# Für jede Kombination aus Massenstrom und Quelleintrittstemperatur eine Tabelle erstellen
for massenstrom in massenstrom_values:
    # Gefilterte Daten für den aktuellen Massenstrom
    filtered_massenstrom_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom]

    for quelle_temp in quelleintrittstemperaturen:
        # Gefilterte Daten für die aktuelle Quelleintrittstemperatur
        filtered_quelle_data = filtered_massenstrom_data[filtered_massenstrom_data['T_eva_in in C'] == quelle_temp]

        # Liste zur Speicherung der Ergebnisse
        results = []

        # Bestes COP für jede Kombination aus TRL und Massenstrom finden
        for ruecklauf_temp in ruecklauf_temperaturen:
            filtered_temp_data = filtered_quelle_data[filtered_quelle_data['T_con_in in C'] == ruecklauf_temp]

            if not filtered_temp_data.empty and filtered_temp_data['COP in - (Coefficient of performance)'].notna().any():
                # Bestes COP für die aktuelle Kombination aus TRL und Massenstrom finden
                max_cop_row = filtered_temp_data.loc[filtered_temp_data['COP in - (Coefficient of performance)'].idxmax()]
                max_cop = max_cop_row['COP in - (Coefficient of performance)']
                max_drehzahl = max_cop_row['n in - (Relative compressor speed)'] * 100  # Drehzahl in Hz

                # Ergebnis speichern
                results.append([ruecklauf_temp, max_drehzahl, massenstrom, max_cop])

        # Überprüfen, ob Ergebnisse vorhanden sind
        if results:
            # Ergebnisse in ein DataFrame umwandeln und in eine CSV-Datei speichern
            output_columns = ['TRL (°C)', 'Drehzahl (Hz)', 'Massenstrom (kg/s)', 'COP']
            results_df = pd.DataFrame(results, columns=output_columns)
            results_df['Drehzahl (Hz)'] = results_df['Drehzahl (Hz)'].astype(int)  # Drehzahl als ganze Zahl darstellen
            results_df['Massenstrom (kg/s)'] = results_df['Massenstrom (kg/s)'].astype(str)
            results_df['TRL (°C)'] = results_df['TRL (°C)'].round(2).astype(str)
            results_df['COP'] = results_df['COP'].round(2).astype(str)

            # CSV-Datei speichern
            output_file = os.path.join(base_dir, f'maxima_per_massenstrom_{massenstrom}_quelle_temp_{quelle_temp:.1f}.csv')
            results_df.to_csv(output_file, index=False, sep=';')
            print(f'Die Datei "{output_file}" wurde erfolgreich erstellt.')
