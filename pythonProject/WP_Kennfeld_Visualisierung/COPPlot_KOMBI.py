import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
import os
import matplotlib.cm as cm
import matplotlib.colors as mcolors

matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Liste der zu betrachtenden Massenströme und Verdampfereintrittstemperaturen
massenstroeme = [0.05, 0.15, 0.25]
verdampfer_temperaturen = [7, 15]  # 7°C und 15°C Verdampfertemperatur

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\kennfeld_visualisierung\COPPlot_zwei_massenströme'
os.makedirs(plot_base_dir, exist_ok=True)

for massenstrom_ziel in massenstroeme:
    fig, axes = plt.subplots(1, 2, subplot_kw={'projection': '3d'}, constrained_layout=True)

    # Zunächst sammeln wir alle COP-Werte für beide Subplots, um die einheitliche Farbskala festzulegen
    cop_all_values = []

    for verdampfer_temp in verdampfer_temperaturen:
        # Filtere Daten für den gewünschten Massenstrom und die spezifische Verdampfereintrittstemperatur
        filtered_temp_data = data[
            (data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel) &
            (data['T_eva_in in C'] == verdampfer_temp)
            ]

        cop_all_values.append(filtered_temp_data['COP in - (Coefficient of performance)'])

    # Berechne das globale Minimum und Maximum der COP-Werte für beide Subplots
    cop_combined = np.concatenate(cop_all_values)
    z_min_global, z_max_global = np.nanmin(cop_combined), np.nanmax(cop_combined)

    # Erstelle eine gemeinsame Farbskala
    norm = mcolors.Normalize(vmin=z_min_global, vmax=z_max_global)
    cmap = cm.get_cmap('viridis')

    for i, verdampfer_temp in enumerate(verdampfer_temperaturen):
        # Filtere Daten für den gewünschten Massenstrom und die spezifische Verdampfereintrittstemperatur
        filtered_temp_data = data[
            (data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel) &
            (data['T_eva_in in C'] == verdampfer_temp)
            ]

        # Hier wird die Drehzahl auf der x-Achse verwendet und die Kondensatortemperatur auf der y-Achse
        x = filtered_temp_data['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
        y = filtered_temp_data['T_con_in in C']  # Kondensatortemperatur
        cop = filtered_temp_data['COP in - (Coefficient of performance)']

        # Überprüfen, ob genügend Variabilität vorhanden ist
        if len(np.unique(x)) < 2 or len(np.unique(y)) < 2:
            print(
                f"Nicht genügend Variabilität in den Daten für Verdampfertemperatur {verdampfer_temp:.2f} °C und Massenstrom {massenstrom_ziel:.2f} kg/s. Interpolation übersprungen.")
            continue

        # NaN-Werte ignorieren
        valid_data_mask = np.isfinite(x) & np.isfinite(y) & np.isfinite(cop)
        x = x[valid_data_mask]
        y = y[valid_data_mask]
        cop = cop[valid_data_mask]

        # Gitter für die Interpolation erstellen
        xi = np.linspace(x.min(), x.max(), 100)
        yi = np.linspace(y.min(), y.max(), 100)
        xi, yi = np.meshgrid(xi, yi)

        # Interpolation für die Oberfläche
        try:
            cop_interpolated = griddata((x, y), cop, (xi, yi), method='linear')
        except Exception as e:
            print(
                f"Fehler bei der Interpolation für Verdampfertemperatur {verdampfer_temp:.2f} °C und Massenstrom {massenstrom_ziel:.2f} kg/s: {e}")
            continue

        # Oberfläche plotten mit der globalen Farbskala
        ax = axes[i]
        surf = ax.plot_surface(xi, yi, cop_interpolated, cmap=cmap, norm=norm)
        ax.set_xlabel('Drehzahl in Hz')
        ax.set_ylabel('Rücklauftemperatur in °C')

        # Z-Achse nur für den linken Plot, der rechte Plot ohne Z-Beschriftung, aber mit Gitter und Ticks
        if i == 0:
            ax.set_zlabel('COP')
        else:
            ax.set_zticklabels([])  # Entferne Z-Beschriftungen vom zweiten (rechten) Plot, aber behalte die Ticks
        ax.set_zlim(z_min_global, z_max_global)

        # Ansicht anpassen: frontaler und X-Achse invertieren
        ax.view_init(elev=20, azim=60)  # Blickwinkel frontaler
        ax.invert_xaxis()  # X-Achse invertieren, damit die Drehzahl von klein nach groß von links nach rechts verläuft

    # Gemeinsame Colorbar für beide Subplots rechts hinzufügen, mit geringem Abstand (dicker und näher)
    cbar = fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ax=axes, location='right', shrink=0.5, aspect=10, pad=0.05)
    cbar.set_label('COP')

    # Plot speichern
    plot_file_svg = os.path.join(plot_base_dir, f'COPPlot_massenstrom_{massenstrom_ziel:.2f}_Vergleich_Verdampfertemp.svg')

    # Speichern als SVG
    plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

    print(f'Der Plot "{plot_file_svg}" wurde erfolgreich erstellt.')

    # Plot anzeigen
    plt.show()
