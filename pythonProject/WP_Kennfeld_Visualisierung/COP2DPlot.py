import pandas as pd
import matplotlib.pyplot as plt
import os

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Rücklauftemperaturen (TRL) und Quelleintrittstemperaturen (T_Quelle, ein) extrahieren
ruecklauf_temperaturen = data['T_con_in in C'].unique()
quelleintrittstemperaturen = data['T_eva_in in C'].unique()

# Ausgewählte Quelleintrittstemperaturen
ausgewaehlte_quellentemps = [7, 15]
colors = ['red', 'grey']  # Farben für die Quelleintrittstemperaturen

# Massenstrom (ṁ_sek) auf 0.15 kg/s beschränken
massenstrom_ziel = 0.15
filtered_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\COP2DPlot'
os.makedirs(plot_base_dir, exist_ok=True)

# Plots für jede Rücklauftemperatur erstellen
for ruecklauf_temp in ruecklauf_temperaturen:
    plt.figure(figsize=(10, 6))  # Größeres Diagramm erstellen

    # Daten für die aktuelle Rücklauftemperatur filtern
    filtered_temp_data = filtered_data[filtered_data['T_con_in in C'] == ruecklauf_temp]

    # Daten für jede ausgewählte Quelleintrittstemperatur plotten
    for i, quellentemp in enumerate(ausgewaehlte_quellentemps):
        quellen_data = filtered_temp_data[filtered_temp_data['T_eva_in in C'] == quellentemp]
        if not quellen_data.empty:
            plt.plot(quellen_data['n in - (Relative compressor speed)'] * 100,  # Drehzahl mit 100 multiplizieren
                     quellen_data['COP in - (Coefficient of performance)'],
                     label=f'$T_{{Quelle, ein}}$ = {str(quellentemp).replace(".", ",")} °C', linestyle='--', marker='o',
                     color=colors[i])  # Farbe anwenden

    plt.xlabel('Drehzahl (Hz)', fontsize=14)
    plt.ylabel('COP', fontsize=14)
   # plt.title(
      #  rf'COP bei $T_{{\mathrm{{RL}}}}$ = {str(ruecklauf_temp).replace(".", ",")} °C, $\dot{{m}}_{{\mathrm{{Sek}}}}$ = {str(massenstrom_ziel).replace(".", ",")} kg/s',
      #  fontsize=16, pad=20)
    plt.legend()
    plt.grid(False)  # Gitter entfernen

    # Plot speichern
    plot_file_png = os.path.join(plot_base_dir, f'COP2DPlot_TRL_{str(ruecklauf_temp)}°C.png')
    plot_file_svg = os.path.join(plot_base_dir, f'COP2DPlot_TRL_{str(ruecklauf_temp)}°C.svg')

    # Speichern als PNG
    plt.savefig(plot_file_png)

    # Speichern als SVG
    plt.savefig(plot_file_svg)

    print(f'Der Plot "{plot_file_png}" und "{plot_file_svg}" wurden erfolgreich erstellt.')

    # Plot anzeigen
    plt.show()
