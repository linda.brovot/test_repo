import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D  # Wichtig für 3D-Plot
import numpy as np
import os

# Einstellungen für die Schriftarten im SVG
matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in_C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in_C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Filterung des Massenstroms auf 0.15 kg/s
massenstrom_ziel = 0.15
filtered_data = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

# Überprüfung auf ausreichende Datenpunkte
if filtered_data.empty:
    print(f"Keine Daten für einen Massenstrom von {massenstrom_ziel} kg/s gefunden.")
else:
    # Extraktion der benötigten Variablen
    x = filtered_data['n in - (Relative compressor speed)'] * 100  # Drehzahl in %
    y = filtered_data['T_eva_in_C']  # Quelleintrittstemperatur
    z = filtered_data['T_con_in_C']  # Rücklauftemperatur
    cop = filtered_data['COP in - (Coefficient of performance)']  # COP-Wert

    # Erstellung des 3D-Streudiagramms
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111, projection='3d')

    sc = ax.scatter(x, y, z, c=cop, cmap='viridis', marker='o', s=50, edgecolor='k', alpha=0.8)

    # Achsenbeschriftungen
    ax.set_xlabel('Drehzahl (%)', fontsize=12, labelpad=10)
    ax.set_ylabel('Quelleintrittstemperatur (°C)', fontsize=12, labelpad=10)
    ax.set_zlabel('Rücklauftemperatur (°C)', fontsize=12, labelpad=10)

    # Hinzufügen der Farbskala für COP
    cbar = plt.colorbar(sc, ax=ax, shrink=0.6, aspect=10, pad=0.1)
    cbar.set_label('COP', fontsize=12)

    # Titel des Plots
   # plt.title(f'COP in Abhängigkeit von Drehzahl, Quelleintritts- und Rücklauftemperatur\n(Massenstrom = {massenstrom_ziel} kg/s)', fontsize=14, pad=20)

    # Anpassung des Layouts
    plt.tight_layout()

    # Verzeichnis für die Speicherung des Plots
    plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\kennfeld_visualisierung\COPPlot'
    os.makedirs(plot_base_dir, exist_ok=True)
    plot_file_svg = os.path.join(plot_base_dir, f'COP_3DScatter_massenstrom_{massenstrom_ziel:.2f}.svg')

    # Speichern des Plots
    plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

    print(f'Der Plot wurde erfolgreich unter "{plot_file_svg}" gespeichert.')

    # Anzeigen des Plots
    plt.show()
