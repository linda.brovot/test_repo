import os
import pandas as pd
import matplotlib.pyplot as plt
import re
import matplotlib.ticker as ticker

# Pfade zu den gespeicherten Ergebnissen
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\CalculationExergy_Exergie_pro_Zeitschritt"
TVLs = [338.15, 343.15]

# Funktion zum Einlesen und Aufbereiten der CSV-Dateien
def read_time_dependent_csv_files(TVL):
    data_frames = []
    # Ordner für das aktuelle TVL
    results_dir = os.path.join(results_base_dir, f"results_{TVL}")

    # Überprüfen, ob der Ordner existiert
    if not os.path.exists(results_dir):
        print(f"Verzeichnis nicht gefunden: {results_dir}")
        return pd.DataFrame()  # Leeren DataFrame zurückgeben

    # Durchsuchen des Ordners nach CSV-Dateien
    for subdir, _, files in os.walk(results_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                file_path = os.path.join(subdir, file)
                try:
                    df = pd.read_csv(file_path, sep=',', decimal='.')

                    # Überprüfen, ob die erwarteten Spalten existieren
                    if 'Leistung_WP' not in df.columns or 'kummulierte Exergie kWh' not in df.columns or 'Sekunde' not in df.columns or 'Dateiname' not in df.columns:
                        print(f"Erwartete Spalten fehlen in Datei: {file_path}")
                        continue

                    # Massenstrom für jede Zeile aus dem Dateinamen extrahieren und hinzufügen
                    df['Massenstrom'] = df['Dateiname'].apply(lambda x: float(re.search(r'_(\d+\.\d+)\.csv', x).group(1)))

                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")

                except Exception as e:
                    print(f"Fehler beim Lesen der Datei {file_path}: {e}")

    if not data_frames:
        print("Keine passenden Daten gefunden.")
        return pd.DataFrame()  # Leeren DataFrame zurückgeben

    return pd.concat(data_frames, ignore_index=True)


# Funktion zur Bestimmung des Stoppzeitpunkts und Plotten der kumulierten Exergie für alle Massenströme
def plot_cumulative_exergy(TVL):
    # Einlesen aller Daten für das aktuelle TVL
    df = read_time_dependent_csv_files(TVL)

    if df.empty:
        print(f"Keine Daten für TVL = {TVL}")
        return

    # Berechnen der kumulierten elektrischen Leistung basierend auf der Spalte "Leistung_WP"
    df["P_total_cum"] = df.groupby("Massenstrom")["Leistung_Gesamt"].cumsum() / (1000 * 3600)  # Umrechnung in kWh

    # Finde die minimale kumulierte elektrische Energie, bei der der Prozess für jeden Massenstrom stoppt
    stopp_values = df.groupby("Massenstrom")["P_total_cum"].max()

    # Bestimme den Massenstrom, der den minimalen Stoppwert erreicht
    stop_mass_flow = stopp_values.idxmin()  # Massenstrom mit der minimalen Stopp-Energie
    min_stopp_energy = stopp_values.min()   # Minimaler Stoppwert

    # Erstellen des Plots
    plt.figure(figsize=(10, 6))

    # Iterieren über die Gruppen nach Massenstrom
    for mass_flow, group_df in df.groupby("Massenstrom"):
        # Filtern der Daten bis zur Stoppbedingung für jeden Massenstrom
        group_df_stopped = group_df[group_df["P_total_cum"] <= min_stopp_energy]

        # Dezimalpunkt durch Komma ersetzen und Massenstrom formatieren
        mass_flow_label = str(mass_flow).replace('.', ',')

        label = f'{mass_flow_label} kg/s'
        if mass_flow == stop_mass_flow:
            label += " (Stopp)"  # Markiere den Massenstrom, der zum Stopp führt

        # Plotten der kumulierten Exergie bis zur Stoppbedingung für jeden Massenstrom
        plt.plot(group_df_stopped["Sekunde"] / 3600, group_df_stopped["kummulierte Exergie kWh"], label=label)

    # Formatter für Achsen, um Dezimalzahlen mit Komma darzustellen
    comma_formatter = ticker.FuncFormatter(lambda x, _: f'{x:.2f}'.replace('.', ','))

    # Achsen-Formatter anwenden
    plt.gca().xaxis.set_major_formatter(comma_formatter)
    plt.gca().yaxis.set_major_formatter(comma_formatter)

    # Ticks auf der x-Achse alle Stunde setzen
    max_hours = df["Sekunde"].max() / 3600  # Maximale Zeit in Stunden
    plt.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))  # Setze Ticks auf jede Stunde

    # Plot-Details hinzufügen
    plt.title(
        f'Kumulierte Exergie über Zeit bis zur Stoppbedingung für TVL = {TVL-273.15} °C\n(Stopp bei {min_stopp_energy:.2f} kWh elektrischer Energie)')
    plt.xlabel("Beladedauer in Stunden")
    plt.ylabel("Kumulierte Exergie in kWh")
    plt.legend()
    plt.grid(True)

    # Speichern und Anzeigen des Plots
    plt.savefig(os.path.join(results_base_dir, f"Exergie_Plot_TVL_{TVL}_mit_Stopp.png"))
    plt.show()


# Hauptlogik zur Erstellung der Plots für alle TVLs und Massenströme
for TVL in TVLs:
    plot_cumulative_exergy(TVL)
    print(f'Plot für TVL = {TVL} K wurde erstellt und gespeichert.')
