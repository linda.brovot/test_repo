import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'system_results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("system_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius}°C")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames


def format_func(x, pos):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        return locale.format_string("%.1f", x, grouping=True)


def plot_data(data_frames, output_dir):
    mass_flows = [0.05, 0.15, 0.25]
    temperatures = sorted(set(df['Temperature'].iloc[0] for df in data_frames))

    print(f"Gefundene Temperaturen: {temperatures}")

    for mass_flow in mass_flows:
        eta_wp_list = []
        eta_sp_list = []
        eta_system_list = []
        hours_list = []
        temp_labels = []

        for temp in temperatures:
            for df in data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        eta_wp = filtered_df['Nutzungsgrad_WP'].values[0]
                        eta_sp = filtered_df['Eta_SP'].values[0]
                        eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                        hours = filtered_df['Hours'].values[0]

                        if not (pd.isna(eta_wp) or pd.isna(eta_sp) or pd.isna(hours)):
                            eta_wp_list.append(eta_wp)
                            eta_sp_list.append(eta_sp)
                            eta_system_list.append(eta_WPSS)
                            hours_list.append(hours)
                            temp_labels.append(temp)
                        print(
                            f"Temperatur: {temp}°C, Massenstrom: {mass_flow}, Eta_WP: {eta_wp}, Eta_SP: {eta_sp}, Eta_WPSS: {eta_WPSS}, Stunden: {hours}")
                    else:
                        print(f"Keine Daten gefunden für Temperatur: {temp}°C und Massenstrom: {mass_flow}")

        if len(temp_labels) > 0:
            fig, ax1 = plt.subplots(figsize=(13, 8))
            fig.suptitle(f'Nutzungsgrad bei konstantem Massenstrom von {mass_flow:.2f} kg/s'.replace('.', ','),
                         fontsize=16)

            bar_width = 0.5
            ax1.bar(temp_labels, hours_list, width=bar_width, color='lightgrey', label='Beladedauer in h')

            ax2 = ax1.twinx()
            ax2.plot(temp_labels, eta_wp_list, marker='o', linestyle='--', color='black', label='Nutzungsgrad WP')
            ax2.plot(temp_labels, eta_sp_list, marker='o', linestyle='--', color='grey', label='Nutzungsgrad Speicher')
            ax2.plot(temp_labels, eta_system_list, marker='o', linestyle='--', color='red',
                     label='Nutzungsgrad WP-Speicher-System')

            ax2.set_ylabel('Nutzungsgrad', fontsize=16)
            ax1.set_ylim(0, max(hours_list) * 1.1)
            ax2.set_ylim(0, 0.7)

            # Eine gemeinsame Legende erstellen
            lines1, labels1 = ax1.get_legend_handles_labels()
            lines2, labels2 = ax2.get_legend_handles_labels()
            lines = lines1 + lines2
            labels = labels1 + labels2
            fig.legend(lines, [label.replace('.', ',') for label in labels], loc='upper center',
                       bbox_to_anchor=(0.5, 0.92), ncol=1, fontsize=14)

            ax1.set_xlabel('Temperatur in °C', fontsize=16)
            ax1.set_ylabel('Beladedauer in h', fontsize=16)
            ax1.grid(False)

            ax1.tick_params(axis='both', which='major', labelsize=16)
            ax2.tick_params(axis='both', which='major', labelsize=16)

            ax1.xaxis.set_major_locator(ticker.FixedLocator([70, 80, 90]))
            ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos)))
            ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos)))
            ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos)))

            # Setzen der schwarzen Ränder
            for spine in ax1.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)
            for spine in ax2.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            plot_filename = os.path.join(output_dir, f"Nutzungsgrad_Massenstrom_{str(mass_flow).replace('.', ',')}.png")
            fig.savefig(plot_filename, format='png')
            fig.savefig(plot_filename.replace('.png', '.svg'), format='svg')
            print(f"Plot gespeichert: {plot_filename}")

            plt.show()
            plt.close(fig)


def format_func_2_decimal(x, pos):
    return locale.format_string("%.2f", x, grouping=True)


def format_func_3_decimal(x, pos):
    return locale.format_string("%.3f", x, grouping=True)


def combined_plot(data_frames, output_dir):
    mass_flows = [0.05, 0.15, 0.25]
    temperatures = sorted(set(df['Temperature'].iloc[0] for df in data_frames))

    eta_system_combined = {mf: [] for mf in mass_flows}
    hours_combined = {mf: [] for mf in mass_flows}

    for mass_flow in mass_flows:
        for df in data_frames:
            if df['Name'].str.contains(f"_{mass_flow}").any():
                filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                eta_system = filtered_df['Eta_WPSS'].values[0]
                eta_system_combined[mass_flow].append(eta_system)
                hours_combined[mass_flow].append(filtered_df['Hours'].values[0])

    if any(hours_combined.values()):
        fig, ax1 = plt.subplots(figsize=(13, 8))
        fig.suptitle('Exergetischer Nutzungsgrad bei unterschiedlichen Massenströmen', fontsize=18, y=0.95)

        bar_width = 0.2
        offsets = [-bar_width, 0, bar_width]
        colors = ['gray', 'silver', 'darkgrey']

        # Plot for the bar charts
        bar_containers = []
        for mass_flow, offset, color in zip(mass_flows, offsets, colors):
            bars = ax1.bar([x + offset for x in temperatures], hours_combined[mass_flow], width=bar_width, color=color, label=f'Beladedauer bei {mass_flow} kg/s')
            bar_containers.append(bars)

        ax2 = ax1.twinx()

        # Plot for the line charts
        line_containers = []
        for mass_flow, color in zip(mass_flows, ['black', 'red', 'darkred']):
            line, = ax2.plot(temperatures, eta_system_combined[mass_flow], marker='^', linestyle='-.', color=color, label=f'Nutzungsgrad bei {mass_flow} kg/s')
            line_containers.append(line)

        ax1.set_xlabel('Temperatur in °C', fontsize=16)
        ax1.set_ylabel('Beladedauer in h', fontsize=16)
        ax2.set_ylabel('Nutzungsgrad', fontsize=16)
        ax1.set_ylim(0, 5)
        ax2.set_ylim(0.15, 0.17)
        ax1.grid(False)

        ax1.tick_params(axis='both', which='major', labelsize=16)
        ax2.tick_params(axis='both', which='major', labelsize=16)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(format_func_3_decimal))

        # Set black borders
        for spine in ax1.spines.values():
            spine.set_edgecolor('black')
            spine.set_linewidth(1.5)
        for spine in ax2.spines.values():
            spine.set_edgecolor('black')
            spine.set_linewidth(1.5)

        # Creating the legend including both bars and lines
        bars_labels = [container[0] for container in bar_containers]
        lines_labels = line_containers
        labels = [f'Beladedauer bei {mass_flow} kg/s' for mass_flow in mass_flows] + [f'Nutzungsgrad bei {mass_flow} kg/s' for mass_flow in mass_flows]

        fig.legend(bars_labels + lines_labels, labels, loc='upper right', bbox_to_anchor=(0.75, 0.88), ncol=1, fontsize=14)

        # Save combined plot
        combined_plot_filename = os.path.join(output_dir, "combined_plot.png")
        fig.savefig(combined_plot_filename, bbox_inches='tight')
        fig.savefig(combined_plot_filename.replace('.png', '.svg'), format='svg')
        plt.show()
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename}")



if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\PLOTS\NutzungsgradPlot"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_csv_files(results_base_dir)
    plot_data(data_frames, output_dir)
    combined_plot(data_frames, output_dir)
