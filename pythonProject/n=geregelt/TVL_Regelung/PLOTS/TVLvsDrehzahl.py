import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()

def read_time_dependent_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    if 'Vorlauftemperatur' in df.columns:
                        df['Vorlauftemperatur'] = df['Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_time_dependent_results(data_frames, output_dir):
    columns_to_plot = [
        ('Verdichterdrehzahl', 'Verdichterdrehzahl'),
        ('Vorlauftemperatur', 'Vorlauftemperatur')
    ]
    identifiers = [0.05, 0.15, 0.25]
    linestyles = ['-', '--']  # Durchgezogen für die erste Temperatur, gestrichelt für die zweite Temperatur

    for identifier in identifiers:
        identifier_str = f"Massenstrom {str(identifier).replace('.', ',')}"
        identifier_dir = os.path.join(output_dir, identifier_str)
        if not os.path.exists(identifier_dir):
            os.makedirs(identifier_dir)

        fig, axs = plt.subplots(2, 1, figsize=(16, 14))  # 2 Subplots untereinander
        fig.suptitle(f'Darstellung von Verdichterdrehzahl und Vorlauftemperatur für Massenstrom {str(identifier).replace(".", ",")} kg/s', y=0.95, fontsize=22)

        for i, (column, title) in enumerate(columns_to_plot):
            ax = axs[i]

            for df in data_frames:
                print(f"Überprüfe Daten für Temperatur {df['Temperature'].iloc[0]:.2f} und Identifier {identifier}")
                filtered_df = df[df['Dateiname'].str.contains(f"_{identifier}.csv")]
                if not filtered_df.empty:
                    temp_label = locale.format_string('%.0f', df['Temperature'].iloc[0])
                    print(f"Plotte Daten für Temperatur {df['Temperature'].iloc[0]:.2f} °C, Identifier {identifier}, Spalte {column}")
                    smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                    ax.plot(filtered_df['Sekunde'] / 3600, smoothed_data, label=f'$T_{{\\mathrm{{VL}}}} = {temp_label} \\ °C$')

            ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
            ax.set_ylabel(title + ' (°C)' if column == 'Vorlauftemperatur' else title, fontsize=20)
            ax.grid(False)
            ax.legend(fontsize=18)
            ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
            ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.tick_params(axis='both', which='major', labelsize=18)

            # Setzen der schwarzen Ränder
            for spine in ax.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

        plot_filename = os.path.join(identifier_dir, f"Verdichterdrehzahl_Vorlauftemperatur_{identifier}".replace(' ', '_').replace('.', ','))
        fig.savefig(f"{plot_filename}.png", format='png')
        fig.savefig(f"{plot_filename}.svg", format='svg')
        print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

        plt.show()

    # Zusätzlicher Plot für alle Massenströme und Temperaturen in einem Diagramm mit zwei Subplots
    fig, axs = plt.subplots(2, 1, figsize=(16, 14))  # 2 Subplots untereinander

    fig.suptitle('Darstellung von Verdichterdrehzahl und Vorlauftemperatur für alle Massenströme und Temperaturen', y=0.95, fontsize=22)

    # Subplot für Vorlauftemperatur
    ax_temp = axs[0]
    for j, identifier in enumerate(identifiers):
        for i, df in enumerate(data_frames):
            linestyle = linestyles[i % len(linestyles)]  # Wähle den Linienstil aus
            temp_label = locale.format_string('%.0f', df['Temperature'].iloc[0])
            filtered_df = df[df['Dateiname'].str.contains(f"_{identifier}.csv")]
            if not filtered_df.empty:
                print(f"Plotte Vorlauftemperatur für Massenstrom {identifier}, Temperatur {df['Temperature'].iloc[0]:.2f}")
                smoothed_data = moving_average(filtered_df['Vorlauftemperatur'], window_size=5)
                ax_temp.plot(filtered_df['Sekunde'] / 3600, smoothed_data, linestyle=linestyle, label=f'Massenstrom {identifier} kg/s, $T_{{\\mathrm{{VL}}}} = {temp_label} \\ °C$')

    ax_temp.set_xlabel('Beladedauer in Stunden', fontsize=20)
    ax_temp.set_ylabel('Vorlauftemperatur (°C)', fontsize=20)
    ax_temp.grid(False)
    ax_temp.legend(fontsize=14)
    ax_temp.locator_params(axis='x', nbins=7)
    ax_temp.locator_params(axis='y', nbins=7)
    ax_temp.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax_temp.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax_temp.tick_params(axis='both', which='major', labelsize=16)

    # Subplot für Verdichterdrehzahl
    ax_rpm = axs[1]
    for j, identifier in enumerate(identifiers):
        for i, df in enumerate(data_frames):
            linestyle = linestyles[i % len(linestyles)]  # Wähle den Linienstil aus
            temp_label = locale.format_string('%.0f', df['Temperature'].iloc[0])
            filtered_df = df[df['Dateiname'].str.contains(f"_{identifier}.csv")]
            if not filtered_df.empty:
                print(f"Plotte Verdichterdrehzahl für Massenstrom {identifier}, Temperatur {df['Temperature'].iloc[0]:.2f}")
                smoothed_data = moving_average(filtered_df['Verdichterdrehzahl'], window_size=5)
                ax_rpm.plot(filtered_df['Sekunde'] / 3600, smoothed_data, linestyle=linestyle, label=f'Massenstrom {identifier} kg/s, $T_{{\\mathrm{{VL}}}} = {temp_label} \\ °C$')

    ax_rpm.set_xlabel('Beladedauer in Stunden', fontsize=20)
    ax_rpm.set_ylabel('Verdichterdrehzahl (U/min)', fontsize=20)
    ax_rpm.grid(False)
    ax_rpm.legend(fontsize=14)
    ax_rpm.locator_params(axis='x', nbins=7)
    ax_rpm.locator_params(axis='y', nbins=7)
    ax_rpm.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax_rpm.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax_rpm.tick_params(axis='both', which='major', labelsize=16)

    plot_filename_combined = os.path.join(output_dir, "Verdichterdrehzahl_Vorlauftemperatur_alle_Massenströme".replace(' ', '_').replace('.', ','))
    fig.savefig(f"{plot_filename_combined}.png", format='png')
    fig.savefig(f"{plot_filename_combined}.svg", format='svg')
    print(f"Zusätzlicher Gesamtplot gespeichert: {plot_filename_combined}.png und {plot_filename_combined}.svg")

    plt.show()

if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\PLOTS\timedependency"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_time_dependent_results(data_frames, output_dir)
