import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import numpy as np
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'system_results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Vorlauftemperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("system_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Vorlauftemperatur {temp_celsius}°C")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames


def get_sums_for_mass_flow(data_frames, mass_flow):
    temperatures = sorted(set(df['Temperature'].iloc[0] for df in data_frames))  # Alle verfügbaren Vorlauftemperaturen
    pel_sums = []
    pel_Pumpe_sums = []
    exergie_sums = []

    for temp in temperatures:
        for df in data_frames:
            if df['Temperature'].iloc[0] == temp:
                filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                if not filtered_df.empty:
                    pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
                    pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600
                    exergie_sum = filtered_df['Sum_Differences_Exergies'].values[0] / 1000 / 3600
                    pel_sums.append(pel_sum)
                    pel_Pumpe_sums.append(pel_Pumpe_sum)
                    exergie_sums.append(exergie_sum)
                else:
                    pel_sums.append(np.nan)
                    pel_Pumpe_sums.append(np.nan)
                    exergie_sums.append(np.nan)

    return temperatures, pel_sums, pel_Pumpe_sums, exergie_sums


def plot_sum_comparison(data_frames, output_dir, script_name, mass_flows, labels, colors, hatches):
    fig = plt.figure(figsize=(20, 8))

    # Erstelle die Subplots: Links gestapelte Plots und Rechts für Exergie (jetzt Balken)
    ax_zoom = plt.subplot2grid((1, 2), (0, 0))  # Linker Subplot (Zoom-In)
    ax_exergie = plt.subplot2grid((1, 2), (0, 1))  # Rechter Subplot (Exergie)

    bar_width = 0.25  # Breite der Balken
    results_data = []  # Hier speichern wir die prozentualen Anteile zur CSV-Erstellung

    for idx, mass_flow in enumerate(mass_flows):
        temperatures, pel_sums, pel_Pumpe_sums, exergie_sums = get_sums_for_mass_flow(data_frames, mass_flow)
        if temperatures:
            # Begrenze die Daten für die ersten drei Temperaturen (falls weniger vorhanden sind, passen wir uns an)
            pel_sums_zoom = pel_sums[:min(3, len(pel_sums))]
            pel_Pumpe_sums_zoom = pel_Pumpe_sums[:min(3, len(pel_Pumpe_sums))]
            exergie_sums_zoom = exergie_sums[:min(3, len(exergie_sums))]
            combined_sums_zoom = [wp + pumpe for wp, pumpe in zip(pel_sums_zoom, pel_Pumpe_sums_zoom)]

            # Gestapelte Balken: WP-Leistung (unten) + Pumpe-Leistung (oben, schraffiert)
            ax_zoom.bar(np.arange(len(pel_sums_zoom)) + idx * bar_width, pel_sums_zoom, bar_width, edgecolor='black',
                        color=colors[idx], label=f'WP {mass_flow} {labels["unit"]}')
            ax_zoom.bar(np.arange(len(pel_sums_zoom)) + idx * bar_width, pel_Pumpe_sums_zoom, bar_width,
                        bottom=pel_sums_zoom, hatch=hatches[idx], edgecolor='black', color=colors[idx])

            # Rechter Subplot: Exergie im Speicher (jetzt als Balkenplot)
            ax_exergie.bar(np.arange(len(exergie_sums_zoom)) + idx * bar_width, exergie_sums_zoom, bar_width,
                           edgecolor='black', color=colors[idx], label=f'Exergie {mass_flow} kg/s')

            # Berechne den prozentualen Anteil der Pumpe an der Gesamtleistung
            pump_percentages = [(pumpe / (wp + pumpe) * 100) if (wp + pumpe) > 0 else 0
                                for wp, pumpe in zip(pel_sums_zoom, pel_Pumpe_sums_zoom)]

            # Bereite die CSV-Daten nur für die vorhandenen Temperaturen vor
            result_entry = {"Massenstrom": f"{mass_flow} {labels['unit']}"}
            for i in range(len(pump_percentages)):
                result_entry[f"Temperatur_{i + 1}"] = f"{pump_percentages[i]:.2f}%"
            results_data.append(result_entry)

            # Anzeige der Ergebnisse im Plot
            for i, percentage in enumerate(pump_percentages):
                ax_zoom.text(i + idx * bar_width, combined_sums_zoom[i] + 0.05, f"{percentage:.2f}%", fontsize=12,
                             ha='center')

    # Einstellungen für den Zoom-In Subplot (links)
    ax_zoom.set_ylabel('el. Energie in kWh', fontsize=16)
    ax_zoom.set_xlabel('Vorlauftemperatur in °C', fontsize=16)
    ax_zoom.grid(False)
    ax_zoom.set_xticks(np.arange(len(temperatures)) + bar_width)
    ax_zoom.set_xticklabels(temperatures)
    ax_zoom.tick_params(axis='both', which='major', labelsize=16)
    ax_zoom.set_ylim([5, 7])

    # Einstellungen für den Exergie Subplot (rechts)
    ax_exergie.set_xlabel('Vorlauftemperatur in °C', fontsize=16)
    ax_exergie.set_ylabel('Exergie im Speicher in kWh', fontsize=16)
    ax_exergie.grid(False)
    ax_exergie.set_xticks(np.arange(len(temperatures)) + bar_width)  # Gleiche Ticks wie im linken Plot
    ax_exergie.set_xticklabels(temperatures)  # Setze die gleichen Labels wie im linken Plot
    ax_exergie.tick_params(axis='both', which='major', labelsize=16)
    ax_exergie.set_ylim([0.9, 1.1])

    # Gleiche Abstände zwischen den Subplots und den Rändern
    plt.subplots_adjust(left=0.07, right=0.93, top=0.88, bottom=0.12, wspace=0.3)

    # Legenden für WP-Leistung und Exergie-Leistung
    custom_legend_patches = [Patch(facecolor=colors[idx], edgecolor='black',
                                   label=f'{mass_flow} kg/s') for idx, mass_flow in enumerate(mass_flows)]
    ax_zoom.legend(handles=custom_legend_patches, loc='upper left', fontsize=12, title='el. Energie WP + Pumpe')
    ax_exergie.legend(handles=custom_legend_patches, loc='upper left', fontsize=12, title='Exergie')

    # Pfad zum Speichern der Plots
    plot_output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\PLOTS"  # Platzhalter für den Pfad, den du später einfügst

    try:
        if not os.path.exists(plot_output_dir):
            os.makedirs(plot_output_dir)
            print(f"Verzeichnis erstellt: {plot_output_dir}")
        else:
            print(f"Verzeichnis existiert bereits: {plot_output_dir}")

        # Erzeuge Dateinamen für PNG und SVG
        plot_filename_base = os.path.join(plot_output_dir, f"{script_name}_plot")
        print(f"Speichern als PNG: {plot_filename_base}.png")
        print(f"Speichern als SVG: {plot_filename_base}.svg")

        # Speichern des Plots als PNG- und SVG-Datei
        fig.savefig(f"{plot_filename_base}.png", format='png')
        fig.savefig(f"{plot_filename_base}.svg", format='svg')

    except Exception as e:
        print(f"Fehler beim Speichern des Plots: {e}")

    plt.show()

    # Speichere die prozentualen Anteile in einer CSV-Datei
    df = pd.DataFrame(results_data)
    output_file = os.path.join(output_dir, f"{script_name}_prozentuale_Anteile.csv")
    df.to_csv(output_file, index=False, sep=';', decimal=',')
    print(f"Prozentuale Anteile in CSV gespeichert: {output_file}")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]

    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = os.path.join(results_base_dir, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    hatches = ['/', '\\', 'x']
    colors = ['darkred', 'red', 'grey']

    data_frames = read_csv_files(results_base_dir)

    plot_sum_comparison(data_frames, output_dir, script_name,
                        mass_flows=[0.05, 0.15, 0.25],
                        labels={"title": "Vorlauftemperatur und Massenströme", "unit": "kg/s"},
                        colors=colors,
                        hatches=hatches)
