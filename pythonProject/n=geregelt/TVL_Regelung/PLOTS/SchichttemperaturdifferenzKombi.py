import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Pfade zu den CSV-Ordnern für Massenstrom und Vorlauftemperatur
mass_flow_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CSV_Results"
temperature_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CSV_Results"

mass_flows = [0.05, 0.15, 0.25]
temperatures = [338.15, 343.15]  # Vorlauftemperaturen in Kelvin

# Dictionaries, um die Temperaturdifferenzen zu speichern
temp_differences_30hz = []
temp_differences_90hz = []
temp_differences_65deg = []
temp_differences_70deg = []

# Schleife über alle Massenströme
for mass_flow in mass_flows:
    # -----------------------------
    # 1. Lese die Massenstrom-Dateien (Drehzahlen)
    # -----------------------------
    folder_path = os.path.join(mass_flow_base_dir, f"csv_{mass_flow}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    temp_diff_30hz = None
    temp_diff_90hz = None

    for csv_file_path in csv_files:
        name = os.path.basename(csv_file_path).replace('.csv', '')
        frequency_str = name.split('_')[-1]

        try:
            frequency = float(frequency_str)
        except ValueError:
            continue

        df = pd.read_csv(csv_file_path, sep=";", decimal=",")
        if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
            continue

        df_end = df.iloc[-1]
        temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
        temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15
        temp_difference = temp_layer_10 - temp_layer_1

        if frequency == 0.3:
            temp_diff_30hz = temp_difference
        elif frequency == 0.9:
            temp_diff_90hz = temp_difference

    if temp_diff_30hz is not None:
        temp_differences_30hz.append(temp_diff_30hz)
    else:
        temp_differences_30hz.append(0)

    if temp_diff_90hz is not None:
        temp_differences_90hz.append(temp_diff_90hz)
    else:
        temp_differences_90hz.append(0)

    # -----------------------------
    # 2. Lese die Vorlauftemperatur-Dateien
    # -----------------------------
    for temperature in temperatures:
        # Pfad für den entsprechenden Temperatur-Ordner
        folder_path = os.path.join(temperature_base_dir, f"csv_{temperature}")

        # Alle Dateien in diesem Ordner durchsuchen und passende Datei für den Massenstrom finden
        csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if
                     file.endswith('.csv') and f'_{mass_flow}' in file]

        # Sollte nur eine Datei pro Massenstrom und Temperatur geben, daher nehmen wir die erste
        if csv_files:
            csv_file_path = csv_files[0]
            df = pd.read_csv(csv_file_path, sep=";", decimal=",")
            if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
                continue

            df_end = df.iloc[-1]
            temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
            temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15
            temp_difference = temp_layer_10 - temp_layer_1

            if temperature == 338.15:
                temp_differences_65deg.append(temp_difference)
            elif temperature == 343.15:
                temp_differences_70deg.append(temp_difference)
        else:
            print(
                f"Datei für Vorlauftemperatur {temperature - 273.15:.2f}°C und Massenstrom {mass_flow} nicht gefunden.")
            if temperature == 338.15:
                temp_differences_65deg.append(0)
            elif temperature == 343.15:
                temp_differences_70deg.append(0)

# -----------------------------
# 3. Erstellung des Balkendiagramms
# -----------------------------
fig, ax = plt.subplots(figsize=(10, 6))
bar_width = 0.2  # Breite der Balken
index = np.arange(len(mass_flows))

# Zeichne die Balken für 30 Hz, 90 Hz, 65°C und 70°C
bars_30hz = ax.bar(index - bar_width, temp_differences_30hz, bar_width, label='30 Hz', color='red')
bars_90hz = ax.bar(index, temp_differences_90hz, bar_width, label='90 Hz', color='green')
bars_65deg = ax.bar(index + bar_width, temp_differences_65deg, bar_width, label='65°C', color='gray', hatch='/')
bars_70deg = ax.bar(index + 2 * bar_width, temp_differences_70deg, bar_width, label='70°C', color='gray', hatch='\\')

# Zahlen über den Balken anzeigen
for bars in [bars_30hz, bars_90hz, bars_65deg, bars_70deg]:
    for bar in bars:
        yval = bar.get_height()
        ax.text(bar.get_x() + bar.get_width() / 2, yval, f'{yval:.2f}', va='bottom', ha='center')

# Beschriftungen und Titel
ax.set_xlabel('Massenstrom (kg/s)')
ax.set_ylabel('Temperaturdifferenz ΔT (°C)')
ax.set_title('Temperaturdifferenz ΔT für verschiedene Drehzahlen und Vorlauftemperaturen')

# X-Achsen-Beschriftungen
ax.set_xticks(index)
ax.set_xticklabels([f'{mf:.2f}'.replace(".", ",") for mf in mass_flows])

# Legende
ax.legend()

# Plot speichern
output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\PLOTS"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

plt.savefig(os.path.join(output_dir, 'temperature_diff_massenstrom_drehzahl_temperatur_plot.png'))
plt.savefig(os.path.join(output_dir, 'temperature_diff_massenstrom_drehzahl_temperatur_plot.svg'))

# Plot anzeigen
plt.tight_layout()
plt.show()
