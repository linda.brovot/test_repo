import matplotlib.pyplot as plt
import pandas as pd
import os
import re
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, temp, mass_flow):
    file_pattern = f"results_WP_Speicher_System_{temp}_{mass_flow}.csv"
    for root, dirs, files in os.walk(results_base_dir):  # Verwende os.walk, um rekursiv durch Unterordner zu gehen
        for file in files:
            if re.match(file_pattern, file):
                system_csv_file = os.path.join(root, file)
                if os.path.exists(system_csv_file):
                    system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
                    print(f"CSV-Datei gefunden und gelesen: {system_csv_file}")
                    return system_results_df
    print(f"CSV-Datei für {temp} und {mass_flow} nicht gefunden")
    return None



def get_sums(system_results_df):
    try:
        pel_sum = system_results_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
        pel_Pumpe_sum = system_results_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600
        sum_diff_exergies = system_results_df['Sum_Differences_Exergies'].values[0] / 1000 / 3600
        eq_integral_wp = system_results_df['Gesamtintegral_E_Q_WP'].values[0] / 1000 / 3600
        eq_integral_sp = system_results_df['Gesamtintegral_E_Q_SP'].values[0] / 1000 / 3600
        return pel_sum, pel_Pumpe_sum, sum_diff_exergies, eq_integral_wp, eq_integral_sp
    except (KeyError, IndexError, TypeError) as e:
        print(f"Fehler beim Abrufen der Summen: {e}")
        return None, None, None, None, None

def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")


def plot_for_temperature(results_base_dir, output_dir, script_name, temp, temp_label, y_limits=None, x_limits=None):
    # Massenströme
    mass_flows = [0.05, 0.15, 0.25]

    pel_sums = {}
    pel_Pumpe_sums = {}
    exergie_sums = {}
    eq_integrals_wp = {}
    eq_integrals_sp = {}

    # Farbzuordnung für die Massenströme
    color_map = {
        '$\\dot{\\mathrm{m}}_{\\mathrm{Sek}} = 0,05 \\ \\mathrm{kg/s}$': 'tab:red',
        '$\\dot{\\mathrm{m}}_{\\mathrm{Sek}} = 0,15 \\ \\mathrm{kg/s}$': 'tab:purple',
        '$\\dot{\\mathrm{m}}_{\\mathrm{Sek}} = 0,25 \\ \\mathrm{kg/s}$': 'tab:brown'
    }

    # Lesen der CSV-Dateien und Erstellen der Summen für jeden Massenstrom
    for mass_flow in mass_flows:
        system_results_df = read_csv_files(results_base_dir, temp, mass_flow)
        if system_results_df is not None:
            pel_sum, pel_Pumpe_sum, sum_diff_exergies, eq_integral_wp, eq_integral_sp = get_sums(system_results_df)

            massenstrom_label = f'$\\dot{{\\mathrm{{m}}}}_{{\\mathrm{{Sek}}}} = {mass_flow:.2f} \\ \\mathrm{{kg/s}}$'.replace(
                '.', ',')
            pel_sums[massenstrom_label] = pel_sum
            pel_Pumpe_sums[massenstrom_label] = pel_Pumpe_sum
            exergie_sums[massenstrom_label] = sum_diff_exergies
            eq_integrals_wp[massenstrom_label] = eq_integral_wp
            eq_integrals_sp[massenstrom_label] = eq_integral_sp

    def format_func(x, pos, decimal_places):
        if x.is_integer():
            return locale.format_string("%.0f", x, grouping=True)
        else:
            format_str = f"%.{decimal_places}f"
            return locale.format_string(format_str, x, grouping=True)

    # Erstelle verschiedene Diagramme
    for metric, title, ylabel, data in [
        ('pel_sums', 'zugeführte elektrische Energie der WP', 'el. Energie in kWh', pel_sums),
        ('pel_Pumpe_sums', 'zugeführte elektrische Energie der Umwälzpumpe', 'el. Energie in kWh', pel_Pumpe_sums),
        ('exergie_sums', 'Exergie der inneren Energie im Speicher', 'Exergie in kWh', exergie_sums),
        ('eq_integrals_wp', 'Exergie des Wärmestroms WP', 'Exergie in kWh', eq_integrals_wp),
        ('eq_integrals_sp', 'Exergie des Wärmestroms SP', 'Exergie in kWh', eq_integrals_sp)
    ]:
        fig, ax = plt.subplots(figsize=(13, 8))
        fig.suptitle(f'{title} bei {temp_label}°C', y=0.95, fontsize=16)

        for identifier, value in data.items():
            color = color_map.get(identifier)
            ax.plot([identifier], [value], marker='o', linestyle='-', label=identifier, color=color)

        ax.set_xlabel('Massenstrom in kg/s', fontsize=16)
        ax.set_ylabel(ylabel, fontsize=16)
        ax.grid(False)
        ax.legend(loc='best')  # Sicherstellen, dass die Legende erstellt wird

        # Achsenbegrenzungen manuell setzen
        if x_limits:
            ax.set_xlim(x_limits)
        if y_limits:
            ax.set_ylim(y_limits)

        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 1)))
        ax.tick_params(axis='both', which='major', labelsize=16)

        # Plot speichern
        plot_and_save(fig, os.path.join(output_dir, f"{script_name}_{metric}_{temp_label}"))

        plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Manuelle Achseneinstellungen
    y_limits = None  # Setze die Y-Achsenbegrenzungen, falls gewünscht
    x_limits = None  # Setze die X-Achsenbegrenzungen, falls gewünscht

    # Erstelle separate Diagramme für jede Vorlauftemperatur
    plot_for_temperature(results_base_dir_2, output_dir, script_name, 338.15, '65', y_limits, x_limits)
    plot_for_temperature(results_base_dir_2, output_dir, script_name, 343.15, '70', y_limits, x_limits)
