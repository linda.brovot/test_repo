import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_time_dependent_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    if 'Vorlauftemperatur' in df.columns:
                        df['Vorlauftemperatur'] = df['Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_time_dependent_results(data_frames, output_dir):
    columns_to_plot = [

        ('Exergie_Differenz_gesamt', 'Exergie im Speicher pro Zeitschritt'),
        ('Wirkungsgrad_gesamt', 'Wirkungsgrad WPSS')
    ]
    identifiers = [0.15, 0.2, 0.25]

    for column, title in columns_to_plot:
        for identifier in identifiers:
            identifier_str = f"Massenstrom {str(identifier).replace('.', ',')}"
            identifier_dir = os.path.join(output_dir, identifier_str)
            if not os.path.exists(identifier_dir):
                os.makedirs(identifier_dir)

            fig, ax = plt.subplots(figsize=(16, 10))
            fig.suptitle(f'{title} für Massenstrom {str(identifier).replace(".", ",")} kg/s', y=0.95, fontsize=22)

            for df in data_frames:
                print(f"Überprüfe Daten für Temperatur {df['Temperature'].iloc[0]:.2f} und Identifier {identifier}")
                filtered_df = df[df['Dateiname'].str.contains(f"_{identifier}.csv")]
                if not filtered_df.empty:
                    temp_label = locale.format_string('%.0f', df['Temperature'].iloc[0])
                    print(f"Plotte Daten für Temperatur {df['Temperature'].iloc[0]:.2f} °C, Identifier {identifier}, Spalte {column}")
                    ax.plot(filtered_df['Sekunde'] / 3600, filtered_df[column], label=f'$T_{{\\mathrm{{VL}}}} = {temp_label} \\ °C$')

            ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
            ax.set_ylabel(title + ' (°C)' if column == 'Vorlauftemperatur' else title, fontsize=20)
            ax.grid(False)
            ax.legend(fontsize=18)
            ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
            ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.tick_params(axis='both', which='major', labelsize=18)

            # Setzen der schwarzen Ränder
            for spine in ax.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            plot_filename = os.path.join(identifier_dir, f"{title}".replace(' ', '_').replace('.', ','))
            fig.savefig(f"{plot_filename}.png", format='png')
            fig.savefig(f"{plot_filename}.svg", format='svg')
            print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

            plt.show()

if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\PLOTS\timedependency"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_time_dependent_results(data_frames, output_dir)
