import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'system_results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("system_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames


def format_func(x, pos):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        return locale.format_string("%.1f", x, grouping=True)


def plot_data(data_frames, output_dir):
    mass_flows = [0.15, 0.2, 0.25]
    temperatures = sorted(set(df['Temperature'].iloc[0] for df in data_frames))

    for mass_flow in mass_flows:
        eta_wp_list = []
        eta_sp_list = []
        eta_system_list = []
        hours_list = []
        temp_labels = []

        for temp in temperatures:
            for df in data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        eta_wp = filtered_df['Nutzungsgrad_WP'].values[0]
                        eta_sp = filtered_df['Eta_SP'].values[0]
                        eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                        hours = filtered_df['Hours'].values[0]

                        if not (pd.isna(eta_wp) or pd.isna(eta_sp) or pd.isna(hours)):
                            eta_wp_list.append(eta_wp)
                            eta_sp_list.append(eta_sp)
                            eta_system_list.append(eta_WPSS)
                            hours_list.append(hours)
                            temp_labels.append(temp)

        if len(temp_labels) > 0:
            fig, ax1 = plt.subplots(figsize=(13, 8))
            fig.suptitle(f'Nutzungsgrad bei konstantem Massenstrom von {mass_flow:.2f} kg/s'.replace('.', ','),
                         fontsize=16)

            bar_width = 0.5
            ax1.bar(temp_labels, hours_list, width=bar_width, color='lightgrey', label='Beladedauer in h')

            ax2 = ax1.twinx()
            ax2.plot(temp_labels, eta_wp_list, marker='o', linestyle='--', color='black', label='Nutzungsgrad WP')
            ax2.plot(temp_labels, eta_sp_list, marker='o', linestyle='--', color='grey', label='Nutzungsgrad Speicher')
            ax2.plot(temp_labels, eta_system_list, marker='o', linestyle='--', color='red',
                     label='Nutzungsgrad WP-Speicher-System')

            ax2.set_ylabel('Nutzungsgrad', fontsize=16)
            ax1.set_ylim(0, max(hours_list) * 1.1)
            ax2.set_ylim(0, 0.7)

            lines1, labels1 = ax1.get_legend_handles_labels()
            lines2, labels2 = ax2.get_legend_handles_labels()
            lines = lines1 + lines2
            labels = labels1 + labels2
            ax1.legend(lines, [label.replace('.', ',') for label in labels], loc='upper center',
                       bbox_to_anchor=(0.5, 1), ncol=2, fontsize=14)

            ax1.set_xlabel('Temperatur in °C', fontsize=16)
            ax1.set_ylabel('Beladedauer in h', fontsize=16)
            ax1.grid(False)

            ax1.tick_params(axis='both', which='major', labelsize=16)
            ax2.tick_params(axis='both', which='major', labelsize=16)

            ax1.xaxis.set_major_locator(ticker.FixedLocator([70, 80, 90]))
            ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos)))
            ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos)))
            ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos)))

            # Setzen der schwarzen Ränder
            for spine in ax1.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)
            for spine in ax2.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            plot_filename = os.path.join(output_dir, f"Nutzungsgrad_Massenstrom_{str(mass_flow).replace('.', ',')}.png")
            fig.savefig(plot_filename, format='png')
            fig.savefig(plot_filename.replace('.png', '.svg'), format='svg')
            print(f"Plot gespeichert: {plot_filename}")

            plt.show()
            plt.close(fig)


def format_func_2_decimal(x, pos):
    return locale.format_string("%.2f", x, grouping=True)

# Im combined_plot anpassen
def combined_plot(data_frames, output_dir):
    mass_flows = [0.15, 0.2, 0.25]
    temperatures = sorted(set(df['Temperature'].iloc[0] for df in data_frames))

    eta_system_combined = {mf: [] for mf in mass_flows}
    hours_combined = {mf: [] for mf in mass_flows}

    for mass_flow in mass_flows:
        for df in data_frames:
            if df['Name'].str.contains(f"_{mass_flow}").any():
                filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                eta_system = filtered_df['Nutzungsgrad_WP'].values[0] * filtered_df['Eta_SP'].values[0]
                eta_system_combined[mass_flow].append(eta_system)
                hours_combined[mass_flow].append(filtered_df['Hours'].values[0])

    if any(hours_combined.values()):
        fig, ax1 = plt.subplots(figsize=(13, 8))
        fig.suptitle('Exergetischer Nutzungsgrad bei unterschiedlichen Massenströmen', fontsize=18)

        bar_width = 0.2
        offsets = [-bar_width, 0, bar_width]
        colors = ['gray', 'silver', 'darkgrey']

        for mass_flow, offset, color in zip(mass_flows, offsets, colors):
            ax1.bar([x + offset for x in temperatures], hours_combined[mass_flow], width=bar_width, color=color, label=f'Beladedauer in h {mass_flow}')

        ax2 = ax1.twinx()
        for mass_flow, color in zip(mass_flows, ['black', 'red', 'darkred']):
            ax2.plot(temperatures, eta_system_combined[mass_flow], marker='^', linestyle='-.', color=color, label=f'WP Speicher System {mass_flow}')

        ax1.set_xlabel('Temperatur in °C', fontsize=16)
        ax1.set_ylabel('Beladedauer in h', fontsize=16)
        ax2.set_ylabel('Nutzungsgrad', fontsize=16)
        ax1.set_ylim(0, 5)
        ax2.set_ylim(0.16, 0.21)
        ax1.grid(False)

        ax1.tick_params(axis='both', which='major', labelsize=16)
        ax2.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(format_func_2_decimal))  # Verwenden Sie die neue Formatierungsfunktion hier

        # Setzen der schwarzen Ränder
        for spine in ax1.spines.values():
            spine.set_edgecolor('black')
            spine.set_linewidth(1.5)
        for spine in ax2.spines.values():
            spine.set_edgecolor('black')
            spine.set_linewidth(1.5)

        # Separate Legenden für Balken und Linien erstellen
        bar_labels = [f'Beladedauer in h {mf}' for mf in mass_flows]
        line_labels = [f'WP Speicher System {mf}' for mf in mass_flows]

        legend_bars = ax1.legend(loc='upper left', bbox_to_anchor=(0, 1), fontsize=13)
        legend_lines = ax2.legend(loc='upper left', bbox_to_anchor=(0, 0.9), fontsize=13)

        # Legende der Balken hinzufügen
        ax1.add_artist(legend_bars)

        # Speichern des kombinierten Plots
        combined_plot_filename = os.path.join(output_dir, "combined_plot.png")
        fig.savefig(combined_plot_filename, bbox_inches='tight')
        fig.savefig(combined_plot_filename.replace('.png', '.svg'), format='svg')
        plt.show()
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename}")

# Verwenden Sie diesen Schnipsel im aktuellen Code


if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\PLOTS\NutzungsgradPlot"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_csv_files(results_base_dir)
    plot_data(data_frames, output_dir)
    combined_plot(data_frames, output_dir)
