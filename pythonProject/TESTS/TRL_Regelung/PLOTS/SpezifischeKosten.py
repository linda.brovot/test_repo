import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'system_results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("system_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    if data_frames:
        return pd.concat(data_frames, ignore_index=True)
    else:
        return None

def get_kennzahlen(system_results_df, mass_flow):
    temperatures = sorted(system_results_df['Temperature'].unique())
    kennzahlen = []

    for temp in temperatures:
        filtered_df = system_results_df[(system_results_df['Temperature'] == temp) & (system_results_df['Name'].str.contains(f"_{mass_flow}"))]
        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0]
            sum_diff_exergies = filtered_df['Sum_Differences_Exergies'].values[0]

            # Berechnung der Kennzahl
            kennzahl = (pel_sum / 1000 / 3600) * 0.298 / (sum_diff_exergies / 1000 / 3600)
            kennzahlen.append(kennzahl)

    return temperatures, kennzahlen

def combined_plot(results_base_dir, output_dir, script_name):
    temperatures_combined = []
    kennzahlen_combined = {}

    mass_flows = [0.15, 0.2, 0.25]

    system_results_df = read_csv_files(results_base_dir)
    if system_results_df is not None:
        for mass_flow in mass_flows:
            temperatures, kennzahlen = get_kennzahlen(system_results_df, mass_flow)
            if temperatures:
                massenstrom_label = f'$\\dot{{\\mathrm{{m}}}}_{{\\mathrm{{Sek}}}} = {mass_flow:.2f} \\ \\mathrm{{kg/s}}$'.replace('.', ',')
                kennzahlen_combined[massenstrom_label] = kennzahlen
                temperatures_combined = temperatures  # Temperaturen sind für alle gleich

    if temperatures_combined:
        fig, ax = plt.subplots(figsize=(13, 8))

        # Titel näher an den Plot setzen
        fig.suptitle('Kosten an el. Energie in € pro kWh Exergie im Speicher', y=0.95, fontsize=18)

        colors = ['lightcoral', 'red', 'darkred']
        for (identifier, kennzahlen), color in zip(kennzahlen_combined.items(), colors):
            ax.plot(temperatures_combined, kennzahlen, marker='o', linestyle='--', color=color, label=identifier)

        ax.set_xlabel('Temperatur in °C', fontsize=16)
        ax.set_ylabel('€/kWh', fontsize=16)
        ax.set_ylim(1.8, 2.3)
        ax.grid(False)

        handles, labels = ax.get_legend_handles_labels()

        # Legende manuell in zwei Zeilen aufteilen
        font_properties = {'size': 16, 'family': 'sans-serif'}
        ax.legend(handles, labels, loc='upper center', bbox_to_anchor=(0.5, 1), ncol=1, prop=font_properties, handletextpad=1, columnspacing=1, frameon=False)

        ax.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Speichern des kombinierten Plots als PNG-Datei
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        plt.savefig(combined_plot_filename_png)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png}")

        # Speichern des kombinierten Plots als SVG-Datei
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_svg, format='svg')
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_svg}")

        # Diagramm anzeigen
        plt.show()

        # Schließen der Figur
        plt.close(fig)

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    # Basisverzeichnisse, in denen die Ergebnisdateien liegen
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\PLOTS\timedependency"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
