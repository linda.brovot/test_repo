import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# Pfade zu den CSV-Ordnern für die verschiedenen Pumpendrehzahlen
base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\CSV_Results"

TVLs = [343.15, 353.15, 363.15]

for TVL in TVLs:
    folder_path = os.path.join(base_dir, f"csv_{TVL}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    for csv_file_path in csv_files:
        name = os.path.splitext(os.path.basename(csv_file_path))[0]
        name2 = name  # Diese Variable wird hier nicht weiter verwendet
        # Lese die Daten aus der CSV-Datei und stelle sicher, dass Dezimalstellen durch Kommata getrennt sind
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Filtere den DataFrame, um nur Daten ab Sekunde 100 zu behalten
        df = df[df.index >= 100]

        # Konvertierung des Index in Stunden
        df["Hour"] = df.index / 3600  # Annahme: Die Indexwerte sind in Sekunden, daher teilen wir durch 3600, um Stunden zu erhalten

        # Plotten der Temperaturverläufe für jede Schicht
        plt.figure(figsize=(10, 6))

        for i in range(1, 11):
            # Konvertierung der Temperatur von Kelvin in Celsius
            df[f'bufferStorage.layer[{i}].T'] = df[f'bufferStorage.layer[{i}].T'] - 273.15
            plt.plot(df["Hour"], df[f'bufferStorage.layer[{i}].T'], label=f'Schicht {i}')

        plt.xlabel('Zeit (Stunden)')
        plt.ylabel('Temperatur (°C)')
        plt.title(f'Temperaturverlauf der Schichten für {name} bei TVL {TVL-273.15} °C')

        # Anpassung der Achsenbeschriftung für Komma statt Punkt
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: f'{x:.1f}'.replace('.', ',')))


        handles, labels = plt.gca().get_legend_handles_labels()
        plt.legend(handles[::-1], labels[::-1])  # Umkehren der Reihenfolge der Legende
        plt.grid(False)  # Entferne das Gitter

        # Speicherort für den Plot
        script_name = os.path.splitext(os.path.basename(__file__))[0]  # Name des Skripts ohne Erweiterung
        results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\PLOTS"
        output_dir = os.path.join(results_base_dir, script_name)

        # Erstelle das Verzeichnis, wenn es nicht existiert
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        plot_path = os.path.join(output_dir, f"temperature_layers_{name}_TVL_{TVL}.png")
        plt.savefig(plot_path)
        plt.close()

        print(f"Der Temperaturverlauf der Schichten für '{name}' bei massenstrom {TVL} wurde unter '{plot_path}' gespeichert.")


    print(f"Alle Plots wurden erstellt und in '{output_dir}' gespeichert.")
