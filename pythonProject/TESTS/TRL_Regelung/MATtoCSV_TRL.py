import pandas as pd
import os
from buildingspy.io.outputfile import Reader
import re

def find_time_variable(sim):
    # Try common time variable names
    possible_time_vars = ["Time", "time", "cpuTime", "CPUtime"]
    for var in possible_time_vars:
        try:
            return sim.values(var)[0], var
        except KeyError:
            continue
    raise KeyError("No suitable time variable found in the .mat file")

def convert_mat_to_csv(mat_file, output_file):
    sim = Reader(mat_file, "dymola")

    try:
        time, time_var_name = find_time_variable(sim)
    except KeyError as e:
        print(f"Error: {e}")
        return

    data = {"Time": time}

    # Define the variables to be extracted
    list_var = ["bufferStorage.layer[{}].U".format(i) for i in range(1, 11)]
    list_var += ["bufferStorage.heatingCoil1.Therm1[{}].Q_flow".format(i) for i in range(9, 0, -1)]
    list_var += ["bufferStorage.layer[{}].U".format(i) for i in range(1, 11)]
    list_var += ["bufferStorage.layer[{}].T".format(i) for i in range(1, 11)]
    list_var += ["bufferStorage.heatingCoil1.pipe[{}].vol_a.T".format(i) for i in range(9, 0, -1)]
    list_var += ["bufferStorage.heatingCoil1.pipe[{}].vol_b.T".format(i) for i in range(9, 0, -1)]
    list_var += ["Q_Flow_HC1", "bufferStorage.heatingCoil1.Therm1[1].Q_flow", "heatPump.sigBus.TConInMea","heatPump.sigBus.TConOutMea",
                 "heatPump.sigBus.TConOutMea","heatPump.innerCycle.PerformanceDataHPHeating.Pel",
                 "heatPump.innerCycle.PerformanceDataHPHeating.Table_COP.y",
                 "heatPump.innerCycle.PerformanceDataHPHeating.Table_QCon.y", "U_ges", "floMacDyn.P", "heatPump.con.vol.T", "sigBus1.nSet"]

    # Extract data for each variable
    for n_var in list_var:
        try:
            var_data = sim.values(n_var)[1]
            data[n_var] = var_data
        except KeyError:
            print(f"Variable {n_var} not found in {mat_file}")

    # Extract the "V" variable (only one value)
    try:
        v_value = sim.values("bufferStorage.layer[1].V")[1][0]
        data["bufferStorage.layer[1].V"] = [v_value] * len(time)
    except KeyError:
        print(f"Variable bufferStorage.layer[1].V not found in {mat_file}")

    # Find the index where heatPump.innerCycle.PerformanceDataHPHeating.Table_QCon.y is zero
    Pel = data.get("heatPump.innerCycle.PerformanceDataHPHeating.Pel", [])
    zero_index = next((i for i, value in enumerate(Pel) if value == 0), len(time))

    # Trim the data
    time = time[:zero_index + 1]
    data = {key: value[:zero_index + 1] for key, value in data.items()}

    # Convert to DataFrame
    df = pd.DataFrame(data)

    # Save CSV file with semicolon as delimiter
    df.to_csv(output_file, sep=";", index=False, decimal=",")
    print(f"CSV file created: {output_file}")


if __name__ == "__main__":
    path_input = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Rücklauftemperaturregelung\MAT_Results"  # Adjust path
    output_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Rücklauftemperaturregelung\CSV_Results"  # Adjust base output path

    if not os.path.exists(output_base_dir):
        os.makedirs(output_base_dir)

    for file_name in os.listdir(path_input):
        if file_name.endswith(".mat"):
            mat_file = os.path.join(path_input, file_name)

            # Extract mass flow rate from file name
            match = re.search(r'_(\d+\.\d+)_', file_name)
            if match:
                mass_flow = match.group(1)
                output_dir = os.path.join(output_base_dir, f"csv_{mass_flow}")

                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                output_file = os.path.join(output_dir, file_name.replace(".mat", ".csv"))
                convert_mat_to_csv(mat_file, output_file)
            else:
                print(f"Mass flow rate not found in file name: {file_name}")
        else:
            print(f"Unsupported file: {file_name}")
