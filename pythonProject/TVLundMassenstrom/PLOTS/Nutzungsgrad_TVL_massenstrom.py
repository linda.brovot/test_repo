import matplotlib.pyplot as plt
import pandas as pd
import os
import re
import numpy as np

# Funktion zum Lesen der CSV-Dateien für Massenströme
def read_mass_flow_csv_files(results_base_dir, mass_flow):
    folder_path = os.path.join(results_base_dir, f"results_{mass_flow}")
    csv_file = os.path.join(folder_path, f"system_results_{mass_flow}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        return data_frame
    else:
        print(f"CSV-Datei für mass_flow {mass_flow} nicht gefunden: {csv_file}")
        return None

# Funktion zum Lesen der CSV-Dateien für Vorlauftemperaturen
def read_temp_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'system_results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("system_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    data_frames.append(df)
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

# Funktion zum Plotten der Daten
def plot_data(data, output_dir, mass_flows, color_mapping):
    fig, ax1 = plt.subplots(figsize=(10, 6))
    ax2 = ax1.twinx()

    bar_width = 0.2
    x_positions = np.linspace(0.5, len(mass_flows) - 0.5, len(mass_flows))

    eta_values_by_color = {label: [] for label in color_mapping}
    x_positions_by_color = {label: [] for label in color_mapping}

    for i, mf in enumerate(mass_flows):
        x_pos = x_positions[i]
        combined_data = data[mf]['temp'] + data[mf]['freq']
        combined_data_sorted = sorted(combined_data, key=lambda x: x['hours'])[::-1]

        for entry in combined_data_sorted:
            color = color_mapping[entry['label']]
            ax1.bar(x_pos, entry['hours'], color=color, width=bar_width, alpha=0.9,
                    label=f"{entry['label']}" if i == 0 else "")
            eta_values_by_color[entry['label']].append(entry['eta'])
            x_positions_by_color[entry['label']].append(x_pos)

    for label in eta_values_by_color:
        ax2.plot(x_positions_by_color[label], eta_values_by_color[label], linestyle='--', color=color_mapping[label], marker='o',
                 markersize=10)

    ax1.set_xlabel('Massenstrom in kg/s', fontsize=14)
    ax1.set_ylabel('Beladedauer in h', fontsize=14)
    ax2.set_ylabel('Nutzungsgrad', fontsize=14)
    ax1.set_xticks(x_positions)
    ax1.set_xticklabels([f"{mf:.2f}".replace('.', ',') + " kg/s" for mf in mass_flows])
    ax1.grid(False)
    ax2.grid(False)

    ax2.yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f"{x:.2f}".replace('.', ',')))
    plt.title('Nutzungsgrad für das WPSS', fontsize=16)

    handles, labels = ax1.get_legend_handles_labels()
    unique_labels = dict(zip(labels, handles))
    ax1.legend(unique_labels.values(), unique_labels.keys(), fontsize=10)

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
    fig.tight_layout(pad=2.0)

    # Speichern des Plots als PNG und SVG
    plot_filename_png = os.path.join(output_dir, "combined_plot.png")
    plot_filename_svg = os.path.join(output_dir, "combined_plot.svg")

    fig.savefig(plot_filename_png, bbox_inches='tight')
    fig.savefig(plot_filename_svg, bbox_inches='tight')

    plt.show()
    plt.close(fig)

# Hauptfunktion zum Kombinieren der Daten
def combined_plot(results_base_dir, results_base_dir_1, output_dir):
    mass_flows = [0.05, 0.15, 0.25]  # Massenströme
    frequencies = [30, 40, 50]  # Frequenzen in Hz
    temperatures = [65, 70]  # Vorlauftemperaturen

    # Festes Farb-Mapping für Frequenzen und Temperaturen
    color_mapping = {
        '65°C': 'tab:blue',
        '70°C': 'tab:orange',
        '30 Hz': 'tab:green',
        '40 Hz': 'tab:red',
        '50 Hz': 'tab:purple'
    }

    data = {}
    for mf in mass_flows:
        data[mf] = {'temp': [], 'freq': []}
        # Daten für Vorlauftemperaturen sammeln
        data_frames = read_temp_csv_files(results_base_dir)
        for temp in temperatures:
            for df in data_frames:
                if 'Temperature' in df.columns and df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mf}")]
                    if not filtered_df.empty:
                        eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                        hours = filtered_df['Hours'].values[0]

                        if not (pd.isna(eta_WPSS) or pd.isna(hours)):
                            data[mf]['temp'].append({'eta': eta_WPSS, 'hours': hours, 'label': f"{int(temp)}°C"})

        # Daten für Frequenzen sammeln
        data_frame = read_mass_flow_csv_files(results_base_dir_1, mf)
        if data_frame is not None:
            for freq in frequencies:
                freq_val = freq / 100  # Umrechnung zu Original-Frequenzwerten
                filtered_df = data_frame[data_frame['Name'].str.contains(f"_{freq_val}")]
                if not filtered_df.empty:
                    eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                    hours = filtered_df['Hours'].values[0]

                    if not (pd.isna(eta_WPSS) or pd.isna(hours)):
                        data[mf]['freq'].append({'eta': eta_WPSS, 'hours': hours, 'label': f"{int(freq)} Hz"})

    # Daten plotten
    plot_data(data, output_dir, mass_flows, color_mapping)

if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Vorlauftemperaturregelung\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Beladestrategie\PLOTS\NutzungsgradKombiniert"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, results_base_dir_1, output_dir)
