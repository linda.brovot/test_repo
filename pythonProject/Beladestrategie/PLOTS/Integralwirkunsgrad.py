import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os
import numpy as np
import re

# Setzen der Schriftart auf 'Palatino Linotype' vor dem Erstellen von Plots
matplotlib.rcParams['svg.fonttype'] = 'none'


def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()


def read_time_dependent_csv_files(base_dir, type):
    data_frames = []
    pattern = re.compile(
        r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren des Wertes (z.B. Temperatur oder Frequenz)

    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    value = float(match.group(1))
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')

                    if type == 'temperature':
                        temp_celsius = value - 273.15  # Umrechnung von Kelvin in Celsius
                        df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    elif type == 'frequency':
                        df['Frequency'] = value  # Hinzufügen der extrahierten Frequenz

                    data_frames.append(df)
    return data_frames


def calculate_total_efficiency(df, column, time_column):
    df = df.dropna(subset=[column, time_column])  # Entfernt Zeilen mit NaN in den relevanten Spalten

    if df.empty:
        print("DataFrame is empty after removing NaN.")
        return np.nan, 0

    # Berechne die Beladedauer (in Sekunden)
    total_time_seconds = df[time_column].max()

    if total_time_seconds == 0 or df[column].isna().all():
        print("Keine gültigen Daten für die Berechnung.")
        return np.nan, 0

    # Berechne das Integral mit der Trapezregel
    integral_value = np.trapz(df[column], df[time_column])

    # Berechne den durchschnittlichen Wirkungsgrad
    average_efficiency = integral_value / total_time_seconds

    return average_efficiency, total_time_seconds / 3600  # Beladedauer in Stunden zurückgeben


def calculate_average_efficiency_for_all_variants(temperature_data_frames, frequency_data_frames, exergy_data_frame):
    columns_to_plot = [('Wirkungsgrad_gesamt', 'Wirkungsgrad vom Wärmepumpen-Speicher-System')]
    mass_flows = [0.05, 0.15, 0.25]

    results = {}

    for column, title in columns_to_plot:
        for mass_flow in mass_flows:
            integrals = []
            labels = []

            frequencies_to_plot = [0.3, 0.4, 0.5]
            temperatures_to_plot = [65.0, 70]

            # Verarbeitung der Temperatur-basierten Daten
            for df in temperature_data_frames:
                temp_celsius = df['Temperature'].iloc[0]
                if temp_celsius in temperatures_to_plot:
                    temp_label = f'{temp_celsius:.1f} °C'
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}.csv")]
                    if not filtered_df.empty:
                        avg_efficiency, beladedauer = calculate_total_efficiency(filtered_df, column, 'Sekunde')
                        integrals.append(avg_efficiency)
                        labels.append(f'Temperature {temp_label}')
                        results[f'Temperature {temp_label} bei {mass_flow} kg/s'] = (avg_efficiency, beladedauer)

            # Verarbeitung der Frequenz-basierten Daten
            for df in frequency_data_frames:
                for frequency in frequencies_to_plot:
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
                    if not filtered_df.empty:
                        freq_label = f'{frequency * 100:.0f} Hz'
                        avg_efficiency, beladedauer = calculate_total_efficiency(filtered_df, column, 'Sekunde')
                        integrals.append(avg_efficiency)
                        labels.append(f'Frequency {freq_label}')
                        results[f'Frequency {freq_label} bei {mass_flow} kg/s'] = (avg_efficiency, beladedauer)

            # Verarbeitung der Exergie-Daten für den speziellen Fall 0.15 kg/s
            if mass_flow == 0.15 and not exergy_data_frame.empty:
                avg_efficiency, beladedauer = calculate_total_efficiency(exergy_data_frame, column, 'Sekunde')
                integrals.append(avg_efficiency)
                labels.append('Exergy COP_max')
                results[f'Exergy COP_max bei {mass_flow} kg/s'] = (avg_efficiency, beladedauer)

    return results


if __name__ == "__main__":
    # Setze die Verzeichnisse für die CSV-Daten
    temperature_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy_Exergie_pro_Zeitschritt"
    frequency_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    exergy_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Beladestrategie\CalculationExergy"

    # Lese die Daten
    temperature_data_frames = read_time_dependent_csv_files(temperature_base_dir, 'temperature')
    frequency_data_frames = read_time_dependent_csv_files(frequency_base_dir, 'frequency')
    exergy_data_frame = pd.DataFrame()  # Für diesen Fall keine Exergie-Daten verwenden

    # Berechne die Durchschnittseffizienz für alle Varianten
    average_efficiency_results = calculate_average_efficiency_for_all_variants(temperature_data_frames,
                                                                               frequency_data_frames, exergy_data_frame)

    # Ausgabe der Ergebnisse
    for variant, (efficiency, beladedauer) in average_efficiency_results.items():
        print(f"{variant}: Durchschnittlicher Wirkungsgrad = {efficiency:.4f}, Beladedauer = {beladedauer:.2f} Stunden")
