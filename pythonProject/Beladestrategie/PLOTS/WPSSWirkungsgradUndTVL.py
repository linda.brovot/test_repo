import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Setzen der Schriftart auf 'Palatino Linotype' vor dem Erstellen von Plots
# plt.rcParams['font.family'] = 'serif'
# plt.rcParams["font.size"] = 11
matplotlib.rcParams['svg.fonttype'] = 'none'

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()

def read_time_dependent_csv_files(base_dir, type):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren des Wertes (z.B. Temperatur oder Frequenz)

    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    value = float(match.group(1))
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')

                    if type == 'temperature':
                        temp_celsius = value - 273.15  # Umrechnung von Kelvin in Celsius
                        df['Temperature'] = temp_celsius  # Hinzufügen der umgerechneten Temperatur
                    elif type == 'frequency':
                        df['Frequency'] = value  # Hinzufügen der extrahierten Frequenz

                    data_frames.append(df)
    return data_frames

def read_exergy_results(base_dir):
    # Aktualisieren Sie den Pfad zur Datei
    file_path = os.path.join(base_dir, "results", "time_dependent_results.csv")
    print(f"Versuche Datei zu lesen: {file_path}")

    # Überprüfen, ob die Datei existiert
    if not os.path.isfile(file_path):
        print(f"Die Datei {file_path} wurde nicht gefunden.")
        return pd.DataFrame()  # Rückgabe eines leeren DataFrames

    df = pd.read_csv(file_path, sep=',', decimal='.')
    return df

def format_func(x, pos, decimal_places=2):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_time_dependent_results(temperature_data_frames, frequency_data_frames, exergy_data_frame, output_dir):
    columns_to_plot = [('Wirkungsgrad_gesamt', 'Wirkungsgrad vom Wärmepumpen-Speicher-System')]
    mass_flows = [0.05, 0.15, 0.25]  # Identifier für verschiedene Massenströme

    for column, title in columns_to_plot:
        for mass_flow in mass_flows:
            mass_flow_str = f"Massenstrom {str(mass_flow).replace('.', ',')}"
            mass_flow_dir = os.path.join(output_dir, mass_flow_str)
            if not os.path.exists(mass_flow_dir):
                os.makedirs(mass_flow_dir)

            fig, (ax1, ax2) = plt.subplots(2, 1,figsize=(11,7), sharex=True, gridspec_kw={'height_ratios': [1.5, 1]})
            fig.suptitle(f'{title} und Vorlauftemperatur bei {str(mass_flow).replace(".", ",")} kg/s', y=0.95, fontsize=11)

            # Frequenzen und Temperaturen, die geplottet werden sollen
            frequencies_to_plot = [0.3, 0.4, 0.5]
            temperatures_to_plot = [65.0, 70]

            # Erster Plot (Wirkungsgrad)
            for df in temperature_data_frames:
                temp_celsius = df['Temperature'].iloc[0]
                if temp_celsius in temperatures_to_plot:
                    temp_label = locale.format_string('%.0f', temp_celsius)
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}.csv")]
                    if not filtered_df.empty:
                        smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                        ax1.plot(filtered_df['Sekunde'] / 3600, filtered_df[column],
                                 label=rf'$T_{{\mathrm{{VL}}}} = {temp_label} \, \mathrm{{°C}}$')

            for df in frequency_data_frames:
                for frequency in frequencies_to_plot:
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
                    if not filtered_df.empty:
                        freq_label = locale.format_string('%.0f', frequency * 100)  # Frequenz in Hz ohne Dezimalstellen
                        smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                        ax1.plot(filtered_df['Sekunde'] / 3600, filtered_df[column], label=f'{freq_label} Hz')

            # Hinzufügen der Exergie-Daten für die Beladestrategie nur für 0.15 kg/s
            if mass_flow == 0.15 and not exergy_data_frame.empty:
                smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                ax1.plot(exergy_data_frame['Sekunde'] / 3600, exergy_data_frame[column], label='$COP_{\mathrm{max}}$', linestyle='--')

            ax1.set_ylabel('Wirkungsgrad', fontsize=11)
            ax1.grid(False)
            ax1.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
            ax1.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
            ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax1.tick_params(axis='both', which='major', labelsize=11)

            # Setzen der schwarzen Ränder
            for spine in ax1.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            # Zweiter Plot (TVL)
            for df in temperature_data_frames:
                temp_celsius = df['Temperature'].iloc[0]
                if temp_celsius in temperatures_to_plot:
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}.csv")]
                    if not filtered_df.empty:
                        if 'Vorlauftemperatur' in filtered_df.columns:
                            filtered_df = filtered_df.copy()  # Erstellt eine Kopie des DataFrames, um die Warnung zu vermeiden
                            filtered_df.loc[:, 'Vorlauftemperatur_Celsius'] = filtered_df['Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                            smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                            ax2.plot(filtered_df['Sekunde'] / 3600, filtered_df['Vorlauftemperatur_Celsius'])



            for df in frequency_data_frames:
                for frequency in frequencies_to_plot:
                    filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
                    if not filtered_df.empty:
                        if 'Vorlauftemperatur' in filtered_df.columns:
                            filtered_df = filtered_df.copy()  # Erstellt eine Kopie des DataFrames, um die Warnung zu vermeiden
                            filtered_df.loc[:, 'Vorlauftemperatur_Celsius'] = filtered_df['Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                            smoothed_data = moving_average(filtered_df[column], window_size=5)  # Glätte die Daten
                            ax2.plot(filtered_df['Sekunde'] / 3600, filtered_df['Vorlauftemperatur_Celsius'])

            # Hinzufügen der Vorlauftemperatur-Daten für die Beladestrategie nur für 0.15 kg/s
            if mass_flow == 0.15 and not exergy_data_frame.empty:
                if 'Vorlauftemperatur' in exergy_data_frame.columns:
                    exergy_data_frame['Vorlauftemperatur_Celsius'] = exergy_data_frame['Vorlauftemperatur'] - 273.15  # Umrechnung von Kelvin in Celsius
                    ax2.plot(exergy_data_frame['Sekunde'] / 3600, exergy_data_frame['Vorlauftemperatur_Celsius'], label='Beladestrategie TVL', linestyle='--')

            ax2.set_xlabel('Beladedauer in Stunden', fontsize=11)
            ax2.set_ylabel('Vorlauftemperatur (°C)', fontsize=11)

            ax2.grid(False)
            ax2.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
            ax2.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
            ax2.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax2.tick_params(axis='both', which='major', labelsize=11)

            # Setzen der schwarzen Ränder
            for spine in ax2.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            # Gemeinsame Legende für beide Plots
            handles, labels = ax1.get_legend_handles_labels()
            ax1.legend(handles, labels, loc='lower right', fontsize=11, bbox_to_anchor=(1, 0))

            # Speicher und Anzeige des Plots
            plot_filename = os.path.join(mass_flow_dir, f"{title}_und_Vorlauftemperatur_{str(mass_flow).replace(' ', '_').replace('.', ',')}")
            fig.savefig(f"{plot_filename}.png", format='png')  # Speichern als PNG
            fig.savefig(f"{plot_filename}.svg", format='svg')  # Speichern als SVG

            plt.subplots_adjust(hspace=0.05)  # Reduziert den Abstand zwischen den Subplots
            plt.show()

if __name__ == "__main__":
    temperature_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy_Exergie_pro_Zeitschritt"
    frequency_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    exergy_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Beladestrategie\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Beladestrategie\PLOTS\WPSSWirkungsgradUndTVL"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    temperature_data_frames = read_time_dependent_csv_files(temperature_base_dir, 'temperature')
    frequency_data_frames = read_time_dependent_csv_files(frequency_base_dir, 'frequency')
    exergy_data_frame = read_exergy_results(exergy_base_dir)

    plot_time_dependent_results(temperature_data_frames, frequency_data_frames, exergy_data_frame, output_dir)
