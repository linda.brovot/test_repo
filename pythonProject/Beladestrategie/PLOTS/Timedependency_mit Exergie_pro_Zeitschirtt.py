import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_exergy_results(base_dir):
    file_path = os.path.join(base_dir, "results", "time_dependent_results.csv")
    print(f"Versuche Datei zu lesen: {file_path}")

    if not os.path.isfile(file_path):
        print(f"Die Datei {file_path} wurde nicht gefunden.")
        return pd.DataFrame()  # Rückgabe eines leeren DataFrames

    df = pd.read_csv(file_path, sep=',', decimal='.')
    return df

def trim_data(df, column):
    """Entfernt NaN-Werte am Ende der Datenreihe."""
    if column in df.columns:
        return df.loc[:df[column].last_valid_index()].dropna(subset=[column])
    return df

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_exergy_results(exergy_data_frame, output_dir):
    columns_to_plot = [
        ('Wirkungsgrad_WP', 'Wirkungsgrad der WP'),
        ('Wirkungsgrad_gesamt', 'Wirkungsgrad des WPSS'),
        ('COP', 'COP'),
        ('Exergie kummuliert kWh', 'kummulierte Exergie in kWh')
    ]

    mass_flow_str = "Massenstrom 0,15"
    mass_flow_dir = os.path.join(output_dir, mass_flow_str)
    if not os.path.exists(mass_flow_dir):
        os.makedirs(mass_flow_dir)

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(16, 10), sharex=True)
    fig.suptitle('Wirkungsgrad der WP, des WPSS und COP für Massenstrom 0,15 kg/s', y=0.95, fontsize=22)

    for column, title in columns_to_plot[:2]:
        # Hinzufügen der Exergie-Daten für die Beladestrategie
        if not exergy_data_frame.empty:
            filtered_exergy_df = trim_data(exergy_data_frame, column)
            ax1.plot(filtered_exergy_df['Sekunde'] / 3600, filtered_exergy_df[column], label=title, linestyle='--')

    ax1.set_ylabel('Wirkungsgrad', fontsize=20)
    ax1.grid(False)
    ax1.legend(fontsize=18)
    ax1.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
    ax1.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
    ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax1.tick_params(axis='both', which='major', labelsize=18)

    # Setzen der schwarzen Ränder
    for spine in ax1.spines.values():
        spine.set_edgecolor('black')
        spine.set_linewidth(1.5)

    # COP Plot
    if not exergy_data_frame.empty:
        filtered_exergy_df = trim_data(exergy_data_frame, 'COP')
        ax2.plot(filtered_exergy_df['Sekunde'] / 3600, filtered_exergy_df['COP'], label='COP', linestyle='--')

    ax2.set_xlabel('Beladedauer in Stunden', fontsize=20)
    ax2.set_ylabel('COP', fontsize=20)
    ax2.grid(False)
    ax2.legend(fontsize=18)
    ax2.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
    ax2.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
    ax2.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax2.tick_params(axis='both', which='major', labelsize=18)

    # Setzen der schwarzen Ränder
    for spine in ax2.spines.values():
        spine.set_edgecolor('black')
        spine.set_linewidth(1.5)

    plot_filename = os.path.join(mass_flow_dir, "Wirkungsgrad_und_COP_WP_und_WPSS".replace(' ', '_').replace('.', ','))
    fig.savefig(f"{plot_filename}.png", format='png')
    fig.savefig(f"{plot_filename}.svg", format='svg')
    print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

    plt.show()

if __name__ == "__main__":
    exergy_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Beladestrategie\COPmax\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Beladestrategie\COPmax\PLOTS\timedependency"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    exergy_data_frame = read_exergy_results(exergy_base_dir)

    plot_exergy_results(exergy_data_frame, output_dir)
