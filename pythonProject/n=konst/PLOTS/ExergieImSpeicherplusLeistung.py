import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(system_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return system_results_df
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {system_csv_file}")
        return None


def plot_exergie_and_percentage_changes(results_base_dir, output_dir, script_name, identifiers, labels, colors,
                                        bar_colors):
    fig, axes = plt.subplots(1, 2, figsize=(8, 4))  # Zwei Subplots nebeneinander

    drehzahlen = [30, 40, 50, 60, 70, 80, 90]  # Alle Drehzahlen
    index = np.arange(len(drehzahlen))  # Positionen für alle Drehzahlen

    all_exergies = {}

    # Linker Plot: Exergie im Speicher als Liniendiagramm
    for idx, identifier in enumerate(identifiers):
        system_results_df = read_csv_files(results_base_dir, identifier)
        if system_results_df is not None:
            # Berechne die Exergie für alle Drehzahlen
            sum_diff_exergies = [
                system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")][
                    'Sum_Differences_Exergies'].values[0] / 1000 / 3600
                for hz in drehzahlen
            ]

            # Plot als Liniendiagramm
            axes[0].plot(drehzahlen, sum_diff_exergies, marker='o', label=f'{identifier} {labels["unit"]}',
                         color=colors[idx], linewidth=2)

            # Exergie für diesen Identifier speichern
            all_exergies[identifier] = sum_diff_exergies

    axes[0].set_xlabel('Drehzahl in Hz', fontsize=18)
    axes[0].set_ylabel('Exergie im Speicher in kWh', fontsize=18)
    axes[0].legend(loc='best', fontsize=14)
    axes[0].tick_params(axis='both', which='major', labelsize=18)
   # axes[0].set_title('Exergie im Speicher', fontsize=18)

    # Rechter Plot: Prozentuale Änderung der Leistung und Exergie
    data = {
        40: {  # Frequenz 40 Hz
            0.15: {'power': 5.05, 'exergy': 1.05},
            0.25: {'power': 5.11, 'exergy': 1.06}
        },
        50: {  # Frequenz 50 Hz
            0.15: {'power': 5.28, 'exergy': 1.04},
            0.25: {'power': 5.32, 'exergy': 1.05}
        }
    }

    percentage_changes = []

    # Berechnung der prozentualen Änderungen
    for frequency in [40, 50]:
        power_15 = data[frequency][0.15]['power']
        power_25 = data[frequency][0.25]['power']
        exergy_15 = data[frequency][0.15]['exergy']
        exergy_25 = data[frequency][0.25]['exergy']

        power_change = ((power_25 - power_15) / power_15) * 100
        exergy_change = ((exergy_25 - exergy_15) / exergy_15) * 100

        percentage_changes.append({
            'frequency': frequency,
            'power_change (%)': power_change,
            'exergy_change (%)': exergy_change
        })

    # Erstellen des Balkendiagramms für die prozentualen Änderungen
    frequencies = [40, 50]
    power_changes = [item['power_change (%)'] for item in percentage_changes]
    exergy_changes = [item['exergy_change (%)'] for item in percentage_changes]

    bar_width = 0.25
    index = np.arange(len(frequencies))

    # Balken für die prozentualen Änderungen mit benutzerdefinierten Farben
    bars1 = axes[1].bar(index, power_changes, bar_width, edgecolor='black',label='Änderung der el. Energie in %', color=bar_colors[0])
    bars2 = axes[1].bar(index + bar_width, exergy_changes, bar_width, edgecolor='black',label='Änderung der Exergie in %',
                        color=bar_colors[1])

    # Werte über die Balken schreiben (mit vergrößerter Schriftgröße)
    for bars in [bars1, bars2]:
        for bar in bars:
            yval = bar.get_height()
            axes[1].text(bar.get_x() + bar.get_width() / 2, yval, f'{yval:.2f}%', ha='center', va='bottom', fontsize=14)

    axes[1].set_xlabel('Drehzahl in Hz', fontsize=18)
    axes[1].set_ylabel('Prozentuale Änderung in % von 0,15 zu 0,25 kg/s', fontsize=18)
    axes[1].set_ylim([0, 2.5])
    axes[1].set_xticks(index + bar_width / 2)
    axes[1].set_xticklabels(frequencies)
    axes[1].legend(loc='best', fontsize=14)
    #axes[1].set_title('Prozentuale Änderungen der Leistung und Exergie', fontsize=18)
    # Schriftgröße der Zahlen auf den Achsen (Ticks) im rechten Plot anpassen
    axes[1].tick_params(axis='both', which='major', labelsize=18)

    # Layout anpassen und Plot anzeigen
    plt.tight_layout()

    # Sicherstellen, dass der Plot im richtigen Verzeichnis gespeichert wird
    plot_file_path_png = os.path.join(output_dir, f"{script_name}_exergie_and_percentage_changes.png")
    plot_file_path_svg = os.path.join(output_dir, f"{script_name}_exergie_and_percentage_changes.svg")

    plt.savefig(plot_file_path_png)
    plt.savefig(plot_file_path_svg)

    print(f"Plot gespeichert unter: {plot_file_path_png}")
    print(f"Plot gespeichert unter: {plot_file_path_svg}")
    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS"

    # Plot für Massenströme Exergie und prozentuale Änderungen
    plot_exergie_and_percentage_changes(results_base_dir, output_dir, script_name,
                                        identifiers=[0.05, 0.15, 0.25],
                                        labels={"unit": "kg/s"},
                                        colors=['darkred', 'red', 'grey'],
                                        bar_colors=['grey', 'steelblue'])
