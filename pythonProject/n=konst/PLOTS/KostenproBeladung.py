import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None

def get_kennzahlen(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    gesamt_kosten = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]
        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0]
            pel_pumpe = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0]
            sum_diff_exergies = filtered_df['Sum_Differences_Exergies'].values[0]

            # Gesamtleistung berechnen
            gesamtleistung = pel_sum + pel_pumpe

            # Berechnung der spezifischen Kosten
            spezifische_kosten = (gesamtleistung/(3600*1000) * 0.298 )
            # Gesamtkosten für die Speicherbeladung berechnen
            gesamt_kosten.append(spezifische_kosten)

    return drehzahlen, gesamt_kosten

def combined_bar_plot(results_base_dir_1, results_base_dir_2, output_dir, script_name):
    identifiers_1 = [5]
    identifiers_2 = [0.05,0.15]

    kennzahlen_combined = {}
    drehzahlen_combined = []

    # Berechnung für deltaT = 5K
    for identifier in identifiers_1:
        system_results_df, layer_results_df = read_csv_files(results_base_dir_1, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, gesamt_kosten = get_kennzahlen(system_results_df, identifier)
            if drehzahlen:
                deltaT_label = f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$'
                kennzahlen_combined[deltaT_label] = gesamt_kosten
                drehzahlen_combined = drehzahlen  # Drehzahlen sind für alle gleich

    # Berechnung für massenstrom = 0.15 und 0.25 kg/s
    for identifier in identifiers_2:
        system_results_df, layer_results_df = read_csv_files(results_base_dir_2, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, gesamt_kosten = get_kennzahlen(system_results_df, identifier)
            if drehzahlen:
                massenstrom_label = f'$\\dot{{\\mathrm{{m}}}}_{{\\mathrm{{Sek}}}} = {identifier:.2f} \\ \\mathrm{{kg/s}}$'.replace(
                    '.', ',')
                kennzahlen_combined[massenstrom_label] = gesamt_kosten
                drehzahlen_combined = drehzahlen  # Drehzahlen sind für alle gleich

    if drehzahlen_combined:
        fig, ax = plt.subplots(figsize=(13, 8))

        # Titel
        fig.suptitle('Kosten pro Beladung des Speichers (€/Beladung)', fontsize=18)

        bar_width = 0.2
        bar_positions = np.arange(len(drehzahlen_combined))
        colors = ['red', 'grey', 'black']  # Farben für die Balken


        # Balken für jede Kategorie pro Drehzahl plotten
        for i, (identifier, gesamt_kosten) in enumerate(kennzahlen_combined.items()):
            ax.bar(bar_positions + i * bar_width, gesamt_kosten, width=bar_width, color=colors[i], label=identifier)

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('Kosten (€/Beladung)', fontsize=16)
        ax.set_xticks(bar_positions + bar_width)
        ax.set_xticklabels(drehzahlen_combined, fontsize=14)
        ax.tick_params(axis='y', labelsize=14)
        ax.grid(True, axis='y', linestyle='--', alpha=0.7)

        # Manuelle Legende
        ax.legend(loc='best', fontsize=16)

        # Speichern des kombinierten Balkendiagramms als PNG-Datei
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined_bar.png")
        plt.savefig(combined_plot_filename_png)
        print(f"Kombinierter Balken-Plot gespeichert: {combined_plot_filename_png}")

        # Speichern des kombinierten Balkendiagramms als SVG-Datei
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined_bar.svg")
        plt.savefig(combined_plot_filename_svg, format='svg')
        print(f"Kombinierter Balken-Plot gespeichert: {combined_plot_filename_svg}")

        # Diagramm anzeigen
        plt.show()

        # Schließen der Figur
        plt.close(fig)

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    # Basisverzeichnisse, in denen die Ergebnisdateien liegen
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_bar_plot(results_base_dir_1, results_base_dir_2, output_dir, script_name)
