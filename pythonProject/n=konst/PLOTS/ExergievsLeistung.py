import os
import pandas as pd
import matplotlib.pyplot as plt
import re
import numpy as np

# Pfade zu den gespeicherten Ergebnissen
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS"
output_dir = os.path.join(results_base_dir_1, "kum_exergie_stopp")
mass_flows = [0.15, 0.25]
frequencies = [40, 50]

# Funktion zum Einlesen und Aufbereiten der CSV-Dateien
def read_time_dependent_csv_files(mass_flow):
    data_frames = []
    # Ordner für das aktuelle mass_flow
    results_dir = os.path.join(results_base_dir, f"results_{mass_flow}")

    if not os.path.exists(results_dir):
        print(f"Verzeichnis nicht gefunden: {results_dir}")
        return pd.DataFrame()

    for subdir, _, files in os.walk(results_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                file_path = os.path.join(subdir, file)
                try:
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    if 'Leistung_WP' not in df.columns or 'kummulierte Exergie kWh' not in df.columns:
                        print(f"Erwartete Spalten fehlen in Datei: {file_path}")
                        continue
                    df['Frequency'] = df['Dateiname'].apply(lambda x: int(float(re.search(r'_(\d+\.\d+)\.csv', x).group(1)) * 100))
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")

                except Exception as e:
                    print(f"Fehler beim Lesen der Datei {file_path}: {e}")

    if not data_frames:
        return pd.DataFrame()

    return pd.concat(data_frames, ignore_index=True)

# Funktion zur Berechnung der prozentualen Änderungen und Erstellung des Plots
def plot_percentage_changes():
    # Beispielwerte für die Berechnungen (hier kannst du die tatsächlichen Daten einsetzen)
    data = {
        40: {  # Frequenz 40 Hz
            0.15: {'power': 5.05, 'exergy': 1.05},
            0.25: {'power': 5.11, 'exergy': 1.06}
        },
        50: {  # Frequenz 50 Hz
            0.15: {'power': 5.28, 'exergy': 1.04},
            0.25: {'power': 5.32, 'exergy': 1.05}
        }
    }

    percentage_changes = []

    # Berechnung der prozentualen Änderungen
    for frequency in frequencies:
        power_15 = data[frequency][0.15]['power']
        power_25 = data[frequency][0.25]['power']
        exergy_15 = data[frequency][0.15]['exergy']
        exergy_25 = data[frequency][0.25]['exergy']

        power_change = ((power_25 - power_15) / power_15) * 100
        exergy_change = ((exergy_25 - exergy_15) / exergy_15) * 100

        percentage_changes.append({
            'frequency': frequency,
            'power_change (%)': power_change,
            'exergy_change (%)': exergy_change
        })

    # Erstellen des Plots
    fig, ax = plt.subplots(figsize=(8, 6))

    index = np.arange(len(frequencies))
    bar_width = 0.35

    # Daten aus dem Berechnungsergebnis extrahieren
    power_changes = [item['power_change (%)'] for item in percentage_changes]
    exergy_changes = [item['exergy_change (%)'] for item in percentage_changes]

    # Plotten der Balkendiagramme
    bars1 = ax.bar(index, power_changes, bar_width, label='Prozentuale Änderung Leistung (%)')
    bars2 = ax.bar(index + bar_width, exergy_changes, bar_width, label='Prozentuale Änderung Exergie (%)', hatch='/')

    # Werte über die Balken schreiben
    for bars in [bars1, bars2]:
        for bar in bars:
            yval = bar.get_height()
            ax.text(bar.get_x() + bar.get_width()/2, yval, f'{yval:.2f}%', ha='center', va='bottom')

    # Achsen- und Plot-Details
    ax.set_xlabel('Frequenz (Hz)')
    ax.set_ylabel('Prozentuale Änderung (%)')
    ax.set_title('Prozentuale Änderungen der Leistung und Exergie (von 0,15 kg/s zu 0,25 kg/s)')
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels([f'{freq} Hz' for freq in frequencies])
    ax.legend()

    # Layout anpassen und Plot anzeigen
    plt.tight_layout()
    plt.savefig(os.path.join(results_base_dir_1, "Prozentuale_Aenderungen_Leistung_Exergie.png"))
    plt.show()

# Hauptlogik zur Erstellung des Plots
plot_percentage_changes()
