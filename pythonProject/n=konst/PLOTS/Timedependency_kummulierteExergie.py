import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()

def read_time_dependent_csv_files(base_dir, pattern):
    data_frames = []
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    data_frames.append((df, match))
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_combined_exergy(temp_data_frames, mass_flow_data_frames, output_dir, y_min=None, y_max=None, x_min=None, x_max=None):
    column = 'kummulierte Exergie kWh'  # Keine Umrechnung in kWh mehr
    title = 'Kummulierte Exergie - Mehrere Linien'
    deltaTs = [5, 8, 10]
    mass_flows = [0.05, 0.15, 0.25]

    fig, ax = plt.subplots(figsize=(16, 10))
    fig.suptitle(f'{title} bei 30 Hz', y=0.95, fontsize=22)

    # Plot für Temperaturspreizungen
    for deltaT in deltaTs:
        deltaT_str = f"ΔT = {deltaT}K"
        filtered_data = []

        for df, match in temp_data_frames:
            if match and match.group(1) == str(deltaT):
                filtered_data.append(df)

        if filtered_data:
            combined_df = pd.concat(filtered_data)
            combined_df.drop_duplicates(subset='Sekunde', inplace=True)  # Entfernen von doppelten Einträgen
            combined_df.sort_values(by='Sekunde', inplace=True)  # Sicherstellen, dass Daten sortiert sind
            ax.plot(combined_df['Sekunde'] / 3600, combined_df[column],
                    label=f'Temperaturspreizung {deltaT_str}')

    # Plot für Massenströme
    for mass_flow in mass_flows:
        filtered_data = []

        for df, match in mass_flow_data_frames:
            if match and match.group(1) == str(mass_flow):
                filtered_data.append(df)

        if filtered_data:
            combined_df = pd.concat(filtered_data)
            combined_df.drop_duplicates(subset='Sekunde', inplace=True)  # Entfernen von doppelten Einträgen
            combined_df.sort_values(by='Sekunde', inplace=True)  # Sicherstellen, dass Daten sortiert sind
            ax.plot(combined_df['Sekunde'] / 3600, combined_df[column], label=f'Massenstrom {mass_flow:.2f} kg/s')

    if ax.get_legend_handles_labels()[1]:  # Prüfen, ob Legenden-Labels vorhanden sind
        ax.legend(fontsize=18)

    ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
    ax.set_ylabel('Kummulierte Exergie', fontsize=20)

    # Manuelles Festlegen der Y-Achse
    if y_min is not None and y_max is not None:
        ax.set_ylim([0.75, 1.1])

    # Manuelles Festlegen der X-Achse
    if x_min is not None and x_max is not None:
        ax.set_xlim([5, 7])

    ax.grid(False)
    ax.locator_params(axis='x', nbins=7)
    ax.locator_params(axis='y', nbins=7)
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax.tick_params(axis='both', which='major', labelsize=18)

    # Setzen der schwarzen Ränder
    for spine in ax.spines.values():
        spine.set_edgecolor('black')
        spine.set_linewidth(1.5)

    plot_filename = os.path.join(output_dir, f"{title}".replace(' ', '_').replace('.', ','))
    fig.savefig(f"{plot_filename}.png", format='png')
    fig.savefig(f"{plot_filename}.svg", format='svg')
    print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

    plt.show()

if __name__ == "__main__":
    # Verzeichnisse für die Temperaturspreizungsdaten und die Massenstromdaten
    temp_results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy_Exergie_pro_Zeitschritt"
    mass_flow_results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\PLOTS\timedependency_combined"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Muster zum Extrahieren von DeltaT für die Temperaturspreizung
    temp_pattern = re.compile(r'time_dependent_results_(\d+)\.csv')
    # Muster zum Extrahieren von Massenstrom für die Massenströme
    mass_flow_pattern = re.compile(r'time_dependent_results_(\d+\.\d+)\.csv')

    # Lesen der CSV-Dateien aus beiden Verzeichnissen
    temp_data_frames = read_time_dependent_csv_files(temp_results_base_dir, temp_pattern)
    mass_flow_data_frames = read_time_dependent_csv_files(mass_flow_results_base_dir, mass_flow_pattern)

    # Erstellen des kombinierten Plots mit manueller X- und Y-Achsen-Einstellung (Beispielwerte)
    plot_combined_exergy(temp_data_frames, mass_flow_data_frames, output_dir, y_min=0, y_max=1.5, x_min=0, x_max=6)
