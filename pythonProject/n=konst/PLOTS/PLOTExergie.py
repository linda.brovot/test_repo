import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None

def get_sums(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    pel_sums = []
    pel_Pumpe_sums =[]
    exergie_sums = []
    eq_integrals_wp = []
    eq_integrals_sp = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz/100:.1f}.csv")]


        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
            pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600
            sum_diff_exergies = filtered_df['Sum_Differences_Exergies'].values[0] / 1000 / 3600
            eq_integral_wp = filtered_df['Gesamtintegral_E_Q_WP'].values[0] / 1000 / 3600
            eq_integral_sp = filtered_df['Gesamtintegral_E_Q_SP'].values[0] / 1000 / 3600


            pel_sums.append(pel_sum)
            pel_Pumpe_sums.append(pel_Pumpe_sum)
            exergie_sums.append(sum_diff_exergies)
            eq_integrals_wp.append(eq_integral_wp)
            eq_integrals_sp.append(eq_integral_sp)

    return drehzahlen, pel_sums, pel_Pumpe_sums, exergie_sums, eq_integrals_wp, eq_integrals_sp

def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")

def plot_individual(results_base_dir_1, results_base_dir_2, output_dir, script_name):
    drehzahlen_combined = []
    pel_sums_combined = {}
    pel_Pumpe_sums_combined ={}
    exergie_sums_combined = {}
    eq_integrals_wp_combined = {}
    eq_integrals_sp_combined = {}

    identifiers_1 = [5, 8, 10]
    identifiers_2 = [0.05, 0.15, 0.25]

    for identifier in identifiers_1:
        system_results_df, layer_results_df = read_csv_files(results_base_dir_1, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums, exergie_sums, eq_integrals_wp, eq_integrals_sp = get_sums(system_results_df, identifier)
            if drehzahlen:
                deltaT_label = f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$'
                pel_sums_combined[deltaT_label] = pel_sums
                pel_Pumpe_sums_combined[deltaT_label] = pel_Pumpe_sums
                exergie_sums_combined[deltaT_label] = exergie_sums
                eq_integrals_wp_combined[deltaT_label] = eq_integrals_wp
                eq_integrals_sp_combined[deltaT_label] = eq_integrals_sp
                drehzahlen_combined = drehzahlen  # Drehzahlen sind für alle gleich

    for identifier in identifiers_2:
        system_results_df, layer_results_df = read_csv_files(results_base_dir_2, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums, exergie_sums, eq_integrals_wp, eq_integrals_sp = get_sums(system_results_df, identifier)
            if drehzahlen:
                massenstrom_label = f'$\\dot{{\\mathrm{{m}}}}_{{\\mathrm{{Sek}}}} = {identifier:.2f} \\ \\mathrm{{kg/s}}$'.replace(
                    '.', ',')
                pel_sums_combined[massenstrom_label] = pel_sums
                pel_Pumpe_sums_combined[massenstrom_label] = pel_Pumpe_sums
                exergie_sums_combined[massenstrom_label] = exergie_sums
                eq_integrals_wp_combined[massenstrom_label] = eq_integrals_wp
                eq_integrals_sp_combined[massenstrom_label] = eq_integrals_sp
                drehzahlen_combined = drehzahlen  # Drehzahlen sind für alle gleich

    def format_func(x, pos, decimal_places):
        if x.is_integer():
            return locale.format_string("%.0f", x, grouping=True)
        else:
            format_str = f"%.{decimal_places}f"
            return locale.format_string(format_str, x, grouping=True)

    if drehzahlen_combined:
        # Pel-Summe Diagramm
        fig, ax = plt.subplots(figsize=(13, 8))
        fig.suptitle('zugeführte elektrische Energie der WP', y=0.95, fontsize=16)

        for (identifier, pel_sums) in pel_sums_combined.items():
            ax.plot(drehzahlen_combined, pel_sums, marker='o', linestyle='-', label=f'{identifier}')

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('el. Energie in kWh', fontsize=16)
        ax.set_ylim(5.5, 8.5)
        ax.grid(False)
        ax.legend()
        ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
        ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 0)))  # Keine Nachkommastellen auf der x-Achse
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 1)))
        ax.tick_params(axis='both', which='major', labelsize=16)

        # Plot speichern
        plot_and_save(fig, os.path.join(output_dir, f"{script_name}_pel_sum"))

        plt.show()

        # Pel_Pumpe-Summe Diagramm
        fig, ax = plt.subplots(figsize=(13, 8))
        fig.suptitle('zugeführte elektrische Energie der Umwälzpumpe', y=0.95, fontsize=16)

        for (identifier, pel_Pumpe_sums) in pel_Pumpe_sums_combined.items():
            ax.plot(drehzahlen_combined, pel_Pumpe_sums, marker='o', linestyle='-', label=f'{identifier}')

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('el. Energie in kWh', fontsize=16)
        ax.set_ylim(0, 0.25)
        ax.grid(False)
        ax.legend()
        ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
        ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
        ax.xaxis.set_major_formatter(
            ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 0)))  # Keine Nachkommastellen auf der x-Achse
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        ax.tick_params(axis='both', which='major', labelsize=16)

        # Plot speichern
        plot_and_save(fig, os.path.join(output_dir, f"{script_name}_pel_sum_Pumpe"))

        plt.show()

        # Summe der Exergien Diagramm
        fig, ax = plt.subplots(figsize=(13, 8))
        fig.suptitle('Exergie der inneren Energie im Speicher', y=0.95, fontsize=16)

        for (identifier, exergie_sums) in exergie_sums_combined.items():
            ax.plot(drehzahlen_combined, exergie_sums, marker='o', linestyle='-', label=f'{identifier}')

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('Exergie im Speicher in kWh', fontsize=16)
        ax.set_ylim(1.2,1.42)
        ax.grid(False)
        ax.legend()
        ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
        ax.locator_params(axis='y', nbins=6)  # Anzahl der Ticks auf der y-Achse anpassen
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 0)))  # Keine Nachkommastellen auf der x-Achse
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        ax.tick_params(axis='both', which='major', labelsize=16)

        # Plot speichern
        plot_and_save(fig, os.path.join(output_dir, f"{script_name}_exergie_sum"))

        plt.show()

        # Gesamtintegral E_Q WP Diagramm
        fig, ax = plt.subplots(figsize=(13, 8))
        fig.suptitle('Exergie des Wärmestroms WP', y=0.95, fontsize=16)

        for (identifier, eq_integrals_wp) in eq_integrals_wp_combined.items():
            ax.plot(drehzahlen_combined, eq_integrals_wp, marker='o', linestyle='-', label=f'{identifier}')

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('Exergie des Wärmestroms WP in kWh', fontsize=16)
        ax.set_ylim(1.85,2.65)
        ax.grid(False)
        ax.legend()
        ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
        ax.locator_params(axis='y', nbins=6)  # Anzahl der Ticks auf der y-Achse anpassen
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 0)))  # Keine Nachkommastellen auf der x-Achse
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        ax.tick_params(axis='both', which='major', labelsize=16)

        # Plot speichern
        plot_and_save(fig, os.path.join(output_dir, f"{script_name}_integral_eq_wp"))

        plt.show()

        # Gesamtintegral E_Q SP Diagramm
        fig, ax = plt.subplots(figsize=(13, 8))
        fig.suptitle('Exergie des Wärmestroms SP', y=0.95, fontsize=16)

        for (identifier, eq_integrals_sp) in eq_integrals_sp_combined.items():
            ax.plot(drehzahlen_combined, eq_integrals_sp, marker='o', linestyle='-', label=f'{identifier}')

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('Exergie des Wärmestroms SP in kWh', fontsize=16)
        ax.set_ylim(1.65,2.06)
        ax.grid(False)
        ax.legend()
        ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
        ax.locator_params(axis='y', nbins=6)  # Anzahl der Ticks auf der y-Achse anpassen
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 0)))  # Keine Nachkommastellen auf der x-Achse
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        ax.tick_params(axis='both', which='major', labelsize=16)

        # Plot speichern
        plot_and_save(fig, os.path.join(output_dir, f"{script_name}_integral_eq_sp"))

        plt.show()

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    plot_individual(results_base_dir_1, results_base_dir_2, output_dir, script_name)
