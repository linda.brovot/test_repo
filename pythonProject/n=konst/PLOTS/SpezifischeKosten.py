import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None

def get_kennzahlen(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    kennzahlen = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]
        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0]
            pel_pumpe = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0]  # Neue Zeile, um P_el_pumpe zu extrahieren
            sum_diff_exergies = filtered_df['Sum_Differences_Exergies'].values[0]

            # Gesamtleistung berechnen
            gesamtleistung = pel_sum + pel_pumpe

            # Berechnung der Kennzahl
            kennzahl = (gesamtleistung * 0.298 )/ (sum_diff_exergies)
            kennzahlen.append(kennzahl)

    return drehzahlen, kennzahlen


def combined_plot(results_base_dir_1, results_base_dir_2, output_dir, script_name):
    drehzahlen_combined = []
    kennzahlen_combined = {}

    identifiers_1 = [5, 8, 10]
    identifiers_2 = [0.05, 0.15, 0.25]


    for identifier in identifiers_1:
        system_results_df, layer_results_df = read_csv_files(results_base_dir_1, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, kennzahlen = get_kennzahlen(system_results_df, identifier)
            if drehzahlen:
                deltaT_label = f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$'
                kennzahlen_combined[deltaT_label] = kennzahlen
                drehzahlen_combined = drehzahlen  # Drehzahlen sind für alle gleich

    for identifier in identifiers_2:
        system_results_df, layer_results_df = read_csv_files(results_base_dir_2, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, kennzahlen = get_kennzahlen(system_results_df, identifier)
            if drehzahlen:
                massenstrom_label = f'$\\dot{{\\mathrm{{m}}}}_{{\\mathrm{{Sek}}}} = {identifier:.2f} \\ \\mathrm{{kg/s}}$'.replace(
                    '.', ',')
                kennzahlen_combined[massenstrom_label] = kennzahlen
                drehzahlen_combined = drehzahlen  # Drehzahlen sind für alle gleich

    if drehzahlen_combined:
        fig, ax = plt.subplots(figsize=(13, 8))

        # Titel näher an den Plot setzen
        fig.suptitle('Kosten an el. Energie in € pro kWh Exergie im Speicher', y=0.95, fontsize=18)

        colors = ['lightcoral', 'red', 'darkred', 'silver', 'grey', 'black']
        for (identifier, kennzahlen), color in zip(kennzahlen_combined.items(), colors):
            ax.plot(drehzahlen_combined, kennzahlen, marker='o', linestyle='-', color=color, label=identifier)

        ax.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax.set_ylabel('€/kWh', fontsize=16)
        ax.set_ylim(1.3, 2.05)
        ax.grid(False)

        # Manuelles Anpassen der Legende
        handles, labels = ax.get_legend_handles_labels()
        deltaT_handles = [handles[i] for i in range(3)]
        deltaT_labels = [labels[i] for i in range(3)]
        pump_handles = [handles[i] for i in range(3, 6)]
        pump_labels = [labels[i] for i in range(3, 6)]

        # Legende manuell in zwei Zeilen aufteilen
        font_properties = {'size': 16, 'family': 'sans-serif'}

        # Erste Legende (Delta T)
        legend1 = ax.legend(deltaT_handles, deltaT_labels, loc='upper left', bbox_to_anchor=(0, 1),
                            ncol=1, prop=font_properties, handletextpad=1, columnspacing=1, frameon=False)

        # Zweite Legende (Massenstrom)
        legend2 = ax.legend(pump_handles, pump_labels, loc='upper left', bbox_to_anchor=(0, 0.84),
                            ncol=1, prop=font_properties, handletextpad=1, columnspacing=1, frameon=False)

        ax.add_artist(legend1)
        ax.add_artist(legend2)

        ax.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Speichern des kombinierten Plots als PNG-Datei
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        plt.savefig(combined_plot_filename_png)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png}")

        # Speichern des kombinierten Plots als SVG-Datei
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_svg, format='svg')
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_svg}")

        # Diagramm anzeigen
        plt.show()

        # Schließen der Figur
        plt.close(fig)

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    # Basisverzeichnisse, in denen die Ergebnisdateien liegen
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir_1, results_base_dir_2, output_dir, script_name)
