import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import matplotlib.patches as mpatches

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(system_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return system_results_df
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {system_csv_file}")
        return None


def calculate_percentage_deviation(values):
    deviations = []
    for i in range(1, len(values)):
        deviation = ((values[i] - values[i - 1]) / values[i - 1]) * 100 if values[i - 1] != 0 else 0
        deviations.append(deviation)
    return deviations


def plot_exergie_linediagram_and_save_deviation(results_base_dir, output_dir, script_name, identifiers, labels, colors):
    fig, ax_exergie = plt.subplots(figsize=(12, 8))

    drehzahlen = [30, 40, 50, 60, 70, 80, 90]  # Alle Drehzahlen
    index = np.arange(len(drehzahlen))  # Positionen für alle Drehzahlen

    all_exergies = {}
    results_data = []  # Speichert die Ergebnisse für die CSV-Datei

    for idx, identifier in enumerate(identifiers):
        system_results_df = read_csv_files(results_base_dir, identifier)
        if system_results_df is not None:
            # Berechne die Exergie für alle Drehzahlen
            sum_diff_exergies = [
                system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")][
                    'Sum_Differences_Exergies'].values[0] / 1000 / 3600
                for hz in drehzahlen]

            # Plot als Liniendiagramm
            ax_exergie.plot(drehzahlen, sum_diff_exergies, marker='o', label=f'{str(identifier).replace(".", ",")} {labels["unit"]}', color=colors[idx], linewidth=2)

            # Exergie für diesen Identifier speichern
            all_exergies[identifier] = sum_diff_exergies

            # Ergebnisse für die CSV-Datei sammeln
            for hz, exergie in zip(drehzahlen, sum_diff_exergies):
                results_data.append({
                    "Massenstrom/Delta T": f"{identifier} {labels['unit']}",
                    "Drehzahl_Hz": f"{hz}",
                    "Exergie_kWh": f"{exergie:.4f}".replace('.', ',')
                })

    # Berechnung der prozentualen Abweichungen zwischen den Massenströmen oder Delta T bei 30 Hz und 90 Hz
    def calculate_percentage_deviation(value_low, value_high):
        if value_low != 0:
            return ((value_high - value_low) / value_low) * 100
        return 0

    # Zeichne die Abweichungen mit Pfeilen und Prozentzahlen bei 30 Hz und 90 Hz
    def add_deviation_arrow(x_pos_start, x_pos_end, exergie_start, exergie_end, deviation):
        # Bestimme die Richtung des Pfeils und den Text
        arrowprops = dict(arrowstyle='->', color='black', lw=2)
        deviation_text = f'{deviation:+.2f}%'.replace('.', ',')

        # Zeichne den Pfeil vom niedrigeren zum höheren Wert
        ax_exergie.annotate('', xy=(x_pos_end, exergie_end), xytext=(x_pos_start, exergie_start), arrowprops=arrowprops)

        # Füge den Text zur prozentualen Abweichung in der Mitte des Pfeils hinzu
        mid_x = (x_pos_start + x_pos_end) / 2
        mid_y = (exergie_start + exergie_end) / 2
        ax_exergie.text(mid_x, mid_y, deviation_text, fontsize=14, ha='center', va='center', color='black')

    # Prozentzahlen und Pfeile für die Abweichungen bei 30 Hz und 90 Hz (zwischen 0,05 und 0,25)
    if len(identifiers) >= 2:
        # Für 90 Hz
        exergie_90_low = all_exergies[identifiers[0]][-1]
        exergie_90_high = all_exergies[identifiers[2]][-1]
        deviation_90 = calculate_percentage_deviation(exergie_90_low, exergie_90_high)

        # Für 30 Hz
        exergie_30_low = all_exergies[identifiers[0]][0]
        exergie_30_high = all_exergies[identifiers[2]][0]
        deviation_30 = calculate_percentage_deviation(exergie_30_low, exergie_30_high)

        # Füge die Abweichungen mit Pfeilen und Text hinzu für 30 Hz und 90 Hz (zwischen 0,05 und 0,25)
        add_deviation_arrow(30, 30, exergie_30_low, exergie_30_high, deviation_30)
        add_deviation_arrow(90, 90, exergie_90_low, exergie_90_high, deviation_90)

    # Berechne und zeichne den Pfeil und die prozentuale Abweichung von 30 Hz zu 90 Hz für denselben Identifier (z.B. 0,05)
    if len(identifiers) >= 1:
        # Werte für 30 Hz und 90 Hz für denselben Identifier (z.B. 0,05)
        exergie_30_same = all_exergies[identifiers[0]][0]
        exergie_90_same = all_exergies[identifiers[0]][-1]
        deviation_same = calculate_percentage_deviation(exergie_30_same, exergie_90_same)

        # Zeichne den Pfeil von 30 Hz zu 90 Hz für denselben Identifier
        add_deviation_arrow(30, 90, exergie_30_same, exergie_90_same, deviation_same)

    # Berechne und zeichne den Pfeil und die prozentuale Abweichung von 30 Hz zu 90 Hz für denselben Identifier (z.B. 0,25 oder 10 K)
    if len(identifiers) >= 2:
        # Für 0,25 oder 10 K
        exergie_30_second = all_exergies[identifiers[2]][0]
        exergie_90_second = all_exergies[identifiers[2]][-1]
        deviation_second = calculate_percentage_deviation(exergie_30_second, exergie_90_second)

        # Zeichne den Pfeil von 30 Hz zu 90 Hz für 0,25 oder 10 K
        add_deviation_arrow(30, 90, exergie_30_second, exergie_90_second, deviation_second)

    # Achsenbeschriftungen und Titel setzen
    ax_exergie.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax_exergie.set_ylabel('Exergie im Speicher in kWh', fontsize=16)
    ax_exergie.tick_params(axis='both', which='major', labelsize=16)

    # Unterschiedliche Y-Achsenlimits je nach Einheit festlegen
    if labels['unit'] == "kg/s":
        ax_exergie.set_ylim(0.93, 1.07)
    elif labels['unit'] == "K":
        ax_exergie.set_ylim(1.01, 1.05)

    # Formatiere die y-Achse, um das Komma als Dezimaltrennzeichen zu verwenden
    ax_exergie.get_yaxis().set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.2f}'.replace('.', ',')))

    # Legende hinzufügen
    ax_exergie.legend(loc='best', fontsize=12)

    # Sicherstellen, dass der Schrägstrich im Dateinamen ersetzt wird
    safe_unit = labels['unit'].replace('/', '_')

    # Plot speichern
    plot_and_save(fig, os.path.join(output_dir, f"{script_name}_exergie_plot_{safe_unit}"))

    # Plot anzeigen
    plt.show()




def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\PLOTS"
    output_dir = results_base_dir_3  # Output im bestehenden PLOTS-Verzeichnis

    # Plot für Massenströme Exergie und Abweichungen
    plot_exergie_linediagram_and_save_deviation(results_base_dir_2, output_dir, script_name,
                                             identifiers=[0.05, 0.15, 0.25],
                                             labels={"unit": "kg/s"},
                                             colors=['darkred', 'red', 'grey'])

    # Plot für Temperaturspreizung (Delta T) Exergie und Abweichungen
    plot_exergie_linediagram_and_save_deviation(results_base_dir_1, output_dir, script_name,
                                             identifiers=[5, 8, 10],
                                             labels={"unit": "K"},
                                             colors=['orange', 'blue', 'green'])
