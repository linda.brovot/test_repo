import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

# Funktion zum Lesen der CSV-Dateien
def read_csv_files(base_dir, identifier):
    folder_path = os.path.join(base_dir, f"results_{identifier}")
    csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {csv_file}")
        return None

# Funktion zum Plotten der Daten
def plot_data(data_frame, identifier):
    eta_system_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]

                if not pd.isna(eta_WPSS):
                    eta_system_list.append(eta_WPSS)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, eta_system_list

# Funktion zum Erstellen des kombinierten Plots
def combined_plot(results_base_dirs, output_dir, script_name):
    fig, ax = plt.subplots(figsize=(13, 8))  # Anpassung der Plot-Größe
    fig.suptitle('Exergetischer Nutzungsgrad', fontsize=18, y=0.95)  # Titel näher an den Plot

    # Farben und Stil wie im Design-Vorbild
    colors = ['lightcoral', 'red', 'darkred', 'silver', 'grey', 'black']
    kennzahlen_combined = {}
    drehzahlen_combined = []

    identifiers_1 = [5, 8, 10]  # Identifikatoren für Delta T
    identifiers_2 = [0.05, 0.2, 0.3]  # Identifikatoren für Massenstrom

    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, eta_system_list = plot_data(system_results_df, identifier)
            if frequencies:
                deltaT_label = f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$'
                kennzahlen_combined[deltaT_label] = eta_system_list
                drehzahlen_combined = frequencies

    for identifier in identifiers_2:
        system_results_df = read_csv_files(results_base_dirs[0], identifier)
        if system_results_df is not None:
            frequencies, eta_system_list = plot_data(system_results_df, identifier)
            if frequencies:
                massenstrom_label = f'$\\dot{{\\mathrm{{m}}}}_{{\\mathrm{{Sek}}}} = {identifier:.2f} \\ \\mathrm{{kg/s}}$'.replace('.', ',')
                kennzahlen_combined[massenstrom_label] = eta_system_list
                drehzahlen_combined = frequencies

    for i, (label, kennzahlen) in enumerate(kennzahlen_combined.items()):
        color = colors[i]
        ax.plot(drehzahlen_combined, kennzahlen, marker='o', linestyle='-', color=color, label=label, markersize=6, linewidth=1.5)

    # Beibehalten der originalen Beschriftungen und Design
    ax.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax.set_ylabel('Nutzungsgrad', fontsize=16)
    ax.set_ylim(0.12, 0.22)
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.02))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.2f', x)))
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.0f', x)))

    ax.grid(False)

    # Schwarze Ränder um den Plot hinzufügen
    for spine in ax.spines.values():
        spine.set_edgecolor('black')
        spine.set_linewidth(1.5)

    # Manuelles Anpassen der Legende
    handles, labels = ax.get_legend_handles_labels()
    deltaT_handles = [handles[i] for i in range(3)]
    deltaT_labels = [labels[i] for i in range(3)]
    pump_handles = [handles[i] for i in range(3, 6)]
    pump_labels = [labels[i] for i in range(3, 6)]

    # Legende manuell in zwei Zeilen aufteilen
    font_properties = {'size': 16, 'family': 'sans-serif'}

    # Erste Legende (Delta T)
    legend1 = ax.legend(deltaT_handles, deltaT_labels, loc='upper left', bbox_to_anchor=(0.7, 1),
                        ncol=1, prop=font_properties, handletextpad=1, columnspacing=1, frameon=False)

    # Zweite Legende (Massenstrom)
    legend2 = ax.legend(pump_handles, pump_labels, loc='upper left', bbox_to_anchor=(0.7, 0.84),
                        ncol=1, prop=font_properties, handletextpad=1, columnspacing=1, frameon=False)

    ax.add_artist(legend1)
    ax.add_artist(legend2)


    ax.tick_params(axis='both', which='major', labelsize=16)

    combined_plot_filename = os.path.join(output_dir, f"{script_name}_combined.png")
    plt.savefig(combined_plot_filename, bbox_inches='tight')
    plt.show()
    plt.close(fig)
    print(f"Kombinierter Plot gespeichert: {combined_plot_filename}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dirs = [
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\CalculationExergy_TEST",
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy_TEST"
    ]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dirs, output_dir, script_name)
