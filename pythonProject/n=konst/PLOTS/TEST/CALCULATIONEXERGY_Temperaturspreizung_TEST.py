import os
import pandas as pd
import numpy as np
from rp_wrapper import RefProp

# Pfade zu den CSV-Ordnern für die verschiedenen Pumpendrehzahlen
base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CSV_Results"
deltaTs = [5, 8, 10]

# Zielordner für die Ergebnisse
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy_TEST"

# Sicherstellen, dass der Zielordner existiert oder erstellt wird
os.makedirs(results_base_dir, exist_ok=True)

# Lokaler Pfad zum 'REFPROP'-Ordner und -DLL
os.environ["RPPREFIX"] = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\REFPROP"
os.environ["RPPREFIX_CUSTOMD_DLL"] = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\REFPROP\REFPRP.DLL"

# Masse des Mediums im Modell
m = 28.855146

# Erstellen einer Instanz des RefProp-Wrappers
tmp_rp = RefProp("Water")

# Zustandsberechnung für den Umgebungszustand
def calc_ambient_state():
    state_ambient = tmp_rp.calc_state('PT', 101325, 273.15 + 15)  # Druck: 101325 Pa, Temperatur: 15 °C
    return state_ambient.u, state_ambient.s, state_ambient.v, state_ambient

U_ambient, s_ambient, v_ambient, state_ambient = calc_ambient_state()

# Durchschnittstemperatur T_m für die Wärmeleistung im Kondensator
T_m = "heatPump.con.vol.T"

# Liste der VL- und RL-Temperaturen für die Wärmeübertrager
T_VL_Sp = "bufferStorage.heatingCoil1.pipe[1].vol_a.T"
T_RL_Sp = "bufferStorage.heatingCoil1.pipe[9].vol_b.T"

def process_csv_files(folder_path):
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]
    system_results = []
    layer_results = []
    time_dependent_results = []

    for csv_file_path in csv_files:
        name = os.path.splitext(os.path.basename(csv_file_path))[0]
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")
        df = df[df.index >= 100]
        df["Hour"] = df.index / 3600  # Index in Stunden umwandeln

        # Speicher
        # Berechnungen der Exergie der Wärmeübertrager im Speicher
       # total_exergy_transfer = 0
        #for i in range(9):
            # thermodynamische Mitteltemperatur pro Wärmeübertrager
          #  df[f"T_m_Sp_{i+1}"] = (df[T_VL_Sp[i]] - df[T_RL_Sp[i]]) / np.log(df[T_VL_Sp[i]] / df[T_RL_Sp[i]])
          #  df[f"Q_Flow_abs_{i+1}"] = df[f"bufferStorage.heatingCoil1.Therm1[{i+1}].Q_flow"].abs()
           # df[f"E_Q_SP_{i+1}"] = (1 - state_ambient.T / df[f"T_m_Sp_{i+1}"]) * df[f"Q_Flow_abs_{i+1}"]
           # total_exergy_transfer += df[f"E_Q_SP_{i+1}"]

        #df["E_Q_SP"] = total_exergy_transfer
        #total_integral_E_Q_SP = np.trapz(df["E_Q_SP"], df.index)

        # Berechnung der Exergieänderung in den Schichten des Speichers
        differences_exergies = []
        for i in range(1, 11):
            first_temp = df[f'bufferStorage.layer[{i}].T'].iloc[0]
            last_temp = df[f'bufferStorage.layer[{i}].T'].iloc[-1]

            state_first = tmp_rp.calc_state('PT', 300000, first_temp)
            s_first = state_first.s
            U_first = state_first.u

            state_last = tmp_rp.calc_state('PT', 300000, last_temp)
            s_last = state_last.s
            U_last = state_last.u

            v_first = df[f'bufferStorage.layer[1].V'].iloc[0] / m
            v_last = df[f'bufferStorage.layer[1].V'].iloc[-1] / m

            exergie_first = m * ((U_first - U_ambient) - state_ambient.T * (s_first - s_ambient) - state_ambient.p * (v_first - v_ambient))
            exergie_last = m * ((U_last - U_ambient) - state_ambient.T * (s_last - s_ambient) - state_ambient.p * (v_last - v_ambient))

            difference_exergie = exergie_last - exergie_first
            differences_exergies.append(difference_exergie)
            layer_results.append({
                "Datei": name,
                "Layer": i,
                "Exergie_in_0": exergie_first,
                "Exergie_in_end": exergie_last,
                "Differenz_Exergie": difference_exergie,
                "U_in_0": U_first,
                "U_last": U_last,
                "v_in_0": v_first,
                "s_in_0": s_first,
                "s_in_last": s_last,
            })

        sum_differences_exergies = sum(differences_exergies)


        #Speicher Wärmestrom
        df["T_m_Sp"] = (df[T_VL_Sp] + df[T_RL_Sp]) / 2
        df["Q_Flow_abs"] = df["Q_Flow_HC1"].abs()
        df["E_Q_SP"] = (1 - state_ambient.T / df["T_m_Sp"]) * df["Q_Flow_abs"]
        total_integral_E_Q_SP = np.trapz(df["E_Q_SP"], df.index)
        eta_sp = sum_differences_exergies / total_integral_E_Q_SP

        # Wärmepumpe
        df["COP_abs"] = df["heatPump.innerCycle.PerformanceDataHPHeating.Table_COP.y"].abs()
        df["Q_CON_abs"] = df["heatPump.innerCycle.PerformanceDataHPHeating.Table_QCon.y"].abs()
        df["E_Q_WP"] = (1 - state_ambient.T / df[T_m]) * df["Q_CON_abs"]
        total_integral_E_Q_WP = np.trapz(df["E_Q_WP"], df.index)
        df["P_el"] = df["heatPump.innerCycle.PerformanceDataHPHeating.Pel"]
        total_sum_Pel = df["P_el"].sum()
        df["P_el_Pumpe"] = df["floMacDyn.P"]
        total_sum_Pel_Pumpe = df["P_el_Pumpe"].sum()
        eta_wp_n_mitpumpe = total_integral_E_Q_WP / (total_sum_Pel + total_sum_Pel_Pumpe)  # Nutzungsgrad der WP mit Pumpe
        df["eta_wp_mitpumpe"] = df["E_Q_WP"] / (df["P_el"] + df["P_el_Pumpe"])  # Wirkungsgrad WP mit Pumpe
        df["eta_wp"] =df["E_Q_WP"] / df["P_el"] #Wirkungsgrad ohne Pumpe
        eta_wp_n = total_integral_E_Q_WP / total_sum_Pel #Nutzungsgrad der WP ohne Pumpe
        df["n"] = df[f"sigBus1.nSet"]

        eta_WPSS = sum_differences_exergies/(total_sum_Pel + total_sum_Pel_Pumpe)  # Nutzungsgrad Gesamtsystem mit Pumpe


        system_results.append({
            "Name": name,
            "Gesamtintegral_E_Q_WP": total_integral_E_Q_WP,
            "Gesamtintegral_E_Q_SP": total_integral_E_Q_SP,
            "Gesamtsumme_Pel": total_sum_Pel,
            "Gesamtsumme_Pel_Pumpe": total_sum_Pel_Pumpe,
            "Nutzungsgrad_WP_mitPumpe": eta_wp_n_mitpumpe,
            "Nutzungsgrad_WP": eta_wp_n,
            "Sum_Differences_Exergies": sum_differences_exergies,
            "Eta_SP": eta_sp,
            "Eta_WPSS": eta_WPSS,
            "Hours": df["Hour"].max(),
        })

        df["Dateiname"] = name
        df["Sekunde"] = df.index


        # Speichern der zeitabhängigen Ergebnisse
        time_dependent_df = df[[
            "Sekunde",  # Neue Spalte hinzufügen
            "Dateiname",  # Neue Spalte hinzufügen
            "eta_wp",
            "eta_wp_mitpumpe",
            "P_el",
            "E_Q_WP",
            "E_Q_SP",
            "n",
            "heatPump.sigBus.TConOutMea"
        ]].copy()
        time_dependent_df.columns = [
            "Sekunde",  # Neue Spalte hinzufügen
            "Dateiname",  # Neue Spalte hinzufügen
            "Wirkungsgrad_WP",
            "Wirkungsgrad_WP_mitPumpe",
            "Leistung_WP",
            "Exergie Wärmestrom_WP",
            "Exergie Wärmestrom_Speicher",
            "Verdichterdrehzahl",
            "Vorlauftemperatur"
        ]
        time_dependent_results.append(time_dependent_df)

    return pd.DataFrame(system_results), pd.DataFrame(layer_results), pd.concat(time_dependent_results)


for deltaT in deltaTs:
    folder_path = os.path.join(base_dir, f"csv_{deltaT}")
    system_results_df, layer_results_df, time_dependent_results_df = process_csv_files(folder_path)

    # Unterordner für die jeweiligen Massendurchflusswerte im Zielordner erstellen
    results_dir = os.path.join(results_base_dir, f"results_{deltaT}")
    os.makedirs(results_dir, exist_ok=True)

    # Pfade für die Ausgabedateien festlegen
    output_system_csv_file = os.path.join(results_dir, f"system_results_{deltaT}.csv")
    output_layer_csv_file = os.path.join(results_dir, f"layer_results_{deltaT}.csv")
    output_time_dependent_csv_file = os.path.join(results_dir, f"time_dependent_results_{deltaT}.csv")

    # Ergebnisse in den CSV-Dateien speichern
    system_results_df.to_csv(output_system_csv_file, index=False)
    layer_results_df.to_csv(output_layer_csv_file, index=False)
    time_dependent_results_df.to_csv(output_time_dependent_csv_file, index=False)

    print(f"Die Ergebnisse wurden in den Dateien '{output_system_csv_file}', '{output_layer_csv_file}', und '{output_time_dependent_csv_file}' gespeichert.")
