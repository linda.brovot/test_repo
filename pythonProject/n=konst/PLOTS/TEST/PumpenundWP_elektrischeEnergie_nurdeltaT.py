import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
from matplotlib.patches import Patch

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None


def get_sums(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    pel_sums = []
    pel_Pumpe_sums = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]

        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
            pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600

            pel_sums.append(pel_sum)
            pel_Pumpe_sums.append(pel_Pumpe_sum)

    return drehzahlen, pel_sums, pel_Pumpe_sums


def calculate_pump_percentage(pel_sums, pel_Pumpe_sums):
    percentages = []
    for wp, pumpe in zip(pel_sums, pel_Pumpe_sums):
        total = wp + pumpe
        if total > 0:
            percentage = (pumpe / total) * 100
        else:
            percentage = 0
        percentages.append(percentage)
    return percentages


def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")


def plot_sum_comparison(results_base_dir, output_dir, script_name, identifiers, labels, colors, hatches):
    # Erstelle einen einzelnen Subplot: gestapeltes Balkendiagramm
    fig, ax_zoom = plt.subplots(figsize=(16, 8))
    fig.suptitle(f'Vergleich der elektrischen Energie: WP + Pumpe vs. WP ({labels["title"]})', y=0.95, fontsize=16)

    bar_width = 0.25  # Breite der Balken
    index = np.arange(7)  # Positionen für 30 bis 90 Hz

    for idx, identifier in enumerate(identifiers):
        system_results_df, layer_results_df = read_csv_files(results_base_dir, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums = get_sums(system_results_df, identifier)
            if drehzahlen:
                # Für den gestapelten Balkendiagramm-Plot
                combined_sums_zoom = [wp + pumpe for wp, pumpe in zip(pel_sums, pel_Pumpe_sums)]

                # Gestapelte Balken: WP-Leistung (unten) + Pumpe-Leistung (oben, schraffiert)
                ax_zoom.bar(index + idx * bar_width, pel_sums, bar_width, color=colors[idx],
                            edgecolor='black',  # Schwarzer Rand um die Balken
                            label=f'WP {labels["label"]} {locale.format_string("%.2f", identifier)}')
                ax_zoom.bar(index + idx * bar_width, pel_Pumpe_sums, bar_width, bottom=pel_sums,
                            hatch=hatches[idx], color=colors[idx], edgecolor='black')  # Schwarzer Rand um die Balken

                # Berechne den prozentualen Anteil der Pumpe an der Gesamtleistung für die Zoom-In Drehzahlen (30, 40, 50 Hz)
                pump_percentages = calculate_pump_percentage(pel_sums, pel_Pumpe_sums)

                # Anzeige der Ergebnisse im Plot
                for i, percentage in enumerate(pump_percentages):
                    ax_zoom.text(index[i] + idx * bar_width, combined_sums_zoom[i] + 0.05,
                                 f"{percentage:.2f}%", fontsize=14, ha='center')

    # Einstellungen für den Zoom-In Subplot (links)
    ax_zoom.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax_zoom.set_ylabel('el. Energie in kWh', fontsize=16)
    ax_zoom.grid(False)
    ax_zoom.set_xticks(index + bar_width)
    ax_zoom.set_xticklabels([30, 40, 50, 60, 70, 80, 90])
    ax_zoom.tick_params(axis='both', which='major', labelsize=16)

    # Starte die Y-Achse des Zoom-In-Plots bei 5 kWh
    ax_zoom.set_ylim([4.92, None])

    # Gleiche Abstände zwischen den Subplots und den Rändern, aber enger zusammen
    plt.subplots_adjust(left=0.07, right=0.93, top=0.88, bottom=0.12)

    # Legende für WP-Leistung
    wp_legend = [Patch(facecolor=colors[idx], edgecolor='black',
                       label=f'{locale.format_string("%.2f", identifier)} {labels["unit"]}') for
                 idx, identifier in enumerate(identifiers)]
    pumpe_legend = [
        Patch(facecolor=colors[idx], edgecolor='black', hatch=hatches[idx],
              label=f'{locale.format_string("%.2f", identifier)} {labels["unit"]}') for
        idx, identifier in enumerate(identifiers)]

    # WP-Leistung und Pumpe-Leistung Legenden hinzufügen
    wp_leg = ax_zoom.legend(handles=wp_legend, loc='upper left', fontsize=12, title='el. Energie WP')
    pumpe_leg = ax_zoom.legend(handles=pumpe_legend, loc='upper center', fontsize=12, title='el. Energie Pumpe')

    # Sicherstellen, dass beide Legenden gleichzeitig angezeigt werden
    ax_zoom.add_artist(wp_leg)
    ax_zoom.add_artist(pumpe_leg)

    # Plot speichern
    plot_and_save(fig, os.path.join(output_dir, f"{script_name}_{labels['filename']}"))

    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    output_dir = os.path.join(results_base_dir_1, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Hatches für die Schraffur der Pumpe-Leistung
    hatches = ['/', '\\', 'x']

    # Plot für Temperaturspreizung mit gestapeltem Balkendiagramm
    plot_sum_comparison(results_base_dir_1, output_dir, script_name,
                        identifiers=[5, 8, 10],
                        labels={"title": "Temperaturdifferenzen", "label": "Delta T", "filename": "delta_t",
                                "unit": "K"},
                        colors=['orange', 'blue', 'green'],
                        hatches=hatches)
