import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import numpy as np

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None


def get_sums(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    pel_sums = []
    pel_Pumpe_sums = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]

        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
            pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600

            pel_sums.append(pel_sum)
            pel_Pumpe_sums.append(pel_Pumpe_sum)

    return drehzahlen, pel_sums, pel_Pumpe_sums


def calculate_pump_percentage(pel_sums, pel_Pumpe_sums):
    percentages = []
    for wp, pumpe in zip(pel_sums, pel_Pumpe_sums):
        total = wp + pumpe
        if total > 0:
            percentage = (pumpe / total) * 100
        else:
            percentage = 0
        percentages.append(percentage)
    return percentages


def calculate_wp_increase(pel_sums):
    increase_30_to_40 = []
    increase_40_to_50 = []
    # Sicherstellen, dass mindestens 3 Werte für 30, 40 und 50 Hz vorhanden sind
    if len(pel_sums) >= 3:
        if pel_sums[0] > 0:
            increase_30_to_40.append(((pel_sums[1] - pel_sums[0]) / pel_sums[0]) * 100)
        else:
            increase_30_to_40.append(0)
        if pel_sums[1] > 0:
            increase_40_to_50.append(((pel_sums[2] - pel_sums[1]) / pel_sums[1]) * 100)
        else:
            increase_40_to_50.append(0)
    return increase_30_to_40, increase_40_to_50


def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")


def save_percentages_to_csv(output_dir, filename, data):
    df = pd.DataFrame(data)
    output_file = os.path.join(output_dir, filename)
    df.to_csv(output_file, index=False, sep=';', decimal=',')
    print(f"Prozentuale Anteile in CSV gespeichert: {output_file}")


def plot_sum_comparison(results_base_dir, output_dir, script_name, identifiers, labels, colors, hatches):
    fig = plt.figure(figsize=(24, 12))

    # Erstelle die Subplots: Links gestapelte Plots für Zoom-In und Gesamtleistung, Rechts für Exergie
    ax_zoom = plt.subplot2grid((2, 2), (0, 0))  # Linker oberer Subplot (Zoom-In)
    ax_total = plt.subplot2grid((2, 2), (1, 0))  # Linker unterer Subplot (Gesamtleistung)
    ax_exergie = plt.subplot2grid((2, 2), (0, 1), rowspan=2)  # Rechter Subplot (Exergie über 2 Reihen)

    fig.suptitle(f'Vergleich der elektrischen Energie: WP + Pumpe vs. WP ({labels["title"]})', y=0.95, fontsize=16)

    bar_width = 0.25  # Breite der Balken
    index = np.arange(3)  # Positionen für 30, 40 und 50 Hz

    results_data = []  # Hier speichern wir die prozentualen Anteile zur CSV-Erstellung

    for idx, identifier in enumerate(identifiers):
        system_results_df, layer_results_df = read_csv_files(results_base_dir, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums = get_sums(system_results_df, identifier)
            if drehzahlen:
                # Für den gestapelten Balkendiagramm-Plot (Zoom-In Bereich)
                drehzahlen_zoom = drehzahlen[:3]
                pel_sums_zoom = pel_sums[:3]
                pel_Pumpe_sums_zoom = pel_Pumpe_sums[:3]
                combined_sums_zoom = [wp + pumpe for wp, pumpe in zip(pel_sums_zoom, pel_Pumpe_sums_zoom)]

                # Gestapelte Balken: WP-Leistung (unten) + Pumpe-Leistung (oben, schraffiert)
                ax_zoom.bar(index + idx * bar_width, pel_sums_zoom, bar_width, color=colors[idx],
                            label=f'WP {labels["label"]} {locale.format_string("%.2f", identifier)}')
                ax_zoom.bar(index + idx * bar_width, pel_Pumpe_sums_zoom, bar_width, bottom=pel_sums_zoom,
                            hatch=hatches[idx], color=colors[idx])

                # Linker Subplot (unten): Gesamtleistung (Linienplot mit Punkten)
                combined_sums_total = [wp + pumpe for wp, pumpe in zip(pel_sums, pel_Pumpe_sums)]
                ax_total.plot(drehzahlen, combined_sums_total, marker='o', linestyle='-',
                              label=f'{identifier} {labels["unit"]}', color=colors[idx])

                # Rechter Subplot: Exergie im Speicher (Linienplot mit Punkten)
                sum_diff_exergies = [system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]['Sum_Differences_Exergies'].values[0] / 1000 / 3600
                                     for hz in drehzahlen]
                ax_exergie.plot(drehzahlen, sum_diff_exergies, marker='o', linestyle='-',
                                label=f'Exergie {identifier}', color=colors[idx])

                # Berechne den prozentualen Anteil der Pumpe an der Gesamtleistung für die Zoom-In Drehzahlen (30, 40, 50 Hz)
                pump_percentages = calculate_pump_percentage(pel_sums_zoom, pel_Pumpe_sums_zoom)

                # Berechne den prozentualen Anstieg der WP-Leistung
                wp_increase_30_to_40, wp_increase_40_to_50 = calculate_wp_increase(pel_sums_zoom)

                # Speichere die Ergebnisse für die CSV-Datei
                results_data.append({
                    "Massenstrom": f"{identifier} {labels['unit']}",
                    "Drehzahl_30_Hz": f"{pump_percentages[0]:.2f}%",
                    "Drehzahl_40_Hz": f"{pump_percentages[1]:.2f}%",
                    "Drehzahl_50_Hz": f"{pump_percentages[2]:.2f}%",
                    "WP Anstieg 30->40 Hz": f"{wp_increase_30_to_40[0]:.2f}%" if wp_increase_30_to_40 else "n/a",
                    "WP Anstieg 40->50 Hz": f"{wp_increase_40_to_50[0]:.2f}%" if wp_increase_40_to_50 else "n/a"
                })

                # Anzeige der Ergebnisse im Plot
                for i, percentage in enumerate(pump_percentages):
                    ax_zoom.text(index[i] + idx * bar_width, combined_sums_zoom[i] + 0.05,
                                 f"{percentage:.2f}%", fontsize=12, ha='center')

    # Einstellungen für den Zoom-In Subplot (oben links)
    ax_zoom.set_ylabel('el. Energie in kWh', fontsize=16)
    ax_zoom.grid(False)
    ax_zoom.set_xticks(index + bar_width)
    ax_zoom.set_xticklabels([30, 40, 50])
    ax_zoom.tick_params(axis='both', which='major', labelsize=16)

    # Starte die Y-Achse des Zoom-In-Plots bei 5 kWh
    ax_zoom.set_ylim([4.92, None])

    # Einstellungen für den Gesamt-Summen Subplot (unten links)
    ax_total.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax_total.set_ylabel('el. Energie in kWh', fontsize=16)
    ax_total.grid(False)
    ax_total.tick_params(axis='both', which='major', labelsize=16)
    ax_total.set_ylim([4.78, 7.6])

    # Einstellungen für den Exergie Subplot (rechts)
    ax_exergie.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax_exergie.set_ylabel('Exergie im Speicher in kWh', fontsize=16)
    ax_exergie.grid(False)
    ax_exergie.tick_params(axis='both', which='major', labelsize=16)

    # Gleiche Abstände zwischen den Subplots und den Rändern, aber enger zusammen
    plt.subplots_adjust(left=0.07, right=0.93, top=0.88, bottom=0.12, wspace=0.3)

    # Legende für WP-Leistung
    wp_legend = [Patch(facecolor=colors[idx], edgecolor='black',
                       label=f'{locale.format_string("%.2f", identifier)} {labels["unit"]}') for
                 idx, identifier in enumerate(identifiers)]
    pumpe_legend = [
        Patch(facecolor=colors[idx], edgecolor='black', hatch=hatches[idx],
              label=f'{locale.format_string("%.2f", identifier)} {labels["unit"]}') for
        idx, identifier in enumerate(identifiers)]

    # WP-Leistung und Pumpe-Leistung Legenden hinzufügen
    wp_leg = ax_zoom.legend(handles=wp_legend, loc='upper center', fontsize=12, title='el. Energie WP')
    pumpe_leg = ax_zoom.legend(handles=pumpe_legend, loc='upper right', fontsize=12, title='el. Energie Pumpe')

    # Sicherstellen, dass beide Legenden gleichzeitig angezeigt werden
    ax_zoom.add_artist(wp_leg)
    ax_zoom.add_artist(pumpe_leg)

    # Legenden für das Linienplot im linken unteren Subplot (Gesamtleistung) und rechten Subplot (Exergie)
    custom_legend_lines_total = [
        Line2D([0], [0], marker='o', linestyle='-', color=color, lw=2,
               label=f'{identifier} {labels["unit"]}')
        for color, identifier in zip(colors, identifiers)
    ]
    custom_legend_lines_exergie = [
        Line2D([0], [0], marker='o', linestyle='-', color=color, lw=2,
               label=f'Exergie {identifier}')
        for color, identifier in zip(colors, identifiers)
    ]

    # Legenden hinzufügen für Gesamtleistung und Exergie
    ax_total.legend(handles=custom_legend_lines_total, loc='best', fontsize=12, title='Gesamtleistung')
    ax_exergie.legend(handles=custom_legend_lines_exergie, loc='best', fontsize=12, title='Exergie')

    # Plot speichern
    plot_and_save(fig, os.path.join(output_dir, f"{script_name}_{labels['filename']}"))

    # Speichere die prozentualen Anteile in einer CSV-Datei
    save_percentages_to_csv(output_dir, f"{script_name}_prozentuale_Anteile.csv", results_data)

    plt.show()





if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Hatches für die Schraffur der Pumpe-Leistung
    hatches = ['/', '\\', 'x']

    # Plot für Temperaturspreizung mit gestapeltem Balkendiagramm und Gesamtleistung
    plot_sum_comparison(results_base_dir_1, output_dir, script_name,
                        identifiers=[5, 8, 10],
                        labels={"title": "Temperaturdifferenzen", "label": "Delta T", "filename": "delta_t",
                                "unit": "K"},
                        colors=['orange', 'blue', 'green'],
                        hatches=hatches)

    # Plot für Massenströme mit gestapeltem Balkendiagramm und Gesamtleistung
    plot_sum_comparison(results_base_dir_2, output_dir, script_name,
                        identifiers=[0.05, 0.15, 0.25],
                        labels={"title": "Massenströme", "label": "Massenstrom", "filename": "mass_flow",
                                "unit": "kg/s"},
                        colors=['darkred', 'red', 'grey'],
                        hatches=hatches)
