import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_time_dependent_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'results_WP_Speicher_System_(\d+)_(\d+\.\d+)\.csv')
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')

                df['DeltaT'] = df['Dateiname'].apply(lambda x: int(pattern.search(x).group(1)))
                df['Frequency'] = df['Dateiname'].apply(
                    lambda x: float(pattern.search(x).group(2)) * 100)

                data_frames.append(df)
                print(f"CSV-Datei gefunden und gelesen: {file_path}")

    return data_frames


def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)


def plot_time_dependent_results(data_frames, output_dir):
    columns_to_plot = [
        ('Wirkungsgrad_gesamt', 'Wirkungsgrad WPSS'),
        ('Wirkungsgrad Speicher', 'Wirkungsgrad Speicher'),
        ('Wirkungsgrad_WP', 'Wirkungsgrad WP')
    ]
    deltaTs = [5, 8, 10]
    frequencies = [30, 40, 50, 60, 70, 80, 90]

    for deltaT in deltaTs:
        deltaT_str = f"$\Delta T = {deltaT}K$"
        deltaT_dir = os.path.join(output_dir, f"Temperaturspreizung_{deltaT}K")
        if not os.path.exists(deltaT_dir):
            os.makedirs(deltaT_dir)

        for frequency in frequencies:
            fig, ax = plt.subplots(figsize=(16, 10))
            fig.suptitle(f'Wirkungsgrade bei {deltaT_str} und {frequency} Hz', y=0.95, fontsize=22)

            for df in data_frames:
                filtered_df = df[(df['DeltaT'] == deltaT) & (df['Frequency'] == frequency)]
                if not filtered_df.empty:
                    for column, title in columns_to_plot:
                        ax.plot(filtered_df['Sekunde'] / 3600, filtered_df[column], label=title)

            ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
            ax.set_ylabel('Wirkungsgrad', fontsize=20)
            ax.grid(False)
            ax.legend(fontsize=18, loc='lower right')
            ax.locator_params(axis='x', nbins=7)
            ax.locator_params(axis='y', nbins=7)
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.tick_params(axis='both', which='major', labelsize=18)

            for spine in ax.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            plot_filename = os.path.join(deltaT_dir, f"Wirkungsgrade_{frequency}Hz")
            fig.savefig(f"{plot_filename}.png", format='png')
            fig.savefig(f"{plot_filename}.svg", format='svg')
            print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

            plt.show()


if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy_Exergie_pro_Zeitschritt"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\PLOTS\Wirkungsgrade"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_time_dependent_results(data_frames, output_dir)
