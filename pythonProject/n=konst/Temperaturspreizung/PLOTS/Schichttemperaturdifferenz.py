import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Pfade zu den CSV-Ordnern für die verschiedenen DeltaTs
base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CSV_Results"
deltaTs = [5, 8, 10]

# Dictionary, um die Temperaturdifferenzen zu speichern
temp_differences_30hz = []
temp_differences_90hz = []

for deltaT in deltaTs:
    # Folder path for current deltaT
    folder_path = os.path.join(base_dir, f"csv_{deltaT}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    temp_diff_30hz = None
    temp_diff_90hz = None

    for csv_file_path in csv_files:
        # Extrahiere den Dateinamen ohne Erweiterungen
        name = os.path.basename(csv_file_path).replace('.csv', '')

        # Extrahiere die Frequenz explizit aus dem Dateinamen
        frequency_str = name.split('_')[-1]

        try:
            # Konvertiere die Frequenz in eine Gleitkommazahl
            frequency = float(frequency_str)
            print(f"Extrahierte Frequenz: {frequency}")  # Debug-Ausgabe zur Überprüfung
        except ValueError:
            print(f"Fehler beim Konvertieren der Frequenz aus '{frequency_str}' in der Datei '{csv_file_path}'")
            continue

        # Lese die CSV-Datei
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Prüfe, ob die relevanten Spalten vorhanden sind
        if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
            print(f"Fehlende Spalten in Datei {csv_file_path}")
            continue

        # Filtere den DataFrame, um nur Daten am Ende des Prozesses zu behalten
        df_end = df.iloc[-1]  # Nehme die letzte Zeile an

        # Konvertiere die Temperaturen der Schichten von Kelvin in Celsius
        temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
        temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15

        # Berechne die Temperaturdifferenz zwischen Schicht 10 und Schicht 1
        temp_difference = temp_layer_10 - temp_layer_1

        # Speichere die Temperaturdifferenz je nach Frequenz
        if frequency == 0.3:
            temp_diff_30hz = temp_difference
        elif frequency == 0.9:
            temp_diff_90hz = temp_difference

    # Füge die Temperaturdifferenzen für den aktuellen DeltaT hinzu, falls vorhanden
    if temp_diff_30hz is not None:
        temp_differences_30hz.append(temp_diff_30hz)
    else:
        temp_differences_30hz.append(0)  # Wenn keine Daten vorhanden sind, füge 0 hinzu

    if temp_diff_90hz is not None:
        temp_differences_90hz.append(temp_diff_90hz)
    else:
        temp_differences_90hz.append(0)  # Wenn keine Daten vorhanden sind, füge 0 hinzu

# Erstellung des Balkendiagramms
fig, ax = plt.subplots(figsize=(10, 6))
bar_width = 0.25  # Balkenbreite

index = np.arange(len(deltaTs))

# Balken für 30 Hz (rot) und 90 Hz (grau)
bars_30hz = ax.bar(index, temp_differences_30hz, bar_width, label='30 Hz', color='red')
bars_90hz = ax.bar(index + bar_width, temp_differences_90hz, bar_width, label='90 Hz', color='gray')

# Zahlen über den Balken anzeigen
for bar in bars_30hz:
    yval = bar.get_height()
    ax.text(bar.get_x() + bar.get_width() / 2, yval, f'{yval:.2f}', va='bottom', ha='center')

for bar in bars_90hz:
    yval = bar.get_height()
    ax.text(bar.get_x() + bar.get_width() / 2, yval, f'{yval:.2f}', va='bottom', ha='center')


# Achsenbeschriftungen und Titel
ax.set_xlabel('Delta T (K)')
ax.set_ylabel('Temperaturdifferenz ΔT (°C)')
ax.set_title('Temperaturdifferenz zwischen Schicht 10 und Schicht 1 für verschiedene Delta Ts')

# X-Achsen-Beschriftungen mit Komma anstelle von Punkt
ax.set_xticks(index + bar_width / 2)
ax.set_xticklabels([f'{dt}'.replace('.', ',') for dt in deltaTs])

# Legende und Gitterlinien
ax.legend()
ax.grid(False)

# Anpassung der Achsenbeschriftung für Komma statt Punkt
ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.1f}'.replace('.', ',')))

# Speicherort für den Plot
script_name = os.path.splitext(os.path.basename(__file__))[0]  # Name des Skripts ohne Erweiterung
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\PLOTS"
output_dir = os.path.join(results_base_dir, script_name)

# Erstelle das Verzeichnis, wenn es nicht existiert
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Speicherpfade für PNG und SVG definieren
plot_path_png = os.path.join(output_dir, f"temperature_diff_layers_deltaT.png")
plot_path_svg = os.path.join(output_dir, f"temperature_diff_layers_deltaT.svg")

# Plot als PNG speichern
plt.savefig(plot_path_png, format='png')

# Plot als SVG speichern
plt.savefig(plot_path_svg, format='svg')

# Plot anzeigen
plt.show()

# Ausgabe der Speicherorte
print(f"Das Balkendiagramm wurde unter '{plot_path_png}' und '{plot_path_svg}' gespeichert.")
