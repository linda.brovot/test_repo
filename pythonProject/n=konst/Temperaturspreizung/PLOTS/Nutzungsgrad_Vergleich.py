import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, deltaT):
    folder_path = os.path.join(results_base_dir, f"results_{deltaT}")
    csv_file = os.path.join(folder_path, f"system_results_{deltaT}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für deltaT {deltaT} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für deltaT {deltaT} nicht gefunden: {csv_file}")
        return None

def combined_plot(results_base_dir, output_dir, script_name):
    frequencies_combined = []
    eta_system_combined = {}
    hours_combined = {}

    for deltaT in [5, 8, 10]:
        data_frame = read_csv_files(results_base_dir, deltaT)
        eta_system_list = []
        hours_list = []
        frequencies = []

        if data_frame is not None:
            for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
                filtered_df = data_frame[data_frame['Name'].str.contains(f"_{deltaT}_{value}.csv")]
                if not filtered_df.empty:
                    eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                    hours = filtered_df['Hours'].values[0]

                    if not (pd.isna(eta_WPSS) or pd.isna(hours)):
                        eta_system_list.append(eta_WPSS)
                        hours_list.append(hours)
                        frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

        if frequencies:
            eta_system_combined[deltaT] = eta_system_list
            hours_combined[deltaT] = hours_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    if frequencies_combined:
        fig, ax1 = plt.subplots(figsize=(13, 8))  # Vergrößern der Breite und Höhe des Plots
        fig.suptitle('Exergetischer Nutzungsgrad bei unterschiedlichen Temperaturdifferenzen', fontsize=18, y=0.95)


        bar_width = 0.5
        offsets = [-bar_width, 0, bar_width]
        colors = ['gray', 'silver', 'darkgrey']
        for deltaT, offset, color in zip([5, 8, 10], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], hours_combined[deltaT], width=bar_width, color=color, label=f'Beladedauer bei ΔT={deltaT}K')

        ax2 = ax1.twinx()
        for deltaT, color in zip([5, 8, 10], ['black', 'red', 'darkred']):
            ax2.plot(frequencies_combined, eta_system_combined[deltaT], marker='^', linestyle='-.', color=color, label=f'Nutzungsgrad bei ΔT={deltaT}K')

        ax1.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax1.set_ylabel('Beladedauer in h', fontsize=16)
        ax2.set_ylabel('Nutzungsgrad WPSS', fontsize=16)

        # Set y-limits for ax1 (Beladedauer)
        ax1.set_ylim(0, 8)

        # Automatically determine y-limits for ax2 (Nutzungsgrad) and set ticks
        ax2.set_ylim(0.14, 0.22)  # Set the desired range
        ax2.yaxis.set_major_locator(ticker.AutoLocator())  # Automatically determine tick locations
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(
            lambda x, pos: locale.format_string('%.3f', x)))  # Adjust precision to 3 decimals if needed

        ax1.grid(False)

        lines1, labels1 = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        lines = lines1 + lines2
        labels = labels1 + labels2

        # Legende oben platzieren und in vier Spalten aufteilen
        fig.legend(lines, labels, loc='upper center', bbox_to_anchor=(0.76, 0.86), ncol=1, fontsize=13)


        ax1.tick_params(axis='both', which='major', labelsize=16)
        ax2.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Speichern des kombinierten Plots als PNG und SVG
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_png)
        plt.savefig(combined_plot_filename_svg)
        plt.show()  # Kombinierten Plot anzeigen
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    # Basisverzeichnis, in dem die Ergebnisdateien liegen
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\PLOTS"
    # Erstellen des Ordners "NutzungsgradPlot" im angegebenen Pfad
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
