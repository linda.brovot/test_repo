import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import numpy as np

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, delta_T):
    folder_path = os.path.join(results_base_dir, f"results_{delta_T}")
    csv_file = os.path.join(folder_path, f"system_results_{delta_T}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für delta_T {delta_T} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für delta_T {delta_T} nicht gefunden: {csv_file}")
        return None

def plot_data(data_frame, delta_T):
    eta_wp_list = []
    eta_sp_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{delta_T}_{value}.csv")]
            if not filtered_df.empty:
                eta_wp = filtered_df['Nutzungsgrad_WP_mitPumpe'].values[0]
                eta_sp = filtered_df['Eta_SP'].values[0]

                if not (pd.isna(eta_wp) or pd.isna(eta_sp)):
                    eta_wp_list.append(eta_wp)
                    eta_sp_list.append(eta_sp)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, eta_wp_list, eta_sp_list

def add_polynomial_trendline(ax, x_data, y_data, color):
    z = np.polyfit(x_data, y_data, 2)
    p = np.poly1d(z)
    x_smooth = np.linspace(min(x_data), max(x_data), 500)
    y_smooth = p(x_smooth)
    ax.plot(x_smooth, y_smooth, linestyle='--', color=color, label=f"Trendkurve", alpha=0.7)

def calculate_percent_difference(values1, values2):
    return [(v2 - v1) / v1 * 100 if v1 != 0 else None for v1, v2 in zip(values1, values2)]

def calculate_percent_difference_sequential(values):
    return [(v2 - v1) / v1 * 100 if v1 != 0 else None for v1, v2 in zip(values[:-1], values[1:])]

def combined_plot(results_base_dir, output_dir, script_name):
    eta_wp_combined = {}
    eta_sp_combined = {}
    frequencies_combined = []

    for delta_T in [5, 8, 10]:
        data_frame = read_csv_files(results_base_dir, delta_T)
        frequencies, eta_wp_list, eta_sp_list = plot_data(data_frame, delta_T)
        if frequencies:
            eta_wp_combined[delta_T] = eta_wp_list
            eta_sp_combined[delta_T] = eta_sp_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    if frequencies_combined:
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(13, 6), gridspec_kw={'width_ratios': [2, 2]})

        bar_width = 1.3
        offsets = [-bar_width, 0, bar_width]
        colors = ['darkred', 'red', 'grey']

        # Subplot 1: Nutzungsgrad WP
        for delta_T, offset, color in zip([5, 8, 10], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], eta_wp_combined[delta_T], width=bar_width, color=color,
                    label=f'ΔT={delta_T}K')

        # Polynomiale Trendkurve für den Nutzungsgrad WP hinzufügen (basierend auf den höchsten dunkelroten Balken, delta_T = 5K)
        highest_eta_wp = eta_wp_combined[5]
        add_polynomial_trendline(ax1, frequencies_combined, highest_eta_wp, 'black')

        # Berechnung der prozentualen Abweichungen für WP
        wp_diff_sequential_5 = calculate_percent_difference_sequential(eta_wp_combined[5])

        # Berechnung des größten prozentualen Unterschieds zwischen den Drehzahlen für WP (delta_T = 5K)
        max_wp_diff_value = max(wp_diff_sequential_5, key=lambda x: abs(x))
        max_wp_diff_index = wp_diff_sequential_5.index(max_wp_diff_value)

        # Zeichnen eines schrägen Pfeils bei der größten prozentualen Änderung für WP
        arrow_props = dict(facecolor='black', arrowstyle='->', lw=1.5)
        ax1.annotate(f"{max_wp_diff_value:.1f}%",
                     xy=(frequencies_combined[max_wp_diff_index + 1], eta_wp_combined[5][max_wp_diff_index + 1]),
                     xytext=(frequencies_combined[max_wp_diff_index], eta_wp_combined[5][max_wp_diff_index] + 0.03),
                     arrowprops=arrow_props, fontsize=12, ha='center', color='black')

        ax1.set_ylabel('Nutzungsgrad WP', fontsize=14)
        ax1.set_xlabel('Drehzahl in Hz', fontsize=14)
        ax1.legend(loc='best', fontsize=12)
        ax1.grid(False)
        ax1.tick_params(axis='both', which='major', labelsize=12)
        ax1.set_ylim([0, 0.6])

        # Subplot 2: Nutzungsgrad Speicher
        for delta_T, offset, color in zip([5, 8, 10], offsets, colors):
            ax2.bar([x + offset for x in frequencies_combined], eta_sp_combined[delta_T], width=bar_width, color=color,
                    label=f'ΔT={delta_T}K')

        # Polynomiale Trendkurve für den Nutzungsgrad Speicher (alle Daten)
        all_eta_sp = [eta for delta_T in eta_sp_combined for eta in eta_sp_combined[delta_T]]
        add_polynomial_trendline(ax2, frequencies_combined * 3, all_eta_sp, 'black')

        # Berechnung der prozentualen Abweichungen für Speicher
        sp_diff_sequential_5 = calculate_percent_difference_sequential(eta_sp_combined[5])

        # Berechnung des größten prozentualen Unterschieds zwischen den Drehzahlen für Speicher (delta_T = 5K)
        max_sp_diff_value = max(sp_diff_sequential_5, key=lambda x: abs(x))
        max_sp_diff_index = sp_diff_sequential_5.index(max_sp_diff_value)

        # Zeichnen eines schrägen Pfeils bei der größten prozentualen Änderung für Speicher
        ax2.annotate(f"{max_sp_diff_value:.1f}%",
                     xy=(frequencies_combined[max_sp_diff_index + 1], eta_sp_combined[5][max_sp_diff_index + 1]),
                     xytext=(frequencies_combined[max_sp_diff_index], eta_sp_combined[5][max_sp_diff_index] + 0.03),
                     arrowprops=arrow_props, fontsize=12, ha='center', color='black')

        ax2.set_ylabel('Nutzungsgrad Speicher', fontsize=14)
        ax2.set_xlabel('Drehzahl in Hz', fontsize=14)
        ax2.legend(loc='best', fontsize=12)
        ax2.grid(False)
        ax2.tick_params(axis='both', which='major', labelsize=12)
        ax2.set_ylim([0, 1])

        # Berechnung des größten Unterschieds zwischen Temperaturdeltas (5K vs. 10K)
        wp_diff_5_to_10 = calculate_percent_difference(eta_wp_combined[5], eta_wp_combined[10])
        max_wp_diff_5_to_10_value = max(wp_diff_5_to_10, key=lambda x: abs(x))
        max_wp_diff_5_to_10_index = wp_diff_5_to_10.index(max_wp_diff_5_to_10_value)

        # Markiere den größten Unterschied für WP (5K vs. 10K)
        ax1.text(frequencies_combined[max_wp_diff_5_to_10_index],
                 eta_wp_combined[5][max_wp_diff_5_to_10_index] + 0.02,
                 f"{max_wp_diff_5_to_10_value:.1f}%",
                 ha='center', color='black', fontsize=12)

        sp_diff_5_to_10 = calculate_percent_difference(eta_sp_combined[5], eta_sp_combined[10])
        max_sp_diff_5_to_10_value = max(sp_diff_5_to_10, key=lambda x: abs(x))
        max_sp_diff_5_to_10_index = sp_diff_5_to_10.index(max_sp_diff_5_to_10_value)

        # Markiere den größten Unterschied für Speicher (5K vs. 10K)
        ax2.text(frequencies_combined[max_sp_diff_5_to_10_index],
                 eta_sp_combined[10][max_sp_diff_5_to_10_index] + 0.02,
                 f"{max_sp_diff_5_to_10_value:.1f}%",
                 ha='center', color='black', fontsize=12)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Anwendung von tight_layout und subplots_adjust
        plt.tight_layout(rect=[0, 0, 1, 0.95])
        plt.subplots_adjust(wspace=0.15)

        # Speichern des kombinierten Plots als PNG und SVG
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_png)
        plt.savefig(combined_plot_filename_svg)
        plt.show()
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

        # Speichern der Abweichungsdaten in einer CSV-Datei
        df_wp_sp_diff = pd.DataFrame({
            'Frequenz (Hz)': frequencies_combined,
            'WP Abweichung 5K zu 10K (%)': wp_diff_5_to_10,
            'SP Abweichung 5K zu 10K (%)': sp_diff_5_to_10,
            'WP Sequential 5K (%)': [None] + wp_diff_sequential_5,  # None für den ersten Wert, da kein vorheriger Wert existiert
            'SP Sequential 5K (%)': [None] + sp_diff_sequential_5
        })

        csv_filename = os.path.join(output_dir, f"{script_name}_abweichungen.csv")
        df_wp_sp_diff.to_csv(csv_filename, index=False, sep=';', decimal=',')
        print(f"Abweichungsdaten gespeichert: {csv_filename}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\PLOTS"
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
