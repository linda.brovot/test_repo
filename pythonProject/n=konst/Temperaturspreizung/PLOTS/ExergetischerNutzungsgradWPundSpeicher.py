import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, deltaT):
    folder_path = os.path.join(results_base_dir, f"results_{deltaT}")
    csv_file = os.path.join(folder_path, f"system_results_{deltaT}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für deltaT {deltaT} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für deltaT {deltaT} nicht gefunden: {csv_file}")
        return None

def plot_data(data_frame, deltaT, output_dir, script_name):
    eta_wp_list = []
    eta_sp_list = []
    eta_system_list = []
    hours_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{deltaT}_{value}.csv")]
            if not filtered_df.empty:
                eta_wp = filtered_df['Nutzungsgrad_WP'].values[0]
                eta_sp = filtered_df['Eta_SP'].values[0]
                eta_wpss = filtered_df['Eta_WPSS'].values[0]
                hours = filtered_df['Hours'].values[0]

                if not (pd.isna(eta_wp) or pd.isna(eta_sp) or pd.isna(hours)):
                    eta_wp_list.append(eta_wp)
                    eta_sp_list.append(eta_sp)
                    eta_system_list.append(eta_wpss)
                    hours_list.append(hours)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    if len(hours_list) > 0 and len(eta_wp_list) > 0 and len(eta_sp_list) > 0 and len(eta_system_list) > 0:
        fig, ax1 = plt.subplots(figsize=(13, 8))
        fig.suptitle(f'Nutzungsgrad bei konstanter Temperaturspreizung von {deltaT} K', fontsize=16, y=0.95)

        bar_width = 2
        ax1.bar(frequencies, hours_list, width=bar_width, color='lightgrey', label='Beladedauer')

        ax2 = ax1.twinx()
        ax2.plot(frequencies, eta_wp_list, marker='o', linestyle='-', color='black', label='Nutzungsgrad WP')
        ax2.plot(frequencies, eta_sp_list, marker='o', linestyle='-', color='grey', label='Nutzungsgrad Speicher')
        ax2.plot(frequencies, eta_system_list, marker='o', linestyle='-', color='red', label='Nutzungsgrad WPSS')

        ax2.set_ylabel('Nutzungsgrad', fontsize=16)
        ax1.set_ylim(0, 9)
        ax2.set_ylim(0,0.9 )

        lines1, labels1 = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        lines = lines1 + lines2
        labels = labels1 + labels2
        ax1.legend(lines, [label.replace('.', ',') for label in labels], loc='upper center',
                   bbox_to_anchor=(0.84, 0.98),
                   ncol=1, fontsize=13)

        ax1.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax1.set_ylabel('Beladedauer in h', fontsize=16)
        ax1.grid(False)

        ax1.tick_params(axis='both', which='major', labelsize=16)
        ax2.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Speichern des Plots als PNG und SVG
        plot_filename_png = os.path.join(output_dir, f"{script_name}_deltaT_{deltaT}.png")
        plot_filename_svg = os.path.join(output_dir, f"{script_name}_deltaT_{deltaT}.svg")
        plt.savefig(plot_filename_png)
        plt.savefig(plot_filename_svg)
        plt.show()  # Plot anzeigen
        plt.close(fig)
        print(f"Plot gespeichert: {plot_filename_png} und {plot_filename_svg}")
        return frequencies, eta_system_list, hours_list  # Rückgabe der Daten für den kombinierten Plot
    else:
        print(f"Die Listen sind leer für deltaT {deltaT}. Überprüfen Sie die Daten in den CSV-Dateien.")
        return None, None, None

def combined_plot(results_base_dir, output_dir, script_name):
    frequencies_combined = []
    eta_system_combined = {}
    hours_combined = {}

    for deltaT in [5, 8, 10]:
        data_frame = read_csv_files(results_base_dir, deltaT)
        frequencies, eta_system_list, hours_list = plot_data(data_frame, deltaT, output_dir, script_name)
        if frequencies is not None:
            eta_system_combined[deltaT] = eta_system_list
            hours_combined[deltaT] = hours_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    if frequencies_combined:
        fig, ax1 = plt.subplots(figsize=(13, 8)) # Vergrößern der Breite und Höhe des Plots
        fig.suptitle('Exergetischer Nutzungsgrad bei unterschiedlichen Temperaturspreizungen', fontsize=16, y=0.95)


        bar_width = 0.5
        offsets = [-bar_width, 0, bar_width]
        colors = ['gray', 'silver', 'darkgrey']
        for deltaT, offset, color in zip([5, 8, 10], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], hours_combined[deltaT], width=bar_width, color=color, label=f'Beladedauer bei {deltaT} K')

        ax2 = ax1.twinx()
        for deltaT, color in zip([5, 8, 10], ['black', 'red', 'darkred']):
            ax2.plot(frequencies_combined, eta_system_combined[deltaT], marker='^', linestyle='-.', color=color, label=f'Nutzungsgrad bei {deltaT} K')

        ax1.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax1.set_ylabel('Beladedauer in h', fontsize=16)
        ax2.set_ylabel('Nutzungsgrad WPSS', fontsize=16)
        ax1.set_ylim(0, 9)
        ax2.set_ylim(0.163, 0.24)  # Bereich der rechten y-Achse festlegen
        ax2.yaxis.set_major_locator(ticker.MultipleLocator(0.02))  # Setzen der Major Ticks für die rechte y-Achse
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.2f', x)))

        ax1.grid(False)

        lines1, labels1 = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        lines = lines1 + lines2
        labels = labels1 + labels2

        # Legende oben platzieren und in vier Spalten aufteilen
        fig.legend(lines, labels, loc='upper center', bbox_to_anchor=(0.78, 0.86), ncol=1, fontsize=13)



        ax1.tick_params(axis='both', which='major', labelsize=16)
        ax2.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Speichern des kombinierten Plots als PNG und SVG
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_png)
        plt.savefig(combined_plot_filename_svg)
        plt.show()  # Kombinierten Plot anzeigen
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    # Basisverzeichnis, in dem die Ergebnisdateien liegen
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\PLOTS"
    # Erstellen des Ordners "NutzungsgradPlot" im angegebenen Pfad
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")


    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)