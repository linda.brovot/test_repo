import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_time_dependent_csv_files(base_dir):
    data_frames = []
    # Muster zum Extrahieren der Frequenz und der Temperaturdifferenz aus dem Spaltenwert 'Dateiname'
    pattern = re.compile(r'results_WP_Speicher_System_(\d+)_(\d+\.\d+)\.csv')
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')

                # Extrahiere die Frequenz und die Temperaturdifferenz aus der Spalte 'Dateiname'
                df['DeltaT'] = df['Dateiname'].apply(lambda x: int(pattern.search(x).group(1)))
                df['Frequency'] = df['Dateiname'].apply(
                    lambda x: float(pattern.search(x).group(2)) * 100)  # Umrechnung auf Hz (0.3 => 30)

                data_frames.append(df)
                print(f"CSV-Datei gefunden und gelesen: {file_path}")

    return data_frames


def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)


def plot_time_dependent_results(data_frames, output_dir):
    columns_to_plot = [
        # ('Wirkungsgrad_WP', 'Wirkungsgrad'),
        # ('Leistung_WP', 'Leistung'),
        # ('Exergie Wärmestrom_WP', 'Exergie Wärmestrom WP'),
        # ('Exergie Wärmestrom_Speicher', 'Exergie Wärmestrom Speicher'),
        # ('Verdichterdrehzahl', 'Verdichterdrehzahl'),
        # ('Vorlauftemperatur', 'Vorlauftemperatur'),
        # ('Exergie_Differenz_gesamt', 'Exergie im Speicher pro Zeitschritt'),
        # ('Wirkungsgrad_gesamt', 'Wirkungsgrad WPSS'),
        ('kummulierte Exergie kWh', 'kummulierte Exergie in kWh')
    ]
    deltaTs = [5, 8, 10]
    frequencies = [30, 40, 50, 60, 70, 80, 90]  # Umrechnung von 0.3, 0.4,... auf 30, 40,...

    for column, title in columns_to_plot:
        for deltaT in deltaTs:
            deltaT_str = f"$\Delta T = {deltaT}K$"  # Delta T als Formelzeichen
            deltaT_dir = os.path.join(output_dir, f"Temperaturspreizung_{deltaT}K")
            if not os.path.exists(deltaT_dir):
                os.makedirs(deltaT_dir)

            fig, ax = plt.subplots(figsize=(16, 10))
            fig.suptitle(f'{title} bei {deltaT_str}', y=0.95, fontsize=22)

            for frequency in frequencies:
                for df in data_frames:
                    # Filterung der DataFrames basierend auf 'DeltaT' und 'Frequency'
                    filtered_df = df[(df['DeltaT'] == deltaT) & (df['Frequency'] == frequency)]
                    if not filtered_df.empty:
                        freq_label = locale.format_string('%.0f', frequency)
                        print(f"Plotte Daten für Frequenz {frequency:.0f} Hz, {deltaT_str}, Spalte {column}")
                        ax.plot(filtered_df['Sekunde'] / 3600, filtered_df[column], label=f'${freq_label} \\ Hz$')


            ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
            ax.set_ylabel(title, fontsize=20)
            ax.grid(False)
            ax.legend(fontsize=18, loc='lower right')  # Legende fest unten rechts platzieren
            ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
            ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.tick_params(axis='both', which='major', labelsize=18)

            # Setzen der schwarzen Ränder
            for spine in ax.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            plot_filename = os.path.join(deltaT_dir, f"{title.replace(' ', '_').replace('.', ',')}")
            fig.savefig(f"{plot_filename}.png", format='png')
            fig.savefig(f"{plot_filename}.svg", format='svg')
            print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

            plt.show()


if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy_Exergie_pro_Zeitschritt"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\PLOTS\timedependency_kummuliert"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_time_dependent_results(data_frames, output_dir)
