import os
from ebcpy import DymolaAPI

def run_parameter_study(dymola_path, working_directory, model_name, modelica_package_path, aixlib_package_path, results_dir):
    try:
        # Initialisieren der Dymola API
        DYM_API = DymolaAPI(
            dymola_path=dymola_path,
            working_directory=working_directory,
            model_name=model_name,
            packages=[modelica_package_path, aixlib_package_path],
            show_window=True,
            equidistant_output=True,
            n_restart=100
        )

        # Verbindung prüfen
        if DYM_API.dymola is not None:
            print("Dymola API erfolgreich initialisiert und verbunden.")
        else:
            print("Fehler beim Initialisieren der Dymola API.")
            return False

        # Parameterbereiche definieren
        drehzahlverdichter_values = [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
        massenstrom_values = [0.05, 0.15, 0.25]

        # Verzeichnis zum Speichern der Ergebnisse erstellen, falls es nicht existiert
        os.makedirs(results_dir, exist_ok=True)

        # Simulationsparameter (explizit setzen)
        start_time = 0
        stop_time = 30000  # 25 Stunden
        output_interval = 1  # Jede Sekunde ein Ausgabeintervall

        # Zusätzliche Einstellungen, um alle Variablen zu speichern
        DYM_API.dymola.ExecuteCommand("Advanced.StoreProtectedVariables := true;")
        DYM_API.dymola.ExecuteCommand("Advanced.LogAllVariables := true;")

        # Parameter-Sweep durchführen
        for drehzahlverdichter in drehzahlverdichter_values:
            for massenstrom in massenstrom_values:
                try:
                    # Parameter im Modell setzen
                    DYM_API.dymola.ExecuteCommand(f"drehzahlverdichter := {drehzahlverdichter};")
                    DYM_API.dymola.ExecuteCommand(f"massenstrom := {massenstrom};")

                    # Ergebnisdatei definieren
                    result_file = os.path.join(results_dir, f"results_WP_Speicher_System_{massenstrom}_{drehzahlverdichter}.mat")

                    # Simulation durchführen mit explizit gesetzter Simulationszeit und Ausgabeintervall
                    result = DYM_API.dymola.simulateExtendedModel(
                        problem=model_name,
                        startTime=start_time,
                        stopTime=stop_time,
                        outputInterval=output_interval,
                        resultFile=result_file
                    )

                    if result:
                        print(f"Simulation erfolgreich abgeschlossen: massenstrom={massenstrom}, drehzahlverdichter={drehzahlverdichter}")
                        print(f"Ergebnisse gespeichert unter {result_file}")
                    else:
                        print(f"Simulation fehlgeschlagen: massenstrom={massenstrom}, drehzahlverdichter={drehzahlverdichter}")
                        log = DYM_API.dymola.getLastErrorLog()
                        print(f"Dymola Fehlerprotokoll: {log}")

                except Exception as e:
                    print(f"Fehler während der Simulation: massenstrom={massenstrom}, drehzahlverdichter={drehzahlverdichter}")
                    print(f"Fehlermeldung: {e}")

        return True

    except Exception as e:
        print(f"Fehler während der Ausführung: {e}")
        return False

    finally:
        if 'DYM_API' in locals():
            # Dymola-Session schließen, wenn DYM_API definiert ist
            DYM_API.dymola.close()


if __name__ == "__main__":
    # Pfade und Parameter für die Simulation festlegen
    dymola_path = r"C:\Program Files\Dymola 2023"
    working_directory = r"C:\Users\hro-lbr\Documents\Dymola"
    model_name = "WP_Speicher_System.FINAL.WP_Speicher_System_m_flow"
    modelica_package_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\simple_hp_tes\WP_Speicher_System.mo"
    aixlib_package_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\AixLib\AixLib\package.mo"
    results_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\MAT_Results"

    # Parameterstudie durchführen
    success = run_parameter_study(dymola_path, working_directory, model_name, modelica_package_path, aixlib_package_path, results_dir)

    if success:
        print("Parameterstudie erfolgreich durchgeführt.")
    else:
        print("Fehler bei der Durchführung der Parameterstudie.")
