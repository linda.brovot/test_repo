import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()

def read_time_dependent_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Drehzahl
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    frequency = float(match.group(1))
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Frequency'] = frequency  # Hinzufügen der extrahierten Drehzahl
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_wirkungsgrad_wpss(data_frames, output_dir):
    column_to_plot = 'Wirkungsgrad_gesamt'
    title = 'Wirkungsgrad WPSS'
    mass_flows = [0.15, 0.25]
    frequencies = [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    for mass_flow in mass_flows:
        plt.figure(figsize=(16, 9))
       # plt.title(f'{title} für Massenstrom {str(mass_flow).replace(".", ",")} kg/s', fontsize=22)

        for df in data_frames:
            for frequency in frequencies:
                filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
                if not filtered_df.empty:
                    freq_label = locale.format_string('%.0f', frequency * 100)
                    print(
                        f"Plotte Daten für Drehzahl {frequency:.0f} Hz, Massenstrom {mass_flow}, Spalte {column_to_plot}")
                    smoothed_data = moving_average(filtered_df[column_to_plot], window_size=5)  # Glätte die Daten
                    plt.plot(filtered_df['Sekunde'] / 3600, smoothed_data,
                             label=f'{freq_label} Hz')  # Hier die LaTeX-Formatierung entfernt

        plt.xlabel('Beladedauer in h', fontsize=18)
        plt.ylabel(title, fontsize=18)
        plt.grid(False)
        plt.legend(fontsize=14)
        plt.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
        plt.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        plt.tick_params(axis='both', which='major', labelsize=14)
        plt.tight_layout()
        # Setzen der schwarzen Ränder
        for spine in plt.gca().spines.values():
            spine.set_edgecolor('black')
            spine.set_linewidth(1.5)

        # Diagramm speichern
        plot_filename = os.path.join(output_dir, f"{title}_Massenstrom_{str(mass_flow).replace('.', ',')}".replace(' ', '_'))
        plt.savefig(f"{plot_filename}.png", format='png')
        plt.savefig(f"{plot_filename}.svg", format='svg')
        print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

        plt.show()

if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS\timedependency"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_wirkungsgrad_wpss(data_frames, output_dir)
