import matplotlib.pyplot as plt
import pandas as pd
import os
import re
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def moving_average(series, window_size):
    return series.rolling(window=window_size, min_periods=1).mean()

# Funktion zum Einlesen der CSV-Dateien und Extrahieren der Frequenz aus dem Dateinamen
def read_time_dependent_csv_files(base_dir):
    data_frames = {}
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren des Massenstroms
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            match = pattern.search(file)
            if match:
                mass_flow = float(match.group(1))  # Extrahiere den Massenstrom aus dem Dateinamen
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                df['MassFlow'] = mass_flow  # Füge den Massenstrom zur DataFrame hinzu
                print(f"CSV-Datei gefunden und gelesen: {file_path}")
                data_frames[mass_flow] = df  # Speichere die DataFrame pro Massenstrom
            else:
                print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

# Funktion zum Plotten des Wirkungsgrads für verschiedene Frequenzen und Massenströme
def plot_wirkungsgrad_wpss(data_frames, output_dir):
    column_to_plot = 'Wirkungsgrad_WP'
    title = 'Wirkungsgrad WP'
    frequencies = [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]  # Die verschiedenen Frequenzen

    for frequency in frequencies:
        fig, ax = plt.subplots(figsize=(10, 6))
        fig.suptitle(f'{title} für Frequenz {frequency} Hz', y=0.95, fontsize=22)

        for mass_flow, df in data_frames.items():
            # Filtere die Zeilen nach Frequenz (wenn in Dateinamen)
            filtered_df = df[df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")]
            if not filtered_df.empty:
                print(f"Plotte Daten für Massenstrom {mass_flow} kg/s und Frequenz {frequency} Hz")
                smoothed_data = moving_average(filtered_df[column_to_plot], window_size=5)  # Glätte die Daten
                ax.plot(filtered_df['Sekunde'] / 3600, smoothed_data, label=f'Massenstrom {mass_flow:.2f} kg/s')

        # Diagrammgestaltung
        ax.set_xlabel('Beladedauer in Stunden', fontsize=18)
        ax.set_ylabel('Wirkungsgrad', fontsize=18)
        ax.grid(False)
        ax.legend(fontsize=14)
        ax.locator_params(axis='x', nbins=7)
        ax.locator_params(axis='y', nbins=7)
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
        ax.tick_params(axis='both', which='major', labelsize=14)

        # Setzen der schwarzen Ränder
        for spine in ax.spines.values():
            spine.set_edgecolor('black')
            spine.set_linewidth(1.5)

        # Speichern des Plots
        plot_filename = os.path.join(output_dir, f"Wirkungsgrad_Frequenz_{str(frequency).replace('.', ',')}")
        fig.savefig(f"{plot_filename}.png", format='png')
        fig.savefig(f"{plot_filename}.svg", format='svg')
        print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

        plt.show()  # Schließe das Diagramm nach dem Speichern

if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Abschlussvortrag\Massenstrom\PLOTS\timedependency"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_wirkungsgrad_wpss(data_frames, output_dir)
