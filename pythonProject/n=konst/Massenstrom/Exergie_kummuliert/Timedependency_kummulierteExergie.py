import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_time_dependent_csv_files(base_dir):
    data_frames = []
    pattern = re.compile(r'results_(\d+\.\d+)\.csv')  # Muster zum Extrahieren der Drehzahl
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                match = pattern.search(file)
                if match:
                    frequency = float(match.group(1))
                    file_path = os.path.join(subdir, file)
                    df = pd.read_csv(file_path, sep=',', decimal='.')
                    df['Frequency'] = frequency  # Hinzufügen der extrahierten Drehzahl
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
                else:
                    print(f"Dateiname entspricht nicht dem erwarteten Format: {file}")
    return data_frames

def format_func(x, pos, decimal_places):
    if x.is_integer():
        return locale.format_string("%.0f", x, grouping=True)
    else:
        format_str = f"%.{decimal_places}f"
        return locale.format_string(format_str, x, grouping=True)

def plot_time_dependent_results(data_frames, output_dir):
    columns_to_plot = [
        # ('Wirkungsgrad_WP', 'Wirkungsgrad'),
        # ('Leistung_WP', 'Leistung'),
        # ('Exergie Wärmestrom_WP', 'Exergie Wärmestrom WP'),
        # ('Exergie Wärmestrom_Speicher', 'Exergie Wärmestrom Speicher'),
        # ('Verdichterdrehzahl', 'Verdichterdrehzahl'),
         #('Vorlauftemperatur', 'Vorlauftemperatur'),
        # ('Exergie_Differenz_gesamt', 'Exergie im Speicher pro Zeitschritt'),
        #('Wirkungsgrad_gesamt', 'Wirkungsgrad WPSS'),
        ('kummulierte Exergie kWh','kummulierte Exergie in kWh')
    ]
    mass_flows = [0.05, 0.15, 0.25]
    frequencies = [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    for column, title in columns_to_plot:
        for mass_flow in mass_flows:
            mass_flow_str = f"Massenstrom {str(mass_flow).replace('.', ',')}"
            mass_flow_dir = os.path.join(output_dir, mass_flow_str)
            if not os.path.exists(mass_flow_dir):
                os.makedirs(mass_flow_dir)

            fig, ax = plt.subplots(figsize=(16, 10))
            fig.suptitle(f'{title} für Massenstrom {str(mass_flow).replace(".", ",")} kg/s', y=0.95, fontsize=22)

            for df in data_frames:
                for frequency in frequencies:
                    filtered_df = df[
                        df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")].copy()  # Kopie erstellen
                    if not filtered_df.empty:
                        freq_label = locale.format_string('%.0f', frequency*100)
                        print(f"Plotte Daten für Drehzahl {frequency:.0f} Hz, Massenstrom {mass_flow}, Spalte {column}")



                        ax.plot(filtered_df['Sekunde'] / 3600, filtered_df[column], label=f'${freq_label} \\ Hz$')

            ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
            ax.set_ylabel(title + ' (°C)' if column == 'Vorlauftemperatur' else title, fontsize=20)
            ax.grid(False)
            ax.legend(fontsize=18)
            ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
            ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
            ax.tick_params(axis='both', which='major', labelsize=18)
            ax.grid(True)  # Fügt ein Gitter für beide Achsen hinzu

            # Setzen der schwarzen Ränder
            for spine in ax.spines.values():
                spine.set_edgecolor('black')
                spine.set_linewidth(1.5)

            plot_filename = os.path.join(mass_flow_dir, f"{title}".replace(' ', '_').replace('.', ','))
            fig.savefig(f"{plot_filename}.png", format='png')
            fig.savefig(f"{plot_filename}.svg", format='svg')
            print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

            plt.show()


def plot_30hz_cumulative_exergy(data_frames, output_dir):
    column = 'kummulierte Exergie kWh'
    title = 'Kummulierte Exergie bei 30 Hz'
    frequency = 0.3  # 30 Hz
    mass_flows = [0.05, 0.15, 0.25]

    fig, ax = plt.subplots(figsize=(16, 10))
    fig.suptitle(f'{title}', y=0.95, fontsize=22)

    for mass_flow in mass_flows:
        mass_flow_str = f"Massenstrom {str(mass_flow).replace('.', ',')}"
        filtered_data = []

        for df in data_frames:
            # Filtere den DataFrame nach der Frequenz (30 Hz) und dem Massenstrom
            filtered_df = df[
                df['Dateiname'].str.contains(f"_{mass_flow}_{frequency}.csv")].copy()
            if not filtered_df.empty:
                filtered_df[column] = filtered_df[column] / (1000 * 3600)  # Umwandlung in kWh
                filtered_data.append(filtered_df)

        # Kombiniere die gefilterten DataFrames (falls mehrere vorhanden sind)
        if filtered_data:
            combined_df = pd.concat(filtered_data)
            combined_df.sort_values(by='Sekunde', inplace=True)
            ax.plot(combined_df['Sekunde'] / 3600, combined_df[column], label=f'Massenstrom {mass_flow:.2f} kg/s')

    ax.set_xlabel('Beladedauer in Stunden', fontsize=20)
    ax.set_ylabel('Kummulierte Exergie (kWh)', fontsize=20)
    ax.grid(False)
    ax.legend(fontsize=18)
    ax.locator_params(axis='x', nbins=7)  # Anzahl der Ticks auf der x-Achse anpassen
    ax.locator_params(axis='y', nbins=7)  # Anzahl der Ticks auf der y-Achse anpassen
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_func(x, pos, 2)))
    ax.tick_params(axis='both', which='major', labelsize=18)
    ax.grid(True)  # Fügt ein Gitter für beide Achsen hinzu

    # Setzen der schwarzen Ränder
    for spine in ax.spines.values():
        spine.set_edgecolor('black')
        spine.set_linewidth(1.5)

    plot_filename = os.path.join(output_dir, f"{title}".replace(' ', '_').replace('.', ','))
    fig.savefig(f"{plot_filename}.png", format='png')
    fig.savefig(f"{plot_filename}.svg", format='svg')
    print(f"Plot gespeichert: {plot_filename}.png und {plot_filename}.svg")

    plt.show()


if __name__ == "__main__":
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Massenstrom\PLOTS\timedependency_kummuliert"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_frames = read_time_dependent_csv_files(results_base_dir)
    plot_time_dependent_results(data_frames, output_dir)
    # Aufruf der neuen Plot-Funktion
    plot_30hz_cumulative_exergy(data_frames, output_dir)