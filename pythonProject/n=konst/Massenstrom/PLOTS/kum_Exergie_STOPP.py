import os
import pandas as pd
import matplotlib.pyplot as plt
import re
import matplotlib.ticker as ticker

# Pfade zu den gespeicherten Ergebnissen
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy_Exergie_pro_Zeitschritt"
results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS"
# Erstellen des Ordners "NutzungsgradPlot" im angegebenen Pfad
output_dir = os.path.join(results_base_dir_1, "kum_exergie_stopp")
mass_flows = [0.05, 0.15, 0.25]

# Funktion zum Einlesen und Aufbereiten der CSV-Dateien
def read_time_dependent_csv_files(mass_flow):
    data_frames = []
    # Ordner für das aktuelle mass_flow
    results_dir = os.path.join(results_base_dir, f"results_{mass_flow}")

    # Überprüfen, ob der Ordner existiert
    if not os.path.exists(results_dir):
        print(f"Verzeichnis nicht gefunden: {results_dir}")
        return pd.DataFrame()  # Leeren DataFrame zurückgeben

    # Durchsuchen des Ordners nach CSV-Dateien
    for subdir, _, files in os.walk(results_dir):
        for file in files:
            if file.startswith("time_dependent_results") and file.endswith(".csv"):
                file_path = os.path.join(subdir, file)
                try:
                    df = pd.read_csv(file_path, sep=',', decimal='.')

                    # Überprüfen, ob die erwarteten Spalten existieren
                    if 'Leistung_WP' not in df.columns or 'kummulierte Exergie kWh' not in df.columns or 'Sekunde' not in df.columns or 'Dateiname' not in df.columns:
                        print(f"Erwartete Spalten fehlen in Datei: {file_path}")
                        continue

                    # Frequenz für jede Zeile extrahieren und hinzufügen
                    df['Frequency'] = df['Dateiname'].apply(lambda x: int(float(re.search(r'_(\d+\.\d+)\.csv', x).group(1)) * 100))

                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")

                except Exception as e:
                    print(f"Fehler beim Lesen der Datei {file_path}: {e}")

    if not data_frames:
        print("Keine passenden Daten gefunden.")
        return pd.DataFrame()  # Leeren DataFrame zurückgeben

    return pd.concat(data_frames, ignore_index=True)


# Funktion zur Bestimmung des Stoppzeitpunkts und Plotten der kumulierten Exergie für alle Frequenzen
def plot_cumulative_exergy(mass_flow):
    # Einlesen aller Daten für das aktuelle mass_flow
    df = read_time_dependent_csv_files(mass_flow)

    if df.empty:
        print(f"Keine Daten für mass_flow = {mass_flow}")
        return

    # Berechnen der kumulierten elektrischen Leistung basierend auf der Spalte "Leistung_WP"
    df["P_total_cum"] = df.groupby("Frequency")["Leistung_Gesamt"].cumsum() / (1000 * 3600)  # Umrechnung in kWh

    # Finde die minimale kumulierte elektrische Energie, bei der der Prozess für jede Frequenz stoppt
    stopp_values = df.groupby("Frequency")["P_total_cum"].max()

    # Bestimme die Frequenz, die den minimalen Stoppwert erreicht
    stop_frequency = stopp_values.idxmin()  # Frequenz mit der minimalen Stopp-Energie
    min_stopp_energy = stopp_values.min()   # Minimaler Stoppwert

    # Erstellen des Plots
    plt.figure(figsize=(10, 6))

    # Iterieren über die Gruppen nach Frequenz (Frequency)
    for frequency, group_df in df.groupby("Frequency"):
        # Filtern der Daten bis zur Stoppbedingung für jede Frequenz
        group_df_stopped = group_df[group_df["P_total_cum"] <= min_stopp_energy]

        # Frequenz formatieren (ohne Nachkommastellen)
        label = f'{int(frequency)} Hz'
        if frequency == stop_frequency:
            label += " (Stopp)"  # Markiere die Frequenz, die zum Stopp führt

        # Plotten der kumulierten Exergie bis zur Stoppbedingung für jede Frequenz
        plt.plot(group_df_stopped["Sekunde"] / 3600, group_df_stopped["kummulierte Exergie kWh"], label=label)

    # Formatter für Achsen, um Dezimalzahlen mit Komma darzustellen
    comma_formatter = ticker.FuncFormatter(lambda x, _: f'{x:.0f}'.replace('.', ','))

    # Achsen-Formatter anwenden (Beladedauer in Stunden ohne Dezimalstellen)
    plt.gca().xaxis.set_major_formatter(comma_formatter)
    plt.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))

    # Y-Achse: kumulierte Exergie, Dezimalzahlen mit Komma und zwei Nachkommastellen
    y_comma_formatter = ticker.FuncFormatter(lambda x, _: f'{x:.2f}'.replace('.', ','))
    plt.gca().yaxis.set_major_formatter(y_comma_formatter)

    # Plot-Details hinzufügen
    plt.title(
        f'Kumulierte Exergie über Zeit bis zur Stoppbedingung für m_sek = {str(mass_flow).replace(".", ",")} kg/s\n(Stopp bei {min_stopp_energy:.2f} kWh elektrischer Energie)')
    plt.xlabel("Beladedauer in Stunden")
    plt.ylabel("Kumulierte Exergie in kWh")
    plt.legend()
    plt.grid(True)

    # Speichern und Anzeigen des Plots
    plt.savefig(os.path.join(results_base_dir, f"Exergie_Plot_mass_flow_{str(mass_flow).replace('.', ',')}_mit_Stopp.png"))
    plt.show()


# Hauptlogik zur Erstellung der Plots für alle Massenströme
for mass_flow in mass_flows:
    plot_cumulative_exergy(mass_flow)
    print(f'Plot für m_sek = {str(mass_flow).replace(".", ",")} kg/s wurde erstellt und gespeichert.')
