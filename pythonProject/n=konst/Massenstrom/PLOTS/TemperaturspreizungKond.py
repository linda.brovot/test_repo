import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# Pfade zu den CSV-Ordnern für die verschiedenen Pumpendrehzahlen
base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CSV_Results"

mass_flows = [0.05, 0.15, 0.25]

for mass_flow in mass_flows:
    folder_path = os.path.join(base_dir, f"csv_{mass_flow}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    for csv_file_path in csv_files:
        name = os.path.splitext(os.path.basename(csv_file_path))[0]
        # Lese die Daten aus der CSV-Datei und stelle sicher, dass Dezimalstellen durch Kommata getrennt sind
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Filtere den DataFrame, um nur Daten ab Sekunde 100 zu behalten
        df = df[df.index >= 100]

        # Berechnung der Temperaturdifferenz (heatPump.sigBus.TConOutMea - heatPump.sigBus.TConInMea)
        df["Temperature_Difference"] = df['heatPump.sigBus.TConOutMea'] - df['heatPump.sigBus.TConInMea']

        # Konvertierung des Index in Stunden
        df["Hour"] = df.index / 3600  # Annahme: Die Indexwerte sind in Sekunden, daher teilen wir durch 3600, um Stunden zu erhalten

        # Plotten der Temperaturdifferenz
        plt.figure(figsize=(10, 6))
        plt.plot(df["Hour"], df["Temperature_Difference"], label="Temperaturdifferenz (TConOutMea - TConInMea)")

        plt.xlabel('Zeit (Stunden)')
        plt.ylabel('Temperaturdifferenz (°C)')
        plt.title(f'Temperaturdifferenz für {name} bei Massenstrom {mass_flow} kg/s')

        # Anpassung der Achsenbeschriftung für Komma statt Punkt
        plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: f'{x:.1f}'.replace('.', ',')))

        plt.legend()
        plt.grid(False)  # Entferne das Gitter

        # Speicherort für den Plot
        script_name = os.path.splitext(os.path.basename(__file__))[0]  # Name des Skripts ohne Erweiterung
        results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS"
        output_dir = os.path.join(results_base_dir, script_name)

        # Erstelle das Verzeichnis, wenn es nicht existiert
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Speicherpfade für PNG und SVG definieren
        plot_path_png = os.path.join(output_dir, f"temperature_difference_{name}_massenstrom_{mass_flow}.png")
        plot_path_svg = os.path.join(output_dir, f"temperature_difference_{name}_massenstrom_{mass_flow}.svg")

        # Plot als PNG speichern
        plt.savefig(plot_path_png, format='png')

        # Plot als SVG speichern
        plt.savefig(plot_path_svg, format='svg')

        # Plot schließen
        plt.close()

        # Ausgabe der Speicherorte
        print(
            f"Der Temperaturdifferenz-Plot für '{name}' bei Massenstrom {mass_flow} wurde unter '{plot_path_png}' und '{plot_path_svg}' gespeichert.")

print(f"Alle Plots wurden erstellt und in '{output_dir}' gespeichert.")
