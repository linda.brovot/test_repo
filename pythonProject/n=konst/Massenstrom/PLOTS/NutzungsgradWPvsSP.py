import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker
import numpy as np

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, mass_flow):
    folder_path = os.path.join(results_base_dir, f"results_{mass_flow}")
    csv_file = os.path.join(folder_path, f"system_results_{mass_flow}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für mass_flow {mass_flow} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für mass_flow {mass_flow} nicht gefunden: {csv_file}")
        return None

def plot_data(data_frame, mass_flow):
    eta_wp_list = []
    eta_sp_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{mass_flow}_{value}.csv")]
            if not filtered_df.empty:
                eta_wp = filtered_df['Nutzungsgrad_WP_mitPumpe'].values[0]
                eta_sp = filtered_df['Eta_SP'].values[0]

                if not (pd.isna(eta_wp) or pd.isna(eta_sp)):
                    eta_wp_list.append(eta_wp)
                    eta_sp_list.append(eta_sp)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, eta_wp_list, eta_sp_list

def add_polynomial_trendline(ax, x_data, y_data, color):
    z = np.polyfit(x_data, y_data, 2)
    p = np.poly1d(z)
    x_smooth = np.linspace(min(x_data), max(x_data), 500)
    y_smooth = p(x_smooth)
    ax.plot(x_smooth, y_smooth, linestyle='--', color=color, label=f"Trendkurve", alpha=0.7)

def calculate_percent_difference(values1, values2):
    return [(v2 - v1) / v1 * 100 if v1 != 0 else None for v1, v2 in zip(values1, values2)]

def calculate_percent_difference_sequential(values):
    return [(v2 - v1) / v1 * 100 if v1 != 0 else None for v1, v2 in zip(values[:-1], values[1:])]

def combined_plot(results_base_dir, output_dir, script_name):
    eta_wp_combined = {}
    eta_sp_combined = {}
    frequencies_combined = []

    for mass_flow in [0.05, 0.15, 0.25]:
        data_frame = read_csv_files(results_base_dir, mass_flow)
        frequencies, eta_wp_list, eta_sp_list = plot_data(data_frame, mass_flow)
        if frequencies:
            eta_wp_combined[mass_flow] = eta_wp_list
            eta_sp_combined[mass_flow] = eta_sp_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    if frequencies_combined:
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(13, 6), gridspec_kw={'width_ratios': [2, 2]})

        bar_width = 1.3
        offsets = [-bar_width, 0, bar_width]
        colors = ['darkred', 'red', 'grey']

        # Subplot 1: Nutzungsgrad WP
        for mass_flow, offset, color in zip([0.05, 0.15, 0.25], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], eta_wp_combined[mass_flow], width=bar_width, color=color,
                    label=f'{mass_flow} kg/s')

        # Polynomiale Trendkurve für den Nutzungsgrad WP hinzufügen (basierend auf den höchsten dunkelroten Balken, mass_flow = 0.05 kg/s)
        highest_eta_wp = eta_wp_combined[0.05]
        add_polynomial_trendline(ax1, frequencies_combined, highest_eta_wp, 'black')

        # Berechnung der prozentualen Abweichungen für WP
        wp_diff_sequential_05 = calculate_percent_difference_sequential(eta_wp_combined[0.05])

        # Berechnung des größten prozentualen Unterschieds zwischen den Drehzahlen für WP (mass_flow = 0.05 kg/s)
        max_wp_diff_value = max(wp_diff_sequential_05, key=lambda x: abs(x))
        max_wp_diff_index = wp_diff_sequential_05.index(max_wp_diff_value)

        # Zeichnen eines schrägen Pfeils bei der größten prozentualen Änderung für WP
        arrow_props = dict(facecolor='black', arrowstyle='->', lw=1.5)
        ax1.annotate(f"{max_wp_diff_value:.1f}%",
                     xy=(frequencies_combined[max_wp_diff_index + 1], eta_wp_combined[0.05][max_wp_diff_index + 1]),
                     xytext=(frequencies_combined[max_wp_diff_index], eta_wp_combined[0.05][max_wp_diff_index] + 0.03),
                     arrowprops=arrow_props, fontsize=12, ha='center', color='black')

        ax1.set_ylabel('Nutzungsgrad WP', fontsize=14)
        ax1.set_xlabel('Drehzahl in Hz', fontsize=14)
        ax1.legend(loc='best', fontsize=12)
        ax1.grid(False)
        ax1.tick_params(axis='both', which='major', labelsize=12)
        ax1.set_ylim([0, 0.6])

        # Subplot 2: Nutzungsgrad Speicher
        for mass_flow, offset, color in zip([0.05, 0.15, 0.25], offsets, colors):
            ax2.bar([x + offset for x in frequencies_combined], eta_sp_combined[mass_flow], width=bar_width, color=color,
                    label=f'{mass_flow} kg/s')

        # Polynomiale Trendkurve für den Nutzungsgrad Speicher (alle Daten)
        all_eta_sp = [eta for mass_flow in eta_sp_combined for eta in eta_sp_combined[mass_flow]]
        add_polynomial_trendline(ax2, frequencies_combined * 3, all_eta_sp, 'black')

        # Berechnung der prozentualen Abweichungen für Speicher
        sp_diff_sequential_05 = calculate_percent_difference_sequential(eta_sp_combined[0.05])

        # Berechnung des größten prozentualen Unterschieds zwischen den Drehzahlen für Speicher (mass_flow = 0.05 kg/s)
        max_sp_diff_value = max(sp_diff_sequential_05, key=lambda x: abs(x))
        max_sp_diff_index = sp_diff_sequential_05.index(max_sp_diff_value)

        # Zeichnen eines schrägen Pfeils bei der größten prozentualen Änderung für Speicher
        ax2.annotate(f"{max_sp_diff_value:.1f}%",
                     xy=(frequencies_combined[max_sp_diff_index + 1], eta_sp_combined[0.05][max_sp_diff_index + 1]),
                     xytext=(frequencies_combined[max_sp_diff_index], eta_sp_combined[0.05][max_sp_diff_index] + 0.03),
                     arrowprops=arrow_props, fontsize=12, ha='center', color='black')

        ax2.set_ylabel('Nutzungsgrad Speicher', fontsize=14)
        ax2.set_xlabel('Drehzahl in Hz', fontsize=14)
        ax2.legend(loc='best', fontsize=12)
        ax2.grid(False)
        ax2.tick_params(axis='both', which='major', labelsize=12)
        ax2.set_ylim([0, 1])

        # Berechnung des größten Unterschieds zwischen Massenströmen (0.05 vs. 0.25)
        wp_diff_05_to_25 = calculate_percent_difference(eta_wp_combined[0.05], eta_wp_combined[0.25])
        max_wp_diff_05_to_25_value = max(wp_diff_05_to_25, key=lambda x: abs(x))
        max_wp_diff_05_to_25_index = wp_diff_05_to_25.index(max_wp_diff_05_to_25_value)

        # Markiere den größten Unterschied für WP (0.05 vs. 0.25)
        ax1.text(frequencies_combined[max_wp_diff_05_to_25_index],
                 eta_wp_combined[0.05][max_wp_diff_05_to_25_index] + 0.02,
                 f"{max_wp_diff_05_to_25_value:.1f}%",
                 ha='center', color='black', fontsize=12)

        sp_diff_05_to_25 = calculate_percent_difference(eta_sp_combined[0.05], eta_sp_combined[0.25])
        max_sp_diff_05_to_25_value = max(sp_diff_05_to_25, key=lambda x: abs(x))
        max_sp_diff_05_to_25_index = sp_diff_05_to_25.index(max_sp_diff_05_to_25_value)

        # Markiere den größten Unterschied für Speicher (0.05 vs. 0.25)
        ax2.text(frequencies_combined[max_sp_diff_05_to_25_index],
                 eta_sp_combined[0.25][max_sp_diff_05_to_25_index] + 0.02,
                 f"{max_sp_diff_05_to_25_value:.1f}%",
                 ha='center', color='black', fontsize=12)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        # Anwendung von tight_layout und subplots_adjust
        plt.tight_layout(rect=[0, 0, 1, 0.95])
        plt.subplots_adjust(wspace=0.15)

        # Speichern des kombinierten Plots als PNG und SVG
        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_png)
        plt.savefig(combined_plot_filename_svg)
        plt.show()
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

        # Speichern der Abweichungsdaten in einer CSV-Datei
        df_wp_sp_diff = pd.DataFrame({
            'Frequenz (Hz)': frequencies_combined,
            'WP Abweichung 0.05 zu 0.25 (%)': wp_diff_05_to_25,
            'SP Abweichung 0.05 zu 0.25 (%)': sp_diff_05_to_25,
            'WP Sequential 0.05 (%)': [None] + wp_diff_sequential_05,  # None für den ersten Wert, da kein vorheriger Wert existiert
            'SP Sequential 0.05 (%)': [None] + sp_diff_sequential_05
        })

        csv_filename = os.path.join(output_dir, f"{script_name}_abweichungen.csv")
        df_wp_sp_diff.to_csv(csv_filename, index=False, sep=';', decimal=',')
        print(f"Abweichungsdaten gespeichert: {csv_filename}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS"
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
