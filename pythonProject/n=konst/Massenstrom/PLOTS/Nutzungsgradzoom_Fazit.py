import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
import locale

# Setze das Locale auf Deutsch für das Komma als Dezimaltrennzeichen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def format_number(value):
    return locale.format_string('%.2f', value, grouping=True).replace('.', ',')


def read_csv_files(results_base_dir, mass_flow):
    folder_path = os.path.join(results_base_dir, f"results_{mass_flow}")
    csv_file = os.path.join(folder_path, f"system_results_{mass_flow}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für mass_flow {mass_flow} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für mass_flow {mass_flow} nicht gefunden: {csv_file}")
        return None


def calculate_percentage_difference(value_start, value_end):
    """Berechnet den prozentualen Unterschied zwischen zwei Werten, bezogen auf den Startwert."""
    return ((value_end - value_start) / value_start) * 100


def combined_zoomed_plots(results_base_dir, output_dir, script_name):
    frequencies_combined = {0.15: [30, 40], 0.25: [80, 90]}  # Zu verwendende Frequenzen
    eta_system_combined = {}
    hours_combined = {}

    for mass_flow in [0.15, 0.25]:
        data_frame = read_csv_files(results_base_dir, mass_flow)
        if data_frame is not None:
            eta_system_list = []
            hours_list = []
            for freq, value in zip(frequencies_combined[mass_flow], [0.3, 0.4] if mass_flow == 0.15 else [0.8, 0.9]):
                filtered_df = data_frame[data_frame['Name'].str.contains(f"_{mass_flow}_{value}.csv")]

                if not filtered_df.empty:
                    eta_system = filtered_df['Eta_WPSS'].values[0]
                    hours = filtered_df['Hours'].values[0]

                    if not pd.isna(eta_system) and not pd.isna(hours):
                        eta_system_list.append(eta_system)
                        hours_list.append(hours)

            eta_system_combined[mass_flow] = eta_system_list
            hours_combined[mass_flow] = hours_list

    # Berechnung der prozentualen Abweichungen
    percentage_differences = []

    for mass_flow in [0.15, 0.25]:
        freq_low = frequencies_combined[mass_flow][0]
        freq_high = frequencies_combined[mass_flow][1]

        # Berechnung der prozentualen Unterschiede immer von der niedrigeren Frequenz zur höheren
        eta_diff = calculate_percentage_difference(eta_system_combined[mass_flow][0], eta_system_combined[mass_flow][1])
        hours_diff = calculate_percentage_difference(hours_combined[mass_flow][0], hours_combined[mass_flow][1])

        # Ergebnisse speichern
        percentage_differences.append({
            'Mass Flow (kg/s)': mass_flow,
            f'Nutzungsgrad Differenz ({freq_low} Hz zu {freq_high} Hz) (%)': eta_diff,
            f'Beladedauer Differenz ({freq_low} Hz zu {freq_high} Hz) (%)': hours_diff
        })

    # Speichern der prozentualen Abweichungen in eine CSV-Datei
    percentage_df = pd.DataFrame(percentage_differences)
    csv_filename = os.path.join(output_dir, f"{script_name}_prozent_differenzen.csv")
    percentage_df.to_csv(csv_filename, sep=';', index=False)
    print(f"Prozentuale Abweichungen in {csv_filename} gespeichert.")

    # Plotten
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))  # Zwei Subplots nebeneinander

    bar_width = 2.5  # Balkenbreite
    offset = bar_width / 2  # Versatz für die Balken
    hatch_pattern = '//'  # Schraffierungsmuster

    # Linker Plot für 30-40 Hz (0.15 kg/s)
    bars1 = ax1.bar(np.array(frequencies_combined[0.15]) - offset, hours_combined[0.15], width=bar_width, color='red',
                    hatch=hatch_pattern, label='Beladedauer')
    ax1_twin = ax1.twinx()
    bars1_twin = ax1_twin.bar(np.array(frequencies_combined[0.15]) + offset, eta_system_combined[0.15], width=bar_width,
                              color='red', label='Nutzungsgrad')

    # Rechter Plot für 80-90 Hz (0.25 kg/s)
    bars2 = ax2.bar(np.array(frequencies_combined[0.25]) - offset, hours_combined[0.25], width=bar_width, color='grey',
                    hatch=hatch_pattern, label='Beladedauer')

    ax2_twin = ax2.twinx()
    bars2_twin = ax2_twin.bar(np.array(frequencies_combined[0.25]) + offset, eta_system_combined[0.25], width=bar_width,
                              color='grey', label='Nutzungsgrad')

    # Einstellungen für den linken Plot (0.15 kg/s)
    ax1.set_xlabel('Drehzahl in Hz', fontsize=14)
    ax1.set_ylabel('Beladedauer in h', fontsize=14)
    ax1.set_title('Zoom in auf 30-40 Hz für 0,15 kg/s', fontsize=14)
    ax1.set_xticks(frequencies_combined[0.15])
    ax1.set_ylim(0, 8.5)
    ax1_twin.set_ylim(0.12, 0.24)
    ax1_twin.set_ylabel('Nutzungsgrad WPSS', fontsize=14)
    ax1.legend(loc='upper left', fontsize=12)
    ax1_twin.legend(loc='upper right', fontsize=12)

    # Prozentzahlen für die Beladedauer im linken Plot
    hours_diff_left = calculate_percentage_difference(hours_combined[0.15][0], hours_combined[0.15][1])
    eta_diff_left = calculate_percentage_difference(eta_system_combined[0.15][0], eta_system_combined[0.15][1])

    for bar in bars1:
        height = bar.get_height()
        sign = '+' if hours_diff_left < 0 else '-'  # Ein Plus, wenn die Beladedauer kürzer ist (schneller)
        ax1.text(bar.get_x() + bar.get_width() / 2, height, f'{sign}{abs(hours_diff_left):.1f}%'.replace('.', ','),
                 ha='center', va='bottom', color='black', fontsize=12)

    for bar in bars1_twin:
        height = bar.get_height()
        sign = '+' if eta_diff_left > 0 else '-'  # Ein Plus, wenn der Nutzungsgrad steigt
        ax1_twin.text(bar.get_x() + bar.get_width() / 2, height, f'{sign}{abs(eta_diff_left):.1f}%'.replace('.', ','),
                      ha='center', va='bottom', color='black', fontsize=12)

    # Einstellungen für den rechten Plot (0.25 kg/s)
    ax2.set_xlabel('Drehzahl in Hz', fontsize=14)
    ax2.set_ylabel('Beladedauer in h', fontsize=14)
    ax2.set_title('Zoom in auf 80-90 Hz für 0,25 kg/s', fontsize=14)
    ax2.set_xticks(frequencies_combined[0.25])
    ax2.set_ylim(0, 8.5)
    ax2_twin.set_ylim(0.12, 0.24)
    ax2_twin.set_ylabel('Nutzungsgrad WPSS', fontsize=14)
    ax2.legend(loc='upper left', fontsize=12)
    ax2_twin.legend(loc='upper right', fontsize=12)

    # Prozentzahlen für die Beladedauer im rechten Plot
    hours_diff_right = calculate_percentage_difference(hours_combined[0.25][0], hours_combined[0.25][1])
    eta_diff_right = calculate_percentage_difference(eta_system_combined[0.25][0], eta_system_combined[0.25][1])

    for bar in bars2:
        height = bar.get_height()
        sign = '+' if hours_diff_right < 0 else '-'  # Ein Plus, wenn die Beladedauer kürzer ist (schneller)
        ax2.text(bar.get_x() + bar.get_width() / 2, height, f'{sign}{abs(hours_diff_right):.1f}%'.replace('.', ','),
                 ha='center', va='bottom', color='black', fontsize=12)

    for bar in bars2_twin:
        height = bar.get_height()
        sign = '+' if eta_diff_right > 0 else '-'  # Ein Plus, wenn der Nutzungsgrad steigt
        ax2_twin.text(bar.get_x() + bar.get_width() / 2, height, f'{sign}{abs(eta_diff_right):.1f}%'.replace('.', ','),
                      ha='center', va='bottom', color='black', fontsize=12)

    plt.tight_layout(rect=[0, 0, 1, 0.95])
    plt.subplots_adjust(wspace=0.25)  # Erhöht/Reduziert den Abstand zwischen den Plots

    combined_plot_filename_png = os.path.join(output_dir, f"{script_name}.png")
    combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}.svg")

    # Speichern als PNG und SVG
    plt.savefig(combined_plot_filename_png)
    plt.savefig(combined_plot_filename_svg, format='svg')

    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS\NutzungsgradPlot"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_zoomed_plots(results_base_dir, output_dir, script_name)
