import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import locale

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(system_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        return system_results_df
    else:
        print(f"CSV-Datei nicht gefunden: {system_csv_file}")
        return None


def get_values_for_drehzahl(system_results_df, identifier, target_drehzahl):
    # Ziel-Drehzahl als Prozentsatz
    target_str = f"_{identifier}_{target_drehzahl / 100:.1f}.csv"

    # Nach der Drehzahl im Dateinamen filtern
    filtered_df = system_results_df[system_results_df['Name'].str.contains(target_str)]

    if not filtered_df.empty:
        pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600  # in kWh
        return pel_sum
    else:
        print(f"Keine Daten für die Drehzahl {target_drehzahl} Hz gefunden.")
        return None


def plot_combined_data(output_dir):
    # Pfade für 15 Grad und 7 Grad
    results_base_dir_15 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_7 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_7\Massenstrom\CalculationExergy"

    # Identifier (Massenstrom 0,15 kg/s)
    identifier = 0.15

    # Ziel-Drehzahlen definieren
    drehzahlen_15_grad = [30, 90]  # Für 15 Grad: 30 Hz und 90 Hz
    drehzahlen_7_grad = [40, 90]  # Für 7 Grad: 40 Hz und 90 Hz

    # Farben für die Balken
    colors_15 = ['tab:blue', 'tab:pink']  # Farben für 15 Grad (30 Hz, 90 Hz)
    colors_7 = ['tab:orange', 'tab:pink']  # Farben für 7 Grad (40 Hz, 90 Hz)

    # Daten aus beiden Pfaden extrahieren
    system_results_15 = read_csv_files(results_base_dir_15, identifier)
    system_results_7 = read_csv_files(results_base_dir_7, identifier)

    if system_results_15 is not None and system_results_7 is not None:
        # Extrahieren der Werte für die spezifischen Drehzahlen
        pel_15 = [get_values_for_drehzahl(system_results_15, identifier, hz) for hz in drehzahlen_15_grad]
        pel_7 = [get_values_for_drehzahl(system_results_7, identifier, hz) for hz in drehzahlen_7_grad]

        if None not in pel_15 and None not in pel_7:
            # Berechnung der prozentualen Abweichung für 30 Hz bei 15 Grad und 40 Hz bei 7 Grad bezogen auf 90 Hz
            deviation_15 = (pel_15[0] - pel_15[1]) / pel_15[1] * 100  # 30 Hz bezogen auf 90 Hz bei 15 Grad
            deviation_7 = (pel_7[0] - pel_7[1]) / pel_7[1] * 100  # 40 Hz bezogen auf 90 Hz bei 7 Grad

            # Plotting der Daten
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))

            # X-Werte
            x_15 = np.array([0, 1])  # X-Positionen für 30 Hz und 90 Hz bei 15 Grad
            x_7 = np.array([0, 1])   # X-Positionen für 40 Hz und 90 Hz bei 7 Grad

            # Breite der Balken
            width = 0.4

            # Balken für 15 Grad mit zwei Drehzahlen (30 Hz, 90 Hz)
            bars_15 = ax1.bar(x_15, pel_15, width=width, color=colors_15)
            ax1.set_title('15 Grad (Massenstrom = 0,15 kg/s)', fontsize=16)
            ax1.set_xlabel('Drehzahl in Hz', fontsize=14)
            ax1.set_ylabel('Elektrische Energie in kWh', fontsize=14)
            ax1.set_xticks(x_15)
            ax1.set_xticklabels(['30 Hz', '90 Hz'])
            ax1.set_ylim(0, max(pel_15) * 1.2)  # Skalierung

            # Prozentuale Abweichung für 15 Grad auf die Balken schreiben
            ax1.text(bars_15[0].get_x() + bars_15[0].get_width() / 2, bars_15[0].get_height(),
                     f'{deviation_15:+.1f}%', ha='center', va='bottom', fontsize=12, color='black')

            # Setze manuell die Grenzen der X-Achse, um den Abstand zu beeinflussen
            ax1.set_xlim(-0.5, 1.5)  # Verschiebt die Balken nach links/rechts, um sie näher an die Y-Achse zu bringen

            # Balken für 7 Grad mit zwei Drehzahlen (40 Hz, 90 Hz)
            bars_7 = ax2.bar(x_7, pel_7, width=width, color=colors_7)
            ax2.set_title('7 Grad (Massenstrom = 0,15 kg/s)', fontsize=16)
            ax2.set_xlabel('Drehzahl in Hz', fontsize=14)
            ax2.set_ylabel('Elektrische Energie (kWh)', fontsize=14)
            ax2.set_xticks(x_7)
            ax2.set_xticklabels(['40 Hz', '90 Hz'])
            ax2.set_ylim(0, max(pel_7) * 1.2)  # Skalierung

            # Prozentuale Abweichung für 7 Grad auf die Balken schreiben
            ax2.text(bars_7[0].get_x() + bars_7[0].get_width() / 2, bars_7[0].get_height(),
                     f'{deviation_7:+.1f}%', ha='center', va='bottom', fontsize=12, color='black')

            # Setze manuell die Grenzen der X-Achse, um den Abstand zu beeinflussen
            ax2.set_xlim(-0.5, 1.5)  # Verschiebt die Balken nach links/rechts, um sie näher an die Y-Achse zu bringen

            # Plot speichern
            fig.tight_layout()
            fig.savefig(os.path.join(output_dir, "subplots_15_7_grad_with_deviation.png"), format='png')
            fig.savefig(os.path.join(output_dir, "subplots_15_7_grad_with_deviation.svg"), format='svg')

            print(f"Plot gespeichert: {os.path.join(output_dir, 'subplots_15_7_grad_with_deviation.png')}")
            print(f"Plot gespeichert: {os.path.join(output_dir, 'subplots_15_7_grad_with_deviation.svg')}")

            # Plot anzeigen
            plt.show()
        else:
            print("Daten für eine der Drehzahlen konnten nicht extrahiert werden.")
    else:
        print("Systemdaten für die angegebenen Pfade konnten nicht geladen werden.")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    plot_combined_data(output_dir)
