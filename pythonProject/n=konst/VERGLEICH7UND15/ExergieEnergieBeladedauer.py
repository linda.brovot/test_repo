import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import locale

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(system_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        return system_results_df
    else:
        print(f"CSV-Datei nicht gefunden: {system_csv_file}")
        return None

def get_values_for_drehzahl(system_results_df, identifier, target_drehzahl):
    # Ziel-Drehzahl als Prozentsatz
    target_str = f"_{identifier}_{target_drehzahl / 100:.1f}.csv"

    # Nach der Drehzahl im Dateinamen filtern
    filtered_df = system_results_df[system_results_df['Name'].str.contains(target_str)]

    if not filtered_df.empty:
        pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600  # in kWh
        exergie_sum = filtered_df['Sum_Differences_Exergies'].values[0] / 1000 / 3600  # in kWh
        beladedauer = filtered_df['Hours'].values[0]  # bereits in Stunden
        return pel_sum, exergie_sum, beladedauer
    else:
        print(f"Keine Daten für die Drehzahl {target_drehzahl} Hz gefunden.")
        return None, None, None

def plot_combined_data(output_dir):
    # Pfade für 15 Grad und 7 Grad
    results_base_dir_15 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_7 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_7\Massenstrom\CalculationExergy"

    # Identifier
    identifier = 0.15  # Beispielhaft gewählt

    # Ziel-Drehzahlen definieren
    target_drehzahl_15 = 30  # Für 15 Grad: 30 Hz
    target_drehzahl_7 = 40  # Für 7 Grad: 40 Hz

    # Daten aus beiden Pfaden extrahieren
    system_results_15 = read_csv_files(results_base_dir_15, identifier)
    system_results_7 = read_csv_files(results_base_dir_7, identifier)

    if system_results_15 is not None and system_results_7 is not None:
        # Extrahieren der Werte für die spezifischen Drehzahlen
        pel_15, exergie_15, beladedauer_15 = get_values_for_drehzahl(system_results_15, identifier, target_drehzahl_15)
        pel_7, exergie_7, beladedauer_7 = get_values_for_drehzahl(system_results_7, identifier, target_drehzahl_7)

        if pel_15 is not None and pel_7 is not None:
            # Exergetischen Nutzungsgrad berechnen
            eta_wpss_15 = exergie_15 / pel_15 if pel_15 != 0 else 0
            eta_wpss_7 = exergie_7 / pel_7 if pel_7 != 0 else 0

            # Prozentuale Änderung des Nutzungsgrades berechnen
            eta_deviation = (eta_wpss_7 - eta_wpss_15) / eta_wpss_15 * 100 if eta_wpss_15 != 0 else 0

            # Plotting der Daten
            labels = ['Elektrische Energie', 'Exergie im Speicher']
            values_15 = [pel_15, exergie_15]
            values_7 = [pel_7, exergie_7]

            # Berechnung der prozentualen Abweichung
            deviations = [(v7 - v15) / v15 * 100 for v7, v15 in zip(values_7, values_15)]

            # Ergebnisse in ein DataFrame für CSV speichern
            results_df = pd.DataFrame({
                'Kategorie': labels,
                'Wert bei 15 Grad (30 Hz)': values_15,
                'Wert bei 7 Grad (40 Hz)': values_7,
                'Prozentuale Abweichung (%)': deviations
            })

            # CSV-Datei speichern
            csv_path = os.path.join(output_dir, 'ergebnisse_abweichung.csv')
            results_df.to_csv(csv_path, sep=';', decimal=',', index=False)
            print(f"Ergebnisse in CSV-Datei gespeichert: {csv_path}")

            # Balkenplot erstellen
            x = np.arange(len(labels))  # Anzahl der Kategorien
            width = 0.35  # Breite der Balken

            fig, (ax1, ax3) = plt.subplots(1, 2, figsize=(15, 6),gridspec_kw={'width_ratios': [3, 1]})

            # Balken für 15 Grad (Grau) und 7 Grad (Rot) mit Transparenz für Energie und Exergie
            bars1 = ax1.bar(x - width / 2, values_15, width, label='15 Grad (30 Hz)', color='grey', edgecolor='black',alpha=0.8)
            bars2 = ax1.bar(x + width / 2, values_7, width, label='7 Grad (40 Hz)', color='red', edgecolor='black',alpha=0.8)

            # zweite Y-Achse für Nutzungsgrad
            ax1_2 = ax1.twinx()
            bars_nutzungsgrad_15 = ax1_2.bar(2 - width / 2, eta_wpss_15, width, edgecolor='black',color='grey', alpha=0.8)
            bars_nutzungsgrad_7 = ax1_2.bar(2 + width / 2, eta_wpss_7, width,edgecolor='black', color='red', alpha=0.8)

            # Y-Achsen-Labels
            ax1.set_ylabel('kWh', fontsize=14)
            ax1.set_ylim([0,7])
            ax1_2.set_ylabel('Nutzungsgrad', fontsize=14)
            ax1_2.set_ylim([0,0.26])

            # x-Achsen-Label und Ticks
            ax1.set_xticks(np.append(x, [2]))
            ax1.set_xticklabels(labels + ['Nutzungsgrad'])

            # Prozentuale Abweichungen hinzufügen (Energie und Exergie)
            for i, (rect, dev) in enumerate(zip(bars2, deviations)):
                height = rect.get_height()
                ax1.text(rect.get_x() + rect.get_width() / 2.0, height,
                         f'{dev:+.1f}%', ha='center', va='bottom', fontsize=12, color='black')

            # Prozentuale Abweichung für Nutzungsgrad hinzufügen
            ax1_2.text(2 + width / 2, eta_wpss_7,
                       f'{eta_deviation:+.1f}%', ha='center', va='bottom', fontsize=12, color='black')

            # Zweiter Plot für Beladedauer mit korrekter Breite und Abstand
            bars_beladedauer_15 = ax3.bar(0 - width / 2, beladedauer_15, width=width, label='15 Grad (30 Hz)',edgecolor='black', color='grey', alpha=0.8)
            bars_beladedauer_7 = ax3.bar(0 + width / 2, beladedauer_7, width=width, label='7 Grad (40 Hz)', color='red',edgecolor='black', alpha=0.8)

            ax3.set_ylabel('Beladedauer in h', fontsize=14)
            ax3.set_xticks([0])
            ax3.set_xticklabels(['Beladedauer'])
            ax3.set_ylim([0,8])

            # Prozentuale Abweichung für Beladedauer hinzufügen
            beladedauer_deviation = (beladedauer_7 - beladedauer_15) / beladedauer_15 * 100 if beladedauer_15 != 0 else 0
            ax3.text(0 + width / 2, beladedauer_7,
                     f'{beladedauer_deviation:+.1f}%', ha='center', va='bottom', fontsize=12, color='black')

            # Eine gemeinsame Legende für beide Plots
            handles, labels = [], []
            for ax in [ax1, ax3]:
                for h, l in zip(*ax.get_legend_handles_labels()):
                    handles.append(h)
                    labels.append(l)

            # Gemeinsame Legende hinzufügen
            fig.legend(handles, labels, loc='upper center', ncol=2)

            # Plot speichern
            fig.tight_layout(rect=[0, 0, 1, 0.95])  # sorgt dafür, dass die Plots ordentlich ausgerichtet sind und Platz für die Legende oben ist
            fig.savefig(os.path.join(output_dir, "combined_plot_eta_beladedauer.png"), format='png')
            fig.savefig(os.path.join(output_dir, "combined_plot_eta_beladedauer.svg"), format='svg')

            print(f"Plot gespeichert: {os.path.join(output_dir, 'combined_plot_eta_beladedauer.png')}")
            print(f"Plot gespeichert: {os.path.join(output_dir, 'combined_plot_eta_beladedauer.svg')}")

            # Plot anzeigen
            plt.show()

        else:
            print("Daten konnten nicht extrahiert werden.")
    else:
        print("Systemdaten für die angegebenen Pfade konnten nicht geladen werden.")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Vergleich7und15\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    plot_combined_data(output_dir)
