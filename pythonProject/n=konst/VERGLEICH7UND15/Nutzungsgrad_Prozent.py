import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

#matplotlib.rcParams['svg.fonttype'] = 'none'


# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(results_base_dir, mass_flow):
    folder_path = os.path.join(results_base_dir, f"results_{mass_flow}")
    csv_file = os.path.join(folder_path, f"system_results_{mass_flow}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für mass_flow {mass_flow} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für mass_flow {mass_flow} nicht gefunden: {csv_file}")
        return None

def calculate_percentage_difference(value_earlier, value_later):
    """Berechnet den prozentualen Abstand zwischen zwei Werten."""
    return ((value_later - value_earlier) / value_earlier) * 100

def save_percentage_differences_to_csv(frequencies, eta_system_combined, hours_combined, output_dir, script_name):
    """Berechnet und speichert die prozentualen Unterschiede in eine CSV-Datei."""
    percentage_differences = []

    for i, freq in enumerate(frequencies):
        # Nutzungsgrad - Prozentsatzberechnungen
        diff_eta_05_to_15 = calculate_percentage_difference(eta_system_combined[0.05][i], eta_system_combined[0.15][i])
        diff_eta_15_to_25 = calculate_percentage_difference(eta_system_combined[0.15][i], eta_system_combined[0.25][i])

        # Beladedauer - Prozentsatzberechnungen (nur für 0,05 zu 0,25)
        diff_hours_05_to_25 = calculate_percentage_difference(hours_combined[0.05][i], hours_combined[0.25][i])

        # Ergebnisse sammeln
        percentage_differences.append({
            'Frequency (Hz)': freq,
            'Eta 0.05 to 0.15 (%)': diff_eta_05_to_15,
            'Eta 0.15 to 0.25 (%)': diff_eta_15_to_25,
            'Hours 0.05 to 0.25 (%)': diff_hours_05_to_25
        })

    # Speichern als CSV
    percentage_df = pd.DataFrame(percentage_differences)
    csv_filename = os.path.join(output_dir, f"{script_name}_percentage_differences.csv")
    percentage_df.to_csv(csv_filename, sep=';', index=False)
    print(f"Prozentuale Abstände in {csv_filename} gespeichert.")

def load_percentage_differences_from_csv(output_dir, script_name):
    """Lädt die prozentualen Unterschiede aus der CSV-Datei."""
    csv_filename = os.path.join(output_dir, f"{script_name}_percentage_differences.csv")
    percentage_df = pd.read_csv(csv_filename, sep=';')
    print(f"Prozentuale Abstände aus {csv_filename} geladen.")
    return percentage_df

def combined_plot(results_base_dir, output_dir, script_name):
    frequencies_combined = []
    eta_system_combined = {}
    hours_combined = {}

    for mass_flow in [0.05, 0.15, 0.25]:
        data_frame = read_csv_files(results_base_dir, mass_flow)
        if data_frame is not None:
            frequencies = []
            eta_system_list = []
            hours_list = []
            for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
                filtered_df = data_frame[data_frame['Name'].str.contains(f"_{mass_flow}_{value}.csv")]
                if not filtered_df.empty:
                    eta_system = filtered_df['Eta_WPSS'].values[0]
                    hours = filtered_df['Hours'].values[0]

                    if not pd.isna(eta_system) and not pd.isna(hours):
                        eta_system_list.append(eta_system)
                        hours_list.append(hours)
                        frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

            eta_system_combined[mass_flow] = eta_system_list
            hours_combined[mass_flow] = hours_list
            frequencies_combined = frequencies  # Frequenzen sind für alle gleich

    # Speichern der Prozentunterschiede in CSV
    save_percentage_differences_to_csv(frequencies_combined, eta_system_combined, hours_combined, output_dir, script_name)

    # Laden der Prozentunterschiede aus der CSV
    percentage_df = load_percentage_differences_from_csv(output_dir, script_name)

    if frequencies_combined:
        fig, ax1 = plt.subplots(figsize=(13, 8))  # Vergrößern der Breite und Höhe des Plots
        fig.suptitle('Exergetischer Nutzungsgrad bei unterschiedlichen Massenströmen', fontsize=16, y=0.95)

        bar_width = 1
        offsets = [-bar_width, 0, bar_width]
        colors = ['darkred', 'red', 'grey']

        # Beladedauer-Balkenplot
        for mass_flow, offset, color in zip([0.05, 0.15, 0.25], offsets, colors):
            ax1.bar([x + offset for x in frequencies_combined], hours_combined[mass_flow], width=bar_width, color=color,
                    label=f'{mass_flow} kg/s')

        # Nutzungsgrad-Linienplot
        ax2 = ax1.twinx()
        for mass_flow, color in zip([0.05, 0.15, 0.25], ['darkred', 'red', 'grey']):
            ax2.plot(frequencies_combined, eta_system_combined[mass_flow],
                     marker='o',  # 'o' für Punkte
                     linestyle='--',  # Gestrichelte Linie
                     linewidth=2.5,  # Dickere Linien
                     markersize=8,  # Größere Punkte
                     color=color, label=f'{mass_flow} kg/s')

        ax1.set_xlabel('Drehzahl in Hz', fontsize=16)
        ax1.set_ylabel('Beladedauer in h', fontsize=16)
        ax2.set_ylabel('Nutzungsgrad WPSS', fontsize=16)

        # Set y-limits for ax1 (Beladedauer)
        ax1.set_ylim(0, 8.5)

        # Automatically determine y-limits for ax2 (Nutzungsgrad) and set ticks
        ax2.set_ylim(0.15, 0.24)  # Set the desired range
        ax2.yaxis.set_major_locator(ticker.AutoLocator())  # Automatically determine tick locations
        ax2.yaxis.set_major_formatter(ticker.FuncFormatter(
            lambda x, pos: locale.format_string('%.2f', x)))  # Adjust precision to 3 decimals if needed

        ax1.grid(False)

        # Markierungen der prozentualen Unterschiede für 30 Hz und 60 Hz basierend auf den CSV-Daten
        # 30 Hz Werte
        percentage_30hz = percentage_df[percentage_df['Frequency (Hz)'] == 30]
        diff_eta_05_to_15_30hz = percentage_30hz['Eta 0.05 to 0.15 (%)'].values[0]
        diff_eta_15_to_25_30hz = percentage_30hz['Eta 0.15 to 0.25 (%)'].values[0]

        # Pfeile und Texte für 30 Hz
        ax2.annotate('', xy=(30, eta_system_combined[0.15][0]), xytext=(30, eta_system_combined[0.05][0]),
                     arrowprops=dict(arrowstyle='->', color='black', lw=2))
        ax2.text(32, (eta_system_combined[0.05][0] + eta_system_combined[0.15][0]) / 2,
                 f'+{abs(diff_eta_05_to_15_30hz):.1f}%'.replace('.', ','), color='black', fontsize=12,
                 verticalalignment='bottom')

        ax2.annotate('', xy=(30, eta_system_combined[0.15][0]), xytext=(30, eta_system_combined[0.25][0]),
                     arrowprops=dict(arrowstyle='->', color='black', lw=2))
        ax2.text(28, (eta_system_combined[0.25][0] + eta_system_combined[0.15][0]) / 2,
                 f'{diff_eta_15_to_25_30hz:.1f}%'.replace('.', ','), color='black', fontsize=12, ha='right',
                 verticalalignment='top')

        # 60 Hz Werte
        percentage_60hz = percentage_df[percentage_df['Frequency (Hz)'] == 60]
        diff_eta_05_to_15_60hz = percentage_60hz['Eta 0.05 to 0.15 (%)'].values[0]
        diff_eta_15_to_25_60hz = percentage_60hz['Eta 0.15 to 0.25 (%)'].values[0]

        # Pfeile und Texte für 60 Hz
        ax2.annotate('', xy=(60, eta_system_combined[0.15][3]), xytext=(60, eta_system_combined[0.05][3]),
                     arrowprops=dict(arrowstyle='->', color='black', lw=2))
        ax2.text(62, (eta_system_combined[0.05][3] + eta_system_combined[0.15][3]) / 2,
                 f'+{abs(diff_eta_05_to_15_60hz):.1f}%'.replace('.', ','), color='black', fontsize=12,
                 verticalalignment='bottom')

        ax2.annotate('', xy=(60, eta_system_combined[0.15][3]), xytext=(60, eta_system_combined[0.25][3]),
                     arrowprops=dict(arrowstyle='->', color='black', lw=2))
        ax2.text(58, (eta_system_combined[0.25][3] + eta_system_combined[0.15][3]) / 2,
                 f'+{abs(diff_eta_15_to_25_60hz):.1f}%'.replace('.', ','), color='black', fontsize=12, ha='right',
                 verticalalignment='top')

        # Berechne die prozentuale Änderung der Beladedauer bei 50 Hz (zwischen 0,05 und 0,25 kg/s)
        idx_50hz = frequencies_combined.index(50)
        beladedauer_05_50hz = hours_combined[0.05][idx_50hz]
        beladedauer_25_50hz = hours_combined[0.25][idx_50hz]
        percentage_diff_hours_05_to_25_50hz = calculate_percentage_difference(beladedauer_05_50hz, beladedauer_25_50hz)
        idx_90hz = frequencies_combined.index(90)
        beladedauer_05_90hz = hours_combined[0.05][idx_90hz]
        beladedauer_25_90hz = hours_combined[0.25][idx_90hz]
        percentage_diff_hours_05_to_25_90hz = calculate_percentage_difference(beladedauer_05_90hz, beladedauer_25_90hz)

        # Linie von der Spitze des 0,25-Balkens bis zur Spitze des 0,05-Balkens bei 50 Hz
        ax1.plot([50 - 0.2, 50 - 0.2], [beladedauer_25_50hz, beladedauer_05_50hz], color='blue', lw=2, linestyle='--')
        # Linie von der Spitze des 0,25-Balkens bis zur Spitze des 0,05-Balkens bei 90 Hz
        ax1.plot([90 - 0.2, 90 - 0.2], [beladedauer_25_90hz, beladedauer_05_90hz], color='blue', lw=2, linestyle='--')


        # Pfeil auf dem 0,05-Balken
        ax1.annotate('', xy=(50 - 0.2, beladedauer_05_50hz), xytext=(50 - 0.2, beladedauer_05_50hz + 0.5),
                     arrowprops=dict(arrowstyle='->', color='blue', lw=2))
        # Pfeil auf dem 0,05-Balken
        ax1.annotate('', xy=(90 - 0.2, beladedauer_05_90hz), xytext=(90 - 0.2, beladedauer_05_90hz + 0.5),
                     arrowprops=dict(arrowstyle='->', color='blue', lw=2))

        # Beispiel für Beladedauer bei 50 Hz
        ax1.text(50 - 0.5, (beladedauer_05_50hz + beladedauer_25_50hz) / 2,
                 f'+{percentage_diff_hours_05_to_25_50hz:.1f}%'.replace('.', ','), color='blue', fontsize=12,
                 verticalalignment='center', horizontalalignment='right')
        # Text neben der Linie
        ax1.text(90 - 0.5, (beladedauer_05_90hz + beladedauer_25_90hz) / 2,
                 f'+{percentage_diff_hours_05_to_25_90hz:.1f}%'.replace('.', ','), color='blue', fontsize=12,
                 verticalalignment='center', horizontalalignment='right')


        # Zusätzliche Pfeile und Texte an anderen Stellen nach dem gleichen Prinzip einfügen

        # Legenden manuell erstellen
        beladedauer_handles, beladedauer_labels = ax1.get_legend_handles_labels()
        beladedauer_labels = [label.replace('.', ',') for label in beladedauer_labels]
        beladedauer_legend = ax1.legend(beladedauer_handles, beladedauer_labels, loc='best',
                                        bbox_to_anchor=(0.82, 1.0),
                                        title="Beladedauer", ncol=1, fontsize=13, title_fontsize=14)

        nutzungsgrad_handles, nutzungsgrad_labels = ax2.get_legend_handles_labels()
        nutzungsgrad_labels = [label.replace('.', ',') for label in nutzungsgrad_labels]
        nutzungsgrad_legend = ax2.legend(nutzungsgrad_handles, nutzungsgrad_labels, loc='best',
                                         bbox_to_anchor=(0.98, 1.0),
                                         title="Nutzungsgrad", ncol=1, fontsize=13, title_fontsize=14)

        ax1.add_artist(beladedauer_legend)
        ax2.add_artist(nutzungsgrad_legend)

        ax1.tick_params(axis='both', which='major', labelsize=16)
        ax2.tick_params(axis='both', which='major', labelsize=16)

        def format_func(x, pos):
            if x.is_integer():
                return locale.format_string("%.0f", x)
            else:
                return locale.format_string("%.1f", x)

        ax1.xaxis.set_major_formatter(ticker.FuncFormatter(format_func))
        ax1.yaxis.set_major_formatter(ticker.FuncFormatter(format_func))

        combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
        combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")
        plt.savefig(combined_plot_filename_png)
        plt.savefig(combined_plot_filename_svg)
        plt.show()
        plt.close(fig)
        print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_7\Massenstrom\CalculationExergy"
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Vergleich7und15\PLOTS"
    output_dir = os.path.join(results_base_dir_1, "NutzungsgradPlot")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dir, output_dir, script_name)
