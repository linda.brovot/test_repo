import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
import os

matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane_kennfeldvisualisierung.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Liste der zu betrachtenden Massenströme
massenstroeme = [0.05, 0.15, 0.25]

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Studie1\kennfeld_visualisierung\COPPlot_TQUELLE_konstant'
os.makedirs(plot_base_dir, exist_ok=True)

# Für jeden Massenstrom und jede Verdampfereintrittstemperatur ein Diagramm erstellen
for massenstrom_ziel in massenstroeme:
    # Filtere Daten für den gewünschten Massenstrom
    filtered_data_massenstrom = data[data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_ziel]

    # Einzigartige Verdampfertemperaturen extrahieren
    verdampfer_temperaturen = filtered_data_massenstrom['T_eva_in in C'].unique()

    for verdampfer_temp in verdampfer_temperaturen:
        # Filtere Daten für die spezifische Verdampfereintrittstemperatur
        filtered_temp_data = filtered_data_massenstrom[filtered_data_massenstrom['T_eva_in in C'] == verdampfer_temp]

        fig = plt.figure()  # Plot ohne feste Größe erstellen
        ax = fig.add_subplot(111, projection='3d')

        # Hier wird die Drehzahl auf der x-Achse verwendet und die Kondensatortemperatur auf der y-Achse
        x = filtered_temp_data['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
        y = filtered_temp_data['T_con_in in C']  # Kondensatortemperatur
        cop = filtered_temp_data['COP in - (Coefficient of performance)']

        # Überprüfen, ob genügend Variabilität vorhanden ist
        if len(np.unique(x)) < 2 or len(np.unique(y)) < 2:
            print(f"Nicht genügend Variabilität in den Daten für Verdampfertemperatur {verdampfer_temp:.2f} °C und Massenstrom {massenstrom_ziel:.2f} kg/s. Interpolation übersprungen.")
            continue

        # NaN-Werte ignorieren
        valid_data_mask = np.isfinite(x) & np.isfinite(y) & np.isfinite(cop)
        x = x[valid_data_mask]
        y = y[valid_data_mask]
        cop = cop[valid_data_mask]

        # Gitter für die Interpolation erstellen
        xi = np.linspace(x.min(), x.max(), 100)
        yi = np.linspace(y.min(), y.max(), 100)
        xi, yi = np.meshgrid(xi, yi)

        # Interpolation für die Oberfläche
        try:
            cop_interpolated = griddata((x, y), cop, (xi, yi), method='linear')
        except Exception as e:
            print(f"Fehler bei der Interpolation für Verdampfertemperatur {verdampfer_temp:.2f} °C und Massenstrom {massenstrom_ziel:.2f} kg/s: {e}")
            continue

        # Oberfläche plotten
        surf = ax.plot_surface(xi, yi, cop_interpolated, cmap='viridis')
        ax.set_xlabel('Drehzahl in Hz', fontsize=11, labelpad=12)
        ax.set_ylabel('Rücklauftemperatur in °C', fontsize=10, labelpad=12)
        ax.set_zlabel('COP', fontsize=11, labelpad=12)

        # Colorbar hinzufügen
        cbar = fig.colorbar(surf, ax=ax, shrink=0.5, aspect=10, pad=0.15)
        cbar.set_label('COP', fontsize=11)

        # Zahlenformatierung für die Colorbar und Achsen
        ax.xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f}'))
        ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
        ax.zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))
        cbar.ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))

        ax.view_init(elev=25, azim=60)  # Blickwinkel anpassen
        ax.invert_xaxis()


        # Plot speichern
        plot_file_svg = os.path.join(plot_base_dir, f'COPPlot_massenstrom_{massenstrom_ziel:.2f}_Verdampfertemp_{verdampfer_temp:.1f}.svg')

        # Speichern als SVG
        plt.savefig(plot_file_svg, format='svg', bbox_inches='tight')

        print(f'Der Plot "{plot_file_svg}" wurde erfolgreich erstellt.')

        # Plot anzeigen
        plt.show()
