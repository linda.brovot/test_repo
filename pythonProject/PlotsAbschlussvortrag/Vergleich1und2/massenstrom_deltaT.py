import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import re
import matplotlib.font_manager as font_manager
import locale

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

# Pfade zu den CSV-Ordnern für die verschiedenen Massenströme und Pumpendrehzahlen
base_dir_mass_flow = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CSV_Results"
base_dir_deltaT = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CSV_Results"

# Speichert die Temperaturdifferenzen und die kumulierten Massenströme
results_temp_diff = {}
results_mass_flow = {}

# Verarbeitung der Temperaturdifferenzen für verschiedene Massenströme
for folder in os.listdir(base_dir_mass_flow):
    if not folder.startswith("csv_"):
        continue  # überspringe nicht passende Ordner

    # Extrahiere den Massenstrom aus dem Ordnernamen (z.B. csv_0.15 -> mass_flow = 0.15)
    try:
        mass_flow = float(folder.split('_')[-1])
    except ValueError:
        print(f"Konnte den Massenstrom aus dem Ordnernamen {folder} nicht extrahieren.")
        continue

    folder_path = os.path.join(base_dir_mass_flow, folder)
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    for csv_file_path in csv_files:
        # Extrahiere die Drehzahl aus dem Dateinamen
        name = os.path.splitext(os.path.basename(csv_file_path))[0]
        cleaned_filename = csv_file_path.replace(".csv.csv", ".csv")
        rpm_match = re.search(r'_(\d+\.\d+)\.csv$', cleaned_filename)
        if rpm_match:
            rpm = float(rpm_match.group(1))
        else:
            continue

        # Lese die Daten aus der CSV-Datei
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Filtere den DataFrame, um nur Daten ab Sekunde 100 zu behalten
        if df.index.max() < 100:
            continue
        df = df[df.index >= 100]

        # Berechnung der Temperaturdifferenz (heatPump.sigBus.TConOutMea - heatPump.sigBus.TConInMea)
        if 'heatPump.sigBus.TConOutMea' not in df.columns or 'heatPump.sigBus.TConInMea' not in df.columns:
            continue

        df["Temperature_Difference"] = df['heatPump.sigBus.TConOutMea'] - df['heatPump.sigBus.TConInMea']
        avg_temperature_difference = df["Temperature_Difference"].mean()

        # Speichere die Ergebnisse
        if mass_flow not in results_temp_diff:
            results_temp_diff[mass_flow] = {}
        results_temp_diff[mass_flow][rpm] = avg_temperature_difference

# Verarbeitung der kumulierten Massenströme für verschiedene DeltaTs
for deltaT_folder in os.listdir(base_dir_deltaT):
    if not deltaT_folder.startswith("csv_"):
        continue  # überspringe nicht passende Ordner

    # Extrahiere das DeltaT aus dem Ordnernamen (z.B. csv_5 -> deltaT = 5)
    deltaT = int(deltaT_folder.split('_')[-1])

    folder_path = os.path.join(base_dir_deltaT, deltaT_folder)
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    for csv_file_path in csv_files:
        # Lese die Daten aus der CSV-Datei
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Filtere den DataFrame, um nur Daten ab Sekunde 100 zu behalten
        df = df[df['Time'] >= 100]

        # Berechne den kumulierten Massenstrom
        total_mass_flow = df["floMacDyn.m_flow"].sum()
        total_time = df["Time"].iloc[-1] - df["Time"].iloc[0]
        avg_mass_flow = total_mass_flow / total_time

        # Extrahiere die Drehzahl aus dem Dateinamen
        name = os.path.splitext(os.path.basename(csv_file_path))[0]
        rpm_match = re.search(r'(\d+\.\d+)', name)
        if rpm_match:
            rpm = float(rpm_match.group(1))
        else:
            continue

        # Speichere die Ergebnisse
        if deltaT not in results_mass_flow:
            results_mass_flow[deltaT] = {}
        results_mass_flow[deltaT][rpm] = avg_mass_flow

# Erstellung der Subplots für die beiden Diagramme
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))

# Plot der Temperaturdifferenzen
colors_temp = {0.05: 'darkred', 0.15: 'red', 0.25: 'grey'}
markers_temp = {0.05: 'o', 0.15: 'o', 0.25: 'o'}

for mass_flow, temperature_differences in results_temp_diff.items():
    rpms = sorted(temperature_differences.keys())
    avg_differences = [temperature_differences[rpm] for rpm in rpms]

    ax1.plot([r * 100 for r in rpms], avg_differences, marker=markers_temp[mass_flow], color=colors_temp[mass_flow], label=f'{locale.format_string("%.2f", mass_flow).replace(".", ",")} kg/s')

# Formatierung der Achsen für die Temperaturdifferenzen
ax1.set_xlabel('Drehzahl in Hz', fontsize=13, fontproperties=prop)
ax1.set_ylabel('Durchschnittliche Temperaturspreizung in K', fontsize=12, fontproperties=prop)
ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: locale.format_string('%.0f', y).replace('.', ',')))
ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: locale.format_string('%.0f', x).replace('.', ',')))
for label in ax1.get_xticklabels() + ax1.get_yticklabels():
    label.set_fontproperties(prop)
ax1.legend(prop=prop)
ax1.grid(False)

# Plot der durchschnittlichen Massenströme
colors_flow = {5: 'cornflowerblue', 8: 'green', 10: 'salmon'}
markers_flow = {5: 'o', 8: 'o', 10: 'o'}

for deltaT, mass_flows in results_mass_flow.items():
    rpms = sorted(mass_flows.keys())
    avg_flows = [mass_flows[rpm] for rpm in rpms]

    ax2.plot(rpms, avg_flows, marker=markers_flow[deltaT], color=colors_flow[deltaT], label=f'{locale.format_string("%.0f", deltaT).replace(".", ",")} K')

# Formatierung der Achsen für die Massenströme
ax2.set_xlabel('Drehzahl in Hz', fontsize=13, fontproperties=prop)
ax2.set_ylabel('Durchschnittlicher Massenstrom in kg/s', fontsize=12, fontproperties=prop)
ax2.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: locale.format_string('%.0f', x * 100).replace('.', ',')))
ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: locale.format_string('%.2f', y).replace('.', ',')))
for label in ax2.get_xticklabels() + ax2.get_yticklabels():
    label.set_fontproperties(prop)
ax2.legend(prop=prop)
ax2.grid(False)

# Anpassung des Layouts und Speichern der Diagramme
plt.tight_layout()

# Speicherort für die Diagramme
script_name = os.path.splitext(os.path.basename(__file__))[0]
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\VergleichStudie1und2\Plots"
output_dir = os.path.join(results_base_dir, script_name)

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Speicherpfade für PNG, SVG und EPS definieren
plot_path_png = os.path.join(output_dir, f"combined_plot_massenstrom_DeltaT.png")
plot_path_svg = os.path.join(output_dir, f"combined_plot_massenstrom_DeltaT.svg")
plot_path_eps = os.path.join(output_dir, f"combined_plot_massenstrom_DeltaT.eps")

# Plot als PNG, SVG und EPS speichern
plt.savefig(plot_path_png, format='png')
plt.savefig(plot_path_svg, format='svg')
plt.savefig(plot_path_eps, format='eps')

# Plot anzeigen
plt.show()

# Ausgabe der Speicherorte
print(f"Die Diagramme wurden unter '{plot_path_png}', '{plot_path_svg}' und '{plot_path_eps}' gespeichert.")
