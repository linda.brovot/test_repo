import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import numpy as np
import matplotlib.font_manager as font_manager

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None


def get_sums(system_results_df, identifier):
    drehzahlen = [30, 40, 50, 60, 70, 80, 90]
    pel_sums = []
    pel_Pumpe_sums = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]

        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
            pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600

            pel_sums.append(pel_sum)
            pel_Pumpe_sums.append(pel_Pumpe_sum)

    return drehzahlen, pel_sums, pel_Pumpe_sums


def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")

    # Speichern des Plots als EPS-Datei
    fig.savefig(f"{filename_base}.eps", format='eps')
    print(f"Plot gespeichert: {filename_base}.eps")


def create_broken_axis(ax, ylim, break_position=5.0, **kwargs):
    """
    Create a broken y-axis on the given axis object.
    """
    ax.set_ylim(ylim)

    # Add break lines
    d = .015  # how big to make the diagonal lines in axes coordinates
    kwargs.update(transform=ax.transAxes, color='k', clip_on=False)
    ax.plot((-d, +d), (-d, +d), **kwargs)  # bottom-left diagonal
   # ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # bottom-right diagonal


def plot_sum_comparison_with_best_case(results_base_dir_1, results_base_dir_2, output_dir, script_name, identifiers,
                                       labels, colors, hatches):
    fig, (ax_zoom, ax_exergie) = plt.subplots(1, 2, figsize=(8, 4))

    bar_width = 0.2  # Breite der Balken
    index = np.arange(3)  # Positionen für 30, 40 und 50 Hz

    # Temperaturdifferenzdaten plotten
    exergie_5K = None  # Variable für Exergie der 5K Kurve
    for idx, identifier in enumerate(identifiers):
        system_results_df, layer_results_df = read_csv_files(results_base_dir_1, identifier)
        if system_results_df is not None and layer_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums = get_sums(system_results_df, identifier)
            if drehzahlen:
                # Zoom-In Bereich (30, 40, 50 Hz)
                drehzahlen_zoom = drehzahlen[:3]
                pel_sums_zoom = pel_sums[:3]
                pel_Pumpe_sums_zoom = pel_Pumpe_sums[:3]
                combined_sums_zoom = [wp + pumpe for wp, pumpe in zip(pel_sums_zoom, pel_Pumpe_sums_zoom)]

                # Gestapelte Balken: WP-Leistung (unten) + Pumpe-Leistung (oben, schraffiert)
                ax_zoom.bar(index + idx * bar_width, pel_sums_zoom, bar_width, color=colors[idx], edgecolor='black',
                            label=f'WP {labels["label"]} {locale.format_string("%.2f", identifier)}')
                ax_zoom.bar(index + idx * bar_width, pel_Pumpe_sums_zoom, bar_width, bottom=pel_sums_zoom,
                            hatch=hatches[idx], edgecolor='black', color=colors[idx])

                # Rechter Subplot: Exergie im Speicher (Linienplot mit Punkten)
                sum_diff_exergies = [
                    system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")][
                        'Sum_Differences_Exergies'].values[0] / 1000 / 3600
                    for hz in drehzahlen]
                ax_exergie.plot(drehzahlen, sum_diff_exergies, marker='o', linestyle='-',
                                label=f'Exergie {identifier}', color=colors[idx])

                # Speichere die Exergie-Werte für 5 K zur späteren Berechnung der Abweichungen
                if identifier == 5:
                    exergie_5K = sum_diff_exergies

    # Best-Case Massenstromdaten hinzufügen
    best_mass_identifier_pump_energy = 0.15
    best_mass_identifier_exergie = 0.25

    # Hole die Daten für den besten Massenstromfall bei 0.15 kg/s für die Energie
    system_results_df_best_pump, _ = read_csv_files(results_base_dir_2, best_mass_identifier_pump_energy)
    if system_results_df_best_pump is not None:
        drehzahlen, pel_sums_best, pel_Pumpe_sums_best = get_sums(system_results_df_best_pump,
                                                                  best_mass_identifier_pump_energy)
        drehzahlen_zoom_best = drehzahlen[:3]
        pel_sums_zoom_best = pel_sums_best[:3]
        pel_Pumpe_sums_zoom_best = pel_Pumpe_sums_best[:3]
        combined_sums_zoom_best = [wp + pumpe for wp, pumpe in zip(pel_sums_zoom_best, pel_Pumpe_sums_zoom_best)]

        # Plot the Best-Case bars
        ax_zoom.bar(index + len(identifiers) * bar_width, pel_sums_zoom_best, bar_width, color='blue',
                    edgecolor='black',
                    label=f'Best-Case')
        ax_zoom.bar(index + len(identifiers) * bar_width, pel_Pumpe_sums_zoom_best, bar_width,
                    bottom=pel_sums_zoom_best, edgecolor='black',
                    hatch='/', color='blue')

    # Hole die Daten für den besten Massenstromfall bei 0.25 kg/s für die Exergie
    system_results_df_best_exergie, _ = read_csv_files(results_base_dir_2, best_mass_identifier_exergie)
    if system_results_df_best_exergie is not None:
        sum_diff_exergies_best = [system_results_df_best_exergie[system_results_df_best_exergie['Name'].str.contains(
            f"_{best_mass_identifier_exergie}_{hz / 100:.1f}.csv")]['Sum_Differences_Exergies'].values[0] / 1000 / 3600
                                  for hz in drehzahlen]
        ax_exergie.plot(drehzahlen, sum_diff_exergies_best, marker='x', linestyle='--',
                        label=f'Best-Case', color='blue')

    # Einstellungen für den Zoom-In Subplot (links)
    create_broken_axis(ax_zoom, ylim=[4.92, 5.6])  # Verwende die Funktion hier
    ax_zoom.set_ylabel('el. Energie in kWh', fontsize=13, fontproperties=prop)
    ax_zoom.set_xlabel('Drehzahl in Hz', fontsize=13, fontproperties=prop)
    ax_zoom.grid(False)
    ax_zoom.set_xticks(index + bar_width)
    ax_zoom.set_xticklabels([30, 40, 50], fontproperties=prop)
    ax_zoom.set_yticklabels([f"{label:.1f}".replace('.', ',') for label in ax_zoom.get_yticks()],
                               fontproperties=prop)
    ax_zoom.tick_params(axis='both', which='major', labelsize=12)
    for label in ax_zoom.get_yticklabels():
        label.set_fontproperties(prop)

    # Einstellungen für den Exergie Subplot (rechts)
    ax_exergie.set_xlabel('Drehzahl in Hz', fontsize=13, fontproperties=prop)
    ax_exergie.set_ylabel('Exergie im Speicher in kWh', fontsize=13, fontproperties=prop)
    ax_exergie.grid(False)
    ax_exergie.set_xticks([30, 40, 50, 60, 70, 80, 90])

    ax_exergie.tick_params(axis='both', which='major', labelsize=12)
    ax_exergie.set_xticklabels([str(int(label)) for label in ax_exergie.get_xticks()], fontproperties=prop)
    ax_exergie.set_yticklabels([f"{label:.2f}".replace('.', ',') for label in ax_exergie.get_yticks()],
                               fontproperties=prop)

    # Legenden für WP-Leistung (einschließlich Best-Case)
    wp_legend = [Patch(facecolor=colors[idx], edgecolor='black',
                       label=f'{locale.format_string("%.0f", identifier)} {labels["unit"]}') for
                 idx, identifier in enumerate(identifiers)]
    wp_legend.append(Patch(facecolor='blue', edgecolor='black', label=f'Best-Case'))

    # WP-Leistung und Pumpe-Leistung Legenden hinzufügen
    # Legenden für WP-Leistung (einschließlich Best-Case)
    wp_leg = ax_zoom.legend(
        handles=wp_legend, loc='upper left', fontsize=10, prop=prop,
        handletextpad=0.5, borderpad=0.4, labelspacing=0.3
    )
    # Legenden für das Linienplot im rechten Subplot (Exergie)
    custom_legend_lines_exergie = [
        Line2D([0], [0], marker='o', linestyle='-', color=color, lw=2,
               label=f'{identifier} K')
        for color, identifier in zip(colors, identifiers)
    ]
    custom_legend_lines_exergie.append(Line2D([0], [0], marker='x', linestyle='--', color='blue', lw=2,
                                              label=f'Best-Case'))

    # Legende für Exergie hinzufügen

    ax_exergie.legend(
        handles=custom_legend_lines_exergie, loc='best', fontsize=10, prop=prop,
        handletextpad=0.5, borderpad=0.4, labelspacing=0.3
    )
    plt.tight_layout()

    # Plot speichern
    plot_and_save(fig, os.path.join(output_dir, f"{script_name}_{labels['filename']}"))

    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\VergleichStudie1und2\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Hatches für die Schraffur der Pumpe-Leistung
    hatches = ['//', '//', '//']

    # Aufruf der neuen Funktion mit den gewünschten Parametern
    plot_sum_comparison_with_best_case(results_base_dir_1, results_base_dir_2, output_dir, script_name,
                                       identifiers=[5, 8, 10],
                                       labels={"title": "Temperaturdifferenzen", "label": "Delta T",
                                               "filename": "delta_t",
                                               "unit": "K"},
                                       colors=['cornflowerblue', 'green', 'salmon'],
                                       hatches=hatches)