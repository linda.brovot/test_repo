import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager  # Import für Schriftarten
import matplotlib

# Lokaleinstellungen für deutsche Formatierung
locale.setlocale(locale.LC_NUMERIC, 'de_DE')
plt.rcParams['axes.formatter.use_locale'] = True

matplotlib.rcParams['svg.fonttype'] = 'path'  # Schriftart in SVG-Dateien als Text erhalten

# Pfad zur Schriftart (Anpassung mit rohem String oder doppeltem Backslash)
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=11)

def format_decimal(value):
    """Formatierungsfunktion für Dezimalzahlen im deutschen Format mit Komma.
    Zeigt 0 anstelle von 0,00 an und entfernt Nachkommastellen bei ganzen Zahlen."""
    if abs(value) < 0.01:  # Wenn der Wert nahe bei 0 liegt, zeige einfach 0 an
        return '0'
    elif value.is_integer():  # Wenn der Wert eine ganze Zahl ist, zeige sie ohne Nachkommastellen an
        return locale.format_string('%.0f', value).replace('.', ',')
    else:  # Bei Dezimalzahlen zeige 2 Nachkommastellen an
        return locale.format_string('%.2f', value).replace('.', ',')

def read_csv_files(base_dir, identifier):
    folder_path = os.path.join(base_dir, f"results_{identifier}")
    csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {csv_file}")
        return None

def plot_efficiency(data_frame, identifier):
    eta_system_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]

                if not pd.isna(eta_WPSS):
                    eta_system_list.append(eta_WPSS)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, eta_system_list

def plot_loading_time(data_frame, identifier):
    beladezeit_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                beladezeit = filtered_df['Hours'].values[0]

                if not pd.isna(beladezeit):
                    beladezeit_list.append(beladezeit)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, beladezeit_list

def combined_subplots(results_base_dirs, output_dir, script_name):
    # Erstellen des Figure- und Axis-Objekts mit den angepassten Größen
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), gridspec_kw={'width_ratios': [1, 2]})
    colors = ['cornflowerblue', 'green', 'salmon', 'blue']

    identifiers_1 = [5, 8, 10]  # Delta T
    min_frequencies = [30, 40, 50, 60, 70, 80, 90]  # Frequenzen

    kennzahlen_combined = {}
    drehzahlen_combined = []
    differenzen_effizienz = []
    system_results_5K = read_csv_files(results_base_dirs[1], 5)
    frequencies_5K, eta_system_5K = plot_efficiency(system_results_5K, 5)

    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, eta_system_list = plot_efficiency(system_results_df, identifier)
            if frequencies:
                deltaT_label = f'{identifier} K'
                kennzahlen_combined[deltaT_label] = eta_system_list
                drehzahlen_combined = frequencies

    max_eta = []
    for freq in min_frequencies:
        massenstrom_identifier = 0.15 if freq in [30, 40] else 0.25
        system_results_df = read_csv_files(results_base_dirs[0], massenstrom_identifier)
        if system_results_df is not None:
            filtered_df = system_results_df[
                system_results_df['Name'].str.contains(f"_{massenstrom_identifier}_{freq / 100}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                max_eta.append(eta_WPSS)
            else:
                max_eta.append(0)

    ax1.plot(min_frequencies, max_eta, marker='x', linestyle='--', color='blue', label="Best-Case", markersize=8, linewidth=2)

    for i, (label, kennzahlen) in enumerate(kennzahlen_combined.items()):
        color = colors[i]
        ax1.plot(drehzahlen_combined, kennzahlen, marker='o', linestyle='-', color=color, label=label, markersize=6, linewidth=1.5)

    for i, freq in enumerate(min_frequencies):
        if freq in frequencies_5K:
            index_5K = frequencies_5K.index(freq)
            eta_5K = eta_system_5K[index_5K]
            eta_max = max_eta[i]
            prozentualer_unterschied = ((eta_max - eta_5K) / eta_5K) * 100 if eta_5K != 0 else 0
            differenzen_effizienz.append({
                'Frequenz (Hz)': freq,
                '5K Nutzungsgrad': eta_5K,
                'Maximaler Nutzungsgrad': eta_max,
                'Prozentuale Differenz (%)': prozentualer_unterschied
            })
            # Hier verwenden wir die format_decimal-Funktion für die Prozentanzeige
            ax1.text(freq, eta_max + 0.005, f'{format_decimal(prozentualer_unterschied)}%', color='black', fontproperties=prop)

    ax1.set_xlabel('Drehzahl in Hz', fontsize=12, fontproperties=prop)
    ax1.set_ylabel('Nutzungsgrad', fontsize=12, fontproperties=prop)
    ax1.set_ylim(0.135, 0.22)
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(0.02))
    ax1.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_decimal(x)))
    ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_decimal(x)))

    ax1.grid(False)

    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend(handles, labels, loc='best', ncol=1, frameon=False, prop=prop)

    bar_width = 0.2
    indices = np.arange(len(min_frequencies))

    frequencies_5K, beladezeit_5K = plot_loading_time(system_results_5K, 5)

    bar_data = {identifier: [] for identifier in identifiers_1}
    min_beladezeit = []
    differenzen_beladezeit = []

    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, beladezeit_list = plot_loading_time(system_results_df, identifier)
            for freq in min_frequencies:
                if freq in frequencies:
                    index = frequencies.index(freq)
                    bar_data[identifier].append(beladezeit_list[index])
                else:
                    bar_data[identifier].append(float('nan'))

    for freq in min_frequencies:
        massenstrom_identifier = 0.05  # Massenstrom für Minimalwerte
        system_results_df = read_csv_files(results_base_dirs[0], massenstrom_identifier)
        if system_results_df is not None:
            filtered_df = system_results_df[
                system_results_df['Name'].str.contains(f"_{massenstrom_identifier}_{freq / 100}.csv")]
            if not filtered_df.empty:
                beladezeit = filtered_df['Hours'].values[0]
                min_beladezeit.append(beladezeit)
            else:
                min_beladezeit.append(float('inf'))

    for i, identifier in enumerate(identifiers_1):
        ax2.bar(indices + i * bar_width, bar_data[identifier], bar_width,
                label=f'{identifier} K', edgecolor='black',
                color=colors[i], linewidth=0.5)

    ax2.bar(indices + len(identifiers_1) * bar_width, min_beladezeit, bar_width, label="Best-Case", color=colors[-1], edgecolor='black', linewidth=0.5)

    for i, freq in enumerate(min_frequencies):
        beladezeit_10K = bar_data[10][i] if not pd.isna(bar_data[10][i]) else float('inf')
        beladezeit_min = min_beladezeit[i]
        if beladezeit_10K != float('inf') and beladezeit_min != float('inf'):
            prozentualer_unterschied = ((beladezeit_10K - beladezeit_min) / beladezeit_min) * 100
            differenzen_beladezeit.append({
                'Frequenz (Hz)': freq,
                '10K Beladedauer (Stunden)': beladezeit_10K,
                'Minimale Beladedauer (Stunden)': beladezeit_min,
                'Prozentuale Differenz (%)': prozentualer_unterschied
            })
            ax2.text(indices[i] + len(identifiers_1) * bar_width, beladezeit_min + 0.05,
                     f'{format_decimal(prozentualer_unterschied)}%', ha='center', color='black', fontproperties=prop)

    ax2.set_xlabel('Drehzahl in Hz', fontsize=12, fontproperties=prop)
    ax2.set_ylabel('Beladedauer in h', fontsize=12, fontproperties=prop)
    ax2.set_xticks(indices + bar_width * 1.5)
    ax2.set_xticklabels([f'{freq}' for freq in min_frequencies], fontproperties=prop)
    ax2.set_ylim([0, 7])
    ax2.legend(loc='best', ncol=1, frameon=False, prop=prop)

    ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: format_decimal(x)))

    # Schriftart auf alle Labels anwenden
    for label in (ax1.get_xticklabels() + ax1.get_yticklabels() + ax2.get_yticklabels()):
        label.set_fontproperties(prop)

    combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined_subplots.png")
    combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined_subplots.svg")
    combined_plot_filename_eps = os.path.join(output_dir, f"{script_name}_combined_subplots.eps")  # Hinzufügen des EPS-Formats

    plt.tight_layout()
    plt.subplots_adjust(wspace=0.2)

    plt.savefig(combined_plot_filename_png, bbox_inches='tight')
    plt.savefig(combined_plot_filename_svg, bbox_inches='tight')
    plt.savefig(combined_plot_filename_eps, format='eps', bbox_inches='tight')  # Speichern als EPS

    plt.show()
    plt.close(fig)

    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png}, {combined_plot_filename_svg}, und {combined_plot_filename_eps}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dirs = [
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy",
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy"
    ]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\VergleichStudie1und2\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_subplots(results_base_dirs, output_dir, script_name)
