import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import re
import matplotlib
import matplotlib.font_manager as font_manager  # Für die Schriftart
from matplotlib.ticker import FuncFormatter

# Lokaleinstellungen für deutsche Zahlenformatierung
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')
plt.rcParams['axes.formatter.use_locale'] = True

# Schriftart laden
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=11)

matplotlib.rcParams['svg.fonttype'] = 'path'  # Schriftart in SVG-Dateien als Text erhalten

def comma_format(x, pos):
    return '{:,.2f}'.format(x).replace('.', ',')

def read_csv_files(base_dir, pattern_str):
    data_frames = []
    pattern = re.compile(pattern_str)
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if pattern.match(file):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15
                    df['Temperature'] = temp_celsius
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius:.2f}°C")
                else:
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames

def combined_plots(mass_flow_data_frames, temperature_data_frames, output_dir):
    best_case_mass_flow = (0.05, 90)
    worst_case_mass_flow = (0.15, 30)

    best_case_eta = (0.15, 30)
    worst_case_eta = (0.05, 90)

    mass_flows_of_interest = [0.05, 0.15, 0.25]
    temperatures_of_interest = [65, 70]

    beladedauer_combined = {'Best Case': [], 'Worst Case': [], '65°C': [], '70°C': []}
    # Labels für die x-Achse mit Zeilenumbruch
    labels_beladedauer = ['Best Case\n(90 Hz)', 'Worst Case\n(30 Hz)', '65°C', '70°C']


    eta_system_combined = {'Best Case': [], 'Worst Case': [], '65°C': [], '70°C': []}
    labels_eta = ['Best Case\n(30 Hz)', 'Worst Case\n(90 Hz)', '65°C', '70°C']

    results = {
        'Category': [],
        'Massenstrom (kg/s)': [],
        'Beladedauer (h)': [],
        'Nutzungsgrad WPSS': [],
        'Prozentuale Änderung (%)': []
    }

    for df in mass_flow_data_frames:
        filtered_df_best = df[df['Name'].str.contains(f"_{best_case_mass_flow[0]}_{best_case_mass_flow[1] / 100}")]
        filtered_df_worst = df[df['Name'].str.contains(f"_{worst_case_mass_flow[0]}_{worst_case_mass_flow[1] / 100}")]
        if not filtered_df_best.empty:
            beladedauer_combined['Best Case'].append(filtered_df_best['Hours'].values[0])
        if not filtered_df_worst.empty:
            beladedauer_combined['Worst Case'].append(filtered_df_worst['Hours'].values[0])

    for df in mass_flow_data_frames:
        filtered_df_best = df[df['Name'].str.contains(f"_{best_case_eta[0]}_{best_case_eta[1] / 100}")]
        filtered_df_worst = df[df['Name'].str.contains(f"_{worst_case_eta[0]}_{worst_case_eta[1] / 100}")]
        if not filtered_df_best.empty:
            eta_system_combined['Best Case'].append(filtered_df_best['Eta_WPSS'].values[0])
        if not filtered_df_worst.empty:
            eta_system_combined['Worst Case'].append(filtered_df_worst['Eta_WPSS'].values[0])

    for temp in temperatures_of_interest:
        key = f'{int(temp)}°C'
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        beladedauer_combined[key].append(filtered_df['Hours'].values[0])
                        eta_system_combined[key].append(filtered_df['Eta_WPSS'].values[0])

    fig, (ax2, ax1) = plt.subplots(1, 2, figsize=(8, 3.5))  # Größere Plot-Größe für LaTeX

    bar_width = 0.2
    index = np.arange(len(labels_beladedauer))

    colors = ['darkred', 'red', 'grey']

    ax1.bar(index[0], beladedauer_combined['Best Case'][0], bar_width, color=colors[0], edgecolor='black',
            label='0,05 kg/s')
    ax1.bar(index[1], beladedauer_combined['Worst Case'][0], bar_width, color=colors[1], edgecolor='black',
            label='0,15 kg/s')

    for i, mass_flow in enumerate(mass_flows_of_interest):
        if i < len(beladedauer_combined['65°C']):
            ax1.bar(index[2] + i * bar_width - bar_width, beladedauer_combined['65°C'][i], bar_width, color=colors[i],
                    edgecolor='black', label=f'{mass_flow}'.replace('.', ',') + ' kg/s')

        if i < len(beladedauer_combined['70°C']):
            ax1.bar(index[3] + i * bar_width - bar_width, beladedauer_combined['70°C'][i], bar_width, color=colors[i],
                    edgecolor='black')

            # Spezifische Prozentsätze einfügen
            if mass_flow == 0.25:
                val1 = beladedauer_combined['70°C'][0]  # Wert für 0,05 kg/s
                val2 = beladedauer_combined['70°C'][2]  # Wert für 0,25 kg/s
                percent_change = ((val2 - val1) / val1) * 100
                ax1.plot([index[3], index[3] + 2 * bar_width], [val1, val2], 'k--', lw=1)
                ax1.text(index[3] + bar_width, val2 + 0.1, f'{percent_change:.1f}%'.replace('.', ','),
                         ha='center', va='bottom', fontproperties=prop)
                results['Category'].append('Beladedauer (70°C, 0,05 vs 0,25)')
                results['Massenstrom (kg/s)'].append(f'0,05 vs 0,25')
                results['Beladedauer (h)'].append(f'{val1} vs {val2}'.replace('.', ','))
                results['Nutzungsgrad WPSS'].append('N/A')
                results['Prozentuale Änderung (%)'].append(f'{percent_change:.1f}'.replace('.', ','))

            if i == 2:
                val_65 = beladedauer_combined['65°C'][i]
                val_70 = beladedauer_combined['70°C'][i]
                percent_change_beladedauer = ((val_65 - val_70) / val_65) * 100
                ax1.text(index[3] + i * bar_width - bar_width, val_70 + 0.1, f'{percent_change_beladedauer:.1f}%'.replace('.', ','), ha='center', va='bottom', fontproperties=prop)
                results['Category'].append('Beladedauer (65°C vs 70°C, 0,25 kg/s)')
                results['Massenstrom (kg/s)'].append(f'0,25 kg/s')
                results['Beladedauer (h)'].append(f'{val_65} vs {val_70}'.replace('.', ','))
                results['Nutzungsgrad WPSS'].append('N/A')
                results['Prozentuale Änderung (%)'].append(f'{percent_change_beladedauer:.1f}'.replace('.', ','))

    ax1.set_ylabel('Beladedauer in h', fontproperties=prop)
    ax1.set_xticks(index)
    ax1.set_xticklabels(labels_beladedauer, fontproperties=prop)

    # Setze das Format für die Y-Achse des Nutzungsgrads auf Komma als Dezimaltrennzeichen
    ax2.yaxis.set_major_formatter(FuncFormatter(comma_format))

    ax2.bar(index[0], eta_system_combined['Best Case'][0], bar_width, color=colors[1], edgecolor='black',
            label='0,15 kg/s')
    ax2.bar(index[1], eta_system_combined['Worst Case'][0], bar_width, color=colors[0], edgecolor='black',
            label='0,05 kg/s')

    for i, mass_flow in enumerate(mass_flows_of_interest):
        if i < len(eta_system_combined['65°C']):
            ax2.bar(index[2] + i * bar_width - bar_width, eta_system_combined['65°C'][i], bar_width, color=colors[i],
                    edgecolor='black', label=f'{mass_flow}'.replace('.', ',') + ' kg/s')

        if i < len(eta_system_combined['70°C']):
            ax2.bar(index[3] + i * bar_width - bar_width, eta_system_combined['70°C'][i], bar_width, color=colors[i],
                    edgecolor='black')

            # Spezifische Prozentsätze einfügen
            if mass_flow == 0.05:
                val1 = eta_system_combined['70°C'][1]  # Wert für 0,15 kg/s
                val2 = eta_system_combined['70°C'][0]  # Wert für 0,05 kg/s
                percent_change = ((val2 - val1) / val1) * 100
                ax2.plot([index[3] - bar_width, index[3]], [val1, val2], 'k--', lw=1)
                ax2.text(index[3] - bar_width/2, val2 + 0.01, f'{percent_change:.1f}%'.replace('.', ','),
                         ha='center', va='bottom', fontproperties=prop)
                results['Category'].append('Nutzungsgrad (70°C, 0,15 vs 0,05)')
                results['Massenstrom (kg/s)'].append(f'0,15 vs 0,05')
                results['Beladedauer (h)'].append('N/A')
                results['Nutzungsgrad WPSS'].append(f'{val1} vs {val2}'.replace('.', ','))
                results['Prozentuale Änderung (%)'].append(f'{percent_change:.1f}'.replace('.', ','))

            if mass_flow == 0.15:
                val_65 = eta_system_combined['65°C'][i]
                val_70 = eta_system_combined['70°C'][i]
                percent_change_eta = ((val_65 - val_70) / val_70) * 100
                ax2.plot([index[2] + bar_width, index[3] + bar_width], [val_65, val_70], 'k--', lw=1)
                ax2.text(index[3] + bar_width, val_70 + 0.01, f'{percent_change_eta:.1f}%'.replace('.', ','),
                         ha='center', va='bottom', fontproperties=prop)
                results['Category'].append('Nutzungsgrad (65°C vs 70°C, 0,15 kg/s)')
                results['Massenstrom (kg/s)'].append(f'0,15 kg/s')
                results['Beladedauer (h)'].append('N/A')
                results['Nutzungsgrad WPSS'].append(f'{val_65} vs {val_70}'.replace('.', ','))
                results['Prozentuale Änderung (%)'].append(f'{percent_change_eta:.1f}'.replace('.', ','))

            if mass_flow == 0.25:
                val_65 = eta_system_combined['65°C'][2]  # Wert für 0,25 kg/s bei 65°C
                val_70 = eta_system_combined['70°C'][2]  # Wert für 0,25 kg/s bei 70°C
                percent_change_eta = ((val_65 - val_70) / val_70) * 100
                ax2.plot([index[2] + 2 * bar_width, index[3] + 2 * bar_width], [val_65, val_70], 'k--', lw=1)
                ax2.text(index[3] + 2 * bar_width, val_70 + 0.01, f'{percent_change_eta:.1f}%'.replace('.', ','),
                         ha='center', va='bottom', fontproperties=prop)
                results['Category'].append('Nutzungsgrad (65°C vs 70°C, 0,25 kg/s)')
                results['Massenstrom (kg/s)'].append(f'0,25 kg/s')
                results['Beladedauer (h)'].append('N/A')
                results['Nutzungsgrad WPSS'].append(f'{val_65} vs {val_70}'.replace('.', ','))
                results['Prozentuale Änderung (%)'].append(f'{percent_change_eta:.1f}'.replace('.', ','))

    ax2.set_ylabel('Nutzungsgrad WPSS', fontproperties=prop)
    ax2.set_xticks(index)
    ax2.set_xticklabels(labels_eta, fontproperties=prop)

    # Schriftart auf die Y-Achsen-Ticks von ax1 und ax2 anwenden (Beladedauer und Nutzungsgrad):
    for label in ax1.get_yticklabels():
        label.set_fontproperties(prop)  # Schriftart auf Y-Ticks für ax1

    for label in ax2.get_yticklabels():
        label.set_fontproperties(prop)  # Schriftart auf Y-Ticks für ax2

    # Falls gewünscht, kannst du die Schriftart auch auf die X-Achsenticks anwenden:
    for label in ax1.get_xticklabels():
        label.set_fontproperties(prop)  # Schriftart auf X-Ticks für ax1

    for label in ax2.get_xticklabels():
        label.set_fontproperties(prop)  # Schriftart auf X-Ticks für ax2

    # Nur eine Legende für beide Plots basierend auf ax1
    handles, labels = ax1.get_legend_handles_labels()

    # Entferne doppelte Einträge
    unique_labels = []
    unique_handles = []
    for handle, label in zip(handles, labels):
        if label not in unique_labels:
            unique_labels.append(label)
            unique_handles.append(handle)

    mass_flow_labels = [
        r'0,05 kg/s',
        r'0,15 kg/s',
        r'0,25 kg/s'
    ]

    # Ersetze die Labels in der Legende mit den neuen Labels
    for i, label in enumerate(mass_flow_labels):
        unique_labels[i] = label

    # Setze die bereinigte Legende ohne "Massenströme:" hinzu
    fig.legend(unique_handles, unique_labels, loc='upper center', bbox_to_anchor=(0.5, 1), ncol=3, prop=prop,
               frameon=True, handletextpad=0.2, borderpad=0.5, labelspacing=0.1)

    # Anpassung des Layouts, um Platz für die Legende zu lassen
    plt.tight_layout(rect=[0, 0, 1, 0.9])

    combined_plot_filename_png = os.path.join(output_dir, "Nutzungsgrad_beladedauer_kombi.png")
    combined_plot_filename_svg = os.path.join(output_dir, "Nutzungsgrad_beladedauer_kombi.svg")
    combined_plot_filename_eps = os.path.join(output_dir, "Nutzungsgrad_beladedauer_kombi.eps")  # EPS-Datei speichern

    plt.savefig(combined_plot_filename_png, bbox_inches='tight')
    plt.savefig(combined_plot_filename_svg, bbox_inches='tight')
    plt.savefig(combined_plot_filename_eps, format='eps', bbox_inches='tight')  # Speichern als EPS

    plt.show()
    plt.close(fig)
    print(f"Kombiniertes Plot gespeichert: {combined_plot_filename_png}, {combined_plot_filename_svg}, und {combined_plot_filename_eps}")

    # Speichern der Ergebnisse in einer CSV-Datei
    results_df = pd.DataFrame(results)
    results_csv_filename = os.path.join(output_dir, "berechnete_ergebnisse.csv")
    results_df.to_csv(results_csv_filename, index=False, sep=';', decimal=',')
    print(f"Ergebnisse gespeichert in: {results_csv_filename}")

if __name__ == "__main__":
    mass_flow_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    temperature_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Vergleich1und3\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    mass_flow_data_frames = read_csv_files(mass_flow_dir, r'system_results_(\d+\.\d+)\.csv')
    temperature_data_frames = read_csv_files(temperature_dir, r'system_results_(\d+\.\d+)\.csv')

    combined_plots(mass_flow_data_frames, temperature_data_frames, output_dir)
