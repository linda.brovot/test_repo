import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import locale
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager  # Import für Schriftarten

# Schriftart und Pfad zur Schriftart festlegen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
prop = font_manager.FontProperties(fname=font_path, size=12)

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

# Pfade zu den CSV-Ordnern für Massenstrom und Vorlauftemperatur
mass_flow_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CSV_Results"
temperature_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CSV_Results"

mass_flows_of_interest = [0.05, 0.15, 0.25]
temperatures = [338.15, 343.15]  # Vorlauftemperaturen in Kelvin

# Dictionaries, um die Temperaturdifferenzen zu speichern
temp_differences_best_case = []
temp_differences_worst_case = []
temp_differences_65deg = []
temp_differences_70deg = []

# Best Case: 90 Hz und 0,05 kg/s, Worst Case: 30 Hz und 0,25 kg/s
best_case = (0.05, 90)
worst_case = (0.25, 30)
best_case_temperature = 343.15  # 70°C für den Schichtungsplot
best_case_mass_flow = 0.05

# -----------------------------
# 1. Lese die Massenstrom-Dateien (Best Case und Worst Case)
# -----------------------------
for mass_flow, frequency in [best_case, worst_case]:
    folder_path = os.path.join(mass_flow_base_dir, f"csv_{mass_flow}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    temp_diff = None

    for csv_file_path in csv_files:
        name = os.path.basename(csv_file_path).replace('.csv', '')
        frequency_str = name.split('_')[-1]

        try:
            file_frequency = float(frequency_str)
        except ValueError:
            continue

        if file_frequency == frequency / 100:
            df = pd.read_csv(csv_file_path, sep=";", decimal=",")
            if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
                continue

            df_end = df.iloc[-1]
            temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
            temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15
            temp_diff = temp_layer_10 - temp_layer_1
            break

    if frequency == 90:
        temp_differences_best_case.append(temp_diff if temp_diff is not None else 0)
    elif frequency == 30:
        temp_differences_worst_case.append(temp_diff if temp_diff is not None else 0)

# -----------------------------
# 2. Lese die Vorlauftemperatur-Dateien
# -----------------------------
for temperature in temperatures:
    for mass_flow in mass_flows_of_interest:
        folder_path = os.path.join(temperature_base_dir, f"csv_{temperature}")
        csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv') and f'_{mass_flow}' in file]

        if csv_files:
            csv_file_path = csv_files[0]
            df = pd.read_csv(csv_file_path, sep=";", decimal=",")
            if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
                continue

            df_end = df.iloc[-1]
            temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
            temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15
            temp_difference = temp_layer_10 - temp_layer_1

            if temperature == 338.15:
                temp_differences_65deg.append(temp_difference)
            elif temperature == 343.15:
                temp_differences_70deg.append(temp_difference)
        else:
            print(f"Datei für Vorlauftemperatur {temperature - 273.15:.2f}°C und Massenstrom {mass_flow} nicht gefunden.")
            if temperature == 338.15:
                temp_differences_65deg.append(0)
            elif temperature == 343.15:
                temp_differences_70deg.append(0)

# -----------------------------
# 3. Erstellung des Subplots für die Temperaturdifferenzen und die Schichtung
# -----------------------------

# Einstellungen für die Temperaturdifferenzen
labels = ['Best Case\n(90Hz)', 'Worst Case\n(30Hz)', '65°C', '70°C']
bar_width = 0.2  # Breite der Balken
index = np.arange(len(labels))

# Farben für Massenströme
colors = ['darkred', 'red', 'grey']

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4.5), gridspec_kw={'width_ratios': [1, 1.1]})

# Temperaturdifferenz-Plot (links)
bars_best_case = ax1.bar(index[0], temp_differences_best_case[0], bar_width, color=colors[0], edgecolor='black', label='0.05 kg/s')
bars_worst_case = ax1.bar(index[1], temp_differences_worst_case[0], bar_width, color=colors[2], edgecolor='black', label='0.25 kg/s')

# Vorlauftemperaturen (65°C und 70°C)
bars_65deg = []
bars_70deg = []
for i, mass_flow in enumerate(mass_flows_of_interest):
    bars_65deg.append(ax1.bar(index[2] + i * bar_width - bar_width, temp_differences_65deg[i], bar_width, color=colors[i], edgecolor='black', label=f'{mass_flow} kg/s'))
    bars_70deg.append(ax1.bar(index[3] + i * bar_width - bar_width, temp_differences_70deg[i], bar_width, color=colors[i], edgecolor='black'))

# Beschriftungen und Titel für den ersten Plot
#ax1.set_xlabel('Szenario', fontsize=12, fontproperties=prop)
ax1.set_ylabel('Temperaturdifferenz im Speicher in K', fontsize=13, fontproperties=prop)
ax1.set_xticks(index)
ax1.set_xticklabels(labels, fontsize=12, fontproperties=prop)

# Setzen der Schriftart und des Formatierers für die Ticks im linken Plot
ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: f'{int(y):.0f}'.replace('.', ',') if y.is_integer() else f'{y:.1f}'.replace('.', ',')))

for label in ax1.get_xticklabels() + ax1.get_yticklabels():
    label.set_fontproperties(prop)

# Legende für den ersten Plot
handles, labels = ax1.get_legend_handles_labels()
by_label = dict(zip(labels, handles))
sorted_handles = [by_label[f'{mass_flow:.2f} kg/s'] for mass_flow in [0.05, 0.15, 0.25]]
ax1.legend(sorted_handles, [f'{mass_flow:.2f}'.replace('.', ',').rstrip('0').rstrip(',') + ' kg/s' for mass_flow in [0.05, 0.15, 0.25]], fontsize=12, title_fontsize=12, prop=prop)

# -----------------------------
# 4. Schichtungsplot für Best Case (70°C, 0.05 kg/s)
# -----------------------------
# CSV-Datei für die Schichtung bei 70°C und 0,05 kg/s
schichtung_path = os.path.join(temperature_base_dir, f"csv_{best_case_temperature}")
schichtung_files = [os.path.join(schichtung_path, file) for file in os.listdir(schichtung_path) if file.endswith('.csv') and f'_{best_case_mass_flow}' in file]

# Wenn es eine Datei gibt, die den Kriterien entspricht, die Schichtung plotten
if schichtung_files:
    schichtung_file = schichtung_files[0]
    df_schichtung = pd.read_csv(schichtung_file, sep=";", decimal=",")
    df_schichtung["Hour"] = df_schichtung.index / 3600  # Zeit in Stunden umrechnen

    # Plot der Schichtung
    for i in range(1, 11):
        df_schichtung[f'bufferStorage.layer[{i}].T'] = df_schichtung[f'bufferStorage.layer[{i}].T'] - 273.15
        ax2.plot(df_schichtung["Hour"], df_schichtung[f'bufferStorage.layer[{i}].T'], label=f'{i}', linewidth=2)

    ax2.set_xlabel('Beladedauer in h', fontsize=13, fontproperties=prop)
    ax2.set_ylabel('Temperatur in °C', fontsize=13, fontproperties=prop)

    handles, labels = ax2.get_legend_handles_labels()
    ax2.legend(handles[::-1], labels[::-1], fontsize=10, prop=prop, ncol=2, title='Schicht', title_fontproperties=prop,
               columnspacing=0.5, handletextpad=0.4, labelspacing=0.4)

    ax2.grid(False)
    ax2.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: f'{int(x):.0f}' if x.is_integer() else f'{x:.1f}'.replace('.', ',')))

    # Entfernen der Nachkommastellen auf der Y-Achse
    ax2.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: f'{int(y):.0f}'.replace('.', ',')))

    # Setzen der Schriftart für die Ticks im rechten Plot
    for label in ax2.get_xticklabels() + ax2.get_yticklabels():
        label.set_fontproperties(prop)

# Plot speichern
output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Vergleich1und3\PLOTS"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

plt.tight_layout()

# Speicherpfade für PNG, SVG und EPS definieren
plot_path_png = os.path.join(output_dir, 'temperature_diff_and_schichtung_plot.png')
plot_path_svg = os.path.join(output_dir, 'temperature_diff_and_schichtung_plot.svg')
plot_path_eps = os.path.join(output_dir, 'temperature_diff_and_schichtung_plot.eps')

# Plot speichern
plt.savefig(plot_path_png, format='png')
plt.savefig(plot_path_svg, format='svg')
plt.savefig(plot_path_eps, format='eps')

# Plot anzeigen
plt.show()

print(f"Der kombinierte Plot wurde unter '{plot_path_png}', '{plot_path_svg}' und '{plot_path_eps}' gespeichert.")
