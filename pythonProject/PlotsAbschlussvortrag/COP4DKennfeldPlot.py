import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
import os

matplotlib.rcParams['svg.fonttype'] = 'none'

# CSV-Datei laden
file_path = 'D:/00_temp/simple_heat_pump/Standard_Propane.csv'
data = pd.read_csv(file_path)

# Umrechnen der Temperaturen von Kelvin in Grad Celsius
data['T_con_in in C'] = data['T_con_in in K (Secondary side condenser inlet temperature)'] - 273.15
data['T_eva_in in C'] = data['T_eva_in in K (Secondary side evaporator inlet temperature)'] - 273.15

# Basisverzeichnis für die Plots festlegen
plot_base_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\PLOTSLATEX\COPPlot'
os.makedirs(plot_base_dir, exist_ok=True)

# Plot 1: Einzelner 3D-Plot
ruecklauf_temp_1 = 50.0
massenstrom_1 = 0.15

fig1 = plt.figure()
ax1 = fig1.add_subplot(111, projection='3d')

filtered_data_1 = data[(data['T_con_in in C'] == ruecklauf_temp_1) &
                       (data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_1)]

x1 = filtered_data_1['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
y1 = filtered_data_1['T_eva_in in C']
cop1 = filtered_data_1['COP in - (Coefficient of performance)']

# Überprüfen, ob genug Datenpunkte vorhanden sind
if len(filtered_data_1) >= 3:
    # NaN-Werte ignorieren
    valid_data_mask_1 = np.isfinite(x1) & np.isfinite(y1) & np.isfinite(cop1)
    x1 = x1[valid_data_mask_1]
    y1 = y1[valid_data_mask_1]
    cop1 = cop1[valid_data_mask_1]

    # Gitter für die Interpolation erstellen
    xi1 = np.linspace(x1.min(), x1.max(), 100)
    yi1 = np.linspace(y1.min(), y1.max(), 100)
    xi1, yi1 = np.meshgrid(xi1, yi1)

    # Interpolation für die Oberfläche
    cop_interpolated_1 = griddata((x1, y1), cop1, (xi1, yi1), method='linear')

    # Oberfläche plotten
    surf1 = ax1.plot_surface(xi1, yi1, cop_interpolated_1, cmap='viridis')
    ax1.set_xlabel('Drehzahl (Hz)')
    ax1.set_ylabel('Quelleintrittstemperatur (°C)')
    ax1.set_zlabel('COP')
    ax1.set_title(f'TRL: {ruecklauf_temp_1:.1f} °C, ṁ_sek: {massenstrom_1:.2f} kg/s')

    # Colorbar hinzufügen
    cbar1 = fig1.colorbar(surf1, ax=ax1, shrink=0.5, aspect=5, pad=0.15)
    cbar1.set_label('COP', fontsize=11)

    # Zahlenformatierung für die Colorbar und Achsen
    ax1.xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f} Hz'))
    ax1.yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
    ax1.zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))
    cbar1.ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))

    # Plot speichern
    plot_file_svg_1 = os.path.join(plot_base_dir, f'COPPlot_TRL_{ruecklauf_temp_1:.1f}_MassFlow_{massenstrom_1:.2f}.svg')
    plt.savefig(plot_file_svg_1, format='svg', bbox_inches='tight')
    print(f'Der Plot "{plot_file_svg_1}" wurde erfolgreich erstellt.')

    # Plot anzeigen
    plt.show()

# Plot 2: Zwei 3D-Subplots nebeneinander
ruecklauf_temp_2 = 50.0
massenstrom_2 = 0.25
ruecklauf_temp_3 = 30.0
massenstrom_3 = 0.15

fig2, axs = plt.subplots(1, 2, subplot_kw={'projection': '3d'}, figsize=(14, 7))  # Zwei Subplots nebeneinander

# Erster Subplot
filtered_data_2 = data[(data['T_con_in in C'] == ruecklauf_temp_2) &
                       (data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_2)]

x2 = filtered_data_2['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
y2 = filtered_data_2['T_eva_in in C']
cop2 = filtered_data_2['COP in - (Coefficient of performance)']

if len(filtered_data_2) >= 3:
    # NaN-Werte ignorieren
    valid_data_mask_2 = np.isfinite(x2) & np.isfinite(y2) & np.isfinite(cop2)
    x2 = x2[valid_data_mask_2]
    y2 = y2[valid_data_mask_2]
    cop2 = cop2[valid_data_mask_2]

    # Gitter für die Interpolation erstellen
    xi2 = np.linspace(x2.min(), x2.max(), 100)
    yi2 = np.linspace(y2.min(), y2.max(), 100)
    xi2, yi2 = np.meshgrid(xi2, yi2)

    # Interpolation für die Oberfläche
    cop_interpolated_2 = griddata((x2, y2), cop2, (xi2, yi2), method='linear')

    # Oberfläche plotten
    surf2 = axs[0].plot_surface(xi2, yi2, cop_interpolated_2, cmap='viridis')
    axs[0].set_xlabel('Drehzahl (Hz)')
    axs[0].set_ylabel('Quelleintrittstemperatur (°C)')
    axs[0].set_zlabel('COP')
    axs[0].set_title(f'TRL: {ruecklauf_temp_2:.1f} °C, ṁ_sek: {massenstrom_2:.2f} kg/s')

    # Colorbar hinzufügen
    cbar2 = fig2.colorbar(surf2, ax=axs[0], shrink=0.5, aspect=5, pad=0.15)
    cbar2.set_label('COP', fontsize=11)

    # Zahlenformatierung für die Colorbar und Achsen
    axs[0].xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f} Hz'))
    axs[0].yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
    axs[0].zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))
    cbar2.ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))

# Zweiter Subplot
filtered_data_3 = data[(data['T_con_in in C'] == ruecklauf_temp_3) &
                       (data['m_flow_con in kg/s (Secondary side condenser mass flow rate)'] == massenstrom_3)]

x3 = filtered_data_3['n in - (Relative compressor speed)'] * 100  # Drehzahl mit 100 multiplizieren
y3 = filtered_data_3['T_eva_in in C']
cop3 = filtered_data_3['COP in - (Coefficient of performance)']

if len(filtered_data_3) >= 3:
    # NaN-Werte ignorieren
    valid_data_mask_3 = np.isfinite(x3) & np.isfinite(y3) & np.isfinite(cop3)
    x3 = x3[valid_data_mask_3]
    y3 = y3[valid_data_mask_3]
    cop3 = cop3[valid_data_mask_3]

    # Gitter für die Interpolation erstellen
    xi3 = np.linspace(x3.min(), x3.max(), 100)
    yi3 = np.linspace(y3.min(), y3.max(), 100)
    xi3, yi3 = np.meshgrid(xi3, yi3)

    # Interpolation für die Oberfläche
    cop_interpolated_3 = griddata((x3, y3), cop3, (xi3, yi3), method='linear')

    # Oberfläche plotten
    surf3 = axs[1].plot_surface(xi3, yi3, cop_interpolated_3, cmap='viridis')
    axs[1].set_xlabel('Drehzahl (Hz)')
    axs[1].set_ylabel('Quelleintrittstemperatur (°C)')
    axs[1].set_zlabel('COP')
    axs[1].set_title(f'TRL: {ruecklauf_temp_3:.1f} °C, ṁ_sek: {massenstrom_3:.2f} kg/s')

    # Colorbar hinzufügen
    cbar3 = fig2.colorbar(surf3, ax=axs[1], shrink=0.5, aspect=5, pad=0.15)
    cbar3.set_label('COP')

    # Zahlenformatierung für die Colorbar und Achsen
    axs[1].xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x:.0f} Hz'))
    axs[1].yaxis.set_major_formatter(plt.FuncFormatter(lambda y, _: f'{y:.0f}'.replace('.', ',')))
    axs[1].zaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))
    cbar3.ax.yaxis.set_major_formatter(plt.FuncFormatter(lambda z, _: f'{z:.2f}'.replace('.', ',')))

# Plot speichern
plot_file_svg_2 = os.path.join(plot_base_dir, f'COPPlot_Comparison.svg')
plt.savefig(plot_file_svg_2, format='svg', bbox_inches='tight')
print(f'Der Plot "{plot_file_svg_2}" wurde erfolgreich erstellt.')

# Plot anzeigen
plt.show()
