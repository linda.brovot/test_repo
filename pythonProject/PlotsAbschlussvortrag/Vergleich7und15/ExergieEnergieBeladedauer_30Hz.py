import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import locale
import matplotlib
import matplotlib.ticker as ticker

# Schriftart setzen
font_path = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\Overleaf\Heuristica-Regular.otf"
matplotlib.font_manager.fontManager.addfont(font_path)
matplotlib.rcParams['font.family'] = 'Heuristica'

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(system_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        return system_results_df
    else:
        print(f"CSV-Datei nicht gefunden: {system_csv_file}")
        return None

def get_values_for_drehzahl(system_results_df, identifier, target_drehzahl):
    # Ziel-Drehzahl als Prozentsatz
    target_str = f"_{identifier}_{target_drehzahl / 100:.1f}.csv"

    # Nach der Drehzahl im Dateinamen filtern
    filtered_df = system_results_df[system_results_df['Name'].str.contains(target_str)]

    if not filtered_df.empty:
        pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600  # in kWh
        exergie_sum = filtered_df['Sum_Differences_Exergies'].values[0] / 1000 / 3600  # in kWh
        beladedauer = filtered_df['Hours'].values[0]  # bereits in Stunden
        return pel_sum, exergie_sum, beladedauer
    else:
        print(f"Keine Daten für die Drehzahl {target_drehzahl} Hz gefunden.")
        return None, None, None

def plot_combined_data(output_dir):
    # Pfade für 15 Grad und 7 Grad
    results_base_dir_15 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_7 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_7\Massenstrom\CalculationExergy"

    # Identifier
    identifier = 0.15  # Beispielhaft gewählt

    # Ziel-Drehzahlen definieren
    target_drehzahl_15 = 30  # Für 15 Grad: 30 Hz
    target_drehzahl_7 = 30  # Für 7 Grad: 40 Hz

    # Daten aus beiden Pfaden extrahieren
    system_results_15 = read_csv_files(results_base_dir_15, identifier)
    system_results_7 = read_csv_files(results_base_dir_7, identifier)

    if system_results_15 is not None and system_results_7 is not None:
        # Extrahieren der Werte für die spezifischen Drehzahlen
        pel_15, exergie_15, beladedauer_15 = get_values_for_drehzahl(system_results_15, identifier, target_drehzahl_15)
        pel_7, exergie_7, beladedauer_7 = get_values_for_drehzahl(system_results_7, identifier, target_drehzahl_7)

        if pel_15 is not None and pel_7 is not None:
            # Exergetischen Nutzungsgrad berechnen
            eta_wpss_15 = exergie_15 / pel_15 if pel_15 != 0 else 0
            eta_wpss_7 = exergie_7 / pel_7 if pel_7 != 0 else 0

            # Prozentuale Änderung des Nutzungsgrades berechnen
            eta_deviation = (eta_wpss_7 - eta_wpss_15) / eta_wpss_15 * 100 if eta_wpss_15 != 0 else 0
            # Prozentuale Änderungen für Elektrische Energie und Exergie berechnen
            deviation_elektrische_energie = (pel_7 - pel_15) / pel_15 * 100 if pel_15 != 0 else 0
            deviation_exergie = (exergie_7 - exergie_15) / exergie_15 * 100 if exergie_15 != 0 else 0
            deviation_nutzungsgrad = eta_deviation  # Bereits berechnet
            deviation_beladedauer = (
                                                beladedauer_7 - beladedauer_15) / beladedauer_15 * 100 if beladedauer_15 != 0 else 0

            # Plotting der Daten
            labels = ['Elektrische\nEnergie', 'Exergie\nim Speicher']
            values_15 = [pel_15, exergie_15]
            values_7 = [pel_7, exergie_7]

            x = np.arange(len(labels))  # Anzahl der Kategorien
            width = 0.25  # Breite der Balken

            fig, (ax1, ax3) = plt.subplots(1, 2, figsize=(8, 4), gridspec_kw={'width_ratios': [3, 1]})

            # Balken für 15 Grad (Grau) und 7 Grad (Rot)
            bars1 = ax1.bar(x - width / 2, values_15, width, label='15 Grad (30 Hz)', color='grey', edgecolor='black',
                            alpha=0.8)
            bars2 = ax1.bar(x + width / 2, values_7, width, label='7 Grad (30 Hz)', color='red', edgecolor='black',
                            alpha=0.8)

            # zweite Y-Achse für Nutzungsgrad
            ax1_2 = ax1.twinx()
            bars_nutzungsgrad_15 = ax1_2.bar(2 - width / 2, eta_wpss_15, width, edgecolor='black', color='grey',
                                             alpha=0.8)
            bars_nutzungsgrad_7 = ax1_2.bar(2 + width / 2, eta_wpss_7, width, edgecolor='black', color='red', alpha=0.8)

            # Text über den roten Balken hinzufügen
            ax1.text(x[0] + width / 2, pel_7 + 0.1, f"+{deviation_elektrische_energie:.1f}%", ha='center', fontsize=12,
                     color='black')
            ax1.text(x[1] + width / 2, exergie_7 + 0.1, f"+{deviation_exergie:.1f}%", ha='center', fontsize=12,
                     color='black')
            ax1_2.text(2 + width / 2, eta_wpss_7 + 0.01, f"+{deviation_nutzungsgrad:.1f}%", ha='center', fontsize=12,
                       color='black')

            # Y-Achsen-Labels
            ax1.set_ylabel('kWh', fontsize=13)
            ax1.set_ylim([0, 7])
            ax1_2.set_ylabel('Nutzungsgrad', fontsize=13)
            ax1_2.set_ylim([0, 0.26])
            ax1_2.yaxis.set_major_formatter(
                ticker.FuncFormatter(
                    lambda x, pos: locale.format_string('%.2f', x).replace('.00', '').replace('0,00', '0').replace('.',
                                                                                                                   ',')
                )
            )

            # Schriftgröße der Ticks für ax1 und ax1_2 anpassen
            ax1.tick_params(axis='both', labelsize=12)  # Größe der Ticks (15) anpassen
            ax1_2.tick_params(axis='both', labelsize=12)  # Größe der Ticks (15) anpassen

            # x-Achsen-Label und Ticks
            ax1.set_xticks(np.append(x, [2]))
            ax1.set_xticklabels(labels + ['Nutzungsgrad'], fontsize=13)

            # Zweiter Plot für Beladedauer (verkleinerte Breite)
            width_beladedauer = 0.02  # Noch dünnere Balken für Beladedauer

            # Balken für Beladedauer so setzen, dass sie sehr eng nebeneinander sind
            bars_beladedauer_15 = ax3.bar(0 - width_beladedauer / 2, beladedauer_15, width=width_beladedauer,
                                          label='15 Grad (30 Hz)', edgecolor='black', color='grey', alpha=0.8)
            bars_beladedauer_7 = ax3.bar(0 + width_beladedauer / 2, beladedauer_7, width=width_beladedauer,
                                         label='7 Grad (30 Hz)', edgecolor='black', color='red', alpha=0.8)

            # Text über dem roten Balken für Beladedauer hinzufügen
            ax3.text(0 + width_beladedauer / 2, beladedauer_7 + 0.1, f"+{deviation_beladedauer:.1f}%", ha='center',
                     fontsize=12, color='black')

            # Y-Achsenbeschriftung und Limits setzen
            ax3.set_ylabel('Beladedauer in h', fontsize=13)
            ax3.set_xticks([0])  # Nur ein Tick für Beladedauer
            ax3.set_xticklabels(['Beladedauer'], fontsize=13)
            ax3.set_ylim([0, 9])

            # Optimierung der Balkenabstände
            ax3.margins(x=0.4)  # Abstand an den Seiten erhöhen, damit die Balken nicht überlappen

            # Schriftgröße der Ticks für ax3 anpassen
            ax3.tick_params(axis='both', labelsize=12)

            # Gemeinsame Legende hinzufügen
            fig.legend([bars1[0], bars2[0]], ['15 Grad (30 Hz)', '7 Grad (30 Hz)'], loc='upper center', ncol=2,
                       fontsize=13, title_fontsize=13, frameon=True)

            # Plot speichern
            fig.tight_layout(rect=[0, 0, 1, 0.94])
            plot_path_png = os.path.join(output_dir, "combined_plot_eta_beladedauer_30.png")
            plot_path_svg = os.path.join(output_dir, "combined_plot_eta_beladedauer_30.svg")
            plot_path_eps = os.path.join(output_dir, "combined_plot_eta_beladedauer_30.eps")
            fig.savefig(plot_path_png, format='png')
            fig.savefig(plot_path_svg, format='svg')
            fig.savefig(plot_path_eps, format='eps')

            print(f"Plot gespeichert: {plot_path_png}")
            print(f"Plot gespeichert: {plot_path_svg}")
            print(f"Plot gespeichert: {plot_path_eps}")

            # Plot anzeigen
            plt.show()


        else:
            print("Daten konnten nicht extrahiert werden.")
    else:
        print("Systemdaten für die angegebenen Pfade konnten nicht geladen werden.")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Latex\Vergleich7und15\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    plot_combined_data(output_dir)
