import pickle
import pandas as pd
import os


def save_dataframes_to_csv(data, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for key, value in data.items():
        if isinstance(value, pd.DataFrame):
            csv_file_path = os.path.join(output_dir, f"{key}.csv")
            value.to_csv(csv_file_path, index=False)
            print(f"DataFrame unter dem Schlüssel '{key}' erfolgreich in {csv_file_path} gespeichert.")
        else:
            print(f"Unerwarteter Datentyp unter dem Schlüssel '{key}': {type(value)}. Wird übersprungen.")


def read_and_save_pkl(pkl_file_path, output_dir):
    # Laden der .pkl-Datei
    with open(pkl_file_path, 'rb') as file:
        data = pickle.load(file)

    # Speichern der DataFrames in separate CSV-Dateien
    save_dataframes_to_csv(data, output_dir)


if __name__ == "__main__":
    # Pfad zur .pkl-Datei und zum Ausgabeverzeichnis
    pkl_file_path = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\data_fixed_comp_freq_lbr.pkl'
    output_dir = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\csv_output'

    # Datei lesen und DataFrames in CSV-Dateien speichern
    read_and_save_pkl(pkl_file_path, output_dir)
