import pandas as pd

# Pfad zur Eingabedatei
input_file = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\a-lfall.csv'

# Pfad zur Ausgabedatei
output_file = r'N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Ressources\schichttemperaturen_output_a-lfall.csv'

# CSV-Datei einlesen
df = pd.read_csv(input_file)

# Spalten auswählen: Zeit und alle T_layer_start Schichttemperatur-Spalten
columns_to_extract = ['Time', 'bufferStorage.layer[1].T', 'bufferStorage.layer[2].T', 'bufferStorage.layer[3].T', 'bufferStorage.layer[4].T',
                      'bufferStorage.layer[5].T', 'bufferStorage.layer[6].T', 'bufferStorage.layer[7].T', 'bufferStorage.layer[8].T', 'bufferStorage.layer[9].T', 'bufferStorage.layer[10].T']

df_filtered = df[columns_to_extract]

# Gefilterte Daten in eine neue CSV-Datei schreiben
df_filtered.to_csv(output_file, index=False)

print(f"Die gefilterte CSV-Datei wurde erfolgreich unter {output_file} gespeichert.")
