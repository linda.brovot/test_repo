import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import re

locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')

def read_csv_files(base_dir, pattern_str):
    data_frames = []
    pattern = re.compile(pattern_str)
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if pattern.match(file):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15
                    df['Temperature'] = temp_celsius
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius:.2f}°C")
                else:
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames

def combined_plots(mass_flow_data_frames, temperature_data_frames, output_dir):
    best_case_mass_flow = (0.05, 90)
    worst_case_mass_flow = (0.15, 30)

    best_case_eta = (0.15, 30)
    worst_case_eta = (0.05, 90)

    mass_flows_of_interest = [0.05, 0.15, 0.25]
    temperatures_of_interest = [65, 70]

    beladedauer_combined = {'Best Case': [], 'Worst Case': [], '65°C': [], '70°C': []}
    labels_beladedauer = ['Best Case (90 Hz)', 'Worst Case (30 Hz)', '65°C', '70°C']

    eta_system_combined = {'Best Case': [], 'Worst Case': [], '65°C': [], '70°C': []}
    labels_eta = ['Best Case (30 Hz)', 'Worst Case (90 Hz)', '65°C', '70°C']

    results = {
        'Category': [],
        'Massenstrom (kg/s)': [],
        'Beladedauer (h)': [],
        'Nutzungsgrad WPSS': [],
        'Prozentuale Änderung (%)': []
    }

    for df in mass_flow_data_frames:
        filtered_df_best = df[df['Name'].str.contains(f"_{best_case_mass_flow[0]}_{best_case_mass_flow[1] / 100}")]
        filtered_df_worst = df[df['Name'].str.contains(f"_{worst_case_mass_flow[0]}_{worst_case_mass_flow[1] / 100}")]
        if not filtered_df_best.empty:
            beladedauer_combined['Best Case'].append(filtered_df_best['Hours'].values[0])
        if not filtered_df_worst.empty:
            beladedauer_combined['Worst Case'].append(filtered_df_worst['Hours'].values[0])

    for df in mass_flow_data_frames:
        filtered_df_best = df[df['Name'].str.contains(f"_{best_case_eta[0]}_{best_case_eta[1] / 100}")]
        filtered_df_worst = df[df['Name'].str.contains(f"_{worst_case_eta[0]}_{worst_case_eta[1] / 100}")]
        if not filtered_df_best.empty:
            eta_system_combined['Best Case'].append(filtered_df_best['Eta_WPSS'].values[0])
        if not filtered_df_worst.empty:
            eta_system_combined['Worst Case'].append(filtered_df_worst['Eta_WPSS'].values[0])

    for temp in temperatures_of_interest:
        key = f'{int(temp)}°C'
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        beladedauer_combined[key].append(filtered_df['Hours'].values[0])
                        eta_system_combined[key].append(filtered_df['Eta_WPSS'].values[0])

    fig, (ax2, ax1) = plt.subplots(1, 2, figsize=(20, 8))

    bar_width = 0.2
    index = np.arange(len(labels_beladedauer))

    colors = ['darkred', 'red', 'grey']

    ax1.bar(index[0], beladedauer_combined['Best Case'][0], bar_width, color=colors[0], edgecolor='black',
            label='0.05 kg/s')
    ax1.bar(index[1], beladedauer_combined['Worst Case'][0], bar_width, color=colors[1], edgecolor='black',
            label='0.15 kg/s')

    for i, mass_flow in enumerate(mass_flows_of_interest):
        if i < len(beladedauer_combined['65°C']):
            ax1.bar(index[2] + i * bar_width - bar_width, beladedauer_combined['65°C'][i], bar_width, color=colors[i],
                    edgecolor='black', label=f'{mass_flow} kg/s')
            # Prozentsatz hinzufügen für 0.25 kg/s vs 0.05 kg/s
            if mass_flow == 0.25:
                val1 = beladedauer_combined['65°C'][0]
                val2 = beladedauer_combined['65°C'][i]
                percent_change = ((val2 - val1) / val1) * 100
                ax1.text(index[2] + i * bar_width - bar_width, val2 + 0.1, f'{percent_change:.1f}%', ha='center', va='bottom', fontsize=14)
                results['Category'].append('Beladedauer (65°C, 0.05 vs 0.25)')
                results['Massenstrom (kg/s)'].append(f'0.05 vs 0.25')
                results['Beladedauer (h)'].append(f'{val1} vs {val2}')
                results['Nutzungsgrad WPSS'].append('N/A')
                results['Prozentuale Änderung (%)'].append(f'{percent_change:.1f}')
        if i < len(beladedauer_combined['70°C']):
            ax1.bar(index[3] + i * bar_width - bar_width, beladedauer_combined['70°C'][i], bar_width, color=colors[i],
                    edgecolor='black')
            # Prozentsatz hinzufügen für 0.25 kg/s vs 0.05 kg/s
            if mass_flow == 0.25:
                val1 = beladedauer_combined['70°C'][0]
                val2 = beladedauer_combined['70°C'][i]
                percent_change = ((val2 - val1) / val1) * 100
                ax1.text(index[3] + i * bar_width - bar_width, val2 + 0.1, f'{percent_change:.1f}%', ha='center', va='bottom', fontsize=14)
                results['Category'].append('Beladedauer (70°C, 0.05 vs 0.25)')
                results['Massenstrom (kg/s)'].append(f'0.05 vs 0.25')
                results['Beladedauer (h)'].append(f'{val1} vs {val2}')
                results['Nutzungsgrad WPSS'].append('N/A')
                results['Prozentuale Änderung (%)'].append(f'{percent_change:.1f}')

            # Prozentuale Abweichung der Beladedauer bei 65°C und 70°C für 0.25 kg/s
            if i == 2:  # Index 2 entspricht 0.25 kg/s
                val_65 = beladedauer_combined['65°C'][i]
                val_70 = beladedauer_combined['70°C'][i]
                percent_change_beladedauer = ((val_65 - val_70) / val_65) * 100
                ax1.text(index[3] + i * bar_width - bar_width, val_70 + 0.1, f'{percent_change_beladedauer:.1f}%', ha='center', va='bottom', fontsize=14)
                results['Category'].append('Beladedauer (65°C vs 70°C, 0.25 kg/s)')
                results['Massenstrom (kg/s)'].append(f'0.25 kg/s')
                results['Beladedauer (h)'].append(f'{val_65} vs {val_70}')
                results['Nutzungsgrad WPSS'].append('N/A')
                results['Prozentuale Änderung (%)'].append(f'{percent_change_beladedauer:.1f}')

    ax1.set_ylabel('Beladedauer in h', fontsize=16)
    ax1.set_xticks(index)
    ax1.set_xticklabels(labels_beladedauer, fontsize=14)

    ax2.bar(index[0], eta_system_combined['Best Case'][0], bar_width, color=colors[1], edgecolor='black',
            label='0.15 kg/s')
    ax2.bar(index[1], eta_system_combined['Worst Case'][0], bar_width, color=colors[0], edgecolor='black',
            label='0.05 kg/s')

    for i, mass_flow in enumerate(mass_flows_of_interest):
        if i < len(eta_system_combined['65°C']):
            ax2.bar(index[2] + i * bar_width - bar_width, eta_system_combined['65°C'][i], bar_width, color=colors[i],
                    edgecolor='black', label=f'{mass_flow} kg/s')
            # Prozentsatz hinzufügen für 0.05 kg/s vs 0.15 kg/s
            if mass_flow == 0.05:
                val1 = eta_system_combined['65°C'][1]
                val2 = eta_system_combined['65°C'][0]
                percent_change = ((val2 - val1) / val1) * 100
                ax2.text(index[2] + i * bar_width - bar_width, val2 + 0.01, f'{percent_change:.1f}%', ha='center', va='bottom', fontsize=14)
                results['Category'].append('Nutzungsgrad (65°C, 0.15 vs 0.05)')
                results['Massenstrom (kg/s)'].append(f'0.15 vs 0.05')
                results['Beladedauer (h)'].append('N/A')
                results['Nutzungsgrad WPSS'].append(f'{val1} vs {val2}')
                results['Prozentuale Änderung (%)'].append(f'{percent_change:.1f}')
        if i < len(eta_system_combined['70°C']):
            ax2.bar(index[3] + i * bar_width - bar_width, eta_system_combined['70°C'][i], bar_width, color=colors[i],
                    edgecolor='black')
            # Prozentsatz hinzufügen für 0.05 kg/s vs 0.15 kg/s
            if mass_flow == 0.05:
                val1 = eta_system_combined['70°C'][1]
                val2 = eta_system_combined['70°C'][0]
                percent_change = ((val2 - val1) / val1) * 100
                ax2.text(index[3] + i * bar_width - bar_width, val2 + 0.01, f'{percent_change:.1f}%', ha='center', va='bottom', fontsize=14)
                results['Category'].append('Nutzungsgrad (70°C, 0.15 vs 0.05)')
                results['Massenstrom (kg/s)'].append(f'0.15 vs 0.05')
                results['Beladedauer (h)'].append('N/A')
                results['Nutzungsgrad WPSS'].append(f'{val1} vs {val2}')
                results['Prozentuale Änderung (%)'].append(f'{percent_change:.1f}')

            # Prozentuale Abweichung des Nutzungsgrads bei 65°C und 70°C für 0.15 kg/s
            if mass_flow == 0.15:
                val_65 = eta_system_combined['65°C'][i]
                val_70 = eta_system_combined['70°C'][i]
                percent_change_eta = ((val_65 - val_70) / val_70) * 100
                ax2.text(index[3] + i * bar_width - bar_width, val_70 + 0.01, f'{percent_change_eta:.1f}%', ha='center', va='bottom', fontsize=14)
                results['Category'].append('Nutzungsgrad (65°C vs 70°C, 0.15 kg/s)')
                results['Massenstrom (kg/s)'].append(f'0.15 kg/s')
                results['Beladedauer (h)'].append('N/A')
                results['Nutzungsgrad WPSS'].append(f'{val_65} vs {val_70}')
                results['Prozentuale Änderung (%)'].append(f'{percent_change_eta:.1f}')

    ax2.set_ylabel('Nutzungsgrad WPSS', fontsize=16)
    ax2.set_xticks(index)
    ax2.set_xticklabels(labels_eta, fontsize=14)

    handles1, labels1 = ax1.get_legend_handles_labels()
    unique_labels1 = dict(zip(labels1, handles1))
    ax1.legend(unique_labels1.values(), unique_labels1.keys(), title='Massenströme', fontsize=14, title_fontsize=16)

    handles2, labels2 = ax2.get_legend_handles_labels()
    unique_labels2 = dict(zip(labels2, handles2))
    ax2.legend(unique_labels2.values(), unique_labels2.keys(), title='Massenströme', fontsize=14, title_fontsize=16)

    plt.tight_layout()

    combined_plot_filename_png = os.path.join(output_dir, "combined_plot.png")
    combined_plot_filename_svg = os.path.join(output_dir, "combined_plot.svg")
    plt.savefig(combined_plot_filename_png)
    plt.savefig(combined_plot_filename_svg)

    plt.show()
    plt.close(fig)
    print(f"Kombiniertes Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

    # Speichern der Ergebnisse in einer CSV-Datei
    results_df = pd.DataFrame(results)
    results_csv_filename = os.path.join(output_dir, "berechnete_ergebnisse.csv")
    results_df.to_csv(results_csv_filename, index=False, sep=';', decimal=',')
    print(f"Ergebnisse gespeichert in: {results_csv_filename}")

if __name__ == "__main__":
    mass_flow_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    temperature_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und3\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    mass_flow_data_frames = read_csv_files(mass_flow_dir, r'system_results_(\d+\.\d+)\.csv')
    temperature_data_frames = read_csv_files(temperature_dir, r'system_results_(\d+\.\d+)\.csv')

    combined_plots(mass_flow_data_frames, temperature_data_frames, output_dir)
