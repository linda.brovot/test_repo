import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir, pattern_str):
    data_frames = []
    pattern = re.compile(pattern_str)  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if pattern.match(file):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    df['Temperature'] = temp_celsius  # Temperatur zur Tabelle hinzufügen
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius:.2f}°C")
                else:
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames


def combined_plots(mass_flow_data_frames, temperature_data_frames, output_dir):
    # Drehzahlen und Temperaturen von Interesse
    speeds_of_interest = [40, 50, 60, 70, 80]  # Drehzahlen
    beladedauer_temperatures_of_interest = [70]  # Vorlauftemperaturen (nur 70°C für die Linien)
    nutzungsgrad_temperatures_of_interest = [65]  # Vorlauftemperaturen (nur 65°C für die Linien)
    mass_flows_of_interest = [0.05, 0.15, 0.25]  # Massenströme

    # Initialisierung der Datenspeicher
    beladedauer_combined = {f'{speed} Hz': [] for speed in speeds_of_interest}
    for temp in beladedauer_temperatures_of_interest:
        beladedauer_combined[f'{temp}°C'] = []

    eta_system_combined = {f'{speed} Hz': [] for speed in speeds_of_interest}
    for temp in nutzungsgrad_temperatures_of_interest:
        eta_system_combined[f'{temp}°C'] = []

    # Labels für beide Plots
    beladedauer_labels = [f'{speed} Hz' for speed in speeds_of_interest] + [f'{temp}°C' for temp in beladedauer_temperatures_of_interest]
    nutzungsgrad_labels = [f'{speed} Hz' for speed in speeds_of_interest] + [f'{temp}°C' for temp in nutzungsgrad_temperatures_of_interest]

    # Daten für Drehzahlen und Temperaturen hinzufügen (Beladedauer)
    for speed in speeds_of_interest:
        for df in mass_flow_data_frames:
            filtered_df = df[df['Name'].str.contains(f"_{speed / 100}")]
            if not filtered_df.empty:
                beladedauer_combined[f'{speed} Hz'].append(filtered_df['Hours'].values[0])
            else:
                beladedauer_combined[f'{speed} Hz'].append(0)

    for temp in beladedauer_temperatures_of_interest:
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        beladedauer_combined[f'{temp}°C'].append(filtered_df['Hours'].values[0])
                    else:
                        beladedauer_combined[f'{temp}°C'].append(0)

    # Daten für Drehzahlen und Temperaturen hinzufügen (Nutzungsgrad)
    for speed in speeds_of_interest:
        for df in mass_flow_data_frames:
            filtered_df = df[df['Name'].str.contains(f"_{speed / 100}")]
            if not filtered_df.empty:
                eta_system_combined[f'{speed} Hz'].append(filtered_df['Eta_WPSS'].values[0])
            else:
                eta_system_combined[f'{speed} Hz'].append(0)

    for temp in nutzungsgrad_temperatures_of_interest:
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        eta_system_combined[f'{temp}°C'].append(filtered_df['Eta_WPSS'].values[0])
                    else:
                        eta_system_combined[f'{temp}°C'].append(0)

    # Plot erstellen
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 8))

    # Beladedauer Plot (Rechts)
    bar_width = 0.2
    beladedauer_index = np.arange(len(beladedauer_labels))
    colors = ['darkred', 'red', 'grey']

    # Plot für Beladedauer (ähnlich wie im Original)
    for i, label in enumerate(beladedauer_labels):
        for j, mass_flow in enumerate(mass_flows_of_interest):
            ax2.bar(beladedauer_index[i] + j * bar_width - bar_width, beladedauer_combined[label][j], bar_width,
                    color=colors[j], edgecolor='black')

    # Füge die horizontale Linie und den markierten Bereich hinzu (ähnlich wie im Original)
    for i, label in enumerate(beladedauer_labels):
        if '70°C' in label:
            for j, mass_flow in enumerate(mass_flows_of_interest):
                if mass_flow == 0.25:
                    beladedauer_value = beladedauer_combined[label][j]
                    x_start = beladedauer_index[0] - bar_width
                    x_end = beladedauer_index[-1] + bar_width
                    ax2.hlines(y=beladedauer_value, xmin=x_start, xmax=x_end, color='blue', linestyle='-', linewidth=2)
                    ax2.fill_between(np.array([x_start, x_end]), beladedauer_value, 0, color='blue', alpha=0.3)

    ax2.set_ylabel('Beladedauer in Stunden', fontsize=14)
    ax2.set_title('Beladedauer bei verschiedenen Drehzahlen und 70°C', fontsize=16)
    ax2.set_xticks(beladedauer_index)
    ax2.set_xticklabels(beladedauer_labels)

    # Nutzungsgrad Plot (Links)
    nutzungsgrad_index = np.arange(len(nutzungsgrad_labels))

    # Plot für Nutzungsgrad (ähnlich wie im Original)
    for i, label in enumerate(nutzungsgrad_labels):
        for j, mass_flow in enumerate(mass_flows_of_interest):
            ax1.bar(nutzungsgrad_index[i] + j * bar_width - bar_width, eta_system_combined[label][j], bar_width,
                    color=colors[j], edgecolor='black')

    # Füge die horizontale Linie und den markierten Bereich hinzu (ähnlich wie im Original)
    eta_value = None
    for i, label in enumerate(nutzungsgrad_labels):
        if '65°C' in label:
            for j, mass_flow in enumerate(mass_flows_of_interest):
                if mass_flow == 0.15:
                    eta_value = eta_system_combined[label][j]
                    break

    if eta_value is not None:
        start_x = nutzungsgrad_index[0] - bar_width
        end_x = nutzungsgrad_index[-1] + 2 * bar_width
        ax1.hlines(y=eta_value, xmin=start_x, xmax=end_x, color='blue', linestyle='-', linewidth=2)
        ax1.fill_between(np.array([start_x, end_x]), eta_value, 0, color='blue', alpha=0.3)

    ax1.set_ylabel('Nutzungsgrad WPSS', fontsize=14)
    ax1.set_title('Nutzungsgrad bei verschiedenen Drehzahlen und 65°C', fontsize=16)
    ax1.set_ylim([0.135, 0.21])
    ax1.set_xticks(nutzungsgrad_index)
    ax1.set_xticklabels(nutzungsgrad_labels)

    # Gemeinsame Legende für beide Subplots
    handles, labels = ax1.get_legend_handles_labels()
    fig.legend(handles[:3], labels[:3], title="Massenströme", fontsize=12, title_fontsize=14, loc='upper center', ncol=3)

    # Diagramm speichern
    combined_plot_filename_png = os.path.join(output_dir, "combined_plot_bestcase.png")
    combined_plot_filename_svg = os.path.join(output_dir, "combined_plot_bestcase.svg")
    plt.savefig(combined_plot_filename_png)
    plt.savefig(combined_plot_filename_svg)
    plt.show()
    plt.close(fig)

    print(f"Kombiniertes Diagramm gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")


if __name__ == "__main__":
    # Verzeichnisse für Massenstrom und Vorlauftemperaturen
    mass_flow_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    temperature_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und3\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # CSV-Dateien für Massenströme lesen
    mass_flow_data_frames = read_csv_files(mass_flow_dir, r'system_results_(\d+\.\d+)\.csv')

    # CSV-Dateien für Vorlauftemperaturen lesen
    temperature_data_frames = read_csv_files(temperature_dir, r'system_results_(\d+\.\d+)\.csv')

    # Kombiniertes Diagramm erstellen
    combined_plots(mass_flow_data_frames, temperature_data_frames, output_dir)
