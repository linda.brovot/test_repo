import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir, pattern_str):
    data_frames = []
    pattern = re.compile(pattern_str)  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if pattern.match(file):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    df['Temperature'] = temp_celsius  # Temperatur zur Tabelle hinzufügen
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius:.2f}°C")
                else:
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames


def combined_plot(mass_flow_data_frames, temperature_data_frames, output_dir):
    # Best Case und Worst Case: Massenstrom-Daten
    best_case = (0.05, 90)
    worst_case = (0.15, 30)

    # Massenströme für TVL-Daten
    mass_flows_of_interest = [0.05, 0.15, 0.25]  # Massenströme für TVL 65°C und 70°C
    temperatures_of_interest = [65, 70]  # Vorlauftemperaturen

    # Werte initialisieren
    beladedauer_combined = {'Best Case': [], 'Worst Case': [], '65°C': [], '70°C': []}
    labels = ['Best Case (90 Hz)', 'Worst Case (30 Hz)', '65°C', '70°C']

    # Best Case hinzufügen (Massenstrom-Daten)
    for df in mass_flow_data_frames:
        filtered_df = df[df['Name'].str.contains(f"_{best_case[0]}_{best_case[1] / 100}")]
        if not filtered_df.empty:
            beladedauer_combined['Best Case'].append(filtered_df['Hours'].values[0])

    # Worst Case hinzufügen (Massenstrom-Daten)
    for df in mass_flow_data_frames:
        filtered_df = df[df['Name'].str.contains(f"_{worst_case[0]}_{worst_case[1] / 100}")]
        if not filtered_df.empty:
            beladedauer_combined['Worst Case'].append(filtered_df['Hours'].values[0])

    # TVL 65°C und 70°C hinzufügen für alle Massenströme (Temperatur-Daten)
    for temp in temperatures_of_interest:
        key = f'{int(temp)}°C'
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        beladedauer_combined[key].append(filtered_df['Hours'].values[0])

    # Bar Plot für gruppierte Balken erstellen
    fig, ax = plt.subplots(figsize=(13, 7))

    bar_width = 0.2
    index = np.arange(len(labels))  # Positionen für die Gruppen auf der x-Achse

    # Farben für Massenströme (dunkelrot, rot, grau)
    colors = ['darkred', 'red', 'grey']

    # Plot für Best Case und Worst Case (nur 1 Balken)
    ax.bar(index[0], beladedauer_combined['Best Case'][0], bar_width, color=colors[0],edgecolor='black', label='0.05 kg/s')
    ax.bar(index[1], beladedauer_combined['Worst Case'][0], bar_width, color=colors[1], edgecolor='black',label='0.15 kg/s')

    # Plot für TVL 65°C (drei Balken für 0,05, 0,15 und 0,25 kg/s)
    for i, mass_flow in enumerate(mass_flows_of_interest):
        ax.bar(index[2] + i * bar_width - bar_width, beladedauer_combined['65°C'][i], bar_width, edgecolor='black',color=colors[i], label=f'{mass_flow} kg/s' if i == 2 else "")

    # Plot für TVL 70°C (drei Balken für 0,05, 0,15 und 0,25 kg/s)
    for i, mass_flow in enumerate(mass_flows_of_interest):
        ax.bar(index[3] + i * bar_width - bar_width, beladedauer_combined['70°C'][i], bar_width, edgecolor='black',color=colors[i], label=f'{mass_flow} kg/s' if i == 2 else "")

    ax.set_ylabel('Beladedauer in Stunden', fontsize=14)
    ax.set_title('Best Case, Worst Case und Vorlauftemperaturen bei verschiedenen Massenströmen (Beladedauer)', fontsize=16)

    ax.set_xticks(index)
    ax.set_xticklabels(labels)

    # Legende mit Massenströmen in den gewünschten Farben und Reihenfolge
    handles, labels = ax.get_legend_handles_labels()
    order = [1, 0, 2]  # Reihenfolge für die Legende (0.05 kg/s, 0.15 kg/s, 0.25 kg/s)
    ax.legend([handles[idx] for idx in order], [labels[idx] for idx in order], title='Massenströme', fontsize=12, title_fontsize=14)

    # Werte auf den Balken anzeigen
    for container in ax.containers:
        for bar in container:
            height = bar.get_height()
            if height > 0:
                ax.annotate(f'{height:.2f}'.replace('.', ','),
                            xy=(bar.get_x() + bar.get_width() / 2, height),
                            xytext=(0, 3),  # 3 Punkte vertikaler Versatz
                            textcoords="offset points",
                            ha='center', va='bottom', fontsize=12)

    # Diagramm speichern
    combined_plot_filename_png = os.path.join(output_dir, "combined_plot_beladedauer.png")
    combined_plot_filename_svg = os.path.join(output_dir, "combined_plot_beladedauer.svg")
    plt.savefig(combined_plot_filename_png)
    plt.savefig(combined_plot_filename_svg)
    plt.show()
    plt.close(fig)
    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")


if __name__ == "__main__":
    # Verzeichnisse für Massenstrom und Vorlauftemperaturen
    mass_flow_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    temperature_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und3\PLOTS\BeladedauerPlot"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # CSV-Dateien für Massenströme lesen
    mass_flow_data_frames = read_csv_files(mass_flow_dir, r'system_results_(\d+\.\d+)\.csv')

    # CSV-Dateien für Vorlauftemperaturen lesen
    temperature_data_frames = read_csv_files(temperature_dir, r'system_results_(\d+\.\d+)\.csv')

    # Kombinierten Plot für Beladedauer erstellen
    combined_plot(mass_flow_data_frames, temperature_data_frames, output_dir)
