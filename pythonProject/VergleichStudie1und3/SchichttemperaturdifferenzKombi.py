import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker

# Pfade zu den CSV-Ordnern für Massenstrom und Vorlauftemperatur
mass_flow_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CSV_Results"
temperature_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CSV_Results"

mass_flows_of_interest = [0.05, 0.15, 0.25]
temperatures = [338.15, 343.15]  # Vorlauftemperaturen in Kelvin

# Dictionaries, um die Temperaturdifferenzen zu speichern
temp_differences_best_case = []
temp_differences_worst_case = []
temp_differences_65deg = []
temp_differences_70deg = []

# Best Case: 90 Hz und 0,05 kg/s, Worst Case: 30 Hz und 0,25 kg/s
best_case = (0.05, 90)
worst_case = (0.25, 30)
best_case_temperature = 343.15  # 70°C für den Schichtungsplot
best_case_mass_flow = 0.05

# -----------------------------
# 1. Lese die Massenstrom-Dateien (Best Case und Worst Case)
# -----------------------------
for mass_flow, frequency in [best_case, worst_case]:
    folder_path = os.path.join(mass_flow_base_dir, f"csv_{mass_flow}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    temp_diff = None

    for csv_file_path in csv_files:
        name = os.path.basename(csv_file_path).replace('.csv', '')
        frequency_str = name.split('_')[-1]

        try:
            file_frequency = float(frequency_str)
        except ValueError:
            continue

        if file_frequency == frequency / 100:
            df = pd.read_csv(csv_file_path, sep=";", decimal=",")
            if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
                continue

            df_end = df.iloc[-1]
            temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
            temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15
            temp_diff = temp_layer_10 - temp_layer_1
            break

    if frequency == 90:
        temp_differences_best_case.append(temp_diff if temp_diff is not None else 0)
    elif frequency == 30:
        temp_differences_worst_case.append(temp_diff if temp_diff is not None else 0)

# -----------------------------
# 2. Lese die Vorlauftemperatur-Dateien
# -----------------------------
for temperature in temperatures:
    for mass_flow in mass_flows_of_interest:
        folder_path = os.path.join(temperature_base_dir, f"csv_{temperature}")
        csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv') and f'_{mass_flow}' in file]

        if csv_files:
            csv_file_path = csv_files[0]
            df = pd.read_csv(csv_file_path, sep=";", decimal=",")
            if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
                continue

            df_end = df.iloc[-1]
            temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15
            temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15
            temp_difference = temp_layer_10 - temp_layer_1

            if temperature == 338.15:
                temp_differences_65deg.append(temp_difference)
            elif temperature == 343.15:
                temp_differences_70deg.append(temp_difference)
        else:
            print(f"Datei für Vorlauftemperatur {temperature - 273.15:.2f}°C und Massenstrom {mass_flow} nicht gefunden.")
            if temperature == 338.15:
                temp_differences_65deg.append(0)
            elif temperature == 343.15:
                temp_differences_70deg.append(0)

# -----------------------------
# 3. Erstellung des Subplots für die Temperaturdifferenzen und die Schichtung
# -----------------------------

# Einstellungen für die Temperaturdifferenzen
labels = ['Best Case (90Hz)', 'Worst Case (30Hz)', '65°C', '70°C']
bar_width = 0.2  # Breite der Balken
index = np.arange(len(labels))

# Farben für Massenströme
colors = ['darkred', 'red', 'grey']

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 8))

# Schriftgrößen anpassen
plt.rcParams.update({'font.size': 16})  # Standard Schriftgröße um 2 erhöhen

# Temperaturdifferenz-Plot (links)
bars_best_case = ax1.bar(index[0], temp_differences_best_case[0], bar_width, color=colors[0], edgecolor='black', label='0.05 kg/s')
bars_worst_case = ax1.bar(index[1], temp_differences_worst_case[0], bar_width, color=colors[2], edgecolor='black', label='0.25 kg/s')

# Vorlauftemperaturen (65°C und 70°C)
bars_65deg = []
bars_70deg = []
for i, mass_flow in enumerate(mass_flows_of_interest):
    bars_65deg.append(ax1.bar(index[2] + i * bar_width - bar_width, temp_differences_65deg[i], bar_width, color=colors[i], edgecolor='black', label=f'{mass_flow} kg/s'))
    bars_70deg.append(ax1.bar(index[3] + i * bar_width - bar_width, temp_differences_70deg[i], bar_width, color=colors[i], edgecolor='black'))

# Zahlen über den Balken anzeigen
for bars in [bars_best_case, bars_worst_case] + bars_65deg + bars_70deg:
    for bar in bars:
        yval = bar.get_height()
        if yval != 0:
            ax1.text(bar.get_x() + bar.get_width() / 2, yval, f'{yval:.2f}', va='bottom', ha='center', fontsize=14)

# Beschriftungen und Titel für den ersten Plot
ax1.set_xlabel('Szenario', fontsize=18)
ax1.set_ylabel('Temperaturdifferenz ΔT in K', fontsize=18)
ax1.set_xticks(index)
ax1.set_xticklabels(labels, fontsize=16)

# Legende für den ersten Plot
handles, labels = ax1.get_legend_handles_labels()
by_label = dict(zip(labels, handles))
sorted_handles = [by_label['0.05 kg/s'], by_label['0.15 kg/s'], by_label['0.25 kg/s']]
ax1.legend(sorted_handles, ['0.05 kg/s', '0.15 kg/s', '0.25 kg/s'], title='Massenströme', fontsize=14, title_fontsize=16)

# -----------------------------
# 4. Schichtungsplot für Best Case (70°C, 0.05 kg/s)
# -----------------------------
# CSV-Datei für die Schichtung bei 70°C und 0,05 kg/s
schichtung_path = os.path.join(temperature_base_dir, f"csv_{best_case_temperature}")
schichtung_files = [os.path.join(schichtung_path, file) for file in os.listdir(schichtung_path) if file.endswith('.csv') and f'_{best_case_mass_flow}' in file]

# Wenn es eine Datei gibt, die den Kriterien entspricht, die Schichtung plotten
if schichtung_files:
    schichtung_file = schichtung_files[0]
    df_schichtung = pd.read_csv(schichtung_file, sep=";", decimal=",")
    df_schichtung["Hour"] = df_schichtung.index / 3600  # Zeit in Stunden umrechnen

    # Plot der Schichtung
    for i in range(1, 11):
        df_schichtung[f'bufferStorage.layer[{i}].T'] = df_schichtung[f'bufferStorage.layer[{i}].T'] - 273.15
        ax2.plot(df_schichtung["Hour"], df_schichtung[f'bufferStorage.layer[{i}].T'], label=f'Schicht {i}', linewidth=2)

    ax2.set_xlabel('Beladedauer in h', fontsize=18)
    ax2.set_ylabel('Temperatur in °C', fontsize=18)

    handles, labels = ax2.get_legend_handles_labels()
    ax2.legend(handles[::-1], labels[::-1], fontsize=14)  # Umgekehrte Reihenfolge der Schicht-Legende

    ax2.grid(False)
    ax2.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: f'{x:.1f}'.replace('.', ',')))

# Plot speichern
output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und3\PLOTS"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)
plt.tight_layout()
plt.savefig(os.path.join(output_dir, 'temperature_diff_and_schichtung_plot.png'))
plt.savefig(os.path.join(output_dir, 'temperature_diff_and_schichtung_plot.svg'))


plt.show()
