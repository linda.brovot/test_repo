import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir, pattern_str):
    data_frames = []
    pattern = re.compile(pattern_str)  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if pattern.match(file):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    df['Temperature'] = temp_celsius  # Temperatur zur Tabelle hinzufügen
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius:.2f}°C")
                else:
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames


def combined_plot(mass_flow_data_frames, temperature_data_frames, output_dir):
    # Drehzahlen und Temperaturen von Interesse
    speeds_of_interest = [30, 40, 50, 60, 70, 80]  # Drehzahlen (inklusive 30 Hz)
    temperatures_of_interest = [65]  # Vorlauftemperaturen (nur 65°C für die Linien)
    mass_flows_of_interest = [0.05, 0.15, 0.25]  # Massenströme

    # Dictionary zur Speicherung der Daten
    eta_system_combined = {f'{speed} Hz': [[] for _ in mass_flows_of_interest] for speed in speeds_of_interest}
    eta_system_combined[f'65°C'] = [[] for _ in mass_flows_of_interest]

    # Labels für die X-Achse
    labels = [f'{speed} Hz' for speed in speeds_of_interest] + [f'65°C']

    # Daten für Drehzahlen hinzufügen (Massenstrom-Daten)
    for speed in speeds_of_interest:
        for df in mass_flow_data_frames:
            filtered_df = df[df['Name'].str.contains(f"_{speed / 100}")]
            if not filtered_df.empty:
                for i, mass_flow in enumerate(mass_flows_of_interest):
                    eta_system_combined[f'{speed} Hz'][i].append(filtered_df['Eta_WPSS'].values[0])
            else:
                print(f"Warnung: Keine Daten für {speed} Hz gefunden.")

    # Daten für Temperaturen hinzufügen (Temperatur-Daten)
    for temp in temperatures_of_interest:
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        eta_system_combined[f'65°C'][mass_flows_of_interest.index(mass_flow)].append(filtered_df['Eta_WPSS'].values[0])
                    else:
                        print(f"Warnung: Keine Daten für 65°C und Massenstrom {mass_flow} kg/s gefunden.")

    # Prüfen, ob alle Daten vollständig sind
    for label, values in eta_system_combined.items():
        for i, v in enumerate(values):
            if len(v) == 0:
                print(f"Fehler: Unvollständige Daten für {label} und Massenstrom {mass_flows_of_interest[i]} kg/s.")
            else:
                print(f"Daten vorhanden für {label} und Massenstrom {mass_flows_of_interest[i]} kg/s: {v}")

    # Liniendiagramm erstellen
    fig, ax = plt.subplots(figsize=(14, 8))

    # X-Positionen für die Frequenzen
    frequencies_numeric = np.arange(len(labels))

    # Plot für jede Massenstromgruppe
    for strom_index, strom in enumerate(mass_flows_of_interest):
        eta_system_values = [np.mean(eta_system_combined[f'{speed} Hz'][strom_index]) for speed in speeds_of_interest]
        eta_system_values.append(np.mean(eta_system_combined[f'65°C'][strom_index]))

        # Linie für jede Massenstromgruppe hinzufügen
        ax.plot(frequencies_numeric, eta_system_values, marker='o', label=f'{strom} kg/s')

    # Markierung von 80 Hz und 65°C
    ax.axvline(x=speeds_of_interest.index(80), color='red', linestyle='--', label='80 Hz')
    ax.axvline(x=len(speeds_of_interest), color='green', linestyle='--', label='65°C')

    # Bereich zwischen 80 Hz und 65°C hervorheben
    ax.axvspan(speeds_of_interest.index(80), len(speeds_of_interest), color='lightblue', alpha=0.3, label='Vergleich 80 Hz - 65°C')

    # Achsenlabels und Titel
    ax.set_xticks(frequencies_numeric)
    ax.set_xticklabels(labels)
    ax.set_xlabel('Frequenzen')
    ax.set_ylabel('Nutzungsgrad WPSS')
    ax.set_title('Vergleich der Nutzungsgrade bei verschiedenen Massenströmen und 65°C')

    # Legende
    ax.legend()

    # Diagramm speichern
    combined_plot_filename_png = os.path.join(output_dir, "Nutzungsgrad_combined_plot_bestcase.png")
    combined_plot_filename_svg = os.path.join(output_dir, "Nutzungsgrad_combined_plot_bestcase.svg")
    plt.savefig(combined_plot_filename_png)
    plt.savefig(combined_plot_filename_svg)
    plt.show()
    plt.close(fig)
    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")


if __name__ == "__main__":
    # Verzeichnisse für Massenstrom und Vorlauftemperaturen
    mass_flow_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    temperature_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und3\PLOTS\NutzungsgradPlot"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # CSV-Dateien für Massenströme lesen
    mass_flow_data_frames = read_csv_files(mass_flow_dir, r'system_results_(\d+\.\d+)\.csv')

    # CSV-Dateien für Vorlauftemperaturen lesen
    temperature_data_frames = read_csv_files(temperature_dir, r'system_results_(\d+\.\d+)\.csv')

    # Kombinierten Plot erstellen
    combined_plot(mass_flow_data_frames, temperature_data_frames, output_dir)
