import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import re

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(base_dir, pattern_str):
    data_frames = []
    pattern = re.compile(pattern_str)  # Muster zum Extrahieren der Temperatur
    for subdir, _, files in os.walk(base_dir):
        for file in files:
            if pattern.match(file):
                file_path = os.path.join(subdir, file)
                df = pd.read_csv(file_path, sep=',', decimal='.')
                match = pattern.search(file)
                if match:
                    temp_kelvin = float(match.group(1))
                    temp_celsius = temp_kelvin - 273.15  # Umrechnung von Kelvin in Celsius
                    df['Temperature'] = temp_celsius  # Temperatur zur Tabelle hinzufügen
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path} mit Temperatur {temp_celsius:.2f}°C")
                else:
                    data_frames.append(df)
                    print(f"CSV-Datei gefunden und gelesen: {file_path}")
            else:
                print(f"Datei ignoriert: {file}")
    return data_frames


def combined_plot(mass_flow_data_frames, temperature_data_frames, output_dir):
    # Drehzahlen und Temperaturen von Interesse
    speeds_of_interest = [40, 50, 60, 70, 80]  # Drehzahlen
    temperatures_of_interest = [70]  # Vorlauftemperaturen (nur 70°C für die Linien)
    mass_flows_of_interest = [0.05, 0.15, 0.25]  # Massenströme

    beladedauer_combined = {f'{speed} Hz': [] for speed in speeds_of_interest}  # Initialisiere für Drehzahlen
    for temp in temperatures_of_interest:
        beladedauer_combined[f'{temp}°C'] = []

    labels = [f'{speed} Hz' for speed in speeds_of_interest] + [f'{temp}°C' for temp in temperatures_of_interest]

    # Daten für Drehzahlen hinzufügen (Massenstrom-Daten)
    for speed in speeds_of_interest:
        for df in mass_flow_data_frames:
            filtered_df = df[df['Name'].str.contains(f"_{speed / 100}")]
            if not filtered_df.empty:
                beladedauer_combined[f'{speed} Hz'].append(filtered_df['Hours'].values[0])
            else:
                beladedauer_combined[f'{speed} Hz'].append(0)  # Füge 0 hinzu, falls keine Daten vorhanden

    # Daten für Temperaturen hinzufügen (Temperatur-Daten)
    for temp in temperatures_of_interest:
        for mass_flow in mass_flows_of_interest:
            for df in temperature_data_frames:
                if df['Temperature'].iloc[0] == temp:
                    filtered_df = df[df['Name'].str.contains(f"_{mass_flow}")]
                    if not filtered_df.empty:
                        beladedauer_combined[f'{temp}°C'].append(filtered_df['Hours'].values[0])
                    else:
                        beladedauer_combined[f'{temp}°C'].append(0)  # Füge 0 hinzu, falls keine Daten vorhanden

    # Bar Plot für gruppierte Balken erstellen
    fig, ax = plt.subplots(figsize=(14, 8))

    bar_width = 0.2
    index = np.arange(len(labels))  # Positionen für die Gruppen auf der x-Achse

    # Farben für Massenströme (dunkelrot, rot, grau)
    colors = ['darkred', 'red', 'grey']

    # Legende einmalig setzen, um alle Massenströme zu berücksichtigen
    legend_labels_set = False

    # Plot für Drehzahlen und 70°C (jeweils 3 Balken für 0,05, 0,15 und 0,25 kg/s)
    for i, label in enumerate(labels):
        if 'Hz' in label:  # Plot für Drehzahlen
            for j, mass_flow in enumerate(mass_flows_of_interest):
                ax.bar(index[i] + j * bar_width - bar_width, beladedauer_combined[label][j], bar_width,
                       color=colors[j], edgecolor='black')

                # Legende nur einmal setzen
                if not legend_labels_set:
                    ax.bar(0, 0, color=colors[j], edgecolor='black', label=f'{mass_flow} kg/s')
            legend_labels_set = True
        else:  # Plot für 70°C (Temperatur)
            for j, mass_flow in enumerate(mass_flows_of_interest):
                ax.bar(index[i] + j * bar_width - bar_width, beladedauer_combined[label][j], bar_width,
                       color=colors[j], edgecolor='black')

                # Ziehe eine horizontale Linie für die Beladedauer bei 70°C und 0,15 kg/s
                if mass_flow == 0.25:
                    beladedauer_value = beladedauer_combined[label][j]

                    # Begrenze die Linie zwischen den Balken
                    x_start = index[0] - bar_width
                    x_end = index[-1] + bar_width

                    # Zeichne die horizontale Linie
                    ax.hlines(y=beladedauer_value, xmin=x_start, xmax=x_end, color='blue', linestyle='-', linewidth=2)

                    # Markiere den Bereich unterhalb der Linie
                    ax.fill_between(np.array([x_start, x_end]), beladedauer_value, 0, color='blue', alpha=0.3, label='Schlechter als 70°C')

    ax.set_ylabel('Beladedauer in Stunden', fontsize=14)
    ax.set_title('Drehzahlen und Vorlauftemperaturen (70°C) bei verschiedenen Massenströmen (Beladedauer)', fontsize=16)

    # Korrekte Positionierung der x-Ticks inklusive 70°C
    ax.set_xticks(index)
    ax.set_xticklabels(labels)

    # Legende für Massenströme und blauer Bereich (sicherstellen, dass alle Massenströme und der markierte Bereich in der Legende sind)
    ax.legend(title='Massenströme', fontsize=12, title_fontsize=14)

    # Diagramm speichern
    combined_plot_filename_png = os.path.join(output_dir, "Beladedauer_combined_plot_bestcase.png")
    combined_plot_filename_svg = os.path.join(output_dir, "Beladedauer_combined_plot_bestcase.svg")
    plt.savefig(combined_plot_filename_png)
    plt.savefig(combined_plot_filename_svg)
    plt.show()
    plt.close(fig)
    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")


if __name__ == "__main__":
    # Verzeichnisse für Massenstrom und Vorlauftemperaturen
    mass_flow_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    temperature_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Vorlauftemperaturregelung\CalculationExergy"
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und3\PLOTS\BeladedauerPlot"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # CSV-Dateien für Massenströme lesen
    mass_flow_data_frames = read_csv_files(mass_flow_dir, r'system_results_(\d+\.\d+)\.csv')

    # CSV-Dateien für Vorlauftemperaturen lesen
    temperature_data_frames = read_csv_files(temperature_dir, r'system_results_(\d+\.\d+)\.csv')

    # Kombinierten Plot für Beladedauer erstellen
    combined_plot(mass_flow_data_frames, temperature_data_frames, output_dir)
