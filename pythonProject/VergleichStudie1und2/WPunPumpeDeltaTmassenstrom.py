import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
from matplotlib.patches import Patch

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'de_DE.UTF-8')


def read_csv_files(results_base_dir, identifier):
    folder_path = os.path.join(results_base_dir, f"results_{identifier}")
    system_csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    layer_csv_file = os.path.join(folder_path, f"layer_results_{identifier}.csv")
    if os.path.exists(system_csv_file) and os.path.exists(layer_csv_file):
        system_results_df = pd.read_csv(system_csv_file, sep=',', decimal='.')
        layer_results_df = pd.read_csv(layer_csv_file, sep=',', decimal='.')
        print(f"CSV-Dateien für {identifier} erfolgreich gelesen.")
        return system_results_df, layer_results_df
    else:
        print(f"CSV-Dateien für {identifier} nicht gefunden: {system_csv_file} oder {layer_csv_file}")
        return None, None


def get_sums(system_results_df, identifier):
    drehzahlen = [30,40,50,60,70, 80, 90]
    pel_sums = []
    pel_Pumpe_sums = []

    for hz in drehzahlen:
        filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{identifier}_{hz / 100:.1f}.csv")]

        if not filtered_df.empty:
            pel_sum = filtered_df['Gesamtsumme_Pel'].values[0] / 1000 / 3600
            pel_Pumpe_sum = filtered_df['Gesamtsumme_Pel_Pumpe'].values[0] / 1000 / 3600

            pel_sums.append(pel_sum)
            pel_Pumpe_sums.append(pel_Pumpe_sum)

    return drehzahlen, pel_sums, pel_Pumpe_sums


def plot_and_save(fig, filename_base):
    # Speichern des Plots als PNG-Datei
    fig.savefig(f"{filename_base}.png", format='png')
    print(f"Plot gespeichert: {filename_base}.png")

    # Speichern des Plots als SVG-Datei
    fig.savefig(f"{filename_base}.svg", format='svg')
    print(f"Plot gespeichert: {filename_base}.svg")


def plot_combined_comparison(results_base_dir_1, results_base_dir_2, output_dir, script_name, identifiers_delta_t,
                             identifiers_mass_flow, colors_delta_t, colors_mass_flow, hatches):
    # Erstelle die Subplots: links für Temperaturspreizung und rechts für Massenströme
    fig, (ax_delta_t, ax_mass_flow) = plt.subplots(1, 2, figsize=(20, 8))

    bar_width = 0.25  # Breite der Balken
    index = np.arange(3)  # Positionen für 30, 40 und 50 Hz

    # Links: Temperaturspreizung (Delta T)
    for idx, identifier in enumerate(identifiers_delta_t):
        system_results_df, _ = read_csv_files(results_base_dir_1, identifier)
        if system_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums = get_sums(system_results_df, identifier)

            if drehzahlen:
                ax_delta_t.bar(index + idx * bar_width, pel_sums, bar_width, color=colors_delta_t[idx],
                               edgecolor='black',
                               label=f'WP {identifier} K')
                ax_delta_t.bar(index + idx * bar_width, pel_Pumpe_sums, bar_width, bottom=pel_sums,
                               hatch=hatches[idx], color=colors_delta_t[idx], edgecolor='black')

    # Einstellungen für den Temperaturspreizung Subplot (links)
    ax_delta_t.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax_delta_t.set_ylabel('el. Energie in kWh', fontsize=16)
    ax_delta_t.grid(False)
    ax_delta_t.set_xticks(index + bar_width)
    ax_delta_t.set_xticklabels([30,40,50,60,70, 80, 90])
    ax_delta_t.tick_params(axis='both', which='major', labelsize=16)
    ax_delta_t.set_ylim([4.92, None])
    ax_delta_t.set_title('Temperaturspreizung (Delta T)', fontsize=16)

    # Rechts: Massenströme
    for idx, identifier in enumerate(identifiers_mass_flow):
        system_results_df, _ = read_csv_files(results_base_dir_2, identifier)
        if system_results_df is not None:
            drehzahlen, pel_sums, pel_Pumpe_sums = get_sums(system_results_df, identifier)

            if drehzahlen:
                ax_mass_flow.bar(index + idx * bar_width, pel_sums, bar_width, color=colors_mass_flow[idx],
                                 edgecolor='black',
                                 label=f'WP {identifier} kg/s')
                ax_mass_flow.bar(index + idx * bar_width, pel_Pumpe_sums, bar_width, bottom=pel_sums,
                                 hatch=hatches[idx], color=colors_mass_flow[idx], edgecolor='black')

    # Einstellungen für den Massenstrom Subplot (rechts)
    ax_mass_flow.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax_mass_flow.set_ylabel('el. Energie in kWh', fontsize=16)
    ax_mass_flow.grid(False)
    ax_mass_flow.set_xticks(index + bar_width)
    ax_mass_flow.set_xticklabels([30,40,50,60,70, 80, 90])
    ax_mass_flow.tick_params(axis='both', which='major', labelsize=16)
    ax_mass_flow.set_ylim([4.92, None])
    ax_mass_flow.set_title('Massenströme', fontsize=16)

    # Gleiche Abstände zwischen den Subplots und den Rändern, aber enger zusammen
    plt.subplots_adjust(left=0.07, right=0.93, top=0.88, bottom=0.12, wspace=0.15)
    plt.tight_layout()

    # Legenden für Delta T (links) und Massenstrom (rechts) getrennt
    wp_legend_delta_t = [Patch(facecolor=colors_delta_t[idx], edgecolor='black', label=f'WP {identifier} K')
                         for idx, identifier in enumerate(identifiers_delta_t)]
    pumpe_legend_delta_t = [
        Patch(facecolor=colors_delta_t[idx], edgecolor='black', hatch=hatches[idx], label=f'Pumpe {identifier} K')
        for idx, identifier in enumerate(identifiers_delta_t)]

    wp_legend_mass_flow = [Patch(facecolor=colors_mass_flow[idx], edgecolor='black', label=f'WP {identifier} kg/s')
                           for idx, identifier in enumerate(identifiers_mass_flow)]
    pumpe_legend_mass_flow = [
        Patch(facecolor=colors_mass_flow[idx], edgecolor='black', hatch=hatches[idx], label=f'Pumpe {identifier} kg/s')
        for idx, identifier in enumerate(identifiers_mass_flow)]

    # Legende zu Delta T hinzufügen
    ax_delta_t.legend(handles=wp_legend_delta_t + pumpe_legend_delta_t, loc='best', fontsize=12)

    # Legende zu Massenströme hinzufügen
    ax_mass_flow.legend(handles=wp_legend_mass_flow + pumpe_legend_mass_flow, loc='best', fontsize=12)

    # Plot speichern
    plot_and_save(fig, os.path.join(output_dir, f"{script_name}_combined"))

    plt.show()


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dir_1 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    results_base_dir_2 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy"
    results_base_dir_3 = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\PLOTS"
    output_dir = os.path.join(results_base_dir_3, script_name)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Hatches für die Schraffur der Pumpe-Leistung
    hatches = ['x', '\\', 'x']

    # Plot für Temperaturspreizung (Delta T) als gestapeltes Balkendiagramm (Blau-, Orange- und Grüntöne)
    plot_combined_comparison(results_base_dir_1, results_base_dir_2, output_dir, script_name,
                             identifiers_delta_t=[5, 8, 10],
                             identifiers_mass_flow=[0.05, 0.15, 0.25],
                             colors_delta_t=['orange', 'blue', 'green'],
                             colors_mass_flow=['darkred', 'red', 'grey'],
                             hatches=hatches)
