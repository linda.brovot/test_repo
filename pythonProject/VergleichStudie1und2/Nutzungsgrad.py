import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(base_dir, identifier):
    folder_path = os.path.join(base_dir, f"results_{identifier}")
    csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {csv_file}")
        return None

def plot_data(data_frame, identifier):
    eta_system_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]

                if not pd.isna(eta_WPSS):
                    eta_system_list.append(eta_WPSS)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, eta_system_list

def combined_plot(results_base_dirs, output_dir, script_name):
    fig, ax = plt.subplots(figsize=(13, 8))  # Anpassung der Plot-Größe
  #  fig.suptitle('Exergetischer Nutzungsgrad', fontsize=18, y=0.95)  # Titel näher an den Plot

    # Farben und Stil
    colors = ['cornflowerblue', 'green', 'salmon']
    kennzahlen_combined = {}
    drehzahlen_combined = []

    # Für die Berechnung der prozentualen Unterschiede
    differenzen = []

    identifiers_1 = [5, 8, 10]  # Identifikatoren für Delta T
    identifiers_2 = [0.05, 0.15, 0.25]  # Identifikatoren für Massenstrom

    # Daten für die 5-K-Linie sammeln
    system_results_5K = read_csv_files(results_base_dirs[1], 5)
    frequencies_5K, eta_system_5K = plot_data(system_results_5K, 5)

    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, eta_system_list = plot_data(system_results_df, identifier)
            if frequencies:
                deltaT_label = f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$'
                kennzahlen_combined[deltaT_label] = eta_system_list
                drehzahlen_combined = frequencies

    # Daten für Maximalwerte extrahieren und als Linie hinzufügen
    max_frequencies = [30, 40, 50, 60, 70, 80, 90]  # Frequenzen
    max_eta = []

    for freq in max_frequencies:
        if freq in [30, 40]:
            massenstrom_identifier = 0.15  # Massenstrom für 30 Hz und 40 Hz
        else:
            massenstrom_identifier = 0.25  # Massenstrom für andere Drehzahlen

        system_results_df = read_csv_files(results_base_dirs[0], massenstrom_identifier)
        if system_results_df is not None:
            filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{massenstrom_identifier}_{freq / 100}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                if not pd.isna(eta_WPSS):
                    max_eta.append(eta_WPSS)
                else:
                    max_eta.append(0)  # Fallback-Wert, falls keine Daten gefunden werden
            else:
                max_eta.append(0)  # Fallback-Wert, falls keine Daten gefunden werden

    # Linie für die maximalen Nutzungsgrade plotten
    ax.plot(max_frequencies, max_eta, marker='x', linestyle='--', color='blue', label="Best-Case", markersize=8, linewidth=2)

    # Berechnung der prozentualen Unterschiede und Markierung im Plot
    for i, freq in enumerate(max_frequencies):
        if freq in frequencies_5K:
            index_5K = frequencies_5K.index(freq)
            eta_5K = eta_system_5K[index_5K]
            eta_max = max_eta[i]
            prozentualer_unterschied = ((eta_max - eta_5K) / eta_5K) * 100
            differenzen.append({
                'Frequenz (Hz)': freq,
                '5K Nutzungsgrad': eta_5K,
                'Maximaler Nutzungsgrad': eta_max,
                'Prozentuale Differenz (%)': prozentualer_unterschied
            })

            # Markierung im Plot
            ax.text(freq, eta_max + 0.005, f'{prozentualer_unterschied:.2f}%', color='black', fontsize=12)

    for i, (label, kennzahlen) in enumerate(kennzahlen_combined.items()):
        color = colors[i]
        ax.plot(drehzahlen_combined, kennzahlen, marker='o', linestyle='-', color=color, label=label, markersize=6, linewidth=1.5)

    # Beibehalten der originalen Beschriftungen und Design
    ax.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax.set_ylabel('Nutzungsgrad', fontsize=16)
    ax.set_ylim(0.135, 0.22)  # Anpassung des Y-Limits für maximalen Wert
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.02))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.2f', x)))
    ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.0f', x)))

    ax.grid(False)

    # Schwarze Ränder um den Plot hinzufügen
    for spine in ax.spines.values():
        spine.set_edgecolor('black')
        spine.set_linewidth(1.5)

    # Manuelles Anpassen der Legende
    handles, labels = ax.get_legend_handles_labels()

    # Erste Legende (Delta T Linien)
    legend1 = ax.legend(handles[:len(kennzahlen_combined)], labels[:len(kennzahlen_combined)], loc='upper left', bbox_to_anchor=(0.7, 1),
                        ncol=1, prop={'size': 16}, handletextpad=1, columnspacing=1, frameon=False)
    ax.add_artist(legend1)

    # Zweite Legende (Maximalwerte Linie)
    legend2 = ax.legend(handles[len(kennzahlen_combined):], labels[len(kennzahlen_combined):], loc='upper left', bbox_to_anchor=(0.7, 0.84),
                        ncol=1, prop={'size': 16}, handletextpad=1, columnspacing=1, frameon=False)
    ax.add_artist(legend2)

    ax.tick_params(axis='both', which='major', labelsize=16)

    # Speicherpfade für PNG und SVG definieren
    combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
    combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")

    # Plot als PNG speichern
    plt.savefig(combined_plot_filename_png, bbox_inches='tight')

    # Plot als SVG speichern
    plt.savefig(combined_plot_filename_svg, bbox_inches='tight')

    # Plot anzeigen und schließen
    plt.show()
    plt.close(fig)

    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

    # Speichern der prozentualen Unterschiede in eine CSV-Datei
    differenzen_df = pd.DataFrame(differenzen)
    differenzen_csv_filename = os.path.join(output_dir, f"{script_name}_differenzen.csv")
    differenzen_df.to_csv(differenzen_csv_filename, sep=';', index=False, decimal=',')
    print(f"Prozentuale Unterschiede in CSV-Datei gespeichert: {differenzen_csv_filename}")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dirs = [
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy",
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    ]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dirs, output_dir, script_name)
