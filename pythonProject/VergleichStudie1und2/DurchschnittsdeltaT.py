import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import re

# Pfade zu den CSV-Ordnern für die verschiedenen Massenströme
base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CSV_Results"

# Speichert die Temperaturdifferenzen pro Drehzahl und Massenstrom
results = {}

# Durchlaufe alle Ordner mit den verschiedenen Massenströmen
for folder in os.listdir(base_dir):
    if not folder.startswith("csv_"):
        continue  # überspringe nicht passende Ordner

    # Extrahiere den Massenstrom aus dem Ordnernamen (z.B. csv_0.15 -> mass_flow = 0.15)
    try:
        mass_flow = float(folder.split('_')[-1])
    except ValueError:
        print(f"Konnte den Massenstrom aus dem Ordnernamen {folder} nicht extrahieren.")
        continue

    folder_path = os.path.join(base_dir, folder)
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    for csv_file_path in csv_files:
        # Extrahiere den Dateinamen ohne die Erweiterung
        name = os.path.splitext(os.path.basename(csv_file_path))[0]

        # Extrahiere die Drehzahl aus dem Dateinamen (z.B. results_WP_Speicher_System_0.15_0.3.csv -> rpm = 0.3)
        try:
            # Entferne das doppelte .csv und extrahiere die letzte Zahl (Drehzahl)
            cleaned_filename = csv_file_path.replace(".csv.csv", ".csv")
            rpm_match = re.search(r'_(\d+\.\d+)\.csv$', cleaned_filename)
            if rpm_match:
                rpm = float(rpm_match.group(1))
            else:
                raise ValueError("Keine Drehzahl gefunden")
        except (ValueError, IndexError):
            print(f"Fehler beim Extrahieren der Drehzahl aus {csv_file_path}, überspringe Datei.")
            continue

        # Lese die Daten aus der CSV-Datei
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Filtere den DataFrame, um nur Daten ab Sekunde 100 zu behalten
        if df.index.max() < 100:
            print(f"Nicht genügend Daten in {csv_file_path}, überspringe Datei.")
            continue
        df = df[df.index >= 100]

        # Berechnung der Temperaturdifferenz (heatPump.sigBus.TConOutMea - heatPump.sigBus.TConInMea)
        if 'heatPump.sigBus.TConOutMea' not in df.columns or 'heatPump.sigBus.TConInMea' not in df.columns:
            print(f"Benötigte Spalten nicht in {csv_file_path}, überspringe Datei.")
            continue

        df["Temperature_Difference"] = df['heatPump.sigBus.TConOutMea'] - df['heatPump.sigBus.TConInMea']

        # Durchschnittliche Temperaturdifferenz
        avg_temperature_difference = df["Temperature_Difference"].mean()

        # Speichere die durchschnittliche Temperaturdifferenz in der entsprechenden Struktur
        if mass_flow not in results:
            results[mass_flow] = {}
        results[mass_flow][rpm] = avg_temperature_difference

# Plotten der Temperaturdifferenzen für verschiedene Massenströme
plt.figure(figsize=(10, 6))

# Definiere Farben und Marker für die verschiedenen Massenströme
colors = {0.05: 'darkred', 0.15: 'red', 0.25: 'grey'}
markers = {0.05: 'o', 0.15: 'o', 0.25: 'o'}

for mass_flow, temperature_differences in results.items():
    rpms = sorted(temperature_differences.keys())
    avg_differences = [temperature_differences[rpm] for rpm in rpms]

    if len(rpms) > 0:
        # Multipliziere die Drehzahlen für die Anzeige mit 100 (30, 40, 50 etc.)
        plt.plot([r * 100 for r in rpms], avg_differences, marker=markers[mass_flow], color=colors[mass_flow], label=f'Massenstrom = {mass_flow} kg/s')

# Formatiere die Achsen und das Diagramm
plt.xlabel('Drehzahl in Hz')
plt.ylabel('Durchschnittliche Temperaturspreizung (ΔT) in °C')

# Formatierung der y-Achse mit Kommas als Dezimaltrennzeichen
plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: f'{y:.2f}'.replace('.', ',')))

# Anpassungen der Legende und des Rasters
plt.legend()
plt.grid(False)

# Speicherort für den Plot
script_name = os.path.splitext(os.path.basename(__file__))[0]  # Name des Skripts ohne Erweiterung
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\PLOTS"
output_dir = os.path.join(results_base_dir, script_name)

# Erstelle das Verzeichnis, wenn es nicht existiert
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Speicherpfade für PNG und SVG definieren
plot_path_png = os.path.join(output_dir, f"temperature_difference_vs_rpm.png")
plot_path_svg = os.path.join(output_dir, f"temperature_difference_vs_rpm.svg")

# Plot als PNG speichern
plt.savefig(plot_path_png, format='png')

# Plot als SVG speichern
plt.savefig(plot_path_svg, format='svg')

# Plot anzeigen
plt.show()

# Ausgabe der Speicherorte
print(f"Der Plot wurde unter '{plot_path_png}' und '{plot_path_svg}' gespeichert.")
