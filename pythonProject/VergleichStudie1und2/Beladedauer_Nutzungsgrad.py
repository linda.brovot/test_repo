import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np
import matplotlib.ticker as ticker

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')


def read_csv_files(base_dir, identifier):
    folder_path = os.path.join(base_dir, f"results_{identifier}")
    csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {csv_file}")
        return None


def plot_efficiency(data_frame, identifier):
    eta_system_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]

                if not pd.isna(eta_WPSS):
                    eta_system_list.append(eta_WPSS)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, eta_system_list


def plot_loading_time(data_frame, identifier):
    beladezeit_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                beladezeit = filtered_df['Hours'].values[0]

                if not pd.isna(beladezeit):
                    beladezeit_list.append(beladezeit)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, beladezeit_list


def combined_subplots(results_base_dirs, output_dir, script_name):
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 8), gridspec_kw={'width_ratios': [1, 2]})
    # Zwei untereinander liegende Subplots

    # Farben und Stil
    colors = ['cornflowerblue', 'green', 'salmon', 'blue']

    # Identifikatoren für Delta T und Massenstrom
    identifiers_1 = [5, 8, 10]  # Delta T
    min_frequencies = [30, 40, 50, 60, 70, 80, 90]  # Frequenzen

    # Daten für den Effizienz-Plot (erster Subplot)
    kennzahlen_combined = {}
    drehzahlen_combined = []
    differenzen_effizienz = []
    system_results_5K = read_csv_files(results_base_dirs[1], 5)
    frequencies_5K, eta_system_5K = plot_efficiency(system_results_5K, 5)

    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, eta_system_list = plot_efficiency(system_results_df, identifier)
            if frequencies:
                deltaT_label = f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$'
                kennzahlen_combined[deltaT_label] = eta_system_list
                drehzahlen_combined = frequencies

    # Daten für Best-Case (Maximalwerte)
    max_eta = []
    for freq in min_frequencies:
        massenstrom_identifier = 0.15 if freq in [30, 40] else 0.25
        system_results_df = read_csv_files(results_base_dirs[0], massenstrom_identifier)
        if system_results_df is not None:
            filtered_df = system_results_df[
                system_results_df['Name'].str.contains(f"_{massenstrom_identifier}_{freq / 100}.csv")]
            if not filtered_df.empty:
                eta_WPSS = filtered_df['Eta_WPSS'].values[0]
                max_eta.append(eta_WPSS)
            else:
                max_eta.append(0)

    # Plot für Effizienz
    ax1.plot(min_frequencies, max_eta, marker='x', linestyle='--', color='blue', label="Best-Case", markersize=8,
             linewidth=2)

    for i, (label, kennzahlen) in enumerate(kennzahlen_combined.items()):
        color = colors[i]
        ax1.plot(drehzahlen_combined, kennzahlen, marker='o', linestyle='-', color=color, label=label, markersize=6,
                 linewidth=1.5)

    # Prozentuale Unterschiede im Effizienz-Plot berechnen und anzeigen
    for i, freq in enumerate(min_frequencies):
        if freq in frequencies_5K:
            index_5K = frequencies_5K.index(freq)
            eta_5K = eta_system_5K[index_5K]
            eta_max = max_eta[i]
            prozentualer_unterschied = ((eta_max - eta_5K) / eta_5K) * 100 if eta_5K != 0 else 0
            differenzen_effizienz.append({
                'Frequenz (Hz)': freq,
                '5K Nutzungsgrad': eta_5K,
                'Maximaler Nutzungsgrad': eta_max,
                'Prozentuale Differenz (%)': prozentualer_unterschied
            })
            ax1.text(freq, eta_max + 0.005, f'{prozentualer_unterschied:.2f}%', color='black', fontsize=12)

    ax1.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax1.set_ylabel('Nutzungsgrad', fontsize=16)
    ax1.set_ylim(0.135, 0.22)
    ax1.yaxis.set_major_locator(ticker.MultipleLocator(0.02))
    ax1.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax1.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.2f', x)))
    ax1.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: locale.format_string('%.0f', x)))

    ax1.grid(False)
    ax1.tick_params(axis='both', which='major', labelsize=16)

    handles, labels = ax1.get_legend_handles_labels()
    # Entferne die Beschränkung auf len(kennzahlen_combined), um alle Labels anzuzeigen
    ax1.legend(handles, labels, loc='best', ncol=1, prop={'size': 16}, frameon=False)


    # Daten für den Beladezeit-Plot (zweiter Subplot)
    bar_width = 0.2
    indices = np.arange(len(min_frequencies))

    # Balken für die 5K-Linie sammeln
    frequencies_5K, beladezeit_5K = plot_loading_time(system_results_5K, 5)

    # Initialisieren von Daten für Delta T Balken
    bar_data = {identifier: [] for identifier in identifiers_1}
    min_beladezeit = []
    differenzen_beladezeit = []

    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, beladezeit_list = plot_loading_time(system_results_df, identifier)
            for freq in min_frequencies:
                if freq in frequencies:
                    index = frequencies.index(freq)
                    bar_data[identifier].append(beladezeit_list[index])
                else:
                    bar_data[identifier].append(float('nan'))

    for freq in min_frequencies:
        massenstrom_identifier = 0.05  # Massenstrom für Minimalwerte
        system_results_df = read_csv_files(results_base_dirs[0], massenstrom_identifier)
        if system_results_df is not None:
            filtered_df = system_results_df[
                system_results_df['Name'].str.contains(f"_{massenstrom_identifier}_{freq / 100}.csv")]
            if not filtered_df.empty:
                beladezeit = filtered_df['Hours'].values[0]
                min_beladezeit.append(beladezeit)
            else:
                min_beladezeit.append(float('inf'))

    # Plot für Beladedauer
    for i, identifier in enumerate(identifiers_1):
        ax2.bar(indices + i * bar_width, bar_data[identifier], bar_width,
                label=f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$', edgecolor='black',
                color=colors[i])

    ax2.bar(indices + len(identifiers_1) * bar_width, min_beladezeit, bar_width, label="Best-Case", color=colors[-1])

    # Prozentuale Unterschiede im Beladezeit-Plot berechnen und anzeigen
    for i, freq in enumerate(min_frequencies):
        beladezeit_10K = bar_data[10][i] if not pd.isna(bar_data[10][i]) else float('inf')
        beladezeit_min = min_beladezeit[i]
        if beladezeit_10K != float('inf') and beladezeit_min != float('inf'):
            prozentualer_unterschied = ((beladezeit_10K - beladezeit_min) / beladezeit_min) * 100
            differenzen_beladezeit.append({
                'Frequenz (Hz)': freq,
                '10K Beladedauer (Stunden)': beladezeit_10K,
                'Minimale Beladedauer (Stunden)': beladezeit_min,
                'Prozentuale Differenz (%)': prozentualer_unterschied
            })
            ax2.text(indices[i] + len(identifiers_1) * bar_width, beladezeit_min + 0.05,
                     f'{prozentualer_unterschied:.2f}%', ha='center', color='black', fontsize=12)

    ax2.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax2.set_ylabel('Beladedauer in h', fontsize=16)
    ax2.set_xticks(indices + bar_width * 1.5)
    ax2.set_xticklabels([f'{freq}' for freq in min_frequencies], fontsize=12)
    ax2.set_ylim([0, 7])
    ax2.legend(loc='best', ncol=1, prop={'size': 16}, frameon=False)
    ax2.tick_params(axis='both', which='major', labelsize=16)

    # Speicherpfade für PNG und SVG definieren
    combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined_subplots.png")
    combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined_subplots.svg")

    plt.tight_layout()

    # Plot als PNG und SVG speichern
    plt.savefig(combined_plot_filename_png, bbox_inches='tight')
    plt.savefig(combined_plot_filename_svg, bbox_inches='tight')

    # Plot anzeigen und schließen
    plt.show()
    plt.close(fig)

    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")


if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dirs = [
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy",
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CalculationExergy"
    ]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und2\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_subplots(results_base_dirs, output_dir, script_name)
