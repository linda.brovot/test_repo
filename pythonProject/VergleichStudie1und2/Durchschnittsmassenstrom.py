import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import re

# Pfade zu den CSV-Ordnern für die verschiedenen Pumpendrehzahlen
base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CSV_Results"

# Speichert die kumulierten Massenströme pro Drehzahl und deltaT
results = {}

# Durchlaufe alle Ordner mit den verschiedenen DeltaTs
for deltaT_folder in os.listdir(base_dir):
    if not deltaT_folder.startswith("csv_"):
        continue  # überspringe nicht passende Ordner

    # Extrahiere das DeltaT aus dem Ordnernamen (z.B. csv_5 -> deltaT = 5)
    deltaT = int(deltaT_folder.split('_')[-1])

    folder_path = os.path.join(base_dir, deltaT_folder)
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    for csv_file_path in csv_files:
        # Extrahiere den Dateinamen ohne die Erweiterung
        name = os.path.splitext(os.path.basename(csv_file_path))[0]

        # Lese die Daten aus der CSV-Datei
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Filtere den DataFrame, um nur Daten ab Sekunde 100 zu behalten
        df = df[df['Time'] >= 100]

        # Berechne den kumulierten Massenstrom
        total_mass_flow = df["floMacDyn.m_flow"].sum()
        total_time = df["Time"].iloc[-1] - df["Time"].iloc[0]  # Berechne die gesamte Zeitdauer in Sekunden

        # Durchschnittlicher Massenstrom
        avg_mass_flow = total_mass_flow / total_time

        # Verwende eine reguläre Expression, um die Drehzahl als Zahl mit einem Dezimalpunkt zu extrahieren
        try:
            rpm_match = re.search(r'(\d+\.\d+)', name)
            if rpm_match:
                rpm = float(rpm_match.group(1))
            else:
                raise ValueError("Keine Drehzahl gefunden")
        except (ValueError, IndexError):
            continue

        # Speichere den durchschnittlichen Massenstrom in der entsprechenden Struktur
        if deltaT not in results:
            results[deltaT] = {}
        results[deltaT][rpm] = avg_mass_flow

# Plotten der durchschnittlichen Massenströme für verschiedene DeltaTs
plt.figure(figsize=(10, 6))

# Definiere Farben und Marker für die verschiedenen DeltaTs
colors = {5: 'cornflowerblue', 8: 'green', 10: 'salmon'}
markers = {5: 'o', 8: 'o', 10: 'o'}

for deltaT, mass_flows in results.items():
    rpms = sorted(mass_flows.keys())
    avg_flows = [mass_flows[rpm] for rpm in rpms]

    plt.plot(rpms, avg_flows, marker=markers[deltaT], color=colors[deltaT], label=f'ΔT = {deltaT} K')

# Formatiere die Achsen und das Diagramm
plt.xlabel('Drehzahl in Hz')
plt.ylabel('Durchschnittlicher Massenstrom')

# Formatierung der x-Achse mit Multiplikation der Werte
plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, _: f'{int(x * 100)}'))

# Formatierung der y-Achse mit Kommas als Dezimaltrennzeichen
plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, _: f'{y:.2f}'.replace('.', ',')))

# Anpassungen der Legende und des Rasters
plt.legend()
plt.grid(False)

# Speicherort für den Plot
script_name = os.path.splitext(os.path.basename(__file__))[0]  # Name des Skripts ohne Erweiterung
results_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\PLOTS"
output_dir = os.path.join(results_base_dir, script_name)

# Erstelle das Verzeichnis, wenn es nicht existiert
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Speicherpfade für PNG und SVG definieren
plot_path_png = os.path.join(output_dir, f"avg_mass_flow_vs_rpm.png")
plot_path_svg = os.path.join(output_dir, f"avg_mass_flow_vs_rpm.svg")

# Plot als PNG speichern
plt.savefig(plot_path_png, format='png')

# Plot als SVG speichern
plt.savefig(plot_path_svg, format='svg')

# Plot anzeigen
plt.show()

print(f"Der Plot wurde unter '{plot_path_png}' und '{plot_path_svg}' gespeichert.")
