import matplotlib.pyplot as plt
import pandas as pd
import os
import locale
import numpy as np

# Dezimaltrennzeichen auf Komma setzen
locale.setlocale(locale.LC_NUMERIC, 'German')

def read_csv_files(base_dir, identifier):
    folder_path = os.path.join(base_dir, f"results_{identifier}")
    csv_file = os.path.join(folder_path, f"system_results_{identifier}.csv")
    if os.path.exists(csv_file):
        data_frame = pd.read_csv(csv_file, sep=',', decimal='.')
        print(f"CSV-Datei für {identifier} erfolgreich gelesen.")
        return data_frame
    else:
        print(f"CSV-Datei für {identifier} nicht gefunden: {csv_file}")
        return None

def plot_data(data_frame, identifier):
    beladezeit_list = []
    frequencies = []

    if data_frame is not None:
        for value in [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
            filtered_df = data_frame[data_frame['Name'].str.contains(f"_{identifier}_{value}.csv")]
            if not filtered_df.empty:
                beladezeit = filtered_df['Hours'].values[0]

                if not pd.isna(beladezeit):
                    beladezeit_list.append(beladezeit)
                    frequencies.append(int(value * 100))  # Frequenzwerte in Hz speichern

    return frequencies, beladezeit_list

def combined_plot(results_base_dirs, output_dir, script_name):
    fig, ax = plt.subplots(figsize=(13, 8))  # Anpassung der Plot-Größe
   # fig.suptitle('Beladedauer ', fontsize=18, y=0.95)  # Titel näher an den Plot

    # Farben und Stil
    colors = ['cornflowerblue', 'green', 'salmon', 'blue']

    # Identifikatoren für Delta T und Massenstrom
    identifiers_1 = [5, 8, 10]  # Delta T
    min_frequencies = [30, 40, 50, 60, 70, 80, 90]  # Frequenzen für minimale Beladedauer

    # Balkenbreite und Position
    bar_width = 0.2
    indices = np.arange(len(min_frequencies))

    # Balken für die 5K-Linie sammeln
    system_results_5K = read_csv_files(results_base_dirs[1], 5)
    frequencies_5K, beladezeit_5K = plot_data(system_results_5K, 5)

    # Initialisieren von Daten für Delta T Balken
    bar_data = {identifier: [] for identifier in identifiers_1}
    min_beladezeit = []

    # Beladedauer-Daten für die Delta T Identifikatoren
    for identifier in identifiers_1:
        system_results_df = read_csv_files(results_base_dirs[1], identifier)
        if system_results_df is not None:
            frequencies, beladezeit_list = plot_data(system_results_df, identifier)
            for freq in min_frequencies:
                if freq in frequencies:
                    index = frequencies.index(freq)
                    bar_data[identifier].append(beladezeit_list[index])
                else:
                    bar_data[identifier].append(float('nan'))

    # Minimalwerte für Beladedauer extrahieren
    for freq in min_frequencies:
        massenstrom_identifier = 0.05  # Massenstrom für Minimalwerte

        system_results_df = read_csv_files(results_base_dirs[0], massenstrom_identifier)
        if system_results_df is not None:
            filtered_df = system_results_df[system_results_df['Name'].str.contains(f"_{massenstrom_identifier}_{freq / 100}.csv")]
            if not filtered_df.empty:
                beladezeit = filtered_df['Hours'].values[0]
                if not pd.isna(beladezeit):
                    min_beladezeit.append(beladezeit)
                else:
                    min_beladezeit.append(float('inf'))  # Fallback-Wert, falls keine Daten gefunden werden
            else:
                min_beladezeit.append(float('inf'))  # Fallback-Wert, falls keine Daten gefunden werden

    # Berechnung der prozentualen Unterschiede und Speicherung in einer Liste
    differenzen = []
    for i, freq in enumerate(min_frequencies):
        beladezeit_10K = bar_data[10][i] if not pd.isna(bar_data[10][i]) else float('inf')
        beladezeit_min = min_beladezeit[i]
        if beladezeit_10K != float('inf') and beladezeit_min != float('inf'):
            prozentualer_unterschied = ((beladezeit_10K - beladezeit_min) / beladezeit_min) * 100
            differenzen.append({
                'Frequenz (Hz)': freq,
                '10K Beladedauer (Stunden)': beladezeit_10K,
                'Minimale Beladedauer (Stunden)': beladezeit_min,
                'Prozentuale Differenz (%)': prozentualer_unterschied
            })

            # Markierung im Plot über den Balken der minimalen Beladedauer
            ax.text(indices[i] + len(identifiers_1) * bar_width, beladezeit_min + 0.05, f'{prozentualer_unterschied:.2f}%',
                    ha='center', color='black', fontsize=12)

    # Balken plotten für jede Delta T-Linie und minimale Beladedauer
    for i, identifier in enumerate(identifiers_1):
        ax.bar(indices + i * bar_width, bar_data[identifier], bar_width, label=f'$\\Delta T_{{\\mathrm{{Kon}}}} = {identifier} \\ \\mathrm{{K}}$',edgecolor='black', color=colors[i])

    # Balken für die minimalen Beladedauern hinzufügen
    ax.bar(indices + len(identifiers_1) * bar_width, min_beladezeit, bar_width, label="Best-Case", color=colors[-1])

    # Achsen und Legenden anpassen
    ax.set_xlabel('Drehzahl in Hz', fontsize=16)
    ax.set_ylabel('Beladedauer in h', fontsize=16)
    ax.set_xticks(indices + bar_width * 1.5)
    ax.set_xticklabels([f'{freq}' for freq in min_frequencies], fontsize=12)
    ax.set_ylim([0,7])
    ax.legend(loc='upper left', bbox_to_anchor=(0.7, 1), ncol=1, prop={'size': 16}, handletextpad=1, columnspacing=1, frameon=False)
    ax.tick_params(axis='both', which='major', labelsize=16)

    # Speicherpfade für PNG und SVG definieren
    combined_plot_filename_png = os.path.join(output_dir, f"{script_name}_combined.png")
    combined_plot_filename_svg = os.path.join(output_dir, f"{script_name}_combined.svg")

    # Plot als PNG speichern
    plt.savefig(combined_plot_filename_png, bbox_inches='tight')

    # Plot als SVG speichern
    plt.savefig(combined_plot_filename_svg, bbox_inches='tight')

    # Plot anzeigen und schließen
    plt.show()
    plt.close(fig)

    print(f"Kombinierter Plot gespeichert: {combined_plot_filename_png} und {combined_plot_filename_svg}")

    # Speichern der prozentualen Unterschiede in eine CSV-Datei
    differenzen_df = pd.DataFrame(differenzen)
    differenzen_csv_filename = os.path.join(output_dir, f"{script_name}_differenzen.csv")
    differenzen_df.to_csv(differenzen_csv_filename, sep=';', index=False, decimal=',')
    print(f"Prozentuale Unterschiede in CSV-Datei gespeichert: {differenzen_csv_filename}")

if __name__ == "__main__":
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    results_base_dirs = [
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Massenstrom\CalculationExergy",
        r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\Temperaturspreizung\CalculationExergy"
    ]
    output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und2\PLOTS"

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    combined_plot(results_base_dirs, output_dir, script_name)
