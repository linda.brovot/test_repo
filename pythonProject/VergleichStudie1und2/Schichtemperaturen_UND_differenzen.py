import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Pfad zu den CSV-Ordnern für Temperaturdifferenzen
delta_t_base_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python\Temperaturspreizung\CSV_Results"

# Temperaturdifferenzen in K
delta_ts = [5, 8, 10]

# Listen, um die Temperaturdifferenzen zu speichern
temp_differences_30hz = []
temp_differences_90hz = []

# Schleife über alle Temperaturdifferenzen für das Balkendiagramm
for delta_t in delta_ts:
    folder_path = os.path.join(delta_t_base_dir, f"csv_{delta_t}")
    csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

    temp_diff_30hz = None
    temp_diff_90hz = None

    for csv_file_path in csv_files:
        name = os.path.basename(csv_file_path).replace('.csv', '')
        frequency_str = name.split('_')[-1]

        try:
            frequency = float(frequency_str)
        except ValueError:
            continue

        df = pd.read_csv(csv_file_path, sep=";", decimal=",")
        if 'bufferStorage.layer[1].T' not in df.columns or 'bufferStorage.layer[10].T' not in df.columns:
            continue

        df_end = df.iloc[-1]
        temp_layer_1 = df_end['bufferStorage.layer[1].T'] - 273.15  # Konvertiere von Kelvin in Celsius
        temp_layer_10 = df_end['bufferStorage.layer[10].T'] - 273.15  # Konvertiere von Kelvin in Celsius
        temp_difference = temp_layer_10 - temp_layer_1

        if frequency == 0.3:
            temp_diff_30hz = temp_difference
        elif frequency == 0.9:
            temp_diff_90hz = temp_difference

    # Füge die gefundenen Differenzen in die Listen ein, falls keine Daten vorliegen, füge 0 ein
    if temp_diff_30hz is not None:
        temp_differences_30hz.append(temp_diff_30hz)
    else:
        temp_differences_30hz.append(0)

    if temp_diff_90hz is not None:
        temp_differences_90hz.append(temp_diff_90hz)
    else:
        temp_differences_90hz.append(0)

# Erstellung des kombinierten Plots mit zwei Subplots (Balkendiagramm links, Temperaturverläufe rechts)
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 6))

# -----------------------------
# 1. Linker Subplot: Balkendiagramm der Temperaturdifferenzen
# -----------------------------
bar_width = 0.35  # Breite der Balken
index = np.arange(len(delta_ts))

# Zeichne die Balken für 30 Hz und 90 Hz
bars_30hz = ax1.bar(index - bar_width / 2, temp_differences_30hz, bar_width, label='30 Hz', color='tab:blue', edgecolor='black')
bars_90hz = ax1.bar(index + bar_width / 2, temp_differences_90hz, bar_width, label='90 Hz', color='tab:pink', edgecolor='black')

# Zahlen über den Balken anzeigen
for bars in [bars_30hz, bars_90hz]:
    for bar in bars:
        yval = bar.get_height()
       # ax1.text(bar.get_x() + bar.get_width() / 2, yval, f'{yval:.0f}', va='bottom', ha='center')

# Beschriftungen und Titel für das Balkendiagramm
ax1.set_xlabel('Temperaturspreizung im Kondensator in K')
ax1.set_ylabel('Temperaturdifferenz im Speicher in K')
ax1.set_xticks(index)
ax1.set_xticklabels([f'{dt:.0f}'.replace(".", ",") for dt in delta_ts])
ax1.legend()

# -----------------------------
# 2. Rechter Subplot: Temperaturverläufe der Schichten bei 90 Hz und 10 K
# -----------------------------
delta_t = 10  # Nur die Temperaturdifferenz von 10 K
frequency = 0.3  # Nur die Frequenz von 90 Hz

# Suche nach der entsprechenden Datei für 90 Hz und 10 K
folder_path = os.path.join(delta_t_base_dir, f"csv_{delta_t}")
csv_files = [os.path.join(folder_path, file) for file in os.listdir(folder_path) if file.endswith('.csv')]

for csv_file_path in csv_files:
    name = os.path.basename(csv_file_path).replace('.csv', '')
    frequency_str = name.split('_')[-1]

    try:
        file_frequency = float(frequency_str)
    except ValueError:
        continue

    if file_frequency == frequency:
        df = pd.read_csv(csv_file_path, sep=";", decimal=",")

        # Konvertiere den Index in Stunden (angenommen, der Index ist in Sekunden)
        df["Hour"] = df.index / 3600

        # Plotten der Temperaturverläufe für jede Schicht (1 bis 10)
        for i in range(1, 11):
            if f'bufferStorage.layer[{i}].T' in df.columns:
                df[f'bufferStorage.layer[{i}].T'] = df[f'bufferStorage.layer[{i}].T'] - 273.15
                ax2.plot(df["Hour"], df[f'bufferStorage.layer[{i}].T'], label=f'Schicht {i}')

        # Achsenbeschriftungen und Titel für das Temperaturverlaufdiagramm
        ax2.set_xlabel('Beladedauer in h')
        ax2.set_ylabel('Temperatur in °C')
        #ax2.set_title(f'Temperaturverlauf der Schichten bei 90 Hz und {delta_t} K')

        # Legende hinzufügen
        ax2.legend(title='Schicht')
        break

# Layout und Plot speichern/anzeigen
plt.tight_layout()

# Speicherort für den Plot festlegen
output_dir = r"N:\Forschung\EBC0931_DFG_OptContrCB_KAP\Students\hro-lbr\WP-Speicher-System\Python_TQUELLE_15\VergleichStudie1und2\PLOTS"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Speicherpfade für PNG und SVG definieren
plot_path_png = os.path.join(output_dir, 'combined_temperature_and_bars_plot.png')
plot_path_svg = os.path.join(output_dir, 'combined_temperature_and_bars_plot.svg')

# Plot speichern
plt.savefig(plot_path_png, format='png')
plt.savefig(plot_path_svg, format='svg')

# Plot anzeigen
plt.show()

print(f"Der kombinierte Plot wurde unter '{plot_path_png}' und '{plot_path_svg}' gespeichert.")
