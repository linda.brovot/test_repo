﻿within WP_Speicher_System_MA_Linda;
model WP_Speicher_System
   extends Modelica.Icons.Example;

    replaceable package Medium_tes = AixLib.Media.Water
    constrainedby Modelica.Media.Interfaces.PartialMedium annotation (choicesAllMatching=true);
  replaceable package Medium_sin = AixLib.Media.Water
    constrainedby Modelica.Media.Interfaces.PartialMedium annotation (choicesAllMatching=true);
  replaceable package Medium_sou = AixLib.Media.Air
    constrainedby Modelica.Media.Interfaces.PartialMedium annotation (choicesAllMatching=true);

  // um die initialbedingungen nutzen zu können, müssen erst die final Parameter
  // im BufferStorage model zu normalen Parametern geändert werden
  // a: durchmischt 60 Grad 86400s
//  parameter Real T_layer_start[10] = {332.36, 332.72, 332.77, 332.83, 332.75, 332.77, 332.82, 332.86, 332.79, 332.82};
  // b: geschichtet 60/10 Grad 86400s
//  parameter Real T_layer_start[10] = {285.21, 285.32, 286.44, 294.62, 322.06, 332.06, 332.55, 332.63, 332.56, 332.6};
  // c: Beladung 180 l/h 21600s
//  parameter Real T_layer_start[10] = {292.87, 292.88, 292.86, 292.96, 292.85, 292.89, 292.93, 292.91, 292.88, 292.92};
//   d: Beladung 500 l/h 14400s
//  parameter Real T_layer_start[10] = {293.02, 293.02, 293.01, 293.09, 292.98, 293, 293.04, 293.03, 293, 293.02};
  // e: Beladung 1000 l/h 10800s
//  parameter Real T_layer_start[10] = {292.91, 292.91, 292.9, 292.98, 292.88, 292.89, 292.94, 292.92, 292.91, 292.94};
 // f: Beladung 1400 l/h 10800s
  parameter Real T_layer_start[10] = {292.45, 292.75, 293.35, 294.25, 295.05, 296.05, 296.85, 297.35, 297.69, 297.75};
 // j: 80 Grad 86400s
//  parameter Real T_layer_start[10] = {351.28, 351.77, 351.84, 351.88, 351.84, 351.86, 351.91, 351.97, 351.87, 351.9};
  // k: 40 Grad 86400s
//  parameter Real T_layer_start[10] = {312.83, 312.97, 313, 313.08, 312.99, 312.99, 313.04, 313.08, 313.03, 313.06};
 // l: Entladung 500 l/h 3600s
//  parameter Real T_layer_start[10] = {332.44, 332.77, 332.83, 332.88, 332.82, 332.84, 332.89, 332.93, 332.87, 332.89};
 // m: Entladung 250 l/h 7200s
//  parameter Real T_layer_start[10] = { 332.33, 332.66, 332.72, 332.79, 332.71, 332.74, 332.79, 332.83, 332.75, 332.77};
 // n: Entladung 1000 l/h 3600s
//  parameter Real T_layer_start[10] = {332.35, 332.68, 332.74, 332.81, 332.74, 332.75, 332.81, 332.83, 332.76, 332.78};
 // p: Beladung 750 l/h 12600s
 //   parameter Real T_layer_start[10] = {292.21, 292.22, 292.21, 292.3, 292.17, 292.2, 292.22, 292.22, 292.19, 292.22};
// r: Entladung DIN 86400s
//  parameter Real T_layer_start[10] = {331.01, 332.15, 332.61, 332.73, 332.68, 332.71, 332.76, 332.81, 332.7, 332.75};

  parameter Real T_start_top = T_layer_start[10];
  parameter Real T_start_bottom = T_layer_start[1];

  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature fixedTemperature(T=280.15)
                annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={134,-6})));
  AixLib.Fluid.Sources.Boundary_pT Input_top_TS(redeclare package Medium =
        Medium_sin, nPorts=1)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=180,
        origin={204,32})));
  AixLib.Fluid.Sources.MassFlowSource_T input_bottom_sink_flow(redeclare
      package Medium = Medium_sin,
    use_m_flow_in=false,
    m_flow=0,
    use_T_in=false,
    T=293.15,                              nPorts=1) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={88,-56})));
  AixLib.Fluid.Sources.MassFlowSource_T Output_top_sink_return(redeclare
      package Medium = Medium_sin,
    m_flow=0,
    nPorts=1)                                        annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={76,44})));
  AixLib.Fluid.Sources.MassFlowSource_T Output_botton(redeclare package Medium
      = Medium_sin, nPorts=1)
    annotation (Placement(transformation(extent={{36,-86},{56,-66}})));

  simple_hp_tes.Experiments.BufferStorage bufferStorage(
    redeclare package Medium = Medium_tes,
    redeclare package MediumHC1 = Medium_sin,
    redeclare package MediumHC2 = Medium_sin,
    m1_flow_nominal=0,
    m2_flow_nominal=0,
    mHC1_flow_nominal=0.001,
    useHeatingCoil1=true,
    useHeatingCoil2=false,
    useHeatingRod=false,
    redeclare simple_hp_tes.Data.Panasonic_PAW data,
    n=10,
    hConHC1=100,
    upToDownHC1=true,
    layer(T_start=T_layer_start),
    topCover(TStartWall=T_start_top, TStartIns=T_start_top),
    storageMantle(TStartWall=T_layer_start, TStartIns=T_layer_start),
    bottomCover(TStartWall=T_start_bottom, TStartIns=T_start_bottom),
    redeclare model HeatTransfer =
        AixLib.Fluid.Storage.BaseClasses.HeatTransferLambdaEff,
    allowFlowReversal_layers=true,
    allowFlowReversal_HC1=true,
    allowFlowReversal_HC2=false)
    annotation (Placement(transformation(extent={{48,-30},{80,10}})));

  AixLib.Fluid.Sensors.TemperatureTwoPort SinkReturnTemp(
    redeclare package Medium = Medium_tes,
    m_flow_nominal=0.000001,
    T_start=293.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={140,30})));
  AixLib.Fluid.Sensors.VolumeFlowRate SinkReturnFlow(redeclare package Medium
      = Medium_tes, m_flow_nominal=0)        annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={174,32})));
  AixLib.Fluid.Sources.MassFlowSource_T                sourceSideMassFlowSource(
    use_T_in=false,
    m_flow=1.5,
    redeclare package Medium = Medium_sou,
    T=280.15,
    nPorts=1) "Ideal mass flow source at the inlet of the source side"
              annotation (Placement(transformation(extent={{-126,-60},{-106,-40}})));
  AixLib.Fluid.Sources.Boundary_pT                  sourceSideFixedBoundary(
      redeclare package Medium = Medium_sou, nPorts=1)
          "Fixed boundary at the outlet of the source side"
          annotation (Placement(transformation(extent={{-11,11},{11,-11}},
        rotation=0,
        origin={-119,11})));

  AixLib.Fluid.Sensors.TemperatureTwoPort senTAct(
    final m_flow_nominal=heatPump.m1_flow_nominal,
    final tau=1,
    final initType=Modelica.Blocks.Types.Init.InitialState,
    final tauHeaTra=1200,
    final allowFlowReversal=heatPump.allowFlowReversalCon,
    final transferHeat=false,
    redeclare final package Medium = Medium_sin,
    final T_start=303.15,
    final TAmb=291.15) "Temperature at sink inlet" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-18,-66})));

  AixLib.Fluid.Sources.Boundary_pT   sinkSideFixedBoundary(redeclare package
      Medium = Medium_sin, nPorts=1)
    "Fixed boundary at the outlet of the sink side" annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={18,-86})));
  Modelica.Blocks.Sources.Constant iceFac(final k=1)
    "Fixed value for icing factor. 1 means no icing/frosting (full heat transfer in heat exchanger)" annotation (Placement(
        transformation(
        extent={{8,8},{-8,-8}},
        rotation=180,
        origin={-86,34})));
  Modelica.Blocks.Sources.Constant nSet3(k=drehzahlverdichter)
                                              annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={2,72})));
  Modelica.Blocks.Logical.Switch switch2
    annotation (Placement(transformation(extent={{-16,40},{-36,60}})));
  Modelica.Blocks.Sources.Constant nSet4(k=0) annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={2,58})));
  Modelica.Blocks.Sources.BooleanExpression modeSet(y=true)
    annotation (Placement(transformation(extent={{-94,48},{-74,68}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-16,70},{-36,90}})));
  Modelica.Blocks.Sources.Constant nSet1(k=0) annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={2,88})));
  Modelica.Blocks.Sources.Constant nSet2(k=drehzahlpumpe)
                                        annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=180,
        origin={4,42})));
  AixLib.Controls.Interfaces.VapourCompressionMachineControlBus sigBus1
    annotation (Placement(transformation(extent={{-76,16},{-46,50}}),
        iconTransformation(extent={{-22,30},{-4,56}})));
  AixLib.Fluid.Movers.SpeedControlled_Nrpm
                                    pumSou(
    redeclare final AixLib.Fluid.Movers.Data.Pumps.Wilo.Stratos25slash1to6 per(
        speed_rpm_nominal=2543),
    final allowFlowReversal=true,
    final addPowerToMedium=false,
    redeclare final package Medium = Medium_sin,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial)
    "Fan or pump at source side of HP" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-10,16})));

  Real U_ges;
  Real COP;
  Real deltaT_con;
  Real V_flow_Pump;
  Real Q_Flow_HC1;
  Real Q_con_actual;
  Real Q_HC1_1;
  Real Q_HC1_2;
  Real Q_HC1_3;
  Real Q_HC1_4;
  Real Q_HC1_5;
  Real Q_HC1_6;
  Real Q_HC1_7;
  Real Q_HC1_8;
  Real Q_HC1_9;
 // Real Q_HC1_10;
  Real Q_HC1_Sum;

  Modelica.Blocks.Logical.Hysteresis hysteresis(uLow=333.15, uHigh=334.15)
    annotation (Placement(transformation(extent={{34,70},{14,90}})));
  Modelica.Blocks.Logical.Hysteresis hysteresis1(uLow=333.15, uHigh=334.15)
    annotation (Placement(transformation(extent={{34,40},{14,60}})));
  AixLib.Fluid.HeatPumps.HeatPump heatPump(
    refIneFre_constant=1,
    useBusConnectorOnly=true,
    CEva=100,
    GEvaOut=5,
    CCon=100,
    GConOut=5,
    dpEva_nominal=0,
    dpCon_nominal=0,
    VCon=0.4,
    use_conCap=false,
    redeclare package Medium_con = Medium_sin,
    redeclare package Medium_eva = Medium_sou,
    use_refIne=false,
    use_rev=true,
    redeclare model PerDataMainHP = .WP_Speicher_System.Data.VCLibMap_4dim (
        refrigerant="Propane",
        flowsheet="Standard",
        interpMethod=SDF.Types.InterpolationMethod.Linear,
        extrapMethod=SDF.Types.ExtrapolationMethod.Linear),
    redeclare model PerDataRevHP =
        AixLib.DataBase.Chiller.PerformanceData.LookUpTable2D (smoothness=
            Modelica.Blocks.Types.Smoothness.LinearSegments, dataTable=
            AixLib.DataBase.Chiller.EN14511.Vitocal200AWO201()),
    VEva=0.04,
    use_evaCap=false,
    scalingFactor=1,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    mFlow_conNominal=0.5,
    mFlow_evaNominal=0.5,
    use_autoCalc=false,
    TAmbEva_nominal=273.15,
    TAmbCon_nominal=288.15,
    TCon_start=303.15) annotation (Placement(transformation(
        extent={{-24,-29},{24,29}},
        rotation=270,
        origin={-60,-25})));

  parameter Real drehzahlpumpe=3600 "Constant output value";
  parameter Real drehzahlverdichter=0.272727272727273
    "Constant output value";
equation

  U_ges=bufferStorage.layer[10].U+bufferStorage.layer[9].U+bufferStorage.layer[8].U+bufferStorage.layer[7].U+bufferStorage.layer[6].U+bufferStorage.layer[5].U+bufferStorage.layer[4].U+bufferStorage.layer[3].U+bufferStorage.layer[2].U+bufferStorage.layer[1].U;
  COP = heatPump.innerCycle.QCon / heatPump.innerCycle.Pel;
  deltaT_con=heatPump.sigBus.TConOutMea-heatPump.sigBus.TConInMea;
  V_flow_Pump=pumSou.eff.V_flow*1000*3600;
  Q_Flow_HC1=-(bufferStorage.heatingCoil1.convHC1Outside[1].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[2].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[3].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[4].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[5].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[6].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[7].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[8].Q_flow+bufferStorage.heatingCoil1.convHC1Outside[9].Q_flow);
  Q_con_actual= heatPump.con.m_flow*heatPump.con.vol.dynBal.cp_default*(heatPump.innerCycle.sigBus.TConInMea-heatPump.innerCycle.sigBus.TConOutMea);
  Q_HC1_1=(bufferStorage.heatingCoil1.pipe[1].vol_a.T-bufferStorage.heatingCoil1.pipe[1].vol_b.T)*bufferStorage.heatingCoil1.pipe[1].m_flow*bufferStorage.heatingCoil1.pipe[1].cp_default;
  Q_HC1_2=(bufferStorage.heatingCoil1.pipe[2].vol_a.T-bufferStorage.heatingCoil1.pipe[2].vol_b.T)*bufferStorage.heatingCoil1.pipe[2].m_flow*bufferStorage.heatingCoil1.pipe[2].cp_default;
  Q_HC1_3=(bufferStorage.heatingCoil1.pipe[3].vol_a.T-bufferStorage.heatingCoil1.pipe[3].vol_b.T)*bufferStorage.heatingCoil1.pipe[3].m_flow*bufferStorage.heatingCoil1.pipe[3].cp_default;
  Q_HC1_4=(bufferStorage.heatingCoil1.pipe[4].vol_a.T-bufferStorage.heatingCoil1.pipe[4].vol_b.T)*bufferStorage.heatingCoil1.pipe[4].m_flow*bufferStorage.heatingCoil1.pipe[4].cp_default;
  Q_HC1_5=(bufferStorage.heatingCoil1.pipe[5].vol_a.T-bufferStorage.heatingCoil1.pipe[5].vol_b.T)*bufferStorage.heatingCoil1.pipe[5].m_flow*bufferStorage.heatingCoil1.pipe[5].cp_default;
  Q_HC1_6=(bufferStorage.heatingCoil1.pipe[6].vol_a.T-bufferStorage.heatingCoil1.pipe[6].vol_b.T)*bufferStorage.heatingCoil1.pipe[6].m_flow*bufferStorage.heatingCoil1.pipe[6].cp_default;
  Q_HC1_7=(bufferStorage.heatingCoil1.pipe[7].vol_a.T-bufferStorage.heatingCoil1.pipe[7].vol_b.T)*bufferStorage.heatingCoil1.pipe[7].m_flow*bufferStorage.heatingCoil1.pipe[7].cp_default;
  Q_HC1_8=(bufferStorage.heatingCoil1.pipe[8].vol_a.T-bufferStorage.heatingCoil1.pipe[8].vol_b.T)*bufferStorage.heatingCoil1.pipe[8].m_flow*bufferStorage.heatingCoil1.pipe[8].cp_default;
  Q_HC1_9=(bufferStorage.heatingCoil1.pipe[9].vol_a.T-bufferStorage.heatingCoil1.pipe[9].vol_b.T)*bufferStorage.heatingCoil1.pipe[9].m_flow*bufferStorage.heatingCoil1.pipe[9].cp_default;
 // Q_HC1_10=(bufferStorage.heatingCoil1.pipe[10].vol_a.T-bufferStorage.heatingCoil1.pipe[10].vol_b.T)*bufferStorage.heatingCoil1.pipe[1].m_flow*bufferStorage.heatingCoil1.pipe[10].cp_default;
  Q_HC1_Sum = Q_HC1_1 + Q_HC1_2 + Q_HC1_3 + Q_HC1_4 + Q_HC1_5 + Q_HC1_6 + Q_HC1_7 + Q_HC1_8 + Q_HC1_9;

  connect(Output_botton.ports[1], bufferStorage.fluidportBottom1) annotation (
      Line(points={{56,-76},{58.6,-76},{58.6,-30.4}},  color={0,127,255}));
  connect(input_bottom_sink_flow.ports[1], bufferStorage.fluidportBottom2)
    annotation (Line(points={{78,-56},{68.6,-56},{68.6,-30.2}},
                                                              color={0,127,255}));
  connect(fixedTemperature.port, bufferStorage.heatportOutside) annotation (
      Line(points={{124,-6},{104.8,-6},{104.8,-8.8},{79.6,-8.8}},
        color={191,0,0}));
  connect(bufferStorage.fluidportTop2, SinkReturnTemp.port_a) annotation (Line(
        points={{69,10.2},{120,10.2},{120,30},{130,30}},
                                                   color={0,127,255}));
  connect(bufferStorage.fluidportTop1, Output_top_sink_return.ports[1])
    annotation (Line(points={{58.4,10.2},{58.4,44},{66,44}},          color={0,
          127,255}));
  connect(SinkReturnTemp.port_b, SinkReturnFlow.port_a)
    annotation (Line(points={{150,30},{150,32},{164,32}},
                                                       color={0,127,255}));
  connect(SinkReturnFlow.port_b, Input_top_TS.ports[1])
    annotation (Line(points={{184,32},{194,32}},
                                               color={0,127,255}));
  connect(iceFac.y,sigBus1. iceFacMea) annotation (Line(points={{-77.2,34},{-62.1,
          34},{-62.1,33.085},{-60.925,33.085}},
                                              color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(bufferStorage.portHC1In,senTAct. port_b) annotation (Line(points={{47.6,
          1.4},{28,1.4},{28,-54},{26,-54},{26,-66},{-8,-66}},
                                                          color={0,127,255}));
  connect(sinkSideFixedBoundary.ports[1],senTAct. port_b)
    annotation (Line(points={{8,-86},{-2,-86},{-2,-66},{-8,-66}},
                                                 color={0,127,255}));
  connect(modeSet.y,sigBus1. modeSet) annotation (Line(points={{-73,58},{-60.925,
          58},{-60.925,33.085}},         color={255,0,255}), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(nSet1.y,switch1. u1)
    annotation (Line(points={{-2.4,88},{-14,88}},color={0,0,127}));
  connect(nSet3.y,switch1. u3)
    annotation (Line(points={{-2.4,72},{-14,72}},color={0,0,127}));
  connect(switch1.y,sigBus1. nSet) annotation (Line(points={{-37,80},{-60.925,80},
          {-60.925,33.085}}, color={0,0,127}), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(nSet2.y,switch2. u3)
    annotation (Line(points={{-0.4,42},{-14,42}},color={0,0,127}));
  connect(nSet4.y,switch2. u1)
    annotation (Line(points={{-2.4,58},{-14,58}},color={0,0,127}));
  connect(pumSou.port_a, bufferStorage.portHC1Out) annotation (Line(points={{0,16},{
          6,16},{6,-4.8},{47.8,-4.8}},         color={0,127,255}));
  connect(switch2.y, pumSou.Nrpm) annotation (Line(points={{-37,50},{-40,50},{-40,
          32},{-10,32},{-10,28}},
                             color={0,0,127}));
  connect(bufferStorage.TTop, hysteresis.u) annotation (Line(points={{48,7.6},{48,
          10},{46,10},{46,80},{36,80}},                          color={0,0,127}));
  connect(hysteresis.y, switch1.u2) annotation (Line(points={{13,80},{-14,80}},
                         color={255,0,255}));
  connect(hysteresis1.y, switch2.u2)
    annotation (Line(points={{13,50},{-14,50}},           color={255,0,255}));
  connect(bufferStorage.TTop, hysteresis1.u) annotation (Line(points={{48,7.6},{
          48,10},{46,10},{46,50},{36,50}},      color={0,0,127}));
  connect(heatPump.port_a1, pumSou.port_b) annotation (Line(points={{-45.5,-1},{
          -44,-1},{-44,12},{-26,12},{-26,16},{-20,16}},  color={0,127,255}));
  connect(heatPump.port_b1, senTAct.port_a) annotation (Line(points={{-45.5,-49},
          {-44,-49},{-44,-66},{-28,-66}},   color={0,127,255}));
  connect(sourceSideMassFlowSource.ports[1], heatPump.port_a2) annotation (Line(
        points={{-106,-50},{-92,-50},{-92,-56},{-74.5,-56},{-74.5,-49}},
                                                                color={0,127,255}));
  connect(heatPump.port_b2, sourceSideFixedBoundary.ports[1]) annotation (Line(
        points={{-74.5,-1},{-74.5,11},{-108,11}},       color={0,127,255}));
  connect(sigBus1, heatPump.sigBus) annotation (Line(
      points={{-61,33},{-61,15.5},{-69.425,15.5},{-69.425,-1.24}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-3,6},{-3,6}},
      horizontalAlignment=TextAlignment.Right));
  annotation (
      experiment(
      StopTime=86400,
      Interval=1,
      __Dymola_Algorithm="Dassl"),
    conversion(from(version={"1",""}, script="modelica://erster/ConvertFromerster_1.mos")),
    __Dymola_experimentSetupOutput,
    __Dymola_experimentFlags(
      Advanced(GenerateVariableDependencies=false, OutputModelicaCode=false),
      Evaluate=false,
      OutputCPUtime=true,
      OutputFlatModelica=false));
end WP_Speicher_System;
