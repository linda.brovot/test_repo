within ;
package WP_Speicher_System_MA_Linda
annotation (uses(
    Modelica(version="4.0.0"),
    AixLib(version="1.4.0"),
    SDF(version="0.4.2")));
end WP_Speicher_System_MA_Linda;
